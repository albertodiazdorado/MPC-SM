# Create ranges
n = 4:20
m = 3:11
T = 10:10:100

# Choose directory
dir = "SoftAccuracy"

# Analyze all the files regarding state size
n_ob_max = hcat(n, zeros(length(n), 2))
n_ob_mean = hcat(n, zeros(length(n), 2))
n_iv= hcat(n, zeros(length(n), 2))
n_status = hcat(n,zeros(length(n),3))
idx = 1
for i in n
    cd(string("/home/add/Desktop/MPC-SM/",dir,"/"))
    cd(string("pn", i))
    # Analyze the objective value
    r = readcsv(string("objval.csv"))
    mehrotra_val = r[:,1]
    qpoases_val = r[:,2]
    gurobi_val = r[:,3]
    status = r[:,4]
    iter_m = r[:,5]
    iter_qp = r[:,6]

    aux1 = (mehrotra_val - gurobi_val)./gurobi_val
    aux2 = (qpoases_val - gurobi_val)./gurobi_val
    
    n_ob_max[idx,2:3] = hcat(maximum(aux1[2:end]),maximum(aux2[2:end]))
    n_ob_mean[idx,2:3] = hcat(mean(aux1[2:end]),mean(aux2[2:end]))
    n_status[idx,2:4] = hcat(1-sum(status)/length(status),sum(isless.(mehrotra_val,gurobi_val))/length(status),sum(isless.(qpoases_val,gurobi_val))/length(status))

    r = readcsv(string("inputval.csv"))
    n_iv[idx,2:3] = hcat(norm(r[:,3]-r[:,1]),norm(r[:,3]-r[:,2]))

    idx = idx + 1
end

# Analyze all the files regarding state size
m_ob_max = hcat(m, zeros(length(m), 2))
m_ob_mean = hcat(m, zeros(length(m), 2))
m_iv = hcat(m, zeros(length(m), 2))
m_status = hcat(m,zeros(length(m),3))
idx = 1
for i in m
    cd(string("/home/add/Desktop/MPC-SM/",dir,"/"))
    cd(string("pm", i))
    # Analyze the objective value
    r = readcsv(string("objval.csv"))
    mehrotra_val = r[:,1]
    qpoases_val = r[:,2]
    gurobi_val = r[:,3]
    status = r[:,4]
    iter_m = r[:,5]
    iter_qp = r[:,6]
    
    aux1 = (mehrotra_val - gurobi_val)./gurobi_val
    aux2 = (qpoases_val - gurobi_val)./gurobi_val
    
    m_ob_max[idx,2:3] = hcat(maximum(aux1[2:end]),maximum(aux2[2:end]))
    m_ob_mean[idx,2:3] = hcat(mean(aux1[2:end]),mean(aux2[2:end]))
    m_status[idx,2:4] = hcat(1-sum(status)/length(status),sum(isless.(mehrotra_val,gurobi_val))/length(status),sum(isless.(qpoases_val,gurobi_val))/length(status))

    r = readcsv(string("inputval.csv"))
    m_iv[idx,2:3] = hcat(norm(r[:,3]-r[:,1]),norm(r[:,3]-r[:,2]))

    idx = idx + 1
end


# Analyze all the files regarding state size
t_ob_max = hcat(T, zeros(length(T), 2))
t_ob_mean = hcat(T, zeros(length(T), 2))
t_iv= hcat(T, zeros(length(T), 2))
t_status = hcat(T,zeros(length(T),3))
idx = 1
for i in T
    cd(string("/home/add/Desktop/MPC-SM/",dir,"/"))
    cd(string("pT", i))
    # Analyze the objective value
    r = readcsv(string("objval.csv"))
    mehrotra_val = r[:,1]
    qpoases_val = r[:,2]
    gurobi_val = r[:,3]
    status = r[:,4]
    iter_m = r[:,5]
    iter_qp = r[:,6]
    
    aux1 = (mehrotra_val - gurobi_val)./gurobi_val
    aux2 = (qpoases_val - gurobi_val)./gurobi_val
    
    t_ob_max[idx,2:3] = hcat(maximum(aux1[2:end]),maximum(aux2[2:end]))
    t_ob_mean[idx,2:3] = hcat(mean(aux1[2:end]),mean(aux2[2:end]))
    t_status[idx,2:4] = hcat(1-sum(status)/length(status),sum(isless.(mehrotra_val,gurobi_val))/length(status),sum(isless.(qpoases_val,gurobi_val))/length(status))

    r = readcsv(string("inputval.csv"))
    t_iv[idx,2:3] = hcat(norm(r[:,3]-r[:,1]),norm(r[:,3]-r[:,2]))

    idx = idx + 1
end

cd(string("/home/add/Desktop/MPC-SM/",dir,"/"))

legend = ["mehrotra" "qpoases"]

writecsv("nobmax.csv", vcat(hcat("n",legend), n_ob_max))
writecsv("nobmean.csv", vcat(hcat("n",legend), n_ob_mean))
writecsv("niv.csv", vcat(hcat("n",legend), n_iv))

writecsv("mobmax.csv", vcat(hcat("m",legend), m_ob_max))
writecsv("mobmean.csv", vcat(hcat("m",legend), m_ob_mean))
writecsv("miv.csv", vcat(hcat("m",legend), m_iv))

writecsv("tobmax.csv", vcat(hcat("t",legend), t_ob_max))
writecsv("tobmean.csv", vcat(hcat("t",legend), t_ob_mean))
writecsv("tiv.csv", vcat(hcat("t",legend), t_iv))

legend = ["status" "mehrotra_off" "qpoases_off"]
writecsv("n_status.csv", vcat(hcat("n",legend),n_status))
writecsv("m_status.csv", vcat(hcat("m",legend),m_status))
writecsv("t_status.csv", vcat(hcat("t",legend),t_status))


cd("/home/add/Desktop/MPC-SM/testbench")

using Plots
plotly()

function plot_results(x::Integer = 1, min::Integer = 0, max::Integer = 0)
    if x == 1
        p1 = plot(n[1+min:end-max],n_ob_max[1+min:end-max,2:3],title = "Max error", label=["mehrotra" "qpoases"], xlabel = "N", ylabel = "%")
        p2 = plot(n[1+min:end-max],n_ob_mean[1+min:end-max,2:3],title = "Mean error", label=["mehrotra" "qpoases"], xlabel = "N", ylabel = "%")
        p3 = plot(n[1+min:end-max],n_iv[1+min:end-max,2:3],title = "Input error (norm)", label=["mehrotra" "qpoases"], xlabel = "N", ylabel = "%")
        p4 = plot(n[1+min:end-max],n_status[1+min:end-max,2:4],title = "Status", label=["qpOASES %" "mehrotra off" "qpoases off"], xlabel = "N", ylabel = "%")
        return [p1,p2,p3,p4]
    elseif x == 2
      p1 = plot(m[1+min:end-max],m_ob_max[1+min:end-max,2:3],title = "Max error", label=["mehrotra" "qpoases"], xlabel = "m", ylabel = "%")
      p2 = plot(m[1+min:end-max],m_ob_mean[1+min:end-max,2:3],title = "Mean error", label=["mehrotra" "qpoases"], xlabel = "m", ylabel = "%")
      p3 = plot(m[1+min:end-max],m_iv[1+min:end-max,2:3],title = "Input error (norm)", label=["mehrotra" "qpoases"], xlabel = "m", ylabel = "%")
      p4 = plot(m[1+min:end-max],m_status[1+min:end-max,2:4],title = "Status", label=["qpOASES %" "mehrotra off" "qpoases off"], xlabel = "m", ylabel = "%")
      return [p1,p2,p3,p4]
    else
      p1 = plot(T[1+min:end-max],t_ob_max[1+min:end-max,2:3],title = "Max error", label=["mehrotra" "qpoases"], xlabel = "T", ylabel = "%")
      p2 = plot(T[1+min:end-max],t_ob_mean[1+min:end-max,2:3],title = "Mean error", label=["mehrotra" "qpoases"], xlabel = "T", ylabel = "%")
      p3 = plot(T[1+min:end-max],t_iv[1+min:end-max,2:3],title = "Input error (norm)", label=["mehrotra" "qpoases"], xlabel = "T", ylabel = "%")
      p4 = plot(T[1+min:end-max],t_status[1+min:end-max,2:4],title = "Status", label=["qpOASES %" "mehrotra off" "qpoases off"], xlabel = "T", ylabel = "%")
      return [p1,p2,p3,p4]
    end
end

function plot_evolution(x::Integer = 1, y::Integer = 4)
    cd(string("/home/add/Desktop/MPC-SM/",dir,"/"))
    if x == 1
        cd(string("pn",y))
        e = "pn"
    elseif x == 2
        cd(string("pm",y))
        e = "pm"
    else
        cd(string("pT", y))
        e = "pT"
    end
    r = readcsv("objval.csv")
    mehrotra = r[:,1]
    qpoases = r[:,2]
    gurobi = r[:,3]
    x = 1:length(mehrotra)
    cd("/home/add/Desktop/MPC-SM/testbench")
    return plot(x,hcat(mehrotra,qpoases,gurobi),label = ["mehrotra" "qpOASES" "Gurobi"], xlabel = "Iteration", ylabel = "f(x*)")
end

cd(string("/home/add/Desktop/MPC-SM/",dir,"/pm4"))
r = readcsv("objval.csv")
mehrotra = r[:,1]
qpoases = r[:,2]
gurobi = r[:,3]
cd("../")
writecsv("evolutionpm4.csv",vcat(hcat("iter","mehrotra", "qpoases" ,"gurobi"),hcat(1:length(mehrotra),mehrotra,qpoases,gurobi)))

cd("/home/add/Desktop/MPC-SM/testbench")
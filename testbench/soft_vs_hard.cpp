/* This file solves the same problem two times:
 *    Once with hard constraints
 *    Once with soft constraints
 *
 * And then evaluates what is the degradation in the
 * performance when replacing hard constraints with soft
 * constraints.
 *
 * Naturally, the disturbance is downscales by 1.4, so that the
 * hard constrained problem is always feasible.
 * 
 * Please, write the data in the folder "SoftvsHard", whose binary
 * files shall be initialiezd usinf soft_benchmark.jl
 */

// Include the qpOASES.hpp header
// It is located in ~/qpoases/include
// Please, do not forget to tell the compiler where to look for include files
constexpr bool ENABLE_DISTURBANCE = true;
constexpr bool PRINT_CSV = true;

// Include additional files that you are going to use
#include <chrono>
#include <iostream>
#include <memory>

// Add your solver
#include "constraints/InputCons.cpp"
#include "constraints/SC_SimpleBounds.cpp"
#include "constraints/SC_Unconstrained.cpp"
#include "constraints/SoftCons.cpp"
#include "detail/extra.cpp"
#include "solver/Mehrotra.cpp"

// Include namespace
using namespace mpc;

// Write the helpers that let you call Mehrotra sovler
template <class real_t, class int_t>
using DiagMat = mpc::matrix::DiagMat<real_t, int_t>;
template <class real_t, class int_t>
using MyInputCons =
    mpc::InputCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat>;
template <class real_t, class int_t>
using MyStateCons =
    mpc::StateCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat,
                                 mpc::matrix::DiagMat>;
using Mehrotra = mpc::solver::Mehrotra<double, int, DiagMat, DiagMat, DiagMat,
                                       MyInputCons, MyStateCons>;
template <class real_t, class int_t>
using SoftStateCons =
    mpc::StateCons::Unconstrained<real_t, int_t, DiagMat, DiagMat,
                                  mpc::TerminalCons::SameCons,
                                  mpc::SoftCons::SimpleBounds>;
using SoftMehrotra = mpc::solver::Mehrotra<double, int, DiagMat, DiagMat,
                                           DiagMat, MyInputCons, SoftStateCons>;

/* ARGUMENT LIST:
 *
 * FIRST ARGUMENT:   Problem instance (defaults to p0)
 * SECOND ARGUMENT:  Number of simulation
 * THIRD ARGUMENT:   Number of simulation steps
 * FORUTH ARGUMENT:  Seed for the random disturbance (defaults to 11)
 */
int main(int argc, char *argv[]) {
  constexpr double divisor{1.4};
  /* Read the input to the program */
  std::string problem_instance{"p0"};
  size_t seed{10}, sim_steps{10};
  std::string sim_number{'0'};
  switch (argc) {
  case 5:
    seed = std::atol(argv[4]);
  case 4:
    sim_steps = std::atol(argv[3]);
  case 3:
    sim_number = argv[2];
  case 2: {
    problem_instance = argv[1];
    break;
  }
  default:
    break;
  }

  /* Read the MPC and the QP problems */
  std::string mpc_address = "/home/add/Desktop/MPC-SM/SoftvsHard/";
  mpc_address = mpc_address + problem_instance + "/mpc.bin";
  extra::mpc_system<double, size_t> sim;
  extra::parse_system(mpc_address, sim);

  /* Create the mpcdata object */
  solver::mpcdata<double> MPC(sim.nx, sim.nu, sim.T, sim.A, sim.B, sim.R, sim.Q,
                              sim.P);

  MPC.lbu = sim.lbu;
  MPC.ubu = sim.ubu;
  MPC.lbx = MPC.lbt = sim.lbx;
  MPC.ubx = MPC.ubt = sim.ubx;

  /* Create the soft mpcdata object */
  solver::mpcdata<double> softMPC(sim.nx, sim.nu, sim.T, sim.A, sim.B, sim.R,
                                  sim.Q, sim.P);

  softMPC.lbu = sim.lbu;
  softMPC.ubu = sim.ubu;
  softMPC.slbx = MPC.slbt = sim.lbx;
  softMPC.subx = MPC.subt = sim.ubx;

  /* Create some initial state */
  std::vector<double> x0(MPC.n);

  /* Create disturbance for some deterministic number */
  extra::disturbance<double> DIS(MPC.n / 2, MPC.lbu[0] / divisor,
                                 MPC.ubu[0] / divisor, seed);

  // *********************************************************************** //
  // ******************************* MEHROTRA ****************************** //
  // *********************************************************************** //
  mpc::solver::mpcoptions<double> MyOptions;
  MyOptions.eta = 0.99;
  MyOptions.optimality_tolerance = 1e-3;
  MyOptions.feasibility_tolerance = 1e-3;
  Mehrotra FastMPC(MPC, x0);
  SoftMehrotra SoftMPC(softMPC, x0);
  FastMPC.set_options(MyOptions);
  SoftMPC.set_options(MyOptions);

  // *********************************************************************** //
  // ************************** Simulation loop **************************** //
  // *********************************************************************** //
  /* Simulation parameters */
  std::vector<double> mehrotraTime(sim_steps), softMehrotraTime(sim_steps);
  std::vector<size_t> iter(sim_steps), softiter(sim_steps);
  std::vector<double> mehrotra_val(sim_steps), softmehrotra_val(sim_steps);
  std::vector<double> mehrotra_input(sim_steps * MPC.m),
      softmehrotra_input(sim_steps * MPC.m);
  std::vector<double> mehrotra_state(sim_steps * MPC.n),
      softmehrotra_state(sim_steps * MPC.n);
  std::vector<double> temp_input(MPC.m * MPC.T), temp_state(MPC.n * MPC.T);

  /* Initial step: Mehrotra */
  auto tstart = std::chrono::high_resolution_clock::now();
  FastMPC.solve();
  auto tend = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> tdiff = tend - tstart;
  mehrotraTime[0] = tdiff.count();
  iter[0] = FastMPC.get_k();
  temp_input = FastMPC.get_opt_input();
  std::copy(temp_input.cbegin(), temp_input.cbegin() + MPC.m,
            mehrotra_input.begin());
  temp_state = FastMPC.get_opt_states();
  std::copy(temp_state.cbegin(), temp_state.cbegin() + MPC.n,
            mehrotra_state.begin());
  mehrotra_val[0] = FastMPC.get_opt_val();

  /* Initial step: Soft Mehrotra */
  tstart = std::chrono::high_resolution_clock::now();
  SoftMPC.solve();
  tend = std::chrono::high_resolution_clock::now();
  tdiff = tend - tstart;
  softMehrotraTime[0] = tdiff.count();
  softiter[0] = SoftMPC.get_k();
  temp_input = SoftMPC.get_opt_input();
  std::copy(temp_input.cbegin(), temp_input.cbegin() + MPC.m,
            softmehrotra_input.begin());
  temp_state = SoftMPC.get_opt_states();
  std::copy(temp_state.cbegin(), temp_state.cbegin() + MPC.n,
            softmehrotra_state.begin());
  softmehrotra_val[0] = SoftMPC.get_opt_val();

  /* Initial step: Simulation */
  sim.set_x0(x0.data());
  if (ENABLE_DISTURBANCE) {
    sim.simulate(FastMPC.get_opt_input().data(), DIS.get_disturbance());
  } else {
    sim.simulate(FastMPC.get_opt_input().data());
  }

  /* Simulation loop */
  for (size_t step = 1; step < sim_steps; step++) {
    /* Initial step: Mehrotra */
    tstart = std::chrono::high_resolution_clock::now();
    FastMPC.solve(sim.get_x0());
    tend = std::chrono::high_resolution_clock::now();
    tdiff = tend - tstart;
    mehrotraTime[step] = tdiff.count();
    iter[step] = FastMPC.get_k();
    temp_input = FastMPC.get_opt_input();
    std::copy(temp_input.cbegin(), temp_input.cbegin() + MPC.m,
              mehrotra_input.begin() + step * MPC.m);
    temp_state = FastMPC.get_opt_states();
    std::copy(temp_state.cbegin(), temp_state.cbegin() + MPC.n,
              mehrotra_state.begin() + step * MPC.n);
    mehrotra_val[step] = FastMPC.get_opt_val();

    /* Initial step: Soft Mehrotra */
    tstart = std::chrono::high_resolution_clock::now();
    SoftMPC.solve(sim.get_x0());
    tend = std::chrono::high_resolution_clock::now();
    tdiff = tend - tstart;
    softMehrotraTime[step] = tdiff.count();
    softiter[step] = SoftMPC.get_k();
    temp_input = SoftMPC.get_opt_input();
    std::copy(temp_input.cbegin(), temp_input.cbegin() + MPC.m,
              softmehrotra_input.begin() + step * MPC.m);
    temp_state = SoftMPC.get_opt_states();
    std::copy(temp_state.cbegin(), temp_state.cbegin() + MPC.n,
              softmehrotra_state.begin() + step * MPC.n);
    softmehrotra_val[step] = SoftMPC.get_opt_val();

    /* Simulation */
    if (ENABLE_DISTURBANCE) {
      sim.simulate(FastMPC.get_opt_input().data(), DIS.get_disturbance());
    } else {
      sim.simulate(FastMPC.get_opt_input().data());
    }
  }

  // ******************************************************************** //
  // **************************** Print to CSV ************************** //
  // ******************************************************************** //
  if (PRINT_CSV) {
    std::string filename{"/home/add/Desktop/MPC-SM/SoftvsHard/"};
    filename += problem_instance + '/' + "time" + sim_number + ".csv";
    extra::writecsv(filename, mehrotraTime.begin(), mehrotraTime.end(),
                    softMehrotraTime.begin(), iter.begin(), softiter.begin(),
                    mehrotra_val.begin(), softmehrotra_val.begin());
    filename = "/home/add/Desktop/MPC-SM/SoftvsHard/" + problem_instance +
               '/' + "inputs" + sim_number + ".csv";
    extra::writecsv(filename, mehrotra_input.begin(), mehrotra_input.end(),
                    softmehrotra_input.begin());
    filename = "/home/add/Desktop/MPC-SM/SoftvsHard/" + problem_instance +
               '/' + "states" + sim_number + ".csv";
    extra::writecsv(filename, mehrotra_state.begin(), mehrotra_state.end(),
                    softmehrotra_state.begin());
  }
  if (*std::max_element(iter.begin(), iter.end()) == 400 &&
      *std::max_element(softiter.begin(), softiter.end()) == 400)
    return 3;
  if (*std::max_element(iter.begin(), iter.end()) == 400)
    return 1;
  if (*std::max_element(softiter.begin(), softiter.end()) == 400)
    return 2;
  return 0;
}
#!/bin/fish
set argc (count $argv)
set seed
if test $argc -gt 2
  set seed (math "$argv[3]")
else
  set seed 1
end
if math "$argc>1" > /dev/null
  set steps (math "$argv[2]")
else
  set steps 20
end
if math "$argc>0" > /dev/null
  set N (math "$argv[1]")
else
  set N 5
end
echo "Simulation length: " $steps " time steps."
echo "Number of runs per simulation: " $N " times."
echo "Using seed = $seed"

set n (seq 4 20)
set m (seq 2 11)
set T (seq 10 10 100)

# echo \n\n"ITERATE OVER THE STATE SIZE."
# set idx 1
# while test $idx -le (count $n)
#   set sim 1
#   cd /home/add/Desktop/MPC-SM/Problems/pn$n[$idx]
#   while test $sim -le $N
#     /home/add/Desktop/MPC-SM/main.out 'pn'$n[$idx] $sim $steps
#     set sim (math "$sim+1")
#   end
#   echo \t"Progress (1/3): " (math "100*$idx/"(count $n)) "%"
#   set idx (math "$idx+1")
# end

# test $idx -eq (math (count $n)+1)

# echo \n\n"ITERATE OVER THE INPUT SIZE"
# set idx 1
# while test $idx -le (count $m)
#   set sim 1
#   cd /home/add/Desktop/MPC-SM/Problems/pm$m[$idx]
#   while test $sim -le $N
#     /home/add/Desktop/MPC-SM/main.out 'pm'$m[$idx] $sim $steps
#     set sim (math "$sim+1")
#   end
#   echo \t"Progress (2/3): " (math "100*$idx/"(count $m)) "%"
#   set idx (math "$idx+1")
# end

# test $idx -eq (math (count $m)+1)

echo \n\n"ITERATE OVER THE PREDICTION HORIZON LENGTH"
set idx 1
while test $idx -le (count $T)
  set sim 1
  while test $sim -le $N
    /home/add/Desktop/MPC-SM/main.out 'pT'$T[$idx] $sim $steps $seed
    set flag $status
    if test $flag -ne 0; and test $sim -eq 1
      echo "pT$T[$idx] is unfeasible. Status = $flag"
      if test $flag -eq 3
        echo \t"Both solvers reach maximum number of iterations"
      end
      if test $flag -eq 2
        echo \t"qpOASES reaches maximum number of iterations"
      end
      if test $flag -eq 1
        echo \t"Mehrotra reaches maximum number of iterations"
      end
    end
    set sim (math "$sim+1")
  end
  echo \t"Progress: " (math "100*$idx/"(count $T)) "%"
  set idx (math "$idx+1")
end
test $idx -eq (math (count $T)+1)
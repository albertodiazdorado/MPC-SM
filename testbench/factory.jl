here = pwd();
using Plots
plotly();

# Struct that manages the constraints
mutable struct constraints
    lb::Vector
    ub::Vector
    F::Matrix
    f::Vector
end
constraints() = constraints(Vector(0), Vector(0), Matrix(0, 0), Vector(0));
constraints(lb::Vector, ub::Vector) = constraints(lb, ub,  Matrix(0, 0), Vector(0));

# Struct that contains all the mpc data
mutable struct mpcdata 
    n::Integer
    m::Integer
    T::Integer
    A::AbstractArray
    B::AbstractArray
    R::AbstractArray
    Q::AbstractArray
    P::AbstractArray
    h_input_cons::constraints
    h_state_cons::constraints
    h_final_cons::constraints
    s_state_cons::constraints
    s_input_cons::constraints
    s_final_cons::constraints
    Bd::AbstractArray
end
mpcdata() = mpcdata(0, 0, 0, zeros(0, 0), zeros(0, 0), zeros(0, 0), zeros(0, 0), zeros(0, 0), constraints(), constraints(), constraints(), constraints(), constraints(), constraints(), zeros(0, 0))
mpcdata(n::Integer, m::Integer) = mpcdata(n, m, 0, zeros(0, 0), zeros(0, 0), zeros(0, 0), zeros(0, 0), zeros(0, 0), constraints(), constraints(), constraints(), constraints(), constraints(), constraints(), zeros(0, 0))
mpcdata(n::Integer, m::Integer, T::Integer) = mpcdata(n, m, T, zeros(0, 0), zeros(0, 0), zeros(0, 0), zeros(0, 0), zeros(0, 0), constraints(), constraints(), constraints(), constraints(), constraints(), constraints(), zeros(0, 0))
mpcdata(n::Integer, m::Integer, T::Integer, A::AbstractArray, B::AbstractArray) = mpcdata(n, m, T, A, B, zeros(0, 0), zeros(0, 0), zeros(0, 0), constraints(), constraints(), constraints(), constraints(), constraints(), constraints(), zeros(0, 0))
mpcdata(n::Integer, m::Integer, T::Integer, A::AbstractArray, B::AbstractArray, R::AbstractArray, Q::AbstractArray, P::AbstractArray) = mpcdata(n, m, T, A, B, R, Q, P, constraints(), constraints(), constraints(), constraints(), constraints(), constraints(), zeros(0, 0))

# Struct that contains the necessary matrices for the dense QP formulation of a given mpc
struct denseQP
    n0::Integer     # State dimensions
    m0::Integer     # Input dimensions
    n::Integer      # Number of variables
    k::Integer      # Number of linear constraints
    H::AbstractArray    # Hessian = R + B'QB
    h::AbstractArray    # h = B'QA, such that h(x0) = h*x0
    # Simple bounds #
    ub::AbstractArray   # Concatenation of ubu
    lb::AbstractArray   # Concatenation of lbu
    # Linear bounds #
    F::AbstractArray    # F = [ B, -B ]
    A::AbstractArray    # Natural response matrix
    B::AbstractArray    # Controllability matrix
    ubx::AbstractArray
    lbx::AbstractArray
    f::AbstractArray    # f = [ubx - Ax0,
                        #      -lbx + Ax0]
    # Constant term
    r::AbstractArray    # A'QA, and then obtain r = x0'*r*x0
    # Linear penalty term
    linear_penalty::AbstractArray
end
denseQP(n0::Integer, m0::Integer, n::Integer, k::Integer,
    H::AbstractArray, h::AbstractArray, ubu::AbstractArray, lbu::AbstractArray,
    F::AbstractArray, A::AbstractArray, B::AbstractArray, ubx::AbstractArray,
    lbx::AbstractArray, f::AbstractArray,
    r::AbstractArray) = denseQP(n0, m0, n, k, H, h, ubu, lbu, F, A, B, ubx, lbx, f, r, zeros(n))


function mpc2denseQP(mpc::mpcdata)
# Get the data
    n = mpc.n
    m = mpc.m
    T = mpc.T


# Build natural and controllability matrices
    A = mpc.A;
    for idx in 2:T
        A = vcat(A, (mpc.A)^idx)    
    end
    B = hcat(mpc.B, zeros(n, m * (T - 1)))
    for row in 2:T
        line = (mpc.A^(row - 1)) * mpc.B
        for col in 2:row - 1
            line = hcat(line, ((mpc.A)^(row - col)) * mpc.B)
        end
        line = hcat(line, mpc.B, zeros(n, (T - row) * m))
        B = vcat(B, line);
    end

# Build Hessian and gradient
    H = zeros(T * m, T * m)
    Q = zeros(T * n, T * n)
    R = zeros(T * m, T * m)

    for idx in 0:T - 1
        Q[1 + idx * n:(idx + 1) * n, 1 + idx * n:(idx + 1) * n] = mpc.Q
        R[1 + idx * m:(idx + 1) * m, 1 + idx * m:(idx + 1) * m] = mpc.R
    end
    Q[end - n + 1:end,end - n + 1:end] = mpc.P
    R[end - m + 1:end,end - m + 1:end] = mpc.R
    H = B' * Q * B + R;
    h = B' * Q * A;

# Build simple bounds
    if (length(mpc.h_input_cons.ub) > 0)
        ubu = mpc.h_input_cons.ub;
        for idx in 2:T
            ubu = vcat(ubu, mpc.h_input_cons.ub)
        end
    end
    if (length(mpc.h_input_cons.lb) > 0)
        lbu = mpc.h_input_cons.lb;
        for idx in 2:T
            lbu = vcat(lbu, mpc.h_input_cons.lb)
        end
    end

# Build linear constraints
    kubx = length(mpc.h_state_cons.ub)
    ubx = zeros(T * kubx)
    for idx in 0:T - 1
        ubx[1 + idx * kubx:(idx + 1) * kubx] = mpc.h_state_cons.ub;
    end
    ubx[end - kubx + 1:end] = mpc.h_final_cons.ub;
    klbx = length(mpc.h_state_cons.lb)
    lbx = zeros(T * klbx)
    for idx in 0:T - 1
        lbx[1 + idx * klbx:(idx + 1) * klbx] = mpc.h_state_cons.lb;
    end
    lbx[end - klbx + 1:end] = mpc.h_final_cons.lb;
    F = [B;-B]
    f = zeros(length(ubx) + length(lbx))
  
# Constant term in the objective function
    r = A' * Q * A;

    return denseQP(n, m, T * m, length(f), H, h, ubu, lbu, F, A, B, ubx, lbx, f, r);
end

function softmpc2denseQP(mpc::mpcdata)
    # Get the data
    n = mpc.n
    m = mpc.m
    T = mpc.T

    # Soft constraints parameters
    K = 50;
    rho = 1;
    subrho = 0;
    
    
    # Build natural and controllability matrices
    A = mpc.A;
    for idx in 2:T
        A = vcat(A, (mpc.A)^idx)    
    end
    # Build standar controllability matrix
    m0 = m + 1
    b = hcat(mpc.B, zeros(n))
    B = hcat(b, zeros(n, m0 * (T - 1)))
    for row in 2:T
        line = hcat((mpc.A^(row - 1)) * mpc.B, zeros(n))
        for col in 2:row - 1
            line = hcat(line, ((mpc.A)^(row - col)) * mpc.B, zeros(n))
        end
        line = hcat(line, b, zeros(n, (T - row) * m0))
        B = vcat(B, line);
    end
    # Build controllability matrix for constraints
    b0 = hcat(mpc.B, -ones(n))
    B1 = hcat(b0, zeros(n, m0 * (T - 1)))
    for row in 2:T
        line = hcat((mpc.A^(row - 1)) * mpc.B, zeros(n))
        for col in 2:row - 1
            line = hcat(line, ((mpc.A)^(row - col)) * mpc.B, zeros(n))
        end
        line = hcat(line, b0, zeros(n, (T - row) * m0))
        B1 = vcat(B1, line);
    end
    b0 = hcat(mpc.B, ones(n))
    B2 = hcat(b0, zeros(n, m0 * (T - 1)))
    for row in 2:T
        line = hcat((mpc.A^(row - 1)) * mpc.B, zeros(n))
        for col in 2:row - 1
            line = hcat(line, ((mpc.A)^(row - col)) * mpc.B, zeros(n))
        end
        line = hcat(line, b0, zeros(n, (T - row) * m0))
        B2 = vcat(B2, line);
    end

    
    # Build Hessian and gradient
    H = zeros(T * m, T * m)
    Q = zeros(T * n, T * n)
    R = zeros(T * m0, T * m0)
    linear_penalty = zeros(T * m0)
    R0 = [mpc.R zeros(m);
    zeros(m)' subrho];
    linear_penalty_0 = vcat(zeros(m), 1)
    
    for idx in 0:T - 1
        Q[1 + idx * n:(idx + 1) * n, 1 + idx * n:(idx + 1) * n] = mpc.Q
        R[1 + idx * m0:(idx + 1) * m0, 1 + idx * m0:(idx + 1) * m0] = R0
        linear_penalty[1 + idx * m0:(idx + 1) * m0] = linear_penalty_0;
    end
    Q[end - n + 1:end,end - n + 1:end] = mpc.P
    R[end - m0 + 1:end,end - m0 + 1:end] = R0
    H = B' * Q * B + R;
    h = B' * Q * A;
    
    # Build simple bounds
    if (length(mpc.h_input_cons.ub) > 0)
        ubu = vcat(mpc.h_input_cons.ub, K);
        for idx in 2:T
            ubu = vcat(ubu, mpc.h_input_cons.ub, K)
        end
    end
    if (length(mpc.h_input_cons.lb) > 0)
        lbu = vcat(mpc.h_input_cons.lb, 0);
        for idx in 2:T
            lbu = vcat(lbu, mpc.h_input_cons.lb, 0)
        end
    end
    
    # Build linear constraints
    kubx = length(mpc.h_state_cons.ub)
    ubx = zeros(T * kubx)
    for idx in 0:T - 1
        ubx[1 + idx * kubx:(idx + 1) * kubx] = mpc.h_state_cons.ub;
    end
    ubx[end - kubx + 1:end] = mpc.h_final_cons.ub;
    klbx = length(mpc.h_state_cons.lb)
    lbx = zeros(T * klbx)
    for idx in 0:T - 1
        lbx[1 + idx * klbx:(idx + 1) * klbx] = mpc.h_state_cons.lb;
    end
    lbx[end - klbx + 1:end] = mpc.h_final_cons.lb;
    F = [B1;-B2]
    f = zeros(length(ubx) + length(lbx))
      
    # Constant term in the objective function
    r = A' * Q * A;
    
    return denseQP(n, m0, T * m0, length(f), H, h, ubu, lbu, F, A, B, ubx, lbx, f, r, linear_penalty);
end


function osc_masses(n::Integer, T::Integer, Ts::Real = 0.5, m = n - 1, ub::Union{AbstractArray,Real} = 0.5, lb::Union{AbstractArray,Real} = -0.5)
    M = ones(n);
    k = ones(n + 1)

    I = zeros(n, n + 1);
    for i in 1:n
        I[i,i] = 1;
        I[i,i + 1] = -1;
    end
    K = I * diagm(k) * I'
    for idx in 1:n
        K[idx,:] = K[idx,:] / M[idx]
    end

    U, bound = givemeU(n, m)
    m = size(U, 2)

    Ac = [zeros(n, n) eye(n); -K zeros(n, n)]
    Bc = [zeros(n, m);U]
    Bdc = [zeros(n, n);eye(n)]

    A = expm(Ac * Ts)
    B = inv(Ac) * (A - eye(2 * n)) * Bc
    Bd = inv(Ac) * (A - eye(2 * n)) * Bdc

    mu = 1
    R = diagm(2 * ones(m))
    Q = mu * diagm(ones(2 * n))
    P = Q;

    MyMPC = mpcdata(2 * n, m, T, A, B, R, Q, P)
    MyMPC.Bd = Bd;

    if length(lb) == 1
        lbu = lb * bound;
    else
        lbu = lb;
    end
    if length(ub) == 1
        ubu = ub * bound;
    else
        ubu = ub
    end
    lbx = -1. * ones(2 * n)
    ubx = 1. * ones(2 * n)

    input_cons = constraints(lbu, ubu);
    state_cons = constraints(lbx, ubx);

    MyMPC.h_input_cons = input_cons;
    MyMPC.h_state_cons = state_cons;
    MyMPC.h_final_cons = state_cons;

    return MyMPC
end
osc_masses() = osc_masses(6, 10)

"""
    givemeU(n)

Calls givemeU(n,n-1)

    givemeU(n,m=0)

Return the input matrix, for a system with n states and m inputs.
m = 0 defaults to m = n-2

For m in the range [1,n-1], there are actuators connecting the masses pairwise, starting from the rightmost pair of masses.
For m in the range [n,2n-1], every additional input acts on a single mass.
m >= 2n is not supported.

It also reports and bounds-scaling-vector.
The first n-1 inputs are scaled to 1.
Every input after n-1 is scaled to 0.1 (10%).
"""
function givemeU(n::Integer, m::Integer=0)
    if (m <= 0)
        m = n - 1;
    elseif m > 2n - 1
        m = 2n - 1
        println("ERROR. m should be at most $(2n - 1).\nm was defaulted to $(2n - 1).\n")
    end

    U = zeros(n, m);
    u = ones(m)

    for col in 1:m
        
    end

    for col in 1:m
        if col <= n - 1
            U[end - col + 1,col] = -1.;
            U[end - col,col] = 1.;
        else
            U[end - (col - n),col] = 1.;
            u[col] = 0.2;
        end
    end

    return U, u
end

function writempc(filename::AbstractString, mpc::mpcdata, x0::Vector = vcat(zeros(mpc.n)))
    # First, write size information
    # Then, write system dynamics A, B, Bd
    # Finally, write penalties R, Q, P
    A = mpc.A
    B = mpc.B
    R = mpc.R
    Q = mpc.Q
    P = mpc.P
    Bd = mpc.Bd
    ubu = mpc.h_input_cons.ub
    lbu = mpc.h_input_cons.lb
    ubx = mpc.h_state_cons.ub
    lbx = mpc.h_state_cons.lb
    open(filename, "w") do io
        write(io, mpc.n)
        write(io, mpc.m)
        write(io, mpc.T)
        write(io, A)
        write(io, B)
        write(io, R)
        write(io, Q)
        write(io, P)
        write(io, Bd)
        write(io, ubu)
        write(io, lbu)
        write(io, ubx)
        write(io, lbx)
        write(io, x0)
    end
end

function writeqp(filename::AbstractString, qp::denseQP)
    # First, write dimensions
    # Then, give objective function information
    # Then, give simple bounds
    # Then, give constraints matrix
    # Finally, give information regarding the independent term of the linear constraints
    open(filename, "w") do io
        write(io, qp.n0)    # Integer
        write(io, qp.n)     # Integer
        write(io, qp.k)     # Integer
        write(io, qp.H)     # Array{Float64,n*n}
        write(io, qp.h)     # Array{Float64,n*n0}
        write(io, qp.ub)    # Array{Float64,n}
        write(io, qp.lb)    # Array{Float64,n}
        write(io, qp.F)     # Array{Float64,k*n}
        write(io, qp.A)     # Array{Float64,k/2*n0}
        write(io, qp.ubx)   # Array{Float64,k}
        write(io, qp.lbx)   # Array{Float64,k}
        write(io, qp.r)     # Array{Float64,n0*n0}
        write(io, qp.linear_penalty)     # Array{Float64,n}
    end
end

function gurobisolve(r::denseQP, x0::Vector)
    env1 = Gurobi.Env()
    setparam!(env1, "Presolve", 0)
    setparam!(env1, "Method", 0)      # Primal simplex
    env2 = Gurobi.Env()
    setparam!(env2, "Presolve", 0)
    setparam!(env2, "Method", 1)      # Dual simplex
    env3 = Gurobi.Env()
    setparam!(env3, "Presolve", 0)
    setparam!(env3, "Method", 2)      # Barrier method
    model1 = gurobi_model(env1; name = "qpOASES", H = r.H, f = r.h * x0 + r.linear_penalty, A = r.F, b = vcat(r.ubx - r.A * x0, -r.lbx + r.A * x0), lb = r.lb, ub = r.ub)
    model2 = gurobi_model(env2; name = "qpOASES", H = r.H, f = r.h * x0 + r.linear_penalty, A = r.F, b = vcat(r.ubx - r.A * x0, -r.lbx + r.A * x0), lb = r.lb, ub = r.ub)
    model3 = gurobi_model(env3; name = "qpOASES", H = r.H, f = r.h * x0 + r.linear_penalty, A = r.F, b = vcat(r.ubx - r.A * x0, -r.lbx + r.A * x0), lb = r.lb, ub = r.ub)
    
    println("\n**********************************")
    optimize(model1);
    println("**********************************\n\n")
    println("**********************************")
    optimize(model2);
    println("**********************************\n\n")
    println("**********************************")
    optimize(model3);
    println("**********************************\n\n")
    
    sol = tuple(get_solution(model1), get_solution(model2), get_solution(model3))
    println("**********************************")
    println("solution (1) = $(sol[1])")
    println("solution (2) = $(sol[2])")
    println("solution (3) = $(sol[3])")
    println("**********************************\n\n")
    
    objv = tuple(get_objval(model1), get_objval(model2), get_objval(model3))
    println("**********************************")
    println("objv (1) = $(objv[1])")
    println("objv (2) = $(objv[2])")
    println("objv (3) = $(objv[3])")
    println("**********************************\n\n")

    full_obj = tuple(objv[1] + 0.5 * x0' * r.r * x0, objv[2] + 0.5 * x0' * r.r * x0, objv[3] + 0.5 * x0' * r.r * x0)
    println("**********************************")
    println("full_obj (1) = $(full_obj[1])")
    println("full_obj (2) = $(full_obj[2])")
    println("full_obj (3) = $(full_obj[3])")
    println("**********************************\n\n")

    m = r.m0
    T = Integer(round(r.n / r.m0))
    choper0 = falses(m, m)
    for i in 1:m
        choper0[i,i] = true;
    end
    choper = choper0;
    for i in 2:T
        choper = vcat(choper, choper0)
    end
    U = plot(sol[1][choper[:,1]], label = "u1", ylims=(-0.5, 0.5), title = "Inputs")
    for i in 2:m
        plot!(U, sol[1][choper[:,i]], label = string("u", i))
    end

    x = r.A * x0 + r.B * sol[1]
    n = r.n0
    choper0 = falses(n, n)
    for i in 1:n
        choper0[i,i] = true;
    end
    choper = choper0;
    for i in 2:T
        choper = vcat(choper, choper0)
    end
    X = plot(x[choper[:,1]], label = "x1", ylims=(-1, 1), title = "States")
    for i in 2:n
        plot!(X, x[choper[:,i]], label = string("x", i))
    end
    P = plot(U, X, layout = (2, 1))

    return tuple(sol[1], full_obj[1], P)
end

mpc = osc_masses(6, 15, 0.5);
qp1 = mpc2denseQP(mpc);
qp2 = softmpc2denseQP(mpc);
x0 = rand(mpc.n) / 4

# Write the binary file for
#   n = 6
#   m = 4
#   T = 15
cd("/home/add/Desktop/MPC-SM/Problems/")
if !isdir("p0")
    mkdir("p0")
end
cd("p0")
writempc("mpc.bin", mpc, x0);
writeqp("qp.bin", qp1);
writeqp("qpsoft.bin", qp2);

cd("/home/add/Desktop/MPC-SM/SoftProblems/")
if !isdir("p0")
    mkdir("p0")
end
cd("p0")
writempc("mpc.bin", mpc, x0);
writeqp("qp.bin", qp1);
writeqp("qpsoft.bin", qp2);

cd("/home/add/Desktop/MPC-SM/SoftAccuracy/")
if !isdir("p0")
    mkdir("p0")
end
cd("p0")
writempc("mpc.bin", mpc, x0);
writeqp("qp.bin", qp1);
writeqp("qpsoft.bin", qp2);

cd(here)




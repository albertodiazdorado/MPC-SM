// Include the qpOASES.hpp header
// It is located in ~/qpoases/include
// Please, do not forget to tell the compiler where to look for include files
constexpr bool ENABLE_DISTURBANCE = true;
constexpr bool PRINT_CSV = true;

// Add the option "-I /home/add/qpoases/include/"
// Add the option "-lqpOASES -L /home/add/qpoases/lib"
#include "gurobi_c++.h"

// Include additional files that you are going to use
#include <chrono>
#include <iostream>
#include <memory>

// Add your solver
#include "constraints/InputCons.cpp"
#include "constraints/SC_SimpleBounds.cpp"
#include "detail/extra.cpp"
#include "solver/Mehrotra.cpp"

static bool
dense_optimize(GRBEnv *env, int rows, int cols,
               double *c,   /* linear portion of objective function */
               double *Q,   /* quadratic portion of objective function */
               double *A,   /* constraint matrix */
               char *sense, /* constraint senses */
               double *rhs, /* RHS vector */
               double *lb,  /* variable lower bounds */
               double *ub,  /* variable upper bounds */
               char *vtype, /* variable types (continuous, binary, etc.) */
               double *solution, double *objvalP);

// Include namespace
using namespace mpc;

// Write the helpers that let you call Mehrotra sovler
template <class real_t, class int_t>
using DiagMat = mpc::matrix::DiagMat<real_t, int_t>;
template <class real_t, class int_t>
using MyInputCons =
    mpc::InputCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat>;
template <class real_t, class int_t>
using MyStateCons =
    mpc::StateCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat,
                                 mpc::matrix::DiagMat>;
using Mehrotra = mpc::solver::Mehrotra<double, int, DiagMat, DiagMat, DiagMat,
                                       MyInputCons, MyStateCons>;

/* ARGUMENT LIST:
 *
 * FIRST ARGUMENT:   Problem instance (defaults to p0)
 * SECOND ARGUMENT:  Number of simulation
 * THIRD ARGUMENT:   Number of simulation steps
 * FORUTH ARGUMENT:  Seed for the random disturbance (defaults to 11)
 */
int main(int argc, char *argv[]) {
  /* Read the input to the program */
  std::string problem_instance{"p0"};
  size_t seed{88}, sim_steps{10};
  std::string sim_number{'0'};
  switch (argc) {
  case 5:
    seed = std::atol(argv[4]);
  case 4:
    sim_steps = std::atol(argv[3]);
  case 3:
    sim_number = argv[2];
  case 2: {
    problem_instance = argv[1];
    break;
  }
  default:
    break;
  }

  /* Read the MPC and the QP problems */
  std::string qp_address = "/home/add/Desktop/MPC-SM/Problems/";
  qp_address = qp_address + problem_instance + "/qpsoft.bin";
  extra::qp_system<double, size_t> QP;
  extra::parse_system(qp_address, QP);

  /* Create some initial state */
  std::vector<double> x0(QP.n0);
  std::fill(x0.begin(), x0.end(), 1.0);

  // *********************************************************************** //
  // ******************************* Gurobi ******************************** //
  // *********************************************************************** //

  // *********************************************************************** //
  // ************************** Simulation loop **************************** //
  // *********************************************************************** //
  // if (ENABLE_GUROBI)
  std::vector<double> gurobiTime(sim_steps), gurobi_val(sim_steps);

  /* Initial step: GUROBI */
  try {
    std::unique_ptr<GRBEnv> env{new GRBEnv()};
    env.get()->set(GRB_IntParam_OutputFlag, 0);
    std::vector<char> sense(QP.k, '<');
    std::vector<double> sol(QP.n);
    dense_optimize(env.get(), QP.k, QP.n, QP.get_soft_gradient(x0.data()),
                   QP.get_half_hessian(), QP.get_constraints_transpose(),
                   sense.data(), QP.get_rhs(x0.data()), QP.get_lb(),
                   QP.get_ub(), NULL, sol.data(), &gurobi_val[0]);
    gurobi_val[0] += QP.get_residual(x0.data());

    std::string filename{"/home/add/Desktop/MPC-SM/Problems/"};
    filename += problem_instance + '/' + "solution" + sim_number + ".csv";
    extra::writecsv(filename, sol.begin(), sol.end(), sol.begin());
    std::cout << "\nOptimal value: " << gurobi_val[0] << std::endl;
  } catch (GRBException e) {
    std::cout << "Error code = " << e.getErrorCode() << std::endl;
    std::cout << e.getMessage() << std::endl;
  } catch (...) {
    std::cout << "Exception during optimization" << std::endl;
  }

  return 0;
}

static bool
dense_optimize(GRBEnv *env, int rows, int cols,
               double *c,   /* linear portion of objective function */
               double *Q,   /* quadratic portion of objective function */
               double *A,   /* constraint matrix */
               char *sense, /* constraint senses */
               double *rhs, /* RHS vector */
               double *lb,  /* variable lower bounds */
               double *ub,  /* variable upper bounds */
               char *vtype, /* variable types (continuous, binary, etc.) */
               double *solution, double *objvalP) {
  GRBModel model = GRBModel(*env);
  int i, j;
  bool success = false;

  /* Add variables to the model */

  GRBVar *vars = model.addVars(lb, ub, NULL, vtype, NULL, cols);

  /* Populate A matrix */

  for (i = 0; i < rows; i++) {
    GRBLinExpr lhs = 0;
    for (j = 0; j < cols; j++)
      if (A[i * cols + j] != 0)
        lhs += A[i * cols + j] * vars[j];
    model.addConstr(lhs, sense[i], rhs[i]);
  }

  GRBQuadExpr obj = 0;

  for (j = 0; j < cols; j++)
    obj += c[j] * vars[j];
  for (i = 0; i < cols; i++)
    for (j = 0; j < cols; j++)
      if (Q[i * cols + j] != 0)
        obj += Q[i * cols + j] * vars[i] * vars[j];

  model.setObjective(obj);

  model.optimize();

  if (model.get(GRB_IntAttr_Status) == GRB_OPTIMAL) {
    *objvalP = model.get(GRB_DoubleAttr_ObjVal);
    for (i = 0; i < cols; i++)
      solution[i] = vars[i].get(GRB_DoubleAttr_X);
    success = true;
  }

  delete[] vars;

  return success;
}
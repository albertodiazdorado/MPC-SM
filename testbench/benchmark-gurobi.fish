#!/bin/fish
set argc (count $argv)
set seed
if test $argc -gt 1
  set seed (math "$argv[2]")
else
  set seed 1
end
if math "$argc>0" > /dev/null
  set steps (math "$argv[1]")
else
  set steps 20
end
echo "Simulation length: " $steps " time steps."
echo "Using seed = $seed"

set n (seq 4 20)
set m (seq 2 11)
set T (seq 10 10 100)


set idx 1
while test $idx -le (count $n)
  cd /home/add/Desktop/MPC-SM/Problems/pn$n[$idx]
  /home/add/Desktop/MPC-SM/main0.out 'pn'$n[$idx] 0 $steps $seed | egrep -v "Academic License"  > /dev/null
  echo \t"Progress (1/3): " (math "100*$idx/"(count $n)) "%"
  set idx (math "$idx+1")
end

set idx 1
while test $idx -le (count $m)
  cd /home/add/Desktop/MPC-SM/Problems/pm$m[$idx]
  /home/add/Desktop/MPC-SM/main0.out 'pm'$T[$idx] 0 $steps $seed | egrep -v "Academic License"  > /dev/null
  echo \t"Progress (2/3): " (math "100*$idx/"(count $m)) "%"
  set idx (math "$idx+1")
end

set idx 1
while test $idx -le (count $T)
  cd /home/add/Desktop/MPC-SM/Problems/pT$T[$idx]
  /home/add/Desktop/MPC-SM/main0.out 'pT'$T[$idx] 0 $steps $seed | egrep -v "Academic License"  > /dev/null
  echo \t"Progress (3/3): " (math "100*$idx/"(count $T)) "%"
  set idx (math "$idx+1")
end

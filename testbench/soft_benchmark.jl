include("/home/add/Desktop/MPC-SM/testbench/factory.jl")

## Uncomment the desired folder
folder = "SoftProblems"
# folder = "SoftAccuracy"

# Set default values
n0 = 6
m0 = n0 - 1
T0 = 20
Ts = 0.5

# Create ranges
n = 4:20
m = 2:11
T = 10:10:100

# Create the sequence for different state size
for i in n
    cd(string("/home/add/Desktop/MPC-SM/", folder, "/"))
    if !isdir(string("pn", i))
        mkdir(string("pn", i))
    end
    cd(string("pn", i))
    mpc = osc_masses(i, T0)
    qp = softmpc2denseQP(mpc)
    writempc("mpc.bin", mpc);
    writeqp("qpsoft.bin", qp);
end

# Create the sequence for different input configurations
for i in m
    cd(string("/home/add/Desktop/MPC-SM/", folder, "/"))
    if !isdir(string("pm", i))
        mkdir(string("pm", i))
    end
    cd(string("pm", i))
    mpc = osc_masses(n0, T0, Ts, i)
    qp = softmpc2denseQP(mpc)
    writempc("mpc.bin", mpc);
    writeqp("qpsoft.bin", qp);
end

# Create the sequence for different prediction horizon lengths
for i in T
    cd(string("/home/add/Desktop/MPC-SM/", folder, "/"))
    if !isdir(string("pT", i))
        mkdir(string("pT", i))
    end
    cd(string("pT", i))
    mpc = osc_masses(n0, i)
    qp = softmpc2denseQP(mpc)
    writempc("mpc.bin", mpc);
    writeqp("qpsoft.bin", qp);
end
cd("/home/add/Desktop/MPC-SM/testbench/")
/* This file runs qpOASES and Mehrotra in competition
 * for efficiency. We are interested in speed; hence,
 * we make time measurements. This is also why Gurobi is not here
 *
 * Please, write the resulting files to the folder
 * "SoftProblems"
 * */

// Include the qpOASES.hpp header
// It is located in ~/qpoases/include
// Please, do not forget to tell the compiler where to look for include files
constexpr bool ENABLE_DISTURBANCE = true;
constexpr bool PRINT_CSV = true;
constexpr bool PRINT_INPUT = true;

// Add the option "-I /home/add/qpoases/include/"
// Add the option "-lqpOASES -L /home/add/qpoases/lib"
#include "qpOASES/QProblem.hpp"

// Include additional files that you are going to use
#include <chrono>
#include <iostream>
#include <memory>

// Add your solver
#include "constraints/InputCons.cpp"
#include "constraints/SC_Unconstrained.cpp"
#include "constraints/SoftCons.cpp"
#include "detail/extra.cpp"
#include "solver/Mehrotra.cpp"

// Include namespace
using namespace mpc;

// Write the helpers that let you call Mehrotra sovler
template <class real_t, class int_t>
using DiagMat = mpc::matrix::DiagMat<real_t, int_t>;
template <class real_t, class int_t>
using MyInputCons =
    mpc::InputCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat>;
template <class real_t, class int_t>
using MyStateCons =
    mpc::StateCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat,
                                 mpc::matrix::DiagMat>;
template <class real_t, class int_t>
using SoftStateCons =
    mpc::StateCons::Unconstrained<real_t, int_t, DiagMat, DiagMat,
                                  mpc::TerminalCons::SameCons,
                                  mpc::SoftCons::SimpleBounds>;
using SoftMehrotra = mpc::solver::Mehrotra<double, int, DiagMat, DiagMat,
                                           DiagMat, MyInputCons, SoftStateCons>;

/* ARGUMENT LIST:
 *
 * FIRST ARGUMENT:   Problem instance (defaults to p0)
 * SECOND ARGUMENT:  Number of simulation
 * THIRD ARGUMENT:   Number of simulation steps
 * FORUTH ARGUMENT:  Seed for the random disturbance (defaults to 1)
 */
int main(int argc, char *argv[]) {
  /* Read the input to the program */
  std::string problem_instance{"p0"};
  size_t seed{1}, sim_steps{10};
  std::string sim_number{'0'};
  switch (argc) {
  case 5:
    seed = std::atol(argv[4]);
  case 4:
    sim_steps = std::atol(argv[3]);
  case 3:
    sim_number = argv[2];
  case 2: {
    problem_instance = argv[1];
    break;
  }
  default:
    break;
  }

  /* Read the MPC and the QP problems */
  std::string mpc_address = "/home/add/Desktop/MPC-SM/SoftProblems/";
  std::string qp_address{mpc_address};
  mpc_address = mpc_address + problem_instance + "/mpc.bin";
  qp_address = qp_address + problem_instance + "/qpsoft.bin";
  extra::mpc_system<double, size_t> sim;
  extra::parse_system(mpc_address, sim);
  extra::qp_system<double, size_t> QP;
  extra::parse_system(qp_address, QP);

  /* Create the mpcdata object */
  solver::mpcdata<double> MPC(sim.nx, sim.nu, sim.T, sim.A, sim.B, sim.R, sim.Q,
                              sim.P);

  MPC.lbu = sim.lbu;
  MPC.ubu = sim.ubu;
  MPC.slbx = MPC.slbt = sim.lbx;
  MPC.subx = MPC.subt = sim.ubx;

  /* Create some initial state */
  std::vector<double> x0(MPC.n);

  /* Create disturbance for some deterministic number */
  extra::disturbance<double> DIS(MPC.n / 2, MPC.lbu[0], MPC.ubu[0], seed);

  // *********************************************************************** //
  // ******************************* MEHROTRA ****************************** //
  // *********************************************************************** //
  mpc::solver::mpcoptions<double> MyOptions;
  MyOptions.eta = 0.99;
  MyOptions.optimality_tolerance = 1e-3;
  MyOptions.feasibility_tolerance = 1e-3;
  SoftMehrotra FastMPC(MPC, x0);
  FastMPC.set_options(MyOptions);

  // *********************************************************************** //
  // ****************************** qpOASES ******************************** //
  // *********************************************************************** //
  qpOASES::QProblem OASES(QP.get_vars(), QP.get_cons());
  qpOASES::Options myOasesOptions;
  myOasesOptions.setToMPC();
  myOasesOptions.printLevel = qpOASES::PL_NONE;
  OASES.setOptions(myOasesOptions);

  // *********************************************************************** //
  // ************************** Simulation loop **************************** //
  // *********************************************************************** //
  /* Simulation parameters */
  std::vector<double> mehrotraTime(sim_steps), qpOasesTime(sim_steps);
  std::vector<qpOASES::int_t> nWSR(sim_steps, 1000);
  std::vector<bool> status(sim_steps);
  std::vector<double> cputime(sim_steps, 1000);
  std::vector<size_t> iter(sim_steps);
  std::vector<double> mehrotra_val(sim_steps), qpOases_val(sim_steps);
  std::vector<double> mehrotra_input(sim_steps * QP.n),
      qpOases_input(sim_steps * QP.n);
  std::vector<double> temp(QP.n);

  /* Initial step: qpOASES */
  auto tstart = std::chrono::high_resolution_clock::now();
  OASES.init(QP.get_hessian(), QP.get_soft_gradient(x0.data()),
             QP.get_constraints(), QP.get_lb(), QP.get_ub(), QP.get_lhs(),
             QP.get_rhs(x0.data()), nWSR[0], &cputime[0]);
  auto tend = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> tdiff = tend - tstart;
  qpOasesTime[0] = tdiff.count();
  OASES.getPrimalSolution(qpOases_input.data());
  qpOases_val[0] = OASES.getObjVal() + QP.get_residual(x0.data());
  status[0] = OASES.isSolved();

  /* Initial step: Mehrotra */
  tstart = std::chrono::high_resolution_clock::now();
  FastMPC.solve();
  tend = std::chrono::high_resolution_clock::now();
  tdiff = tend - tstart;
  mehrotraTime[0] = tdiff.count();
  iter[0] = FastMPC.get_k();
  temp = FastMPC.get_opt_input();
  auto it = temp.begin();
  for (size_t idx = 0; idx < MPC.T; idx++) {
    temp.insert(it + MPC.m, 0);
    it = temp.begin() + (idx + 1) * (MPC.m + 1);
  }
  std::copy(temp.cbegin(), temp.cend(), mehrotra_input.begin());
  mehrotra_val[0] = FastMPC.get_opt_val();

  /* Initial step: Simulation */
  sim.set_x0(x0.data());
  if (ENABLE_DISTURBANCE)
    sim.simulate(FastMPC.get_opt_input().data(), DIS.get_disturbance());
  else
    sim.simulate(FastMPC.get_opt_input().data());

  /* Simulation loop */
  for (size_t step = 1; step < sim_steps; step++) {
    /* qpOASES with hotstart */
    tstart = std::chrono::high_resolution_clock::now();
    OASES.hotstart(QP.get_soft_gradient(sim.get_x0()), QP.get_lb(), QP.get_ub(),
                   QP.get_lhs(), QP.get_rhs(sim.get_x0()), nWSR[step],
                   &cputime[step]);
    tend = std::chrono::high_resolution_clock::now();
    tdiff = tend - tstart;
    qpOasesTime[step] = tdiff.count();
    OASES.getPrimalSolution(qpOases_input.data() + step * QP.n);
    qpOases_val[step] = OASES.getObjVal() + QP.get_residual(sim.get_x0());
    status[step] = OASES.isSolved();

    /* Mehrotra with hotstart */
    tstart = std::chrono::high_resolution_clock::now();
    FastMPC.solve(sim.get_x0());
    tend = std::chrono::high_resolution_clock::now();
    tdiff = tend - tstart;
    mehrotraTime[step] = tdiff.count();
    iter[step] = FastMPC.get_k();
    temp = FastMPC.get_opt_input();
    std::copy(temp.cbegin(), temp.cend(), mehrotra_input.begin() + step * QP.n);
    mehrotra_val[step] = FastMPC.get_opt_val();

    /* Simulation */
    if (ENABLE_DISTURBANCE)
      sim.simulate(FastMPC.get_opt_input().data(), DIS.get_disturbance());
    else
      sim.simulate(FastMPC.get_opt_input().data());
  }
  std::transform(cputime.cbegin(), cputime.cend(), cputime.begin(),
                 [](double time) { return time * 1000; });

  // ******************************************************************** //
  // **************************** Print to CSV ************************** //
  // ******************************************************************** //
  if (PRINT_CSV) {
    std::string filename{"/home/add/Desktop/MPC-SM/SoftProblems/"};
    filename += problem_instance + '/' + "time" + sim_number + ".csv";
    extra::writecsv(filename, mehrotraTime.begin(), mehrotraTime.end(),
                    qpOasesTime.begin(), cputime.begin(), iter.begin(),
                    nWSR.begin(), mehrotra_val.begin(), qpOases_val.begin(),
                    status.begin());
    if (PRINT_INPUT) {
      filename = "/home/add/Desktop/MPC-SM/SoftProblems/" + problem_instance +
                 '/' + "inputs" + sim_number + ".csv";
      extra::writecsv(filename, mehrotra_input.begin(), mehrotra_input.end(),
                      qpOases_input.begin());
    }
  }
  if (*std::max_element(iter.begin(), iter.end()) == 400 &&
      *std::max_element(nWSR.begin(), nWSR.end()) == 1000)
    return 3;
  if (*std::max_element(iter.begin(), iter.end()) == 400)
    return 1;
  if (*std::max_element(nWSR.begin(), nWSR.end()) == 1000)
    return 2;
  return 0;
}
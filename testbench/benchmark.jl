include("/home/add/Desktop/MPC-SM/testbench/factory.jl")

# Set default values
n0 = 6
m0 = n0-1
T0 = 20
Ts = 0.5

# Create ranges
n = 4:20
m = 2:11
T = 10:10:100

# Create the sequence for different state size
for i in n
    cd("/home/add/Desktop/MPC-SM/Problems/")
    if !isdir(string("pn", i))
        mkdir(string("pn", i))
    end
    cd(string("pn", i))
    mpc = osc_masses(i, T0)
    qp = mpc2denseQP(mpc)
    writempc("mpc.bin", mpc);
    writeqp("qp.bin", qp);
end

# Create the sequence for different input configurations
for i in m
    cd("/home/add/Desktop/MPC-SM/Problems/")
    if !isdir(string("pm", i))
        mkdir(string("pm", i))
    end
    cd(string("pm", i))
    mpc = osc_masses(n0, T0, Ts, i)
    qp = mpc2denseQP(mpc)
    writempc("mpc.bin", mpc);
    writeqp("qp.bin", qp);
end

# Create the sequence for different prediction horizon lengths
for i in T
    cd("/home/add/Desktop/MPC-SM/Problems/")
    if !isdir(string("pT", i))
        mkdir(string("pT", i))
    end
    cd(string("pT", i))
    mpc = osc_masses(n0, i)
    qp = mpc2denseQP(mpc)
    writempc("mpc.bin", mpc);
    writeqp("qp.bin", qp);
end
cd("/home/add/Desktop/MPC-SM/testbench/")
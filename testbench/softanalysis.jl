# Create ranges
n = 4:20
m = 2:11
T = 10:10:100

## Select folder
folder = "SoftvsHard"

# Number of simulations per case (adjust this parameter accordingly)
if !isdefined(:N)
    N = 50
end

# Analyze all the files regarding state size
nmax = hcat(n, zeros(length(n), 2))
nmean = hcat(n, zeros(length(n), 2))
nitermax = hcat(n, zeros(length(n), 2))
nitermean = hcat(n, zeros(length(n), 2))
idx = 1
for i in n
    cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
    cd(string("pn", i))
    # Analyze the times
    r = readcsv(string("time1.csv"))
    hard = r[:,1]
    soft = r[:,2]
    iter = r[:,3]
    softiter = r[:,4]
    for sim in 2:N
        r = readcsv(string("time", sim, ".csv"))
        hard = hcat(hard,r[:,1])
        soft = hcat(soft,r[:,2])
    end
    ht = minimum(hard, 2)
    st = minimum(soft, 2)
    nmax[idx,2:3] = hcat(maximum(ht), maximum(st))
    nmean[idx,2:3] = hcat(mean(ht), mean(st))
    nitermax[idx,2:3] = hcat(maximum(iter), maximum(softiter))
    nitermean[idx,2:3] = hcat(mean(iter), mean(softiter))
    idx = idx + 1
end

# Analyze all the files regarding input size
mmax = hcat(m, zeros(length(m), 2))
mmean = hcat(m, zeros(length(m), 2))
mitermax = hcat(m, zeros(length(m), 2))
mitermean = hcat(m, zeros(length(m), 2))
idx = 1
for i in m
    cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
    cd(string("pm", i))
    # Analyze the times
    r = readcsv(string("time1.csv"))
    hard = r[:,1]
    soft = r[:,2]
    iter = r[:,3]
    softiter = r[:,4]
    for sim in 2:N
        r = readcsv(string("time", sim, ".csv"))
        hard = hcat(hard,r[:,1])
        soft = hcat(soft,r[:,2])
    end
    ht = minimum(hard, 2)
    st = minimum(soft, 2)
    mmax[idx,2:3] = hcat(maximum(ht), maximum(st))
    mmean[idx,2:3] = hcat(mean(ht), mean(st))
    mitermax[idx,2:3] = hcat(maximum(iter), maximum(softiter))
    mitermean[idx,2:3] = hcat(mean(iter), mean(softiter))
    idx = idx + 1
end


# Analyze all the files regarding prediction horizon length
tmax = hcat(T, zeros(length(T), 2))
tmean = hcat(T, zeros(length(T), 2))
titermax = hcat(T, zeros(length(T), 2))
titermean = hcat(T, zeros(length(T), 2))
idx = 1
for i in T
    cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
    cd(string("pT", i))
    # Analyze the times
    r = readcsv(string("time1.csv"))
    hard = r[:,1]
    soft = r[:,2]
    iter = r[:,3]
    softiter = r[:,4]
    for sim in 2:N
        r = readcsv(string("time", sim, ".csv"))
        hard = hcat(hard,r[:,1])
        soft = hcat(soft,r[:,2])
    end
    ht = minimum(hard, 2)
    st = minimum(soft, 2)
    tmax[idx,2:3] = hcat(maximum(ht), maximum(st))
    tmean[idx,2:3] = hcat(mean(ht), mean(st))
    titermax[idx,2:3] = hcat(maximum(iter), maximum(softiter))
    titermean[idx,2:3] = hcat(mean(iter), mean(softiter))
    idx = idx + 1
end

legend1 = ["n" "hard" "soft"]
legend2 = ["m" "hard" "soft"]
legend3 = ["T" "hard" "soft"]

cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
writecsv("nmax.csv", vcat(legend1, nmax))
writecsv("nmean.csv", vcat(legend1, nmean))
writecsv("mmax.csv", vcat(legend2, mmax))
writecsv("mmean.csv", vcat(legend2, mmean))
writecsv("tmax.csv", vcat(legend3, tmax))
writecsv("tmean.csv", vcat(legend3, tmean))
writecsv("n_max_iter.csv", vcat(legend1, nitermax))
writecsv("n_mean_iter.csv", vcat(legend1, nitermean))
writecsv("m_max_iter.csv", vcat(legend2, mitermax))
writecsv("m_mean_iter.csv", vcat(legend2, mitermean))
writecsv("t_max_iter.csv", vcat(legend3, titermax))
writecsv("t_mean_iter.csv", vcat(legend3, titermean))

# Plot one single evolution
cd(string("/home/add/Desktop/MPC-SM/",folder,"/pn6"))
n = 6
m = 5
T = 20

r = readcsv(string("time", 1, ".csv"))
u = readcsv(string("inputs", 1, ".csv"))
x = readcsv(string("states", 1, ".csv"))

uh = zeros(size(r,1),m)
xh = zeros(size(r,1),n)
vh = zeros(size(r,1),n)
us = zeros(size(r,1),m)
xs = zeros(size(r,1),n)
vs = zeros(size(r,1),n)

cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))

# Extract inputs
legend = ("iter")
for i in 1:m
    legend = hcat(legend,string("u",i))
    chop0 = vcat(falses(i-1),true,falses(m-i))
    chop =chop0
    for idx in 2:size(r,1)
        chop = vcat(chop,chop0)
    end
    uh[:,i] = u[chop,1]
    us[:,i] = u[chop,2]
end
# Save plot
writecsv("hard_input_evol.csv", vcat(legend, hcat(1:size(r,1),uh)))
writecsv("soft_input_evol.csv", vcat(legend, hcat(1:size(r,1),us)))

# Extract states
legend = "iter"
for i in 1:n
    legend = hcat(legend,string("x",i))
    chop0 = vcat(falses(i-1),true,falses(2*n-i))
    chop =chop0
    for idx in 2:size(r,1)
        chop = vcat(chop,chop0)
    end
    xh[:,i] = x[chop,1]
    xs[:,i] = x[chop,2]
end
# Save plot
writecsv("hard_state_evol.csv", vcat(legend, hcat(1:size(r,1),xh)))
writecsv("soft_state_evol.csv", vcat(legend, hcat(1:size(r,1),xs)))

# Extract velocities
legend = "iter"
for i in 1:n
    legend = hcat(legend,string("v",i))
    chop0 = vcat(falses(n+i-1),true,falses(n-i))
    chop =chop0
    for idx in 2:size(r,1)
        chop = vcat(chop,chop0)
    end
    vh[:,i] = x[chop,1]
    vs[:,i] = x[chop,2]
end
# Save plot
writecsv("hard_speed_evol.csv", vcat(legend, hcat(1:size(r,1),vh)))
writecsv("soft_speed_evol.csv", vcat(legend, hcat(1:size(r,1),vs)))


cd("/home/add/Desktop/MPC-SM/testbench")

using Plots
plotly()

"""

    plot_results(x::Integer=1, min::Integer=0, max::Integer=0)

Choose x = {1,2,3}

Choose 0 <= min < max <= N, where N is the number of simulations


Returns 4 plots:

  p1 plots the maximum computation time of hard and soft constraints,
  for the time steps [min,max]

  p2 plots the mean computation time

  p3 plots the maximum number of iterations

  p4 plots the mean number of iterations
"""
function plot_results(x::Integer = 1, min::Integer = 0, max::Integer = 0)
    if x == 1
        p1 = plot(nmax[1+min:end-max,1],nmax[1+min:end-max,2:3],title = "Max time", label=["Hard" "Soft"], xlabel = "N", ylabel = "ms", yscale = :log10)
        p2 = plot(nmax[1+min:end-max,1],nmean[1+min:end-max,2:3],title = "Mean time", label=["Hard" "Soft"], xlabel = "N", ylabel = "ms", yscale = :log10)
        p3 = plot(nmax[1+min:end-max,1],nitermax[1+min:end-max,2:3],title = "Max Iters", label=["Hard" "Soft"], xlabel = "N", ylabel = "iters")
        p4 = plot(nmax[1+min:end-max,1],nitermean[1+min:end-max,2:3],title = "Mean iters", label=["Hard" "Soft"], xlabel = "N", ylabel = "iters")
        return [p1,p2,p3,p4]
    elseif x == 2
        p1 = plot(mmax[1+min:end-max,1],mmax[1+min:end-max,2:3],title = "Max time", label=["Hard" "Soft"], xlabel = "N", ylabel = "ms", yscale = :log10)
        p2 = plot(mmean[1+min:end-max,1],mmean[1+min:end-max,2:3],title = "Mean time", label=["Hard" "Soft"], xlabel = "N", ylabel = "ms", yscale = :log10)
        p3 = plot(mitermax[1+min:end-max,1],mitermax[1+min:end-max,2:3],title = "Max time", label=["Hard" "Soft"], xlabel = "N", ylabel = "iters")
        p4 = plot(mitermean[1+min:end-max,1],mitermean[1+min:end-max,2:3],title = "Mean iters", label=["Hard" "Soft"], xlabel = "N", ylabel = "iters")
        return [p1,p2,p3,p4]
    else
        p1 = plot(tmax[1+min:end-max,1],tmax[1+min:end-max,2:3],title = "Max time", label=["Hard" "Soft"], xlabel = "N", ylabel = "ms", yscale = :log10)
        p2 = plot(tmean[1+min:end-max,1],tmean[1+min:end-max,2:3],title = "Mean time", label=["Hard" "Soft"], xlabel = "N", ylabel = "ms", yscale = :log10)
        p3 = plot(titermax[1+min:end-max,1],titermax[1+min:end-max,2:3],title = "Max time", label=["Hard" "Soft"], xlabel = "N", ylabel = "iters")
        p4 = plot(titermean[1+min:end-max,1],titermean[1+min:end-max,2:3],title = "Mean iters", label=["Hard" "Soft"], xlabel = "N", ylabel = "iters")
        return [p1,p2,p3,p4]
    end
end

"""

plot_evolution(x::Integer=1, output::Integer=0)


Choose x = {1,2,3}

Choose output = {0,1}


Returns the evolution of inputs, positions and velocities for
the problem chosen in the script.


For output = 0, returns the plots of variables vs time.

For output = 1, returns the data in form of matrices.
"""
function plot_evolution(x::Integer = 1, output::Integer = 0)
    cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
    if x == 1
        U = readcsv("hard_input_evol.csv")
        X = readcsv("hard_state_evol.csv")
        V = readcsv("hard_speed_evol.csv")
        preamble = "Hard "
    elseif x==2
        U = readcsv("soft_input_evol.csv")
        X = readcsv("soft_state_evol.csv")
        V = readcsv("soft_speed_evol.csv")
        preamble = "Soft "
    end
    p1 = plot(U[2:end,1],U[2:end,2:end],label=["u1" "u2" "u3" "u4" "u5"], ylabel = "u", title = string(preamble,"Inputs"))
    p2 = plot(X[2:end,1],X[2:end,2:end],label=["x1" "x2" "x3" "x4" "x5" "x6"], ylabel = "x", title = string(preamble,"Positions"))
    p3 = plot(V[2:end,1],V[2:end,2:end],label=["v1" "v2" "v3" "v4" "v5" "v6"], ylabel = "x", title = string("Velocities"))
    cd("/home/add/Desktop/MPC-SM/testbench")
    if output==0
        return [p1,p2,p3]
    else
        return [U,X,V]
    end
end

h = plot_evolution()
s = plot_evolution(2)
p2 = plot(h[2],s[2],layout=(2,1))
p3 = plot(h[3],s[3],layout=(2,1))
p1 = plot(h[1],s[1],layout=(2,1))
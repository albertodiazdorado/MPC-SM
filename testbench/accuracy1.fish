#!/bin/fish
set --local foldername SoftAccuracy

# First argument is the number of simulation steps
# Second argument is the seed (please give 10)
set argc (count $argv)
set seed
if test $argc -gt 1
  set seed (math "$argv[2]")
else
  set seed 10
end
if math "$argc>0" > /dev/null
  set steps (math "$argv[1]")
else
  set steps 20
end
set sim 0
echo "Simulation length: " $steps " time steps."
echo "Number of runs per simulation: 1 time. (Accuracy measurement)."
echo "Using seed = $seed"

set n (seq 4 20)
set m (seq 2 11)
set T (seq 10 10 100)

echo \n\n"ITERATE OVER THE STATE SIZE."
set idx 1
while test $idx -le (count $n)
  cd /home/add/Desktop/MPC-SM/$foldername/pn$n[$idx]
  /home/add/Desktop/MPC-SM/main.out 'pn'$n[$idx] $sim $steps $seed | egrep "Academic License" > /dev/null
  echo \t"Progress: " (math "100*$idx/"(count $n)) "%"
  set idx (math "$idx+1")
end

# echo \n\n"ITERATE OVER THE INPUT SIZE."
# set idx 1
# while test $idx -le (count $m)
#   cd /home/add/Desktop/MPC-SM/Accuracy/pm$m[$idx]
#   /home/add/Desktop/MPC-SM/main.out 'pm'$m[$idx] $sim $steps $seed | egrep "Academic License" > /dev/null
#   echo \t"Progress: " (math "100*$idx/"(count $m)) "%"
#   set idx (math "$idx+1")
# end

# echo \n\n"ITERATE OVER THE HORIZON LENGTH."
# set idx 1
# while test $idx -le (count $T)
#   cd /home/add/Desktop/MPC-SM/Accuracy/pT$T[$idx]
#   /home/add/Desktop/MPC-SM/main.out 'pT'$T[$idx] $sim $steps $seed | egrep "Academic License" > /dev/null
#   echo \t"Progress: " (math "100*$idx/"(count $T)) "%"
#   set idx (math "$idx+1")
# end
# Create ranges
n = 4:20
m = 2:11
T = 10:10:100

## Uncomment the work folder
# folder = "Problems"
folder = "SoftProblems"

# Number of simulations per case (adjust this parameter accordingly)
if !isdefined(:N)
    N = 50
end

# Analyze all the files regarding state size
n1 = hcat(n, zeros(length(n), 3))
n2 = hcat(n, zeros(length(n), 3))
n3 = hcat(n, zeros(length(n), 3))
n4 = hcat(n, zeros(length(n), 2))
n5 = hcat(n, zeros(length(n), 2))
n6 = hcat(n, zeros(length(n), 2))
nvar = hcat(n, zeros(length(n),3))
idx = 1
for i in n
    cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
    cd(string("pn", i))
    # Analyze the times
    r = readcsv(string("time1.csv"))
    mehrotra = r[:,1]
    qpoases = r[:,2]
    cputime = r[:,3]
    iter_m = r[:,4]
    iter_qp = r[:,5]
    diff1 = r[:,6] - r[:,7]
    diff2 = -diff1
    for sim in 2:N
        r = readcsv(string("time", sim, ".csv"))
        mehrotra = hcat(mehrotra, r[:,1])
        qpoases = hcat(qpoases, r[:,2])
        cputime = hcat(cputime, r[:,3])
        iter_m = hcat(iter_m, r[:,4])
        iter_qp = hcat(iter_qp, r[:,5])
    end
    mt = minimum(mehrotra, 2)
    qt = minimum(qpoases, 2)
    ct = minimum(cputime, 2)
    im = minimum(iter_m, 2)
    iq = minimum(iter_qp, 2)
    n1[idx,2:4] = hcat(maximum(mt), maximum(qt), maximum(ct))
    n2[idx,2:4] = hcat(median(mt), median(qt), median(ct))
    n3[idx,2:4] = hcat(mean(mt), mean(qt), mean(ct))
    n4[idx,2:3] = hcat(maximum(im), maximum(iq))
    n5[idx,2:3] = hcat(mean(im), mean(iq))
    n6[idx,2:3] = hcat(maximum(diff1), maximum(diff2))
    nvar[idx,2:4] = hcat(var(mt),var(qt),var(ct))
    idx = idx + 1
end

# Analyze all the files regarding input size
m1 = hcat(m, zeros(length(m), 3))
m2 = hcat(m, zeros(length(m), 3))
m3 = hcat(m, zeros(length(m), 3))
m4 = hcat(m, zeros(length(m), 2))
m5 = hcat(m, zeros(length(m), 2))
m6 = hcat(m, zeros(length(m), 2))
mvar = hcat(m, zeros(length(m),3))
idx = 1
for i in m
    cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
    cd(string("pm", i))
    # Analyze the times
    r = readcsv(string("time1.csv"))
    mehrotra = r[:,1]
    qpoases = r[:,2]
    cputime = r[:,3]
    iter_m = r[:,4]
    iter_qp = r[:,5]
    diff1 = r[:,6] - r[:,7]
    diff2 = -diff1
    for sim in 2:N
        r = readcsv(string("time", sim, ".csv"))
        mehrotra = hcat(mehrotra, r[:,1])
        qpoases = hcat(qpoases, r[:,2])
        cputime = hcat(cputime, r[:,3])
        iter_m = hcat(iter_m, r[:,4])
        iter_qp = hcat(iter_qp, r[:,5])
    end
    mt = minimum(mehrotra, 2)
    qt = minimum(qpoases, 2)
    ct = minimum(cputime, 2)
    im = minimum(iter_m, 2)
    iq = minimum(iter_qp, 2)
    m1[idx,2:4] = hcat(maximum(mt), maximum(qt), maximum(ct))
    m2[idx,2:4] = hcat(median(mt), median(qt), median(ct))
    m3[idx,2:4] = hcat(mean(mt), mean(qt), mean(ct))
    m4[idx,2:3] = hcat(maximum(im), maximum(iq))
    m5[idx,2:3] = hcat(mean(im), mean(iq))
    m6[idx,2:3] = hcat(maximum(diff1), maximum(diff2))
    mvar[idx,2:4] = hcat(var(mt),var(qt),var(ct))
    idx = idx + 1
end


# Analyze all the files regarding prediction horizon length
t1 = hcat(T, zeros(length(T), 3))
t2 = hcat(T, zeros(length(T), 3))
t3 = hcat(T, zeros(length(T), 3))
t4 = hcat(T, zeros(length(T), 2))
t5 = hcat(T, zeros(length(T), 2))
t6 = hcat(T, zeros(length(T), 2))
tvar = hcat(T, zeros(length(T),3))
idx = 1;
for i in T
    cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
    cd(string("pT", i))
    # Analyze the times
    r = readcsv(string("time1.csv"))
    mehrotra = r[:,1]
    qpoases = r[:,2]
    cputime = r[:,3]
    iter_m = r[:,4]
    iter_qp = r[:,5]
    diff1 = r[:,6] - r[:,7]
    diff2 = -diff1
    for sim in 2:N
        r = readcsv(string("time", sim, ".csv"))
        mehrotra = hcat(mehrotra, r[:,1])
        qpoases = hcat(qpoases, r[:,2])
        cputime = hcat(cputime, r[:,3])
        iter_m = hcat(iter_m, r[:,4])
        iter_qp = hcat(iter_qp, r[:,5])
    end
    mt = minimum(mehrotra, 2)
    qt = minimum(qpoases, 2)
    ct = minimum(cputime, 2)
    im = minimum(iter_m, 2)
    iq = minimum(iter_qp, 2)
    t1[idx,2:4] = hcat(maximum(mt), maximum(qt), maximum(ct))
    t2[idx,2:4] = hcat(median(mt), median(qt), median(ct))
    t3[idx,2:4] = hcat(mean(mt), mean(qt), mean(ct))
    t4[idx,2:3] = hcat(maximum(im), maximum(iq))
    t5[idx,2:3] = hcat(mean(im), mean(iq))
    t6[idx,2:3] = hcat(maximum(diff1), maximum(diff2))
    tvar[idx,2:4] = hcat(var(mt),var(qt),var(ct))
    idx = idx + 1
end

legend1 = ["n" "mehrotra" "qpoases" "cputime"]
legend2 = ["m" "mehrotra" "qpoases" "cputime"]
legend3 = ["T" "mehrotra" "qpoases" "cputime"]

cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
writecsv("nmin.csv", vcat(legend1, n1))
writecsv("nmedian.csv", vcat(legend1, n2))
writecsv("nmean.csv", vcat(legend1, n3))
writecsv("mmin.csv", vcat(legend2, m1))
writecsv("mmedian.csv", vcat(legend2, m2))
writecsv("mmean.csv", vcat(legend2, m3))
writecsv("tmin.csv", vcat(legend3, t1))
writecsv("tmedian.csv", vcat(legend3, t2))
writecsv("tmean.csv", vcat(legend3, t3))
writecsv("n_variance.csv",vcat(legend1,nvar))
writecsv("m_variance.csv",vcat(legend2,mvar))
writecsv("t_variance.csv",vcat(legend3,tvar))

legend1 = ["n" "mehrotra" "qpoases"]
legend2 = ["m" "mehrotra" "qpoases"]
legend3 = ["T" "mehrotra" "qpoases"]
writecsv("n_max_iter.csv", vcat(legend1, n4))
writecsv("n_mean_iter.csv", vcat(legend1, n5))
writecsv("m_max_iter.csv", vcat(legend2, m4))
writecsv("m_mean_iter.csv", vcat(legend2, m5))
writecsv("t_max_iter.csv", vcat(legend3, t4))
writecsv("t_mean_iter.csv", vcat(legend3, t5))

legend = ["max_mehrotra" "max_qpoases"]
writecsv("n_diff.csv", vcat(hcat("n",legend), n6))
writecsv("m_diff.csv", vcat(hcat("m",legend), m6))
writecsv("t_diff.csv", vcat(hcat("T",legend), t6))


cd("/home/add/Desktop/MPC-SM/testbench")

using Plots
plotly()

function plot_results(x::Integer = 1, min::Integer = 0, max::Integer = 0)
    if x == 1
        p1 = plot(n[1+min:end-max],n1[1+min:end-max,2:4],title = "Max time", label=["mehrotra" "qpoases" "cputime"], xlabel = "N", ylabel = "ms", yscale = :log10)
        p2 = plot(n[1+min:end-max],n2[1+min:end-max,2:4],title = "Median time", label=["mehrotra" "qpoases" "cputime"], xlabel = "N", ylabel = "ms", yscale = :log10)
        p3 = plot(n[1+min:end-max],n3[1+min:end-max,2:4],title = "Mean time", label=["mehrotra" "qpoases" "cputime"], xlabel = "N", ylabel = "ms", yscale = :log10)
        p4 = plot(n[1+min:end-max],n4[1+min:end-max,2:3],title = "Max iters", label=["mehrotra" "qpoases" "cputime"], xlabel = "N", ylabel = "iters"  )
        p5 = plot(n[1+min:end-max],n5[1+min:end-max,2:3],title = "Mean iters", label=["mehrotra" "qpoases" "cputime"], xlabel = "N", ylabel = "iters"  )
        p6 = plot(n[1+min:end-max],n6[1+min:end-max,2:3],title = "Solution accuracy", label=["Mehrotra - qpOASES" "qpOASES - Mehrotra"], xlabel = "N")
        return [p1,p2,p3,p4,p5,p6]
    elseif x == 2
        p1 = plot(m[1+min:end-max],m1[1+min:end-max,2:4],title = "Max time", label=["mehrotra" "qpoases" "cputime"], xlabel = "m", ylabel = "ms", yscale = :log10)
        p2 = plot(m[1+min:end-max],m2[1+min:end-max,2:4],title = "Median time", label=["mehrotra" "qpoases" "cputime"], xlabel = "m", ylabel = "ms", yscale = :log10  )
        p3 = plot(m[1+min:end-max],m3[1+min:end-max,2:4],title = "Mean time", label=["mehrotra" "qpoases" "cputime"], xlabel = "m", ylabel = "ms", yscale = :log10  )
        p4 = plot(m[1+min:end-max],m4[1+min:end-max,2:3],title = "Max iters", label=["mehrotra" "qpoases" "cputime"], xlabel = "m", ylabel = "iters"  )
        p5 = plot(m[1+min:end-max],m5[1+min:end-max,2:3],title = "Mean iters", label=["mehrotra" "qpoases" "cputime"], xlabel = "m", ylabel = "iters"  )
        p6 = plot(m[1+min:end-max],m6[1+min:end-max,2:3],title = "Solution accuracy", label=["Mehrotra - qpOASES" "qpOASES - Mehrotra"], xlabel = "m")
        return [p1,p2,p3,p4,p5,p6]
    else
        p1 = plot(T[1+min:end-max],t1[1+min:end-max,2:4],title = "Max time", label=["mehrotra" "qpoases" "cputime"], xlabel = "T", ylabel = "ms", yscale = :log10)
        p2 = plot(T[1+min:end-max],t2[1+min:end-max,2:4],title = "Median time", label=["mehrotra" "qpoases" "cputime"], xlabel = "T", ylabel = "ms", yscale = :log10)
        p3 = plot(T[1+min:end-max],t3[1+min:end-max,2:4],title = "Mean time", label=["mehrotra" "qpoases" "cputime"], xlabel = "T", ylabel = "ms", yscale = :log10)
        p4 = plot(T[1+min:end-max],t4[1+min:end-max,2:3],title = "Max iters" ,label=["mehrotra" "qpoases" "cputime"], xlabel = "T", ylabel = "iters")
        p5 = plot(T[1+min:end-max],t5[1+min:end-max,2:3],title = "Mean iters", label=["mehrotra" "qpoases" "cputime"], xlabel = "T", ylabel = "iters")
        p6 = plot(T[1+min:end-max],t6[1+min:end-max,2:3],title = "Solution accuracy", label=["Mehrotra - qpOASES" "qpOASES - Mehrotra"], xlabel = "T")
        return [p1,p2,p3,p4,p5,p6]
    end
end

function plot_evolution(x::Integer = 1, y::Integer = 4)
    cd(string("/home/add/Desktop/MPC-SM/",folder,"/"))
    if x == 1
        cd(string("pn",y))
        e = "pn"
    elseif x == 2
        cd(string("pm",y))
        e = "pm"
    else
        cd(string("pT", y))
        e = "pT"
    end
    r = readcsv(string("time1.csv"))
    y1 = r[:,6]
    y2 = r[:,7]
    x = 1:length(y1)
    return plot(x,hcat(y1,y2),label = ["mehrotra" "qpOASES"], xlabel = "Iteration", ylabel = "f(x*)")
end
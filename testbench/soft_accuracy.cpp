/* Here, we run the three solvers with soft constraints
 * and full disturbances. We are not interested in timing, 
 * but in accuracy as compared against Gurobi */


// Include the qpOASES.hpp header
// It is located in ~/qpoases/include
// Please, do not forget to tell the compiler where to look for include files
constexpr bool ENABLE_DISTURBANCE = true;
constexpr bool PRINT_CSV = true;
constexpr bool PRINT_INPUT = true;

// Add the option "-I /home/add/qpoases/include/"
// Add the option "-lqpOASES -L /home/add/qpoases/lib"
#include "gurobi_c++.h"
#include "qpOASES/QProblem.hpp"

// Include additional files that you are going to use
#include <iostream>
#include <memory>

// Add your solver
#include "constraints/InputCons.cpp"
#include "constraints/SC_Unconstrained.cpp"
#include "constraints/SoftCons.cpp"
#include "detail/extra.cpp"
#include "solver/Mehrotra.cpp"

bool dense_optimize(GRBEnv *env, int rows, int cols,
                    double *c,   /* linear portion of objective function */
                    double *Q,   /* quadratic portion of objective function */
                    double *A,   /* constraint matrix */
                    char *sense, /* constraint senses */
                    double *rhs, /* RHS vector */
                    double *lb,  /* variable lower bounds */
                    double *ub,  /* variable upper bounds */
                    char *vtype, /* variable types (continuous, binary, etc.) */
                    double *solution, double *objvalP);

template <class Container1, class Container2>
Container2 &copy_helper(Container1 &&from, Container2 &to) {
  std::copy(std::begin(from), std::end(from), std::begin(to));
  return to;
}

// Include namespace
using namespace mpc;

// Write the helpers that let you call Mehrotra sovler
template <class real_t, class int_t>
using DiagMat = mpc::matrix::DiagMat<real_t, int_t>;
template <class real_t, class int_t>
using MyInputCons =
    mpc::InputCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat>;
template <class real_t, class int_t>
using MyStateCons =
    mpc::StateCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat,
                                 mpc::matrix::DiagMat>;
template <class real_t, class int_t>
using SoftStateCons =
    mpc::StateCons::Unconstrained<real_t, int_t, DiagMat, DiagMat,
                                  mpc::TerminalCons::SameCons,
                                  mpc::SoftCons::SimpleBounds>;
using SoftMehrotra = mpc::solver::Mehrotra<double, int, DiagMat, DiagMat,
                                           DiagMat, MyInputCons, SoftStateCons>;

/* ARGUMENT LIST:
 *
 * FIRST ARGUMENT:   Problem instance (defaults to p0)
 * SECOND ARGUMENT:  Number of simulation
 * THIRD ARGUMENT:   Number of simulation steps
 * FORUTH ARGUMENT:  Seed for the random disturbance (defaults to 11)
 */
int main(int argc, char *argv[]) {
  /* Read the input to the program */
  std::string problem_instance{"p0"};
  size_t seed{10}, sim_steps{10};
  std::string sim_number{'0'};
  switch (argc) {
  case 5:
    seed = std::atol(argv[4]);
  case 4:
    sim_steps = std::atol(argv[3]);
  case 3:
    sim_number = argv[2];
  case 2: {
    problem_instance = argv[1];
    break;
  }
  default:
    break;
  }

  /* Read the MPC and the QP problems */
  std::string mpc_address = "/home/add/Desktop/MPC-SM/SoftAccuracy/";
  std::string qp_address{mpc_address};
  mpc_address = mpc_address + problem_instance + "/mpc.bin";
  qp_address = qp_address + problem_instance + "/qpsoft.bin";
  extra::mpc_system<double, size_t> sim;
  extra::parse_system(mpc_address, sim);
  extra::qp_system<double, size_t> QP;
  extra::parse_system(qp_address, QP);

  /* Create the mpcdata object */
  solver::mpcdata<double> MPC(sim.nx, sim.nu, sim.T, sim.A, sim.B, sim.R, sim.Q,
                              sim.P);

  MPC.lbu = sim.lbu;
  MPC.ubu = sim.ubu;
  MPC.slbx = MPC.slbt = sim.lbx;
  MPC.subx = MPC.subt = sim.ubx;

  /* Create some initial state */
  std::vector<double> x0(MPC.n);

  /* Create disturbance for some deterministic number */
  extra::disturbance<double> DIS(MPC.n / 2, MPC.lbu[0], MPC.ubu[0], seed);

  // *********************************************************************** //
  // ******************************* MEHROTRA ****************************** //
  // *********************************************************************** //
  mpc::solver::mpcoptions<double> MyOptions;
  MyOptions.eta = 0.99;
  MyOptions.optimality_tolerance = 1e-3;
  MyOptions.feasibility_tolerance = 1e-3;
  SoftMehrotra FastMPC(MPC, x0);
  FastMPC.set_options(MyOptions);

  // *********************************************************************** //
  // ****************************** qpOASES ******************************** //
  // *********************************************************************** //
  qpOASES::QProblem OASES(QP.get_vars(), QP.get_cons());
  qpOASES::Options myOasesOptions;
  myOasesOptions.setToMPC();
  myOasesOptions.printLevel = qpOASES::PL_NONE;
  OASES.setOptions(myOasesOptions);

  // *********************************************************************** //
  // ******************************* Gurobi ******************************** //
  // *********************************************************************** //
  GRBEnv env;
  env.set(GRB_IntParam_OutputFlag, 0);
  std::vector<char> sense(QP.k, '<');

  // *********************************************************************** //
  // ************************** Simulation loop **************************** //
  // *********************************************************************** //
  /* Simulation parameters */
  std::vector<qpOASES::int_t> nWSR(sim_steps, 1000);
  std::vector<size_t> iter(sim_steps);
  std::vector<double> mehrotra_val(sim_steps), qpOases_val(sim_steps),
      gurobi_val(sim_steps);
  std::vector<double> mehrotra_input(sim_steps * MPC.m),
      qpOases_input(sim_steps * MPC.m), gurobi_input(sim_steps * MPC.m);
  std::vector<double> temp(QP.n);
  std::vector<bool> status(sim_steps);

  /* Initial step: qpOASES */
  OASES.init(QP.get_hessian(), QP.get_soft_gradient(x0.data()),
             QP.get_constraints(), QP.get_lb(), QP.get_ub(), QP.get_lhs(),
             QP.get_rhs(x0.data()), nWSR[0]);
  OASES.getPrimalSolution(temp.data());
  std::copy(temp.cbegin(), temp.cbegin() + MPC.m, qpOases_input.begin());
  qpOases_val[0] = OASES.getObjVal() + QP.get_residual(x0.data());
  status[0] = OASES.isSolved();

  /* Initial step: Mehrotra */
  FastMPC.solve();
  iter[0] = FastMPC.get_k();
  copy_helper(FastMPC.get_opt_input(), temp);
  std::copy(temp.cbegin(), temp.cbegin() + MPC.m, mehrotra_input.begin());
  mehrotra_val[0] = FastMPC.get_opt_val();

  /* Initial step: Gurobi */
  dense_optimize(&env, QP.k, QP.n, QP.get_soft_gradient(x0.data()),
                 QP.get_half_hessian(), QP.get_constraints_transpose(),
                 sense.data(), QP.get_rhs(x0.data()), QP.get_lb(), QP.get_ub(),
                 nullptr, temp.data(), &gurobi_val[0]);
  std::copy(temp.cbegin(), temp.cbegin() + MPC.m, gurobi_input.begin());
  gurobi_val[0] += QP.get_residual(x0.data());

  /* Initial step: Simulation */
  sim.set_x0(x0.data());
  if (ENABLE_DISTURBANCE)
    sim.simulate(FastMPC.get_opt_input().data(), DIS.get_disturbance());
  else
    sim.simulate(FastMPC.get_opt_input().data());

  /* Simulation loop */
  for (size_t step = 1; step < sim_steps; step++) {
    /* qpOASES with hotstart */
    OASES.hotstart(QP.get_soft_gradient(sim.get_x0()), QP.get_lb(), QP.get_ub(),
                   QP.get_lhs(), QP.get_rhs(sim.get_x0()), nWSR[step]);
    OASES.getPrimalSolution(temp.data());
    std::copy(temp.cbegin(), temp.cbegin() + MPC.m,
              qpOases_input.begin() + step * MPC.m);
    qpOases_val[step] = OASES.getObjVal() + QP.get_residual(sim.get_x0());
    status[step] = OASES.isSolved();

    /* Mehrotra with hotstart */
    FastMPC.solve(sim.get_x0());
    iter[step] = FastMPC.get_k();
    copy_helper(FastMPC.get_opt_input(), temp);
    std::copy(temp.cbegin(), temp.cbegin() + MPC.m,
              mehrotra_input.begin() + step * MPC.m);
    mehrotra_val[step] = FastMPC.get_opt_val();

    /* Gurobi */
    dense_optimize(&env, QP.k, QP.n, QP.get_soft_gradient(sim.get_x0()),
                   QP.get_half_hessian(), QP.get_constraints_transpose(),
                   sense.data(), QP.get_rhs(sim.get_x0()), QP.get_lb(),
                   QP.get_ub(), NULL, temp.data(), &gurobi_val[step]);
    std::copy(temp.cbegin(), temp.cbegin() + MPC.m,
              gurobi_input.begin() + step * MPC.m);
    gurobi_val[step] += QP.get_residual(sim.get_x0());

    /* Simulation */
    if (ENABLE_DISTURBANCE)
      sim.simulate(FastMPC.get_opt_input().data(), DIS.get_disturbance());
    else
      sim.simulate(FastMPC.get_opt_input().data());
  }

  // ******************************************************************** //
  // **************************** Print to CSV ************************** //
  // ******************************************************************** //
  if (PRINT_CSV) {
    std::string filename{"/home/add/Desktop/MPC-SM/SoftAccuracy/"};
    filename += problem_instance + '/' + "objval" + ".csv";
    extra::writecsv(filename, mehrotra_val.begin(), mehrotra_val.end(),
                    qpOases_val.begin(), gurobi_val.begin(), status.begin(),
                    iter.begin(), nWSR.begin());
    if (PRINT_INPUT) {
      filename = "/home/add/Desktop/MPC-SM/SoftAccuracy/" + problem_instance +
                 '/' + "inputval" + ".csv";
      extra::writecsv(filename, mehrotra_input.begin(), mehrotra_input.end(),
                      qpOases_input.begin(), gurobi_input.begin());
    }
  }
  if (*std::max_element(iter.begin(), iter.end()) == 400 &&
      *std::max_element(nWSR.begin(), nWSR.end()) == 1000)
    return 3;
  if (*std::max_element(iter.begin(), iter.end()) == 400)
    return 1;
  if (*std::max_element(nWSR.begin(), nWSR.end()) == 1000)
    return 2;
  return 0;
}

// template <class GRBVar> void gurobideleter(GRBVar *ptr) { delete[] ptr; }

bool dense_optimize(GRBEnv *env, int rows, int cols,
                    double *c,   /* linear portion of objective function */
                    double *Q,   /* quadratic portion of objective function */
                    double *A,   /* constraint matrix */
                    char *sense, /* constraint senses */
                    double *rhs, /* RHS vector */
                    double *lb,  /* variable lower bounds */
                    double *ub,  /* variable upper bounds */
                    char *vtype, /* variable types (continuous, binary, etc.) */
                    double *solution, double *objvalP) {
  GRBModel model = GRBModel(*env);
  int i, j;
  bool success = false;

  /* Add variables to the model */

  std::unique_ptr<GRBVar[]> vars{
      model.addVars(lb, ub, nullptr, vtype, nullptr, cols)};

  /* Populate A matrix */

  for (i = 0; i < rows; i++) {
    GRBLinExpr lhs = 0;
    for (j = 0; j < cols; j++)
      if (A[i * cols + j] != 0)
        lhs += A[i * cols + j] * vars.get()[j];
    model.addConstr(lhs, sense[i], rhs[i]);
  }

  GRBQuadExpr obj;

  for (j = 0; j < cols; j++)
    obj += c[j] * vars[j];
  for (i = 0; i < cols; i++)
    for (j = 0; j < cols; j++)
      if (Q[i * cols + j] != 0)
        obj += Q[i * cols + j] * vars.get()[i] * vars.get()[j];

  model.setObjective(obj);

  model.optimize();

  if (model.get(GRB_IntAttr_Status) == GRB_OPTIMAL) {
    *objvalP = model.get(GRB_DoubleAttr_ObjVal);
    for (i = 0; i < cols; i++)
      solution[i] = vars.get()[i].get(GRB_DoubleAttr_X);
    success = true;
  }

  return success;
}
#!/bin/fish
set argc (count $argv)
if math "$argc>0" > /dev/null
  set N (math "$argv[1]+1")
else
  set N 100
end

set n 4 6 10
set profile 1 2 3
set T 10 20 40

set size "Small" "Medium" "Large"
set inputs "Normal" "Extended" "Minimalist"
set length "Short" "Long" "Longest"

echo "Start to roll."

set i 1
while math "$i<="(count $n) > /dev/null
  set j 1
  while math "$j<="(count $T) > /dev/null
    set k 1
    while math "$k<="(count $profile) > /dev/null
      echo ( math "(100*($i-1))/"(count $n)"+(100/"(count $n)")*($j-1)/"(count $T)"+(100/("(count $n)"+"(count $T)"))*($k-1)/"(count $profile) )"% completed"
      set sim 1
      cd /home/add/Desktop/MPC-SM/Figures/Diagnosis/
      if test ! -d $size[$i]'-'$inputs[$k]'-'$length[$j]
        mkdir $size[$i]'-'$inputs[$k]'-'$length[$j]
      end
      cd $size[$i]'-'$inputs[$k]'-'$length[$j]
      while math "$sim<$N" > /dev/null
        /home/add/Desktop/MPC-SM/simulation.out 'Figures/Diagnosis/'$size[$i]'-'$inputs[$k]'-'$length[$j]'/sim'$sim $n[$i] $T[$j] $profile[$k]
        set sim (math "$sim+1")
      end
      set k (math "$k+1") 
    end
    set j (math "$j+1")
  end
  set i (math "$i+1")
end




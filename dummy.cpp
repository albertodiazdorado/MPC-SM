#include <iostream>

struct Base {
  static int addition(int x, int y) { return x + y; }
};

struct Integer : Base {
  static int division(int x, int y) { return x / y; }
};

struct Rational : Base {
  static double division(int x, int y) { return double{x} / double{y}; }
};

int main() {
  std::cout << "\n5 + 2: " << Integer::addition(5, 2) << std::endl;
  std::cout << "\n5 / 2: " << Integer::division(5, 2) << std::endl;
  std::cout << "\n5 + 2: " << Rational::addition(5, 2) << std::endl;
  std::cout << "\n5 / 2: " << Rational::division(5, 2) << std::endl;
  return 0;
}

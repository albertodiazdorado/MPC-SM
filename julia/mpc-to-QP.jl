mutable struct mpcdata 
    n::Integer
    m::Integer
    T::Integer
    A::AbstractArray
    B::AbstractArray
    R::AbstractArray
    Q::AbstractArray
    P::AbstractArray
    lbu::AbstractArray
    ubu::AbstractArray
    Fu::AbstractArray
    fu::AbstractArray
    lbx::AbstractArray
    ubx::AbstractArray
    Fx::AbstractArray
    fx::AbstractArray
    lbt::AbstractArray
    ubt::AbstractArray
    Ft::AbstractArray
    ft::AbstractArray
end

mpcdata() = mpcdata(0, 0, 0, [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [])
mpcdata(n::Integer, m::Integer) = mpcdata(n, m, 0, [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [])
mpcdata(n::Integer, m::Integer, T::Integer) = mpcdata(n, m, T, [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [])
mpcdata(n::Integer, m::Integer, T::Integer, A::AbstractArray, B::AbstractArray) = mpcdata(n, m, T, A, B, [], [], [], [], [], [], [], [], [], [], [], [], [], [], [])
mpcdata(n::Integer, m::Integer, T::Integer, A::AbstractArray, B::AbstractArray, R::AbstractArray, Q::AbstractArray, P::AbstractArray) = mpcdata(n, m, T, A, B, R, Q, P, [], [], [], [], [], [], [], [], [], [], [], [])

struct denseQP
    n::Integer
    k::Integer
    H::AbstractArray
    h::AbstractArray
    ub::AbstractArray
    lb::AbstractArray
    F::AbstractArray
    f::AbstractArray
    residual::Real
end

function mpc2denseQP(mpc::mpcdata, x0::AbstractArray)
  # Get the data
    n = mpc.n
    m = mpc.m
    T = mpc.T

  # Build natural and controllability matrices
    A = mpc.A;
    for idx in 2:T
        A = vcat(A, (mpc.A)^idx)    
    end
    B = hcat(mpc.B, zeros(n, m * (T - 1)))
    for row in 2:T
        line = (mpc.A^(row - 1)) * mpc.B
        for col in 2:row - 1
            line = hcat(line, ((mpc.A)^(row - col)) * mpc.B)
        end
        line = hcat(line, mpc.B, zeros(n, (T - row) * m))
        B = vcat(B, line);
    end

  # Build Hessian and gradient
    H = zeros(T * m, T * m)
    Q = zeros(T * n, T * n)
    R = zeros(T * m, T * m)

    for idx in 0:T - 1
        Q[1 + idx * n:(idx + 1) * n, 1 + idx * n:(idx + 1) * n] = mpc.Q
        R[1 + idx * m:(idx + 1) * m, 1 + idx * m:(idx + 1) * m] = mpc.R
    end

    Q[end - n + 1:end,end - n + 1:end] = mpc.P
    R[end - m + 1:end,end - m + 1:end] = mpc.R

    H = B' * Q * B + R;
    h = B' * Q * A * x0;

  # Build simple bounds
    if (length(mpc.ubu) > 0)
        ubu = mpc.ubu;
        for idx in 2:T
            ubu = vcat(ubu, mpc.ubu)
        end
    end
    if (length(mpc.lbu) > 0)
        lbu = mpc.lbu;
        for idx in 2:T
            lbu = vcat(lbu, mpc.lbu)
        end
    end

  # Build linear constraints
    F = zeros(0, T * m)
    f = zeros(0);

    kfu = length(mpc.fu)
    if kfu > 0
        Fu = zeros(T * kfu, T * m)
        fu = []
        for idx in 0:T
            Fu[1 + idx * kfu:idx * kfu + kfu, 1 + idx * m:(idx + 1) * m] = mpc.Fu;
            fu = vcat(fu, mpc.fu);
        end
        F = vcat(F, Fu);
        f = vcat(f, fu);
    end

    kubx = length(mpc.ubx)
    klbx = length(mpc.lbx)

    if kubx > 0
        ubx = zeros(T * kubx)
        for idx in 0:T - 1
            ubx[1 + idx * kubx:(idx + 1) * kubx] = mpc.ubx;
        end
        ubx[end - kubx + 1:end] = mpc.ubt;
        F = vcat(F, B);
        f = vcat(f, ubx - A * x0);
    end

    if klbx > 0
        lbx = zeros(T * klbx)
        for idx in 0:T - 1
            lbx[1 + idx * klbx:(idx + 1) * klbx] = mpc.lbx;
        end
        lbx[end - klbx + 1:end] = mpc.lbt;
        F = vcat(F, -B);
        f = vcat(f, -lbx + A * x0);
    end


    kfx = length(mpc.fx)
    kft = length(mpc.ft)
    kfx = (T - 1) * kfx + kft
    if (kfx > 0)
        Fx = zeros(kfx, T * n)
        fx = zeros(kfx)
        for idx in 0:T - 1
            Fx[1 + idx * kfx:idx * kfx + kfx,1 + idx * n:(idx + 1) * n] = mpc.Fx
            fx[1 + idx * kfx:(idx + 1) * kfx] = mpc.fx;
        end
        Fx[end - kft + 1:end,end - n:end] = mpc.Ft;
        fx[end - kfx + 1:end] = mpc.ft;
        F = vcat(F, Fx * B);
        f = vcat(f, fx - Fx * A * x0);
    end
    
    residual = 0.5 * x0' * A' * Q * A * x0;

    return denseQP(T * m, length(f), H, h, ubu, lbu, F, f, residual);
end
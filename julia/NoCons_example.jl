## Give values
A = [1 2;1 0.]'
B = [1;0.]
R = diagm(0.8);
Q = diagm([1,2.]);
P = diagm([4,3.]);
x0 = [5,5.];
n = 2;
m = 1;
T = 10;
x = zeros(T * (n + m));
nu = zeros(T * n);
z = [true,false,false];
choper = [true,false,false];
for i in 2:T
    choper = vcat(choper, z)
end


## Create KKT matrix
## Create H
diagonal = vcat(diag(R), diag(Q));
for i in 2:T - 1
    diagonal = vcat(diagonal, diag(R), diag(Q));
end
diagonal = vcat(diagonal, diag(R), diag(P));
H = diagm(diagonal);

## Create C
C = [B -eye(n, n) zeros(n, (T - 1) * (n + m));
      zeros(n, m) A B -eye(n, n) zeros(n, (n + m) * (T - 2))]
for i in 3:T - 1
    C = [C;zeros(n, m) zeros(n, (n + m) * (i - 2)) A B -eye(n, n) zeros(n, (n + m) * (T - i))]
end
C = [C;zeros(n, m) zeros(n, (n + m) * (T - 2)) A B -eye(n, n)]

## Create KKT
K = [H C';C zeros(T * n, T * n)];

## Create right hand side
# Create d
d = vcat(-A * x0, zeros(n * (T - 1)));

# Create residuals
rc = H * x + C' * nu;
re = C * x - d;
rhs = vcat(-rc, -re);

## Solve
sol = K \ rhs;
sol1 = sol[1:T * (n + m)];
nu_aff = sol[T * (n + m) + 1:end];
u_aff = sol1[choper];
x_aff = sol1[.!(choper)];

## Solve in the other way
Shur = C * inv(H) * C';
rhs2 = re + C * inv(H) * rc;
nu_aff_2 = Shur \ rhs2;
sol2 = -H \ (rc + C' * nu_aff_2);
u_aff_2 = sol2[choper];
x_aff_2 = sol2[.!(choper)];








ENABLE_GUROBI = true;

if (!ENABLE_GUROBI)
    workspace();
    ENABLE_GUROBI = false;
end

include("mpc-to-QP.jl")

n = 6;
m = 3;
Ts = 0.5;
T = 60;

I = zeros(n, n + 1);
for i in 1:n
    I[i,i] = 1;
    I[i,i + 1] = -1;
end
K = I * diagm(ones(n + 1)) * I'
U = [1 0 0;-1 0 0;0 1 0;0 0 1;0 -1 0;0 0 -1]

Ac = [zeros(n, n) eye(n); -K zeros(n, n)]
Bc = [zeros(n, m);U]

A = expm(Ac * Ts)
B = inv(Ac) * (A - eye(2 * n)) * Bc

R = diagm(2 * ones(m))
Q = diagm(ones(2 * n))
for idx in 1:n
    Q[idx,idx] = 5;
end
P = Q;

MyMPC = mpcdata(2 * n, m, T, A, B, R, Q, P)

lbu = -0.5 * ones(m)
ubu = -lbu
lbx = -4 * ones(2 * n)
ubx = 4 * ones(2 * n)

MyMPC.lbu = lbu;
MyMPC.ubu = ubu;
MyMPC.ubx = MyMPC.ubt = ubx;
MyMPC.lbx = MyMPC.lbt = lbx;

x0 = [1,-1,1,-1,1,-1,0,0,0,0,0,0]

r = mpc2denseQP(MyMPC, x0);

if (ENABLE_GUROBI)
    using Gurobi;
    env = Gurobi.Env()
    model = gurobi_model(env; name = "qpOASES", H = r.H, f = r.h, A = r.F, b = r.f, lb = r.lb, ub = r.ub)
    optimize(model);

    sol = get_solution(model)
    println("soln = $(sol)")

    objv = get_objval(model)
    println("objv = $(objv)")

    full_obj = objv + r.residual;
    println("full_obj_val = $(full_obj)")
end

function writeqp(filename::AbstractString, qp::mpcdata)
    A = qp.A
    B = qp.B
    R = diag(qp.R)
    Q = diag(qp.Q)
    P = diag(qp.P) 
    open(filename, "w") do io
        write(io, A)
        write(io, B)
        write(io, R)
        write(io, Q)
        write(io, P)
    end
end
  



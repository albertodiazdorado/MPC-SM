using Plots
gr()

function mpcplot(N::Integer, filename::String, ITERATIONS::Bool = true)
    # Get size of the simulation
    r = readcsv("sim1.csv")
    T = length(r[:,1])

    # Allocate memory
    mehrotra_time = Array{Float64,2}(zeros(T, N))
    mehrotra_iterations = Array{Integer,2}(zeros(T, N))
    qpoases_time = Array{Float64,2}(zeros(T, N));
    qpoases_iterations = Array{Integer,2}(zeros(T, N));

    # Read the values of the N simulations
    for sim in 1:N
        r = readcsv(string("sim", sim, ".csv"))
        mehrotra_time[:,sim] = r[:,1]
        qpoases_time[:,sim] = r[:,2]
        mehrotra_iterations[:,sim] = r[:,3]
        qpoases_iterations[:,sim] = r[:,4]
    end

    # Allocate memory for the mean and median
    # First column is for mehrotra
    # Second column is for qpOASES
    mean_time = Array{Float64,2}(zeros(T, 2))
    median_time = Array{Float64,2}(zeros(T, 2))
    var_time = Array{Float64,2}(zeros(T, 2))
    median_iters = Array{Float64,2}(zeros(T, 2))
    var_iters = Array{Float64,2}(zeros(T, 2))

    # Calculate mean, median and variance
    for step in 1:T
      # Mean times
        mean_time[step,1] = mean(mehrotra_time[step,:])
        mean_time[step,2] = mean(qpoases_time[step,:])
      # Median times and iters
        median_time[step,1] = median(mehrotra_time[step,:])
        median_time[step,2] = median(qpoases_time[step,:])
        median_iters[step,1] = median(mehrotra_iterations[step,:])
        median_iters[step,2] = median(qpoases_iterations[step,:])
      # Variance of times and iters
        var_time[step,1] = var(mehrotra_time[step,:])
        var_time[step,2] = var(qpoases_time[step,:])
        var_iters[step,1] = var(mehrotra_iterations[step,:])
        var_iters[step,2] = var(qpoases_iterations[step,:])
    end

    # Do not plot variance, but standard deviation
    var_time = sqrt.(var_time);
    var_iters = sqrt.(var_iters);

    # Plot the mean times
    p1 = plot(1:T, median_time[:,1], label="Mehrotra", color="red");
    plot!(p1, 1:T, var_time[:,1], label="Variance", color="pink");
    plot!(p1, 1:T, median_time[:,2], label="qpOASES", color="blue");
    plot!(p1, 1:T, var_time[:,2], label="Variance", color="lightblue");
    plot!(p1, ylabel="ms", title="Convergence time (median)");


    if (ITERATIONS)
        # Plot the optimization steps
        p2 = plot(1:T, median_iters[:,1], label = "Mehrotra", color = "red");
        plot!(p2, 1:T, var_iters[:,1], label = "Variance", color = "pink");
        plot!(p2, 1:T, median_iters[:,2], label = "qpOASES", color = "blue");
        plot!(p2, 1:T, var_iters[:,2], label = "Variance", color = "lightblue");
        plot!(p2, ylabel = "iter", xlabel = "Step", title = "Required iterations (median)");

        # Pack the three plots together
        P = plot(p1, p2, layout = (2, 1))
    else
        P = plot(p1)
    end
    
    # Save the goddamn plot
    savefig(P, string("/home/add/Desktop/MPC-SM/", filename, ".pdf"))

end
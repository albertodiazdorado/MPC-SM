workspace()
include("/home/add/Desktop/MPC-SM/julia/PlotingUtilities.jl")

cd("/home/add/Desktop/MPC-SM/Figures/")
cd("Diagnosis")

ITERATIONS = Bool(false);

N = 100 #Number of simulations per use case

F = readdir();      # List of folders
C = length(F);      # Number of folders

for folder in 1:C
    cd(F[folder]);
    mpcplot(N,string("Figures/Diagnosis/",F[folder]), ITERATIONS);
    cd("..");
end

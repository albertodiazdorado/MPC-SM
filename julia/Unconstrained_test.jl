# Clean workspace
workspace()

## Give values
# A = [0.5 -1.25;1 0];
# B = [2;0.];
A = [1 2;-1 0.]'
# B = [1;0.]
B = eye(2);
R = [200 0;0 200];
Q = diagm([1,2.]);
P = [5 1;1 7];
x0 = [5,5.];

# Constraints
lbu = [-5, -5]
ubu = [5, 5]
lbx = [-5,-5.]
ubx = [5,5.]
# Constraints

# Problem data
n = 2;
m = 2;
T = 10;
ic = 2 * m;
#Problem data

# Initialize x
x = zeros(T * (n + m));

# Initialize s
s0 = ones(ic)
s0[1:m] = ubu - x[1:m]
s0[m + 1:2 * m] = x[1:m] - lbu
s = ones(0)
for i in 1:T
    s = vcat(s, s0)
end
nu = zeros(T * n);
lambda = ones(T * ic) * 10;

# variables
z = [true,true,false,false];
choper = z
for i in 2:T
    choper = vcat(choper, z)
end


## Create KKT matrix
## Create H
H = [R zeros(m, n);zeros(n, m) Q];
for i = 2:T
    size = (i - 1) * (n + m);
    H = [H zeros(size, m + n);zeros(m + n, size) [R zeros(m, n);zeros(n, m) Q] ];
end
H[end - n + 1:end,end - n + 1:end] = P;

## Create C
C = [B -eye(n, n) zeros(n, (T - 1) * (n + m));
      zeros(n, m) A B -eye(n, n) zeros(n, (n + m) * (T - 2))]
for i in 3:T - 1
    C = [C;zeros(n, m) zeros(n, (n + m) * (i - 2)) A B -eye(n, n) zeros(n, (n + m) * (T - i))]
end
C = [C;zeros(n, m) zeros(n, (n + m) * (T - 2)) A B -eye(n, n)]

## Create F
F0 = [eye(m) zeros(m, n);
      -eye(m) zeros(m, n)]
F = F0;
for i in 2:T 
    F = [F zeros(ic * (i - 1), m + n);
        zeros(ic, (m + n) * (i - 1)) F0];
end

## Create KKT
K = [H C' F' zeros(T * (n + m), T * ic);
      C zeros(T * n, T * (n + ic + ic));
      F zeros(T * ic, T * (n + ic)) eye(T * ic);
      zeros(T * ic, T * (n + n + m)) diagm(s) diagm(lambda)]

## Create right hand side
# Create d
d = vcat(-A * x0, zeros(n * (T - 1)));
# Create b
b = [];
b0 = vcat(ubu, -lbu);
for i in 1:T
    b = vcat(b, b0);
end

# Create residuals
rc = H * x + C' * nu + F' * lambda;
re = C * x - d;
ri = F * x + s - b;
rs = s .* lambda;
rhs = vcat(-rc, -re, -ri, -rs);

## Affine solve
sol = K \ rhs;
sol1 = sol[1:T * (n + m)];
nu_aff = sol[T * (n + m) + 1:T * (n + m + n)];
lambda_aff = sol[T * (n + m + n) + 1:T * (n + m + n + ic)];
s_aff = sol[T * (n + m + n + ic) + 1:end];
u_aff = sol1[choper];
x_aff = sol1[.!(choper)];

# Solve (mode 2)
# Build matrices
Phi = H + F' * inv(diagm(s)) * diagm(lambda) * F;
Shur = C * inv(Phi) * C';
# Build residuals
rd = rc + F' * inv(diagm(s)) * (diagm(lambda) * ri - rs);
rhs2 = re - C * inv(Phi) * rd;
# Solve
nu_aff_2 = Shur \ rhs2;
# Recover variables
sol2 = -Phi \ (rd + C' * nu_aff_2);
u_aff_2 = sol2[choper];
x_aff_2 = sol2[.!(choper)];

lambda_aff_2 = inv(diagm(s)) * diagm(lambda) * (ri + F * sol2 - inv(diagm(lambda)) * rs)
s_aff_2 = -inv(diagm(lambda)) * (rs + diagm(s) * lambda_aff_2)

s_aff_3 = -(F * sol1 + ri)

# Linesearch
alpha_p_aff = Inf;
aux = -s ./ s_aff;
for i in 1:length(aux)
    if ( aux[i] > 0 && aux[i] < alpha_p_aff )
        alpha_p_aff = aux[i];
    end
end
alpha_p_aff = min(alpha_p_aff, 1);
alpha_d_aff = Inf;
aux = -lambda ./ lambda_aff;
for i in 1:length(aux)
    if ( aux[i] > 0 && aux[i] < alpha_d_aff )
        alpha_d_aff = aux[i];
    end
end
alpha_d_aff = min(alpha_d_aff, 1);

# Centering
mu = lambda' * s;
muaff = (lambda + alpha_d_aff * lambda_aff)' * (s + alpha_p_aff * s_aff);
sigma = (muaff / mu)^3
sigma2 = min(sigma, 1)


# Centering plus correction step
rc_cc = zeros(length(x));
re_cc = zeros(T * n);
ri_cc = zeros(length(s));
rs_cc =  -sigma * muaff * ones(length(s)) + s_aff .* lambda_aff;
rhs_cc = vcat(-rc_cc, -re_cc, -ri_cc, -rs_cc);

# Solve (mode 1)
sol_cc = K \ rhs_cc;
sol_cc_1 = sol_cc[1:T * (n + m)];
nu_cc = sol_cc[T * (n + m) + 1:T * (n + m + n)];
lambda_cc = sol_cc[T * (n + m + n) + 1:T * (n + m + n + ic)];
s_cc = sol_cc[T * (n + m + n + ic) + 1:end];
u_cc = sol_cc_1[choper];
x_cc = sol_cc_1[.!(choper)];

# Solve (mode 2)
rd_cc = rc_cc + F' * inv(diagm(s)) * (diagm(lambda) * ri_cc - rs_cc);
rhs_cc_2 = re_cc - C * inv(Phi) * rd_cc;
nu_cc_2 = Shur \ rhs_cc_2;
# Recover variables
sol_cc_2 = -Phi \ (rd_cc + C' * nu_cc_2);
u_cc_2 = sol_cc_2[choper];
x_cc_2 = sol_cc_2[.!(choper)];
lambda_cc_2 = inv(diagm(s)) * diagm(lambda) * (ri_cc + F * sol_cc_2 - rs_cc)
s_cc_2 = -inv(diagm(lambda)) * (rs_cc + diagm(s) * lambda_cc_2)

# Aggregate steps
nu_final = nu_aff + nu_cc
x_final = x_aff + x_cc
u_final = u_aff + u_cc
s_final = s_aff + s_cc
lambda_final = lambda_aff + lambda_cc
sol_final = sol1  + sol_cc_1;

# Linesearch
alpha_p = Inf;
aux = -s ./ s_final;
for i in 1:length(aux)
    if ( aux[i] > 0 && aux[i] < alpha_p )
        alpha_p = aux[i];
    end
end
alpha_p = min(alpha_p, 1);
alpha_d = Inf;
aux = -lambda ./ lambda_final;
for i in 1:length(aux)
    if ( aux[i] > 0 && aux[i] < alpha_d )
        alpha_d = aux[i];
    end
end
alpha_d = min(alpha_d, 1);

# Take step
alpha = 0.99 * min(alpha_p, alpha_d)
nu_new = nu + alpha * nu_final
x_new = x[.!choper] + alpha * x_final;
u_new = x[choper] + alpha * u_final;
s_new = s + alpha * s_final;
lambda_new = lambda + alpha * lambda_final;
z_new = x + sol_final * alpha;

struct parameters
    H::AbstractMatrix
    C::AbstractMatrix
    F::AbstractMatrix
    d::AbstractArray
    b::AbstractArray
    x0::AbstractArray
    choper::AbstractArray
end

struct variables
    T::Real
    n::Real
    m::Real
    ic::Real
    z::AbstractArray
    x::AbstractArray
    u::AbstractArray
    nu::AbstractArray
    lambda::AbstractArray
    s::AbstractArray
    opt_val::Real
    k::Int
end

obj = parameters(H, C, F, d, b, x0, choper)
vars = variables(T, n, m, ic, x, x[.!choper], x[choper], nu, lambda, s, x' * H * x, 0)


function iterate(obj::parameters, vars::variables)
# Recover data
    H = obj.H
    C = obj.C
    F = obj.F
    d = obj.d
    b = obj.b
    x0 = obj.x0
    choper = obj.choper

# Recover variables
    m = vars.m
    n = vars.n
    T = vars.T
    ic = vars.ic
    x = vars.x
    z = vars.z
    u = vars.u
    nu = vars.nu
    s = vars.s
    lambda = vars.lambda
    counter = vars.k;

# Create KKT matrix
    K = [H C' F' zeros(T * (n + m), T * ic);
      C zeros(T * n, T * (n + ic + ic));
      F zeros(T * ic, T * (n + ic)) eye(T * ic);
      zeros(T * ic, T * (n + n + m)) diagm(s) diagm(lambda)]

# Create residuals
    rc = H * z + C' * nu + F' * lambda;
    re = C * z - d;
    ri = F * z + s - b;
    rs = s .* lambda;
    rhs = vcat(-rc, -re, -ri, -rs);

## Affine solve
    sol = K \ rhs;
    z_aff = sol[1:T * (n + m)];
    nu_aff = sol[T * (n + m) + 1:T * (n + m + n)];
    lambda_aff = sol[T * (n + m + n) + 1:T * (n + m + n + ic)];
    s_aff = sol[T * (n + m + n + ic) + 1:end];
    u_aff = z_aff[choper];
    x_aff = z_aff[.!(choper)];


# Linesearch
    alpha_p = Inf;
    aux = -s ./ s_aff;
    for i in 1:length(aux)
        if ( aux[i] > 0 && aux[i] < alpha_p )
            alpha_p = aux[i];
        end
    end
    alpha_p = min(alpha_p, 1);
    alpha_d = Inf;
    aux = -lambda ./ lambda_aff;
    for i in 1:length(aux)
        if ( aux[i] > 0 && aux[i] < alpha_d )
            alpha_d = aux[i];
        end
    end
    alpha_d = min(alpha_d, 1);

# Centering
    mu = lambda' * s;
    muaff = (lambda + alpha_d * lambda_aff)' * (s + alpha_p * s_aff);
    sigma = (muaff / mu)^3


# Centering plus correction step
    rc_cc = zeros(length(z));
    re_cc = zeros(T * n);
    ri_cc = zeros(length(s));
    rs_cc = s_aff .* lambda_aff - sigma * muaff * ones(length(s));
    rhs_cc = vcat(-rc_cc, -re_cc, -ri_cc, -rs_cc);

# Solve (mode 1)
    sol_cc = K \ rhs_cc;
    z_cc = sol_cc[1:T * (n + m)];
    nu_cc = sol_cc[T * (n + m) + 1:T * (n + m + n)];
    lambda_cc = sol_cc[T * (n + m + n) + 1:T * (n + m + n + ic)];
    s_cc = sol_cc[T * (n + m + n + ic) + 1:end];
    u_cc = z_cc[choper];
    x_cc = z_cc[.!(choper)];

# Aggregate steps
    nu_final = nu_aff + nu_cc
    x_final = x_aff + x_cc
    u_final = u_aff + u_cc
    s_final = s_aff + s_cc
    lambda_final = lambda_aff + lambda_cc
    z_final = z_aff  + z_cc;

# Linesearch
    alpha_p = Inf;
    aux = -s ./ s_final;
    for i in 1:length(aux)
        if ( aux[i] > 0 && aux[i] < alpha_p )
            alpha_p = aux[i];
        end
    end
    alpha_p = min(alpha_p, 1);
    alpha_d = Inf;
    aux = -lambda ./ lambda_final;
    for i in 1:length(aux)
        if ( aux[i] > 0 && aux[i] < alpha_d )
            alpha_d = aux[i];
        end
    end
    alpha_d = min(alpha_d, 1);

# Take the minimum of both
    alpha = 0.99 * min(alpha_p, alpha_d)

# Take step
    nu_new = nu + alpha * nu_final
    x_new = z[.!choper] + alpha * x_final;
    u_new = z[choper] + alpha * u_final;
    s_new = s + alpha * s_final;
    lambda_new = lambda + alpha * lambda_final;
    z_new = z + z_final * alpha;

# Return
    vars = variables(T, n, m, ic, z_new, x_new, u_new, nu_new, lambda_new, s_new, 0.5 * z_new' * H * z_new, counter + 1)

    return vars
end

function ContMat(A::AbstractArray, B::AbstractArray)
    p = size(A, 1);
    C = B;
    for i in 1:p - 1
        C = hcat(C, (A^p) * B);
    end
    return C
end


function solve(obj, vars)
    while vars.k < 100
        vars = iterate(obj, vars)
        if (vars.s' * vars.lambda < 1e-5 && norm(C * vars.z - obj.d) < 1e-2)
            break
        end
    end
    return vars
end




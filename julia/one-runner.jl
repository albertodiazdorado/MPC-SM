z = z_new;
nu = nu_new;
lambda = lambda_new;
s = s_new;

# Create KKT matrix
K = [H C' F' zeros(T * (n + m), T * ic);
C zeros(T * n, T * (n + ic + ic));
F zeros(T * ic, T * (n + ic)) eye(T * ic);
zeros(T * ic, T * (n + n + m)) diagm(s) diagm(lambda)]

# Create residuals
  rc = H * z + C' * nu + F' * lambda;
  re = C * z - d;
  ri = F * z + s - b;
  rs = s .* lambda;
  rhs = vcat(-rc, -re, -ri, -rs);

## Affine solve
  sol = K \ rhs;
  z_aff = sol[1:T * (n + m)];
  nu_aff = sol[T * (n + m) + 1:T * (n + m + n)];
  lambda_aff = sol[T * (n + m + n) + 1:T * (n + m + n + ic)];
  s_aff = sol[T * (n + m + n + ic) + 1:end];
  u_aff = z_aff[choper];
  x_aff = z_aff[.!(choper)];


# Solve (mode 2)
# Build matrices
  Phi = H + F' * inv(diagm(s)) * diagm(lambda) * F;
  Shur = C * inv(Phi) * C';
# Build residuals
  rd = rc + F' * inv(diagm(s)) * (diagm(lambda) * ri - rs);
  rhs2 = re - C * inv(Phi) * rd;
# Solve
  nu_aff_2 = Shur \ rhs2;
# Recover variables
  z_aff_2 = -Phi \ (rd + C' * nu_aff_2);
  u_aff_2 = z_aff_2[choper];
  x_aff_2 = z_aff_2[.!(choper)];
  lambda_aff_2 = inv(diagm(s)) * diagm(lambda) * (ri + F * z_aff_2 - inv(diagm(lambda)) * rs)
  s_aff_2 = -inv(diagm(lambda)) * (rs + diagm(s) * lambda_aff_2)

# Linesearch
  alpha_p_aff = Inf;
  aux = -s ./ s_aff;
  for i in 1:length(aux)
    if ( aux[i] > 0 && aux[i] < alpha_p_aff )
        alpha_p_aff = aux[i];
    end
end
  alpha_p_aff = min(alpha_p_aff, 1);
  alpha_d_aff = Inf;
  aux = -lambda ./ lambda_aff;
  for i in 1:length(aux)
    if ( aux[i] > 0 && aux[i] < alpha_d_aff )
        alpha_d_aff = aux[i];
    end
end
  alpha_d_aff = min(alpha_d_aff, 1);

# Centering
  mu = lambda' * s;
  muaff = (lambda + alpha_d_aff * lambda_aff)' * (s + alpha_p_aff * s_aff);
  sigma = (muaff / mu)^3


# Centering plus correction step
  rc_cc = zeros(length(z));
  re_cc = zeros(T * n);
  ri_cc = zeros(length(s));
  rs_cc = s_aff .* lambda_aff - sigma * muaff * ones(length(s));
  rhs_cc = vcat(-rc_cc, -re_cc, -ri_cc, -rs_cc);

# Solve (mode 1)
  sol_cc = K \ rhs_cc;
  z_cc = sol_cc[1:T * (n + m)];
  nu_cc = sol_cc[T * (n + m) + 1:T * (n + m + n)];
  lambda_cc = sol_cc[T * (n + m + n) + 1:T * (n + m + n + ic)];
  s_cc = sol_cc[T * (n + m + n + ic) + 1:end];
  u_cc = z_cc[choper];
  x_cc = z_cc[.!(choper)];

# Solve (mode 2)
  rd_cc = rc_cc + F' * inv(diagm(s)) * (diagm(lambda) * ri_cc - rs_cc);
  rhs_cc_2 = re_cc - C * inv(Phi) * rd_cc;
  nu_cc_2 = Shur \ rhs_cc_2;
# Recover variables
  sol_cc_2 = -Phi \ (rd_cc + C' * nu_cc_2);
  u_cc_2 = sol_cc_2[choper];
  x_cc_2 = sol_cc_2[.!(choper)];
  lambda_cc_2 = inv(diagm(s)) * diagm(lambda) * (ri_cc + F * sol_cc_2 - rs_cc)
  s_cc_2 = -inv(diagm(lambda)) * (rs_cc + diagm(s) * lambda_cc_2)

# Aggregate steps
  nu_final = nu_aff + nu_cc
  x_final = x_aff + x_cc
  u_final = u_aff + u_cc
  s_final = s_aff + s_cc
  lambda_final = lambda_aff + lambda_cc
  z_final = z_aff  + z_cc;

# Linesearch
  alpha_p = Inf;
  aux = -s ./ s_final;
  for i in 1:length(aux)
    if ( aux[i] > 0 && aux[i] < alpha_p )
        alpha_p = aux[i];
    end
end
  alpha_p = min(alpha_p, 1);
  alpha_d = Inf;
  aux = -lambda ./ lambda_final;
  for i in 1:length(aux)
    if ( aux[i] > 0 && aux[i] < alpha_d )
        alpha_d = aux[i];
    end
end
  alpha_d = min(alpha_d, 1);

# Take the minimum of both
  alpha = 0.99 * min(alpha_p, alpha_d)

# Take step
nu_new = nu + alpha * nu_final
x_new = z[.!choper] + alpha * x_final;
u_new = z[choper] + alpha * u_final;
s_new = s + alpha * s_final;
lambda_new = lambda + alpha * lambda_final;
z_new = z + z_final * alpha;


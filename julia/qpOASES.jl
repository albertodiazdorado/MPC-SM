ENABLE_GUROBI = true;

if (!ENABLE_GUROBI)
    workspace();
end

A = [1 -1;2 0]
B = eye(2);
R = eye(2);
Q = [0.1 0;0 0.2]
P = [5 1;1 7.]
n = 2
m = 2
T = 3
ubx = ones(2, 1) * 6;
lbx = -ubx;
ubu = ones(2, 1) * 5;
lbu = -ubu;
Fu = [eye(m);-eye(m)]
fu = vcat(ubu, -lbu);
Fx = [eye(n);-eye(n)]
fx = vcat(ubx, -lbx);


superA = A
for i in 2:T
    superA = vcat(superA, A^i);
end

column = B;
for row in 2:T 
    column = vcat(column, A^(row - 1) * B);
end
superB = column;

for col in 2:T
    column = vcat(zeros((col - 1) * n, m), B);
    for row in col + 1:T
        column = vcat(column, A^(row - col) * B);
    end
    superB = hcat(superB, column);
end

x0 = [5,5.]

q = diag(Q);
r = diag(R)
for i in 2:T
    q = vcat(q, diag(Q));
    r = vcat(r, diag(R));
end
superQ = diagm(q);
superQ[end - 1:end,end - 1:end] = P
superR = diagm(r);


H = superB' * superQ * superB + superR;
h = superB' * superQ * superA * x0

superFu = zeros(2 * T * m, T * m);
superfu = zeros(2 * T * m);
superFx = zeros(2 * T * n, T * n);
superfx = zeros(2 * T * n);

for idx in 0:T - 1
    superFu[1 + 2 * m * idx:2 * m * (idx + 1) , 1 + m * idx:m * (idx + 1)] = Fu;
    superFx[1 + 2 * n * idx:2 * n * (idx + 1), 1 + n * idx:n * (idx + 1)] = Fx;
    superfu[1 + 2 * m * idx:2 * m * (idx + 1)] = fu;
    superfx[1 + 2 * n * idx:2 * n * (idx + 1)] = fx;
end

superF = vcat(superFx * superB, superFu);
superf = vcat(superfx - superFx * superA * x0, superfu);

f = superf;
F = superF;

residual_obj_val = 0.5 * x0' * superA' * superQ * superA * x0;

qp_lb = lbu;
qp_ub = ubu;
qp_lbF = lbx;
qp_ubF = ubx;
for idx in 2:T
    qp_lb = vcat(qp_lb, lbu);
    qp_ub = vcat(qp_ub, ubu);
    qp_lbF = vcat(qp_lbF, lbx);
    qp_ubF = vcat(qp_ubF, ubx);
end
qp_lbF = qp_lbF - superA * x0;
qp_ubF -= superA * x0;

if (ENABLE_GUROBI)
    using Gurobi;
    env = Gurobi.Env()
    model = gurobi_model(env; name = "qpOASES", H = H, f = h, A = F, b = f)
    optimize(model);

    sol = get_solution(model)
    println("soln = $(sol)")

    objv = get_objval(model)
    println("objv = $(objv)")

    full_obj = objv + residual_obj_val;
    println("full_obj_val = $(full_obj)")
end
# Model Predictive Control - Solver Mehrotra
MPC-SM is a project that implements the Mehrotra predictor-corrector algorithm for numerical optimization, applied to MPC-type problems.
The folder *src* contains all the source code of the project, whereas the other folders are used for debugging and testing.
The code is heavily templated and has a modular design, aiming for the best efficiency for every particular instance of MPC problems, such as bounded variables (as opposed to linearly constrained variables) and diagonal penalty matrix (as opposed to dense penalty matrices).

## Policy-Based Design
Our solver consist on a **shell**, defined under src/solver, that inherits from different policies.
Those policies tailor the behaviour of the solver, so that it is best adapted to every instance of the MPC problem.
A detailed inheritance tree is available in [this report](https://gitlab.com/albertodiazdorado/Thesis).

## Sub-folders
The folders within **src** are organized as follows:
* **constraints**: Contains the policies that implement the behaviour for different types of constraints (simple bounds, linear constraints, etc)
* **detail**: Implementation detail, such as linear algebra operation and traits.
* **matrix**: Light-weight matrix classes used throughout the project.
* **policies**: Additional policies, different from the constraint policies. So far, a single policy has been implemented, which determines the centering parameter of the Mehrotra predictor-corrector algorithm.
#ifndef SPDMAT_CPP_
#define SPDMAT_CPP_

#include "matrix/imatrix.cpp"
#include <array>

namespace mpc {
namespace matrix {
template <class real_t, class int_t, int_t Nvals>
struct SPDMat : public IMatrix<real_t, int_t> {
  SPDMat() = default;
  SPDMat(const int_t m, const int_t n) : IMatrix<real_t, int_t>{m, n} {
    // if (m*n != N)
    //   throw;
  }
  // Inherited functions
  void factorize();
  void fastsolve(real_t *, const int_t = 1) override;
  void backsolve(real_t *b, const int_t N) override;
  real_t &operator[](const std::size_t idx) override { return values[idx]; }
  const real_t &operator[](const std::size_t idx) const override {
    return values[idx];
  }

private:
  std::array<real_t, Nvals> values;
};
} // namespace matrix
} // namespace mpc

#endif

#ifndef DIAGMAT_CPP_
#define DIAGMAT_CPP_

#include "detail/matops.cpp"

#include "imatrix.cpp"
#include <algorithm>
#include <functional>
#include <vector>

namespace mpc {
namespace matrix {
template <class real_t, class int_t>
struct DiagMat : public IMatrix<real_t, int_t> {
  // Constructors
  DiagMat() = default;
  DiagMat(const int_t);
  DiagMat(const int_t, const int_t);
  DiagMat(std::vector<real_t>);
  DiagMat(const int_t, const std::vector<real_t>);
  DiagMat(const int_t, const real_t *, const char = 'D');

  // Inherited functions
  void factorize();
  void backsolve(real_t *, const int_t = 1) override;
  void fastsolve(real_t *, const int_t = 1) const override;
  real_t &operator()(const std::size_t, const std::size_t) override;
  const real_t &operator()(const std::size_t, const std::size_t) const override;

  // Inherited functions (algorithm)
  void add_bounds(const real_t *) override;
  void add_bounds(const real_t *, const real_t *) override;

  // Own functions (not inherited)

  // Just debugging purposes

private:
  real_t null{0};
}; // namespace matrix

// ************************************************************************** //
// ******************************* CONSTRUCTORS ***************************** //
// ************************************************************************** //
template <class real_t, class int_t>
DiagMat<real_t, int_t>::DiagMat(const int_t n)
    : IMatrix<real_t, int_t>{n, n, n} {}

template <class real_t, class int_t>
DiagMat<real_t, int_t>::DiagMat(const int_t m, const int_t n)
    : DiagMat<real_t, int_t>{m} {}

template <class real_t, class int_t>
DiagMat<real_t, int_t>::DiagMat(std::vector<real_t> vals)
    : IMatrix<real_t, int_t>(vals.size(), vals.size(), vals.size()) {
  IMatrix<real_t, int_t>::matrix = std::move(vals);
}

template <class real_t, class int_t>
DiagMat<real_t, int_t>::DiagMat(const int_t n, const std::vector<real_t> vals)
    : IMatrix<real_t, int_t>(n, n, n) {
  if (static_cast<int_t>(vals.size()) == n)
    IMatrix<real_t, int_t>::matrix = std::move(vals);
  else {
    for (int_t col = 0; col < n; col++)
      IMatrix<real_t, int_t>::matrix[col] = vals[col + col * n];
  }
}

template <class real_t, class int_t>
DiagMat<real_t, int_t>::DiagMat(const int_t n, const real_t *vals, const char D)
    : IMatrix<real_t, int_t>(n, n) {
  if (D == 'D')
    std::copy(vals, vals + n, IMatrix<real_t, int_t>::matrix.data());
  else {
    for (int_t col = 0; col < n; col++)
      IMatrix<real_t, int_t>::matrix[col] = vals[col + col * n];
  }
}

// ************************************************************************** //
// ********************************* GETTERS ******************************** //
// ************************************************************************** //
template <class real_t, class int_t>
real_t &DiagMat<real_t, int_t>::operator()(const std::size_t row,
                                           const std::size_t col) {
  if (row == col)
    return IMatrix<real_t, int_t>::matrix[row];
  else
    return null;
}

template <class real_t, class int_t>
const real_t &DiagMat<real_t, int_t>::operator()(const std::size_t row,
                                                 const std::size_t col) const {
  if (row == col)
    return IMatrix<real_t, int_t>::matrix[row];
  else
    return null;
}

// ************************************************************************** //
// ********************************** SOLVERS ******************************* //
// ************************************************************************** //
template <class real_t, class int_t>
void DiagMat<real_t, int_t>::fastsolve(real_t *b, int_t nrhs) const {
  int_t n = IMatrix<real_t, int_t>::n;
  for (int_t idx = 0; idx < nrhs; idx++)
    std::transform(b + idx * n, b + n + idx * n,
                   IMatrix<real_t, int_t>::matrix.cbegin(), b + idx * n,
                   std::divides<real_t>());
}

template <class real_t, class int_t>
void DiagMat<real_t, int_t>::backsolve(real_t *b, const int_t nrhs) {
  fastsolve(b, nrhs);
}

// ************************************************************************** //
// ******************************** ALGORITHM ******************************* //
// ************************************************************************** //
template <class real_t, class int_t>
void DiagMat<real_t, int_t>::add_bounds(const real_t *x) {
  mpc::detail::MatOps<real_t *>::vecsum(IMatrix<real_t, int_t>::n, 1., x,
                                        IMatrix<real_t, int_t>::matrix.data());
}

template <class real_t, class int_t>
void DiagMat<real_t, int_t>::add_bounds(const real_t *x, const real_t *y) {
  mpc::detail::MatOps<real_t *>::vecsum(IMatrix<real_t, int_t>::n, 1., x,
                                        IMatrix<real_t, int_t>::matrix.data());
  mpc::detail::MatOps<real_t *>::vecsum(IMatrix<real_t, int_t>::n, 1., y,
                                        IMatrix<real_t, int_t>::matrix.data());
}

} // namespace matrix
} // namespace mpc

#endif

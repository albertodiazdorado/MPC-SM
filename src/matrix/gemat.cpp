#ifndef GEMAT_CPP_
#define GEMAT_CPP_

#include "detail/matops.cpp"
#include "matrix/imatrix.cpp"
#include <vector>

namespace mpc {
namespace matrix {
template <class real_t, class int_t>
struct GEMat : public IMatrix<real_t, int_t> {
  // Constructors
  GEMat() = default;
  GEMat(const int_t, const int_t);
  GEMat(const int_t, const int_t, std::vector<real_t>);
  GEMat(const int_t, const int_t, const real_t *);

  // Inherited functions
  void factorize();
  void backsolve(real_t *, const int_t = 1) override;
  void fastsolve(real_t *, const int_t = 1) const override;
  real_t &operator()(const std::size_t, const std::size_t) override;
  const real_t &operator()(const std::size_t, const std::size_t) const override;

  // Inherited functions (algorithm)
  // void add_bounds(const real_t *) override;
  // void add_bounds(const real_t *, const real_t *) override;

  // Own functions (not inherited)
  void backsolve(const char, real_t *, const int_t = 1);
  void fastsolve(const char, real_t *, const int_t = 1) const;
  void initialize(const mpc::matrix::IMatrix<real_t, int_t> &,
                  const mpc::matrix::GEMat<real_t, int_t> &);

  // Just debugging purposes

private:
  std::vector<real_t> factor; // here, we assume std::allocator for vector
  std::vector<int> ipiv;
}; // namespace matrix

// ************************************************************************** //
// ******************************* CONSTRUCTORS ***************************** //
// ************************************************************************** //
template <class real_t, class int_t>
GEMat<real_t, int_t>::GEMat(const int_t m, const int_t n)
    : IMatrix<real_t, int_t>{m, n}, factor{std::vector<real_t>(n * m)},
      ipiv{std::vector<int>(m < n ? m : n)} {}

template <class real_t, class int_t>
GEMat<real_t, int_t>::GEMat(const int_t m, const int_t n,
                            std::vector<real_t> vals) {
  IMatrix<real_t, int_t>::m = m;
  IMatrix<real_t, int_t>::n = n;
  IMatrix<real_t, int_t>::matrix = std::move(vals);
  factor = IMatrix<real_t, int_t>::matrix;
  ipiv = std::vector<int>(m < n ? m : n);
}

template <class real_t, class int_t>
GEMat<real_t, int_t>::GEMat(const int_t m, const int_t n, const real_t *vals) {
  IMatrix<real_t, int_t>::m = m;
  IMatrix<real_t, int_t>::n = n;
  std::copy(vals, vals + n * m, IMatrix<real_t, int_t>::matrix.begin());
  factor = IMatrix<real_t, int_t>::matrix;
  ipiv = std::vector<int>(m < n ? m : n);
}

// ************************************************************************** //
// ********************************* GETTERS ******************************** //
// ************************************************************************** //
template <class real_t, class int_t>
real_t &GEMat<real_t, int_t>::operator()(const std::size_t row,
                                         const std::size_t col) {
  return IMatrix<real_t, int_t>::matrix[row + IMatrix<real_t, int_t>::m * col];
}

template <class real_t, class int_t>
const real_t &GEMat<real_t, int_t>::operator()(const std::size_t row,
                                               const std::size_t col) const {
  return IMatrix<real_t, int_t>::matrix[row + IMatrix<real_t, int_t>::m * col];
}

// ************************************************************************** //
// ********************************** SOLVERS ******************************* //
// ************************************************************************** //
template <class real_t, class int_t> void GEMat<real_t, int_t>::factorize() {
  std::copy(IMatrix<real_t, int_t>::matrix.cbegin(),
            IMatrix<real_t, int_t>::matrix.cend(), factor.begin());
  mpc::detail::MatOps<real_t *>::gefactor(
      IMatrix<real_t, int_t>::m, IMatrix<real_t, int_t>::n, factor.data(),
      IMatrix<real_t, int_t>::m, ipiv.data());
}

template <class real_t, class int_t>
void GEMat<real_t, int_t>::fastsolve(real_t *b, int_t nrhs) const {
  mpc::detail::MatOps<real_t *>::gesolve(
      'N', IMatrix<real_t, int_t>::n, nrhs, factor.data(),
      IMatrix<real_t, int_t>::n, ipiv.data(), b, IMatrix<real_t, int_t>::n);
}

template <class real_t, class int_t>
void GEMat<real_t, int_t>::fastsolve(const char trans, real_t *b,
                                     int_t nrhs) const {
  mpc::detail::MatOps<real_t *>::gesolve(
      trans, IMatrix<real_t, int_t>::n, nrhs, factor.data(),
      IMatrix<real_t, int_t>::n, ipiv.data(), b, IMatrix<real_t, int_t>::n);
}

template <class real_t, class int_t>
void GEMat<real_t, int_t>::backsolve(real_t *b, const int_t nrhs) {
  factorize();
  fastsolve(b, nrhs);
}

template <class real_t, class int_t>
void GEMat<real_t, int_t>::backsolve(const char trans, real_t *b,
                                     const int_t nrhs) {
  factorize();
  fastsolve(b, nrhs, trans);
}

// ************************************************************************** //
// ******************************** ALGORITHM ******************************* //
// ************************************************************************** //
// template <class real_t, class int_t>
// void GEMat<real_t, int_t>::add_bounds(const real_t *x) {
//   // Do nothing (undefined, please, do not give me a rectangular, non-symmetric
//   // penalty matrix)
// }

// template <class real_t, class int_t>
// void GEMat<real_t, int_t>::add_bounds(const real_t *x, const real_t *y) {
//   // Do nothing (see comment above)
// }

template <class real_t, class int_t>
void GEMat<real_t, int_t>::initialize(
    const mpc::matrix::IMatrix<real_t, int_t> &P,
    const mpc::matrix::GEMat<real_t, int_t> &G) {
  *(this) = G;
  P.fastsolve(this->data(), this->ncols());
}

} // namespace matrix
} // namespace mpc

#endif

#ifndef PDMAT_CPP_
#define PDMAT_CPP_

#include "detail/matops.cpp"
#include "matrix/imatrix.cpp"
#include <vector>

namespace mpc {
namespace matrix {
template <class real_t, class int_t>
struct PDMat : public IMatrix<real_t, int_t> {
  // Constructors
  PDMat() = default;
  PDMat(const int_t);
  PDMat(const int_t, const int_t);
  PDMat(const int_t, std::vector<real_t>);
  PDMat(const int_t, const real_t *, const char = 'N');

  // Inherited functions
  void factorize();
  void backsolve(real_t *, const int_t = 1) override;
  void fastsolve(real_t *, const int_t = 1) const override;
  real_t &operator()(const std::size_t, const std::size_t) override;
  const real_t &operator()(const std::size_t, const std::size_t) const override;

  // Inherited functions (algorithm)
  void add_bounds(const real_t *) override;
  void add_bounds(const real_t *, const real_t *) override;

  // Own functions (not inherited)
  void semisolve(const char, real_t *, const int_t = 1);
  void fastsemisolve(const char, real_t *, const int_t = 1) const;
  void add_constraints(const int, const real_t *, const real_t *);

  // Just debugging purposes
  std::vector<real_t> &getfactor() { return factor; };
  const std::vector<real_t> &getfactor() const { return factor; };

private:
  std::vector<real_t> factor; // here, we assume std::allocator for vector
};                            // namespace matrix

// ************************************************************************** //
// ******************************* CONSTRUCTORS ***************************** //
// ************************************************************************** //
template <class real_t, class int_t>
PDMat<real_t, int_t>::PDMat(const int_t n)
    : IMatrix<real_t, int_t>{n, n, n * (n + 1) / 2}, factor{std::vector<real_t>(
                                                         n * (n + 1) / 2)} {}

template <class real_t, class int_t>
PDMat<real_t, int_t>::PDMat(const int_t m, const int_t n)
    : PDMat<real_t, int_t>{m} {}

template <class real_t, class int_t>
PDMat<real_t, int_t>::PDMat(const int_t n, std::vector<real_t> vals)
    : IMatrix<real_t, int_t>(n, n, n * (n + 1) / 2) {
  if (static_cast<int_t>(vals.size()) == n * (n + 1) / 2)
    IMatrix<real_t, int_t>::matrix = std::move(vals);
  else {
    auto s_first{vals.cbegin()}, s_last = s_first;
    auto d_first{IMatrix<real_t, int_t>::matrix.begin()};
    for (int_t skip = 0; skip < n; skip++) {
      s_last = s_first + n - skip;
      std::copy(s_first, s_last, d_first);
      d_first += n - skip;
      s_first += n + 1;
    }
  }
  std::copy(IMatrix<real_t, int_t>::matrix.cbegin(),
            IMatrix<real_t, int_t>::matrix.cend(), std::back_inserter(factor));
}

template <class real_t, class int_t>
PDMat<real_t, int_t>::PDMat(const int_t n, const real_t *vals, const char P)
    : IMatrix<real_t, int_t>(n, n, n * (n + 1) / 2) {
  if (P == 'P' || P == 'p')
    std::copy(vals, vals + IMatrix<real_t, int_t>::matrix.size(),
              IMatrix<real_t, int_t>::matrix.data());
  else {
    int_t s_first{0}, s_last = s_first;
    int_t d_first{0};
    for (int_t skip = 0; skip < n; skip++) {
      s_last = s_first + n - skip;
      std::copy(vals + s_first, vals + s_last,
                IMatrix<real_t, int_t>::matrix.data() + d_first);
      d_first += n - skip;
      s_first += n + 1;
    }
  }
  std::copy(IMatrix<real_t, int_t>::matrix.cbegin(),
            IMatrix<real_t, int_t>::matrix.cend(), std::back_inserter(factor));
}

// ************************************************************************** //
// ********************************* GETTERS ******************************** //
// ************************************************************************** //
template <class real_t, class int_t>
real_t &PDMat<real_t, int_t>::operator()(const std::size_t row,
                                         const std::size_t col) {
  auto M = std::max(col, row) + 1;
  auto m = std::min(col, row) + 1;
  return IMatrix<real_t, int_t>::matrix
      [M + ((2 * IMatrix<real_t, int_t>::ncols() - m) * (m - 1) / 2) - 1];
}

template <class real_t, class int_t>
const real_t &PDMat<real_t, int_t>::operator()(const std::size_t row,
                                               const std::size_t col) const {
  auto M = std::max(col, row) + 1;
  auto m = std::min(col, row) + 1;
  return IMatrix<real_t, int_t>::matrix
      [M + ((2 * IMatrix<real_t, int_t>::ncols() - m) * (m - 1) / 2) - 1];
}

// template <class real_t, class int_t>
// std::vector<real_t> &PDMat<real_t, int_t>::get_factor() {
//   return factor;
// }

// template <class real_t, class int_t>
// const std::vector<real_t> &PDMat<real_t, int_t>::get_factor() const {
//   return factor;
// }

// ************************************************************************** //
// ********************************** SOLVERS ******************************* //
// ************************************************************************** //
template <class real_t, class int_t> void PDMat<real_t, int_t>::factorize() {
  std::copy(IMatrix<real_t, int_t>::matrix.cbegin(),
            IMatrix<real_t, int_t>::matrix.cend(), factor.begin());
  mpc::detail::MatOps<real_t *>::ppfactor('L', IMatrix<real_t, int_t>::ncols(),
                                          factor.data());
}

template <class real_t, class int_t>
void PDMat<real_t, int_t>::fastsolve(real_t *b, int_t nrhs) const {
  mpc::detail::MatOps<real_t *>::ppsolve('L', IMatrix<real_t, int_t>::n, nrhs,
                                         factor.data(), b,
                                         IMatrix<real_t, int_t>::n);
}

template <class real_t, class int_t>
void PDMat<real_t, int_t>::backsolve(real_t *b, const int_t nrhs) {
  factorize();
  fastsolve(b, nrhs);
}

template <class real_t, class int_t>
void PDMat<real_t, int_t>::fastsemisolve(const char T, real_t *b,
                                         int_t nrhs) const {
  mpc::detail::MatOps<real_t *>::tpsolve(T, IMatrix<real_t, int_t>::n,
                                         factor.data(), b, nrhs);
}

template <class real_t, class int_t>
void PDMat<real_t, int_t>::semisolve(const char T, real_t *b,
                                     const int_t nrhs) {
  factorize();
  fastsemisolve(T, b, nrhs);
}

// ************************************************************************** //
// ******************************** ALGORITHM ******************************* //
// ************************************************************************** //
template <class real_t, class int_t>
void PDMat<real_t, int_t>::add_bounds(const real_t *x) {
  for (int_t i = 0; i < IMatrix<real_t, int_t>::n; i++)
    this->operator()(i, i) += x[i];
}

template <class real_t, class int_t>
void PDMat<real_t, int_t>::add_bounds(const real_t *x, const real_t *y) {
  for (int_t i = 0; i < IMatrix<real_t, int_t>::n; i++)
    this->operator()(i, i) += x[i] + y[i];
}

/**
 * @brief Performs the addition P += A' * D * A
 *
 * @tparam real_t Real type parameter
 * @tparam int_t Integer type parameter
 * @param k Number of linear constraints (length of D)
 * @param A Matrix in column major format
 * @param D Vector
 */
template <class real_t, class int_t>
void PDMat<real_t, int_t>::add_constraints(const int k, const real_t *A,
                                           const real_t *D) {
  for (int_t i = 0; i < k; i++)
    mpc::detail::MatOps<real_t *>::pprank(
        IMatrix<real_t, int_t>::n, D[i], A + i, k,
        IMatrix<real_t, int_t>::matrix.data());
}

} // namespace matrix
} // namespace mpc

#endif

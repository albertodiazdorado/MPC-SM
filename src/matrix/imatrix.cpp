#ifndef IMATRIX_CPP_
#define IMATRIX_CPP_

#include <vector>

namespace mpc {
namespace matrix {
template <class real_t, class int_t> struct IMatrix {
  // Constructors
  IMatrix() : m{0}, n{0}, matrix{std::vector<real_t>(0)} {};
  IMatrix(const int_t m, const int_t n)
      : m{m}, n{n}, matrix{std::vector<real_t>(m * n)} {}
  IMatrix(const int_t m, const int_t n, const int_t length)
      : m{m}, n{n}, matrix{std::vector<real_t>(length)} {}

  // Virtual destructor
  virtual ~IMatrix() = default;

  // Public interface (pure virtual methods)
  virtual void backsolve(real_t *, const int_t) = 0;
  virtual void fastsolve(real_t *, const int_t) const = 0;
  virtual real_t &operator()(const std::size_t, const std::size_t) = 0;
  virtual const real_t &operator()(const std::size_t,
                                   const std::size_t) const = 0;

  // Public interface(pure virtual methods) related to the solver
  virtual void add_bounds(const real_t *) = 0;
  virtual void add_bounds(const real_t *, const real_t *) = 0;

  // Public interface (non virtual)
  int_t nrows() const { return m; };
  int_t ncols() const { return n; };
  real_t *data() { return matrix.data(); };
  const real_t *data() const { return matrix.data(); }
  real_t &operator[](const std::size_t idx) { return matrix[idx]; };
  const real_t &operator[](const std::size_t idx) const { return matrix[idx]; };

protected:
  int_t m, n;
  std::vector<real_t> matrix;
};
} // namespace matrix
} // namespace mpc

#endif

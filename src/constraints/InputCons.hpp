#ifndef INPUT_CONS_HPP_
#define INPUT_CONS_HPP_

#include <vector>

#include "constraints/SoftCons.cpp"
#include "detail/traits.cpp"
#include "matrix/diagmat.cpp"
#include "matrix/pdmat.cpp"

namespace mpc {
namespace InputCons {

// ********************************************************************* //
// ************************** SIMPLE BOUNDS **************************** //
// ********************************************************************* //
template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons = mpc::SoftCons::Unconstrained>
class SimpleBounds : public SoftCons<real_t, int_t> {
private:
  using SoC = SoftCons<real_t, int_t>;
  using mat_t = typename std::conditional<
      (detail::is_pdmat<R_t>::value) || (detail::softcons<SoftCons>::linear),
      matrix::PDMat<real_t, int_t>, matrix::DiagMat<real_t, int_t>>::type;

  // Problem data
  const int_t m, n, T;
  R_t<real_t, int_t> R;
  std::vector<real_t> B;
  const std::vector<real_t> lb, ub;

  // Variables
  std::vector<real_t> u, u_aff, u_cc;
  std::vector<real_t> lambda, s, d;
  std::vector<real_t> lambda_aff, s_aff;
  std::vector<real_t> lambda_cc, s_cc;
  std::vector<real_t> rdu, ri;
  std::vector<mat_t> phir;
  std::vector<real_t> phirBt;
  real_t alpha;
  std::vector<real_t> aux;
  std::vector<real_t> Xr;

  void initialize(std::false_type);
  void initialize(std::true_type);
  void aff_fs(std::false_type, real_t *L, real_t *nu, real_t *nu_aff);
  void aff_fs(std::true_type, real_t *L, real_t *nu, real_t *nu_aff);
  void aff_fs(std::false_type, real_t *L, real_t *nu, real_t *nu_aff, int_t k);
  void aff_fs(std::true_type, real_t *L, real_t *nu, real_t *nu_aff, int_t k);

public:
  SimpleBounds() = default;
  SimpleBounds(const int_t n, const int_t m, const int_t T,
               R_t<real_t, int_t> &R, std::vector<real_t> &B,
               std::vector<real_t> &lb, std::vector<real_t> &ub,
               std::vector<real_t> &F = std::vector<real_t>(0),
               std::vector<real_t> &f = std::vector<real_t>(0),
               std::vector<real_t> &slb = std::vector<real_t>(0),
               std::vector<real_t> &sub = std::vector<real_t>(0),
               std::vector<real_t> &sF = std::vector<real_t>(0),
               std::vector<real_t> &sf = std::vector<real_t>(0));

  void aff_fs(real_t *L, real_t *nu, real_t *nu_aff);
  void aff_fs(real_t *L, real_t *nu, real_t *nu_aff, int_t k);
  void aff_ic();
  void aff_bs(const real_t *nu_aff, const int_t k);
  void aff_fc();

  void cc_pc(real_t sigma_mu_aff);
  void cc_fs(real_t *nu_cc);
  void cc_fs(real_t *nu_cc, int_t k);
  void cc_ic();
  void cc_bs(const real_t *nu_cc);
  void cc_bs(const real_t *nu_cc, int_t k);
  void cc_fc();

  real_t duality_gap() const;
  real_t affine_gap(const real_t &, const real_t &);
  real_t affine_gap(const real_t &);
  void linesearch(real_t &alpha_p, real_t &alpha_d);
  void linesearch(real_t &alpha);
  void take_step(const real_t);
  void new_x0();
  void eq_residual(real_t *, const int_t) const;

  std::vector<real_t> optimal_input() const { return u; };
  void opt_val(real_t &) const;
  int_t ncons() const;
};

} // namespace InputCons
} // namespace mpc

#endif
#ifndef SOFT_CONS_CPP_
#define SOFT_CONS_CPP_

#include <algorithm>
#include <cmath>
#include <vector>

#include "detail/matops.cpp"
#include "matrix/diagmat.cpp"
#include "matrix/pdmat.cpp"

namespace mpc {

namespace matrix {
template <class val_t, class idx_t> struct IMatrix;
template <class val_t, class idx_t> struct DiagMat;
template <class val_t, class idx_t> struct PDMat;
} // namespace matrix

namespace SoftCons {

// Forward declarations
template <class real_t, class int_t> struct Unconstrained;
template <class real_t, class int_t> struct SimpleBounds;
template <class real_t, class int_t> struct LinearBounds;
template <class real_t, class int_t> struct FullBounds;

// ********************************************************************* //
// ************************* UNCONSTRAINED ***************************** //
// ********************************************************************* //
template <class real_t, class int_t> struct Unconstrained {
  // Constructors
  Unconstrained() = default;
  Unconstrained(int_t T, std::vector<real_t> &slb, std::vector<real_t> &sub,
                std::vector<real_t> &sF, std::vector<real_t> &sf){};

  // Virtual destructor
  ~Unconstrained() = default;

  void precompute(const std::vector<real_t> &x) {
    // Do nothing
  }

  void augment_phi(mpc::matrix::DiagMat<real_t, int_t> &Phi, const int_t i) {
    // Do nothing
  }

  void augment_phi(mpc::matrix::PDMat<real_t, int_t> &Phi, const int_t i) {
    // Do nothing
  }

  void augment_rd(real_t *rd, const int_t i) {
    // Do nothing
  }

  void opt_val(real_t &opt_val, const real_t *x, const int_t i) {
    // Do nothing
  }

  void new_x0() {
    // Do nothing
  }
};

// ********************************************************************* //
// ************************* SIMPLE BOUNDS ***************************** //
// ********************************************************************* //
template <class real_t, class int_t> struct SimpleBounds {
private:
  int_t T, n;
  std::vector<real_t> lb, ub;
  std::vector<real_t> e, dg, dh;
  std::vector<real_t> violation;
  real_t rho{1000};

public:
  // Constructors
  SimpleBounds() = default;
  SimpleBounds(int_t _T, std::vector<real_t> &_lb, std::vector<real_t> &_ub,
               std::vector<real_t> &sF, std::vector<real_t> &sf)
      : T{_T}, n{static_cast<int_t>(_lb.size())}, lb{std::move(_lb)},
        ub{std::move(_ub)}, e{std::vector<real_t>(2 * T * n)}, dg{e}, dh{e},
        violation{std::vector<real_t>(2 * n)} {}

  ~SimpleBounds() = default;

  void new_x0() { rho = 1000; }

  void precompute(const std::vector<real_t> &x) {
    // rho update rule
    rho *= 1000;

    // Evaluate dg (gradient) and dh (hessian) for the given point x
    for (int_t i = 0; i < T; i++) {
      std::transform(x.cbegin() + n * i, x.cbegin() + (i + 1) * n, ub.cbegin(),
                     e.begin() + 2 * n * i, std::minus<real_t>());
      std::transform(lb.cbegin(), lb.cend(), x.cbegin() + i * n,
                     e.begin() + 2 * n * i + n, std::minus<real_t>());
    }

    std::transform(e.cbegin(), e.cend(), e.begin(),
                   [](real_t e) { return 1 * e; });

    for (int_t i = 0; i < 2 * T * n; i++) {
      if (e[i] < 0) {
        e[i] = std::exp(rho * e[i]);
        dg[i] = e[i] / (1 + e[i]);
        dh[i] = rho * e[i] / (1 + e[i]) / (1 + e[i]);
      } else {
        e[i] = std::exp(-rho * e[i]);
        dg[i] = 1 / (1 + e[i]);
        dh[i] = rho * e[i] / (1 + e[i]) / (1 + e[i]);
      }
    }
  }

  void augment_phi(mpc::matrix::DiagMat<real_t, int_t> &Phi, const int_t i) {
    Phi.add_bounds(dh.data() + 2 * n * i, dh.data() + 2 * n * i + n);
  }

  void augment_phi(mpc::matrix::PDMat<real_t, int_t> &Phi, const int_t i) {
    Phi.add_bounds(dh.data() + 2 * n * i, dh.data() + 2 * n * i + n);
  }

  void augment_rd(real_t *rd, const int_t i) {
    for (int_t j = 0; j < n; j++)
      rd[j] += dg[2 * n * i + j] - dg[2 * n * i + n + j];
  }

  void opt_val(real_t &opt_val, const real_t *x, const int_t i) {
    std::transform(ub.data(), ub.data() + n, x, violation.data(),
                   [](const real_t a, const real_t b) { return a - b; });
    std::transform(lb.data(), lb.data() + n, x, violation.data() + n,
                   [](const real_t a, const real_t b) { return b - a; });
    opt_val -= std::min(real_t{0},
                        *std::min_element(violation.begin(), violation.end())) *
               2;
  }
};
} // namespace SoftCons
} // namespace mpc

#endif
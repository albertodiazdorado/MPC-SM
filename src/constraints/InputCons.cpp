#ifndef INPUT_CONS_CPP_
#define INPUT_CONS_CPP_

#include <algorithm>
#include <limits>
#include <vector>

#include "constraints/InputCons.hpp"
#include "detail/matops.cpp"
#include "matrix/diagmat.cpp"
#include "matrix/pdmat.cpp"

namespace mpc {
namespace InputCons {
// ********************************************************************* //
// ***************************** BOUNDS ******************************** //
// ********************************************************************* //
template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
SimpleBounds<real_t, int_t, R_t, SoftCons>::SimpleBounds(
    const int_t n, const int_t m, const int_t T, R_t<real_t, int_t> &R,
    std::vector<real_t> &B, std::vector<real_t> &_lb, std::vector<real_t> &_ub,
    std::vector<real_t> &F, std::vector<real_t> &f, std::vector<real_t> &slb,
    std::vector<real_t> &sub, std::vector<real_t> &sF, std::vector<real_t> &sf)
    : SoC{T, slb, sub, sF, sf}, m{m}, n{n}, T{T}, R{std::move(R)},
      B{std::move(B)}, lb{std::move(_lb)}, ub{std::move(_ub)},
      u{std::vector<real_t>(T * m)}, u_aff{std::vector<real_t>(T * m)},
      u_cc{std::vector<real_t>(T * m)}, lambda{std::vector<real_t>(2 * m * T,
                                                                   real_t{10})},
      s{std::vector<real_t>(2 * m * T, real_t{10})}, d{std::vector<real_t>(
                                                         2 * m * T,
                                                         real_t{10})},
      lambda_aff{std::vector<real_t>(2 * m * T, real_t{10})},
      s_aff{std::vector<real_t>(2 * m * T, real_t{10})},
      lambda_cc{std::vector<real_t>(2 * m * T, real_t{10})},
      s_cc{std::vector<real_t>(2 * m * T, real_t{10})}, rdu{std::vector<real_t>(
                                                            T * m)},
      ri{std::vector<real_t>(2 * T * m)}, phir{std::vector<mat_t>(T)},
      phirBt{std::vector<real_t>(T * n * m)}, aux{std::vector<real_t>(
                                                  2 * m * T, real_t{10})} {
  // Transpose B
  detail::transpose(n, m, this->B);

  // Initialize u to the middle point of the constraints
  std::transform(ub.cbegin(), ub.cend(), lb.cbegin(),
                 u.begin(), [](real_t lb, real_t ub) { return (lb + ub) / 2; });
  std::transform(ub.cbegin(), ub.cend(), u.cbegin(), s.begin(),
                 [](real_t ub, real_t u) { return ub - u; });
  std::transform(lb.cbegin(), lb.cend(), u.cbegin(), s.begin() + m,
                 [](real_t lb, real_t u) { return u - lb; });
  for (int_t i = 1; i < T; i++) {
    std::copy(u.cbegin(), u.cbegin() + m, u.begin() + m * i);
    std::copy(s.cbegin(), s.cbegin() + 2 * m, s.begin() + 2 * m * i);
  }
  std::transform(lambda.cbegin(), lambda.cend(), s.cbegin(), d.begin(),
                 std::divides<real_t>());

  initialize(typename std::conditional < detail::is_pdmat<R_t>::value ||
                 detail::softcons<SoftCons>::linear,
             std::true_type, std::false_type > ::type{});
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::initialize(std::false_type) {}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::initialize(std::true_type) {
  this->Xr = std::vector<real_t>(n * m);
  R.factorize();
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::aff_fs(real_t *L, real_t *nu,
                                                        real_t *nu_aff) {
  SoC::precompute(u);
  aff_fs(typename std::conditional < detail::is_pdmat<R_t>::value ||
             detail::softcons<SoftCons>::linear,
         std::true_type, std::false_type > ::type{}, L, nu, nu_aff);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::aff_fs(std::false_type,
                                                        real_t *L, real_t *nu,
                                                        real_t *nu_aff) {
  // Copies s_u into ri_u, for the whole vector (all time steps)
  std::copy(s.cbegin(), s.cend(), ri.begin());
  // Initialize first block in the Shur Complement
  phir[0] = R;
  phir[0].add_bounds(d.data(), d.data() + m);
  SoC::augment_phi(phir[0], 0);
  for (int_t i = 0; i < m; i++)
    mpc::detail::MatOps<real_t *>::pprank(n, 1 / phir[0][i], B.data() + i, m,
                                          L);
  // Creates the first block in ri (ri is initialized to s)
  mpc::detail::MatOps<real_t *>::vecsum(m, 1., u.data(), ri.data());
  mpc::detail::MatOps<real_t *>::vecsum(m, -1., u.data(), ri.data() + m);
  mpc::detail::MatOps<real_t *>::vecsum(m, -1., ub.data(), ri.data());
  mpc::detail::MatOps<real_t *>::vecsum(m, 1., lb.data(), ri.data() + m);
  // Create rdu
  detail::affasign(R, u.data(), rdu.data());
  detail::MatOps<real_t *>::affoperator('N', m, n, 1., B.data(), m, nu, 1.,
                                        rdu.data());
  for (int_t i = 0; i < m; i++)
    rdu[i] += ri[i] * d[i] - ri[m + i] * d[m + i];
  SoC::augment_rd(rdu.data(), 0);
  // Create the semiproduct phirBt = inv(phir)*Bt
  std::copy(B.data(), B.data() + m * n, phirBt.data());
  phir[0].backsolve(phirBt.data(), n);
  // Creates the residual of the Shur Complement (stored in nu_aff)
  mpc::detail::MatOps<real_t *>::affoperator('T', m, n, -1., phirBt.data(), m,
                                             rdu.data(), 1., nu_aff);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::aff_fs(std::true_type,
                                                        real_t *L, real_t *nu,
                                                        real_t *nu_aff) {
  // Copies s_u into ri_u, for the whole vector (all time steps)
  std::copy(s.cbegin(), s.cend(), ri.begin());
  // Initialize first block in the Shur Complement
  phir[0] = R;
  phir[0].add_bounds(d.data(), d.data() + m);
  SoC::augment_phi(phir[0], 0);
  phir[0].factorize();
  std::copy(B.cbegin(), B.cend(), Xr.begin());
  phir[0].fastsemisolve('N', Xr.data(), n);
  std::fill(L, L + n * (n + 1) / 2, real_t{0});
  mpc::detail::MatOps<real_t *>::pprankkt(n, 1., Xr.data(), L, m);
  // Creates the first block in ri (ri is initialized to s)
  mpc::detail::MatOps<real_t *>::vecsum(m, 1., u.data(), ri.data());
  mpc::detail::MatOps<real_t *>::vecsum(m, -1., u.data(), ri.data() + m);
  mpc::detail::MatOps<real_t *>::vecsum(m, -1., ub.data(), ri.data());
  mpc::detail::MatOps<real_t *>::vecsum(m, 1., lb.data(), ri.data() + m);
  // Create rdu
  detail::affasign(R, u.data(), rdu.data());
  detail::MatOps<real_t *>::affoperator('N', m, n, 1., B.data(), m, nu, 1.,
                                        rdu.data());
  for (int_t i = 0; i < m; i++)
    rdu[i] += ri[i] * d[i] - ri[m + i] * d[m + i];
  SoC::augment_rd(rdu.data(), 0);
  // Create the semiproduct phirBt = inv(phir)*Bt
  std::copy(B.data(), B.data() + m * n, phirBt.data());
  phir[0].fastsolve(phirBt.data(), n);
  // Creates the residual of the Shur Complement (stored in nu)
  mpc::detail::MatOps<real_t *>::affoperator('T', m, n, -1., phirBt.data(), m,
                                             rdu.data(), 1., nu_aff);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::aff_fs(real_t *L, real_t *nu,
                                                        real_t *nu_aff,
                                                        int_t k) {
  aff_fs(typename std::conditional < detail::is_pdmat<R_t>::value ||
             detail::softcons<SoftCons>::linear,
         std::true_type, std::false_type > ::type{}, L, nu, nu_aff, k);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::aff_fs(std::false_type,
                                                        real_t *L, real_t *nu,
                                                        real_t *nu_aff,
                                                        int_t k) {
  // Update the corresponding block in the Shur Complement
  phir[k] = R;
  phir[k].add_bounds(d.data() + 2 * m * k, d.data() + 2 * m * k + m);
  SoC::augment_phi(phir[k], k);
  for (int_t i = 0; i < m; i++)
    detail::MatOps<real_t *>::pprank(n, 1 / phir[k][i], B.data() + i, m, L);
  // Creates the first block in ri (ri is initialized to s)
  detail::MatOps<real_t *>::vecsum(m, 1., u.data() + k * m,
                                   ri.data() + 2 * m * k);
  detail::MatOps<real_t *>::vecsum(m, -1., u.data() + k * m,
                                   ri.data() + 2 * m * k + m);
  detail::MatOps<real_t *>::vecsum(m, -1., ub.data(), ri.data() + 2 * m * k);
  detail::MatOps<real_t *>::vecsum(m, 1., lb.data(), ri.data() + 2 * m * k + m);
  // Create rdu
  detail::affasign(R, u.data() + k * m, rdu.data() + k * m);
  detail::MatOps<real_t *>::affoperator('N', m, n, 1., B.data(), m, nu, 1.,
                                        rdu.data() + k * m);
  for (int_t i = 0; i < m; i++)
    rdu[k * m + i] += ri[2 * k * m + i] * d[2 * k * m + i] -
                      ri[2 * k * m + m + i] * d[2 * k * m + m + i];
  SoC::augment_rd(rdu.data() + k * m, k);
  // Create the semiproduct phirBt = inv(phir)*Bt
  std::copy(B.data(), B.data() + m * n, phirBt.data() + k * m * n);
  phir[k].backsolve(phirBt.data() + k * m * n, n);
  // Updates the residual of the Shur Complement (stored in nu)
  detail::MatOps<real_t *>::affoperator('T', m, n, -1.,
                                        phirBt.data() + k * m * n, m,
                                        rdu.data() + k * m, 1., nu_aff);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::aff_fs(std::true_type,
                                                        real_t *L, real_t *nu,
                                                        real_t *nu_aff,
                                                        int_t k) {
  // Initialize first block in the Shur Complement
  phir[k] = R;
  phir[k].add_bounds(d.data() + 2 * k * m, d.data() + 2 * k * m + m);
  SoC::augment_phi(phir[k], k);
  phir[k].factorize();
  std::copy(B.cbegin(), B.cend(), Xr.begin());
  phir[k].fastsemisolve('N', Xr.data(), n);
  detail::MatOps<real_t *>::pprankkt(n, 1., Xr.data(), L, m);
  // Creates the first block in ri (ri is initialized to s)
  detail::MatOps<real_t *>::vecsum(m, 1., u.data() + k * m,
                                   ri.data() + 2 * m * k);
  detail::MatOps<real_t *>::vecsum(m, -1., u.data() + k * m,
                                   ri.data() + 2 * m * k + m);
  detail::MatOps<real_t *>::vecsum(m, -1., ub.data(), ri.data() + 2 * m * k);
  detail::MatOps<real_t *>::vecsum(m, 1., lb.data(), ri.data() + 2 * m * k + m);
  // Create rdu
  detail::affasign(R, u.data() + k * m, rdu.data() + k * m);
  detail::MatOps<real_t *>::affoperator('N', m, n, 1., B.data(), m, nu, 1.,
                                        rdu.data() + k * m);
  for (int_t i = 0; i < m; i++)
    rdu[k * m + i] += ri[2 * k * m + i] * d[2 * k * m + i] -
                      ri[2 * k * m + m + i] * d[2 * k * m + m + i];
  SoC::augment_rd(rdu.data() + k * m, k);
  // Create the semiproduct phirBt = inv(phir)*Bt
  std::copy(B.data(), B.data() + m * n, phirBt.data() + k * m * n);
  phir[k].fastsolve(phirBt.data() + k * m * n, n);
  // Creates the residual of the Shur Complement (stored in nu)
  detail::MatOps<real_t *>::affoperator('T', m, n, -1.,
                                        phirBt.data() + k * m * n, m,
                                        rdu.data() + k * m, 1., nu_aff);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::aff_ic() {
  // Copy rdu into u_aff (helpful for backsolving)
  std::transform(rdu.cbegin(), rdu.cend(), u_aff.begin(),
                 std::negate<real_t>());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::aff_bs(const real_t *nu_aff,
                                                        const int_t k) {
  // Calculate u_affine[i], which is already initialized to -rdu[i]
  phir[k].fastsolve(u_aff.data() + k * m);
  mpc::detail::MatOps<real_t *>::affoperator('N', m, n, -1.,
                                             phirBt.data() + k * n * m, m,
                                             nu_aff, 1., u_aff.data() + m * k);
  // Calculate "lambda_aff[T-1]=Fu*u_aff+Fu*u-fu" & "s_aff[T-1]=-Fu*u_aff-ri"
  std::copy(u_aff.data() + k * m, u_aff.data() + (k + 1) * m,
            lambda_aff.begin() + k * 2 * m);
  std::transform(lambda_aff.cbegin() + 2 * k * m,
                 lambda_aff.cbegin() + 2 * k * m + m,
                 lambda_aff.begin() + 2 * k * m + m, std::negate<real_t>());
  // Evaluate s_aff
  std::transform(lambda_aff.cbegin() + 2 * k * m,
                 lambda_aff.cbegin() + 2 * (k + 1) * m,
                 s_aff.begin() + 2 * k * m, std::negate<real_t>());
  std::transform(s_aff.cbegin() + 2 * k * m, s_aff.cbegin() + 2 * (k + 1) * m,
                 ri.cbegin() + 2 * k * m, s_aff.begin() + 2 * k * m,
                 std::minus<real_t>());
  // Evaluate lambda_aff
  mpc::detail::MatOps<real_t *>::vecsum(m, 1., u.data() + k * m,
                                        lambda_aff.data() + 2 * k * m);
  mpc::detail::MatOps<real_t *>::vecsum(m, -1., ub.data(),
                                        lambda_aff.data() + 2 * k * m);
  mpc::detail::MatOps<real_t *>::vecsum(m, -1., u.data() + k * m,
                                        lambda_aff.data() + 2 * k * m + m);
  mpc::detail::MatOps<real_t *>::vecsum(m, 1., lb.data(),
                                        lambda_aff.data() + 2 * k * m + m);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::aff_fc() {
  // Final evaluation lambda_aff *= d
  std::transform(lambda_aff.cbegin(), lambda_aff.cend(), d.cbegin(),
                 lambda_aff.begin(), std::multiplies<real_t>());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::cc_pc(real_t sigma_mu_aff) {
  // Evaluates rs and stores it in ri
  for (int_t i = 0; i < 2 * T * m; i++)
    ri[i] = (s_aff[i] * lambda_aff[i] - sigma_mu_aff) / s[i];
  std::fill(rdu.begin(), rdu.end(), real_t{0});
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::cc_fs(real_t *nu_cc) {
  // Initialize nu_cc to zero (equality residual)
  std::fill(nu_cc, nu_cc + T * n, real_t{0});
  // Create first block in rd
  std::transform(ri.cbegin(), ri.cbegin() + m, rdu.begin(),
                 std::negate<real_t>());
  detail::MatOps<real_t *>::vecsum(m, 1, ri.data() + m, rdu.data());
  // Initialize first block in rsc = (phirBt)^T*rd (stored in nu)
  mpc::detail::MatOps<real_t *>::affoperator('T', m, n, -1., phirBt.data(), m,
                                             rdu.data(), 0., nu_cc);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::cc_fs(real_t *nu_cc, int_t k) {
  // Create i-th block in rd
  std::transform(ri.cbegin() + 2 * k * m, ri.cbegin() + 2 * k * m + m,
                 rdu.data() + k * m, std::negate<real_t>());
  detail::MatOps<real_t *>::vecsum(m, 1., ri.data() + 2 * k * m + m,
                                   rdu.data() + k * m);
  // Update i-th block in rsc += (phirBt)^T*rd (stored in nu)
  mpc::detail::MatOps<real_t *>::affoperator('T', m, n, -1.,
                                             phirBt.data() + k * m * n, m,
                                             rdu.data() + k * m, 1., nu_cc);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::cc_ic() {
  // Copy rdu into u_aff (helpful for backsolving)
  std::transform(rdu.cbegin(), rdu.cend(), u_cc.begin(), std::negate<real_t>());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::cc_bs(const real_t *nu_cc) {
  // Calculate u_cc[T-1], which is initialized to -rdu
  phir[T - 1].fastsolve(u_cc.data() + (T - 1) * m);
  mpc::detail::MatOps<real_t *>::affoperator(
      'N', m, n, -1., phirBt.data() + (T - 1) * m * n, m, nu_cc, 1.,
      u_cc.data() + m * (T - 1));
  // Calculate "lambda_cc[T-1]=Fu*u_cc+Fu*u-fu" & "s_aff[T-1]=-Fu*u_aff"
  std::copy(u_cc.data() + (T - 1) * m, u_cc.data() + T * m,
            lambda_cc.begin() + (T - 1) * 2 * m);
  std::transform(lambda_cc.cbegin() + 2 * (T - 1) * m,
                 lambda_cc.cbegin() + 2 * (T - 1) * m + m,
                 lambda_cc.begin() + 2 * (T - 1) * m + m,
                 std::negate<real_t>());
  // Evaluate s_cc
  std::transform(lambda_cc.cbegin() + 2 * (T - 1) * m,
                 lambda_cc.cbegin() + 2 * T * m, s_cc.begin() + 2 * (T - 1) * m,
                 std::negate<real_t>());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::cc_bs(const real_t *nu_cc,
                                                       const int_t k) {
  // Calculate u_cc[T-1], which is initialized to -rdu
  phir[k].fastsolve(u_cc.data() + k * m);
  mpc::detail::MatOps<real_t *>::affoperator('N', m, n, -1.,
                                             phirBt.data() + k * m * n, m,
                                             nu_cc, 1., u_cc.data() + m * k);
  // Calculate "lambda_cc[T-1]=Fu*u_cc+Fu*u-fu" & "s_aff[T-1]=-Fu*u_aff"
  std::copy(u_cc.data() + k * m, u_cc.data() + (k + 1) * m,
            lambda_cc.begin() + k * 2 * m);
  std::transform(lambda_cc.cbegin() + 2 * k * m,
                 lambda_cc.cbegin() + 2 * k * m + m,
                 lambda_cc.begin() + 2 * k * m + m, std::negate<real_t>());
  // Evaluate s_cc
  std::transform(lambda_cc.cbegin() + 2 * k * m,
                 lambda_cc.cbegin() + 2 * (k + 1) * m, s_cc.begin() + 2 * k * m,
                 std::negate<real_t>());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::cc_fc() {
  // Final computation lambda_cc = d * (lambda_cc - inv(LAMBDA)*rs)
  for (int_t i = 0; i < 2 * T * m; i++)
    lambda_cc[i] = d[i] * lambda_cc[i] - ri[i];
  // Aggregate affine and cc steps
  detail::MatOps<real_t *>::vecsum(T * m, real_t{1}, u_cc.data(), u_aff.data());
  detail::MatOps<real_t *>::vecsum(2 * T * m, 1., lambda_cc.data(),
                                   lambda_aff.data());
  detail::MatOps<real_t *>::vecsum(2 * T * m, 1., s_cc.data(), s_aff.data());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
real_t SimpleBounds<real_t, int_t, R_t, SoftCons>::duality_gap() const {
  return mpc::detail::MatOps<real_t *>::dotproduct(2 * T * m, lambda.data(),
                                                   s.data());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
real_t
SimpleBounds<real_t, int_t, R_t, SoftCons>::affine_gap(const real_t &alpha_p,
                                                       const real_t &alpha_d) {
  s_cc = s;
  lambda_cc = lambda;
  detail::MatOps<real_t *>::vecsum(2 * T * m, alpha_p, s_aff.data(),
                                   s_cc.data());
  detail::MatOps<real_t *>::vecsum(2 * T * m, alpha_d, lambda_aff.data(),
                                   lambda_cc.data());
  return mpc::detail::MatOps<real_t *>::dotproduct(2 * T * m, lambda_cc.data(),
                                                   s_cc.data());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
real_t
SimpleBounds<real_t, int_t, R_t, SoftCons>::affine_gap(const real_t &alpha) {
  s_cc = s;
  lambda_cc = lambda;
  detail::MatOps<real_t *>::vecsum(2 * T * m, alpha, s_aff.data(), s_cc.data());
  detail::MatOps<real_t *>::vecsum(2 * T * m, alpha, lambda_aff.data(),
                                   lambda_cc.data());
  return mpc::detail::MatOps<real_t *>::dotproduct(2 * T * m, lambda_cc.data(),
                                                   s_cc.data());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::linesearch(real_t &alpha_p,
                                                            real_t &alpha_d) {
  // Performs linesearch on affine vector
  alpha_p = alpha_d = std::numeric_limits<float>::lowest();
  std::transform(s.cbegin(), s.cend(), s_aff.cbegin(), aux.begin(),
                 std::divides<real_t>());
  for (const auto &elem : aux) {
    if (elem >= 0)
      continue;
    if (elem > alpha_p)
      alpha_p = elem;
  }
  std::transform(lambda.cbegin(), lambda.cend(), lambda_aff.cbegin(),
                 aux.begin(), std::divides<real_t>());
  for (const auto &elem : aux) {
    if (elem >= 0)
      continue;
    if (elem > alpha_d)
      alpha_d = elem;
  }
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::linesearch(real_t &alpha) {
  // Performs linesearch on affine vector
  alpha = std::numeric_limits<float>::lowest();
  std::transform(s.cbegin(), s.cend(), s_aff.cbegin(), aux.begin(),
                 std::divides<real_t>());
  for (const auto &elem : aux) {
    if (elem >= 0)
      continue;
    if (elem > alpha)
      alpha = elem;
  }
  std::transform(lambda.cbegin(), lambda.cend(), lambda_aff.cbegin(),
                 aux.begin(), std::divides<real_t>());
  for (const auto &elem : aux) {
    if (elem >= 0)
      continue;
    if (elem > alpha)
      alpha = elem;
  }
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::take_step(const real_t a) {
  detail::MatOps<real_t *>::vecsum(T * m, a, u_aff.data(), u.data());
  detail::MatOps<real_t *>::vecsum(2 * T * m, a, lambda_aff.data(),
                                   lambda.data());
  detail::MatOps<real_t *>::vecsum(2 * T * m, a, s_aff.data(), s.data());
  std::transform(lambda.cbegin(), lambda.cend(), s.cbegin(), d.begin(),
                 std::divides<real_t>());
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::new_x0() {
  // Rotate
  std::rotate(u.begin(), u.begin() + m, u.end());
  std::rotate(lambda.begin(), lambda.begin() + 2 * m, lambda.end());
  std::rotate(s.begin(), s.begin() + 2 * m, s.end());
  // Complete the tail
  std::copy(u.cbegin() + (T - 2) * m, u.cbegin() + (T - 1) * m,
            u.begin() + (T - 1) * m);
  std::copy(lambda.cbegin() + 2 * (T - 2) * m,
            lambda.cbegin() + 2 * (T - 1) * m,
            lambda.begin() + 2 * (T - 1) * m);
  std::copy(s.cbegin() + 2 * (T - 2) * m, s.cbegin() + 2 * (T - 1) * m,
            s.begin() + 2 * (T - 1) * m);
  // Add some constant
  std::transform(lambda.cbegin(), lambda.cend(), lambda.begin(),
                 [](real_t input) { return input + 1; });
  std::transform(s.cbegin(), s.cend(), s.begin(),
                 [](real_t input) { return input + 1; });
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::eq_residual(
    real_t *nu_aff, const int_t i) const {
  detail::MatOps<real_t *>::affoperator('T', m, n, real_t{1}, B.data(), m,
                                        u.data() + m * i, real_t{1}, nu_aff);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, R_t, SoftCons>::opt_val(
    real_t &opt_val) const {
  for (int_t i = 0; i < T; i++)
    opt_val += detail::quadratic_form(R, u.data() + i * m);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class SoftCons>
int_t SimpleBounds<real_t, int_t, R_t, SoftCons>::ncons() const {
  return 2 * T * m;
}

// ********************************************************************* //
// ************************* END BOUNDS ******************************** //
// ********************************************************************* //

} // namespace InputCons
} // namespace mpc

#endif
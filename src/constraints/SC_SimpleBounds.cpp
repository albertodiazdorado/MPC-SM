#ifndef SC_SIMPLE_BOUNDS_CPP_
#define SC_SIMPLE_BOUNDS_CPP_

#include <algorithm>
#include <vector>

#include "constraints/StateCons.hpp"
#include "detail/matops.cpp"

namespace mpc {
namespace StateCons {

// ********************************************************************* //
// ************************* SIMPLE BOUNDS ***************************** //
// ********************************************************************* //
// Constructor
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::SimpleBounds(
    const int_t n, const int_t m, const int_t T, Q_t<real_t, int_t> &Q,
    P_t<real_t, int_t> &P, std::vector<real_t> &A, std::vector<real_t> &lb,
    std::vector<real_t> &ub, std::vector<real_t> &F, std::vector<real_t> &f,
    std::vector<real_t> &slb, std::vector<real_t> &sub, std::vector<real_t> &sF,
    std::vector<real_t> &sf, std::vector<real_t> &lbf, std::vector<real_t> &ubf,
    std::vector<real_t> &Ff, std::vector<real_t> &ff, std::vector<real_t> &slbf,
    std::vector<real_t> &subf, std::vector<real_t> &sFf,
    std::vector<real_t> &sff)
    : SoC{T, slb, sub, sF, sf}, TC{n,    static_cast<int_t>(2 * n),
                                   lbf,  ubf,
                                   Ff,   ff,
                                   slbf, subf,
                                   sFf,  sff},
      m{m}, n{n}, T{T}, Q{std::move(Q)}, P{std::move(P)}, A{std::move(A)},
      lb{std::move(lb)}, ub{std::move(ub)}, k{static_cast<int_t>(
                                                int_t{2} * n * (T - int_t{1}) +
                                                TC::k)},
      x{std::vector<real_t>(T * n)}, x_aff{x}, x_cc{x},
      rdx{std::vector<real_t>(T * n)}, phiq{std::vector<mat_t>(T - int_t{1})},
      phip{this->P}, phiqAt{std::vector<real_t>(T * n * n)} {
  // Transpose A
  detail::transpose(n, n, this->A);

  // Initialize dual variables with the proper size
  lambda = std::vector<real_t>(k, real_t{10});
  lambda_aff = lambda_cc = lambda;
  s = s_cc = s_aff = lambda;
  d = aux = ri = lambda;

  // Initialize x to the middle point of the constraints
  std::transform(this->ub.cbegin(), this->ub.cend(), this->lb.cbegin(),
                 x.begin(), [](real_t lb, real_t ub) { return (lb + ub) / 2; });
  std::transform(this->ub.cbegin(), this->ub.cend(), x.cbegin(), s.begin(),
                 [](real_t ub, real_t x) { return ub - x; });
  std::transform(this->lb.cbegin(), this->lb.cend(), x.cbegin(), s.begin() + n,
                 [](real_t lb, real_t x) { return x - lb; });
  for (int_t i = 1; i < T; i++) {
    std::copy(x.cbegin(), x.cbegin() + n, x.begin() + n * i);
    std::copy(s.cbegin(), s.cbegin() + 2 * n, s.begin() + 2 * n * i);
  }
  TC::initialize_x(x.data() + n * (T - 1), s.data() + 2 * n * (T - 1));
  std::transform(lambda.cbegin(), lambda.cend(), s.cbegin(), d.begin(),
                 std::divides<real_t>());

  initialize(typename std::conditional < detail::is_pdmat<Q_t>::value ||
                 detail::softcons<SoftCons>::linear ||
                 detail::is_pdmat<P_t>::value,
             std::true_type, std::false_type > ::type{});
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::initialize(
    std::false_type) {}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::initialize(
    std::true_type) {
  this->Xq = std::vector<real_t>(n * n);
  identity = std::vector<real_t>(n * n);
  reset_identity();
}

// Affine solve, first block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_fb(
    real_t *L, real_t *M, const real_t *nu, real_t *nu_aff) {
  SoC::precompute(x);
  // Copy -nu into rdx
  std::transform(nu, nu + T * n, rdx.data(), std::negate<real_t>());
  // Call appropriate function
  aff_fs_fb(typename std::conditional < detail::is_pdmat<Q_t>::value ||
                detail::softcons<SoftCons>::linear,
            std::true_type, std::false_type > ::type{}, L, M, nu, nu_aff);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_fb(
    std::false_type, real_t *L, real_t *M, const real_t *nu, real_t *nu_aff) {
  // Copies s_x into ri_x, for the whole vector (all time steps)
  std::copy(s.cbegin(), s.cend(), ri.begin());
  // Update first block in the Shur Complement
  phiq[0] = Q;
  phiq[0].add_bounds(d.data(), d.data() + n);
  SoC::augment_phi(phiq[0], 0);
  for (int_t i = 0; i < n; i++)
    L[i + (2 * n - i - 1) * i / 2] += 1 / phiq[0][i];
  // Initialize next block in the Shur Complement
  for (int_t i = 0; i < n; i++)
    detail::MatOps<real_t *>::pprank(n, 1 / (phiq[0][i]), A.data() + i, n,
                                     L + n * (n + 1) / 2);
  // Creates the first block in ri (ri is initialized to s)
  detail::MatOps<real_t *>::vecsum(n, 1., x.data(), ri.data());
  detail::MatOps<real_t *>::vecsum(n, -1., x.data(), ri.data() + n);
  detail::MatOps<real_t *>::vecsum(n, -1., ub.data(), ri.data());
  detail::MatOps<real_t *>::vecsum(n, 1., lb.data(), ri.data() + n);
  // Create rdx
  detail::affoperator(Q, x.data(), rdx.data());
  detail::MatOps<real_t *>::affoperator('N', n, n, 1., A.data(), n, nu + n, 1.,
                                        rdx.data());
  for (int_t i = 0; i < n; i++)
    rdx[i] += ri[i] * d[i] - ri[n + i] * d[n + i];
  SoC::augment_rd(rdx.data(), 0);
  // Create the semiproduct phiaQt = inv(phiq[0])*At
  std::copy(A.data(), A.data() + n * n, phiqAt.data());
  phiq[0].backsolve(phiqAt.data(), n);
  std::transform(phiqAt.data(), phiqAt.data() + n * n, M,
                 std::negate<real_t>());
  // Updates the residual of the Shur Complement (stored in nu)
  std::copy(rdx.cbegin(), rdx.cbegin() + n, aux.begin());
  phiq[0].backsolve(aux.data());
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, aux.data(), nu_aff);
  // Updates the residual of the Shur Complement (next block)
  detail::MatOps<real_t *>::affoperator('T', n, n, -1., phiqAt.data(), n,
                                        rdx.data(), 1., nu_aff + n);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_fb(
    std::true_type, real_t *L, real_t *M, const real_t *nu, real_t *nu_aff) {
  // Copies s_u into ri_u, for the whole vector (all time steps)
  std::copy(s.cbegin(), s.cend(), ri.begin());
  // Initialize first block in the Shur Complement
  phiq[0] = Q;
  phiq[0].add_bounds(d.data(), d.data() + n);
  SoC::augment_phi(phiq[0], 0);
  phiq[0].factorize();
  std::copy(A.cbegin(), A.cend(), Xq.begin());
  phiq[0].fastsemisolve('N', Xq.data(), n);
  phiq[0].fastsolve(identity.data(), n);
  detail::MatOps<real_t *>::pd2pp(n, identity.data(), identity.data());
  detail::MatOps<real_t *>::vecsum(n + (n + 1) / 2, real_t{1}, identity.data(),
                                   L);
  reset_identity();
  // Initialize NEXT block in the Shur Complement
  detail::MatOps<real_t *>::pprankkt(n, 1., Xq.data(), L + n * (n + 1) / 2, n);
  // Creates the first block in ri (ri is initialized to s)
  detail::MatOps<real_t *>::vecsum(n, 1., x.data(), ri.data());
  detail::MatOps<real_t *>::vecsum(n, -1., x.data(), ri.data() + n);
  detail::MatOps<real_t *>::vecsum(n, -1., ub.data(), ri.data());
  detail::MatOps<real_t *>::vecsum(n, 1., lb.data(), ri.data() + n);
  // Create rdx
  detail::affoperator(Q, x.data(), rdx.data());
  detail::MatOps<real_t *>::affoperator('N', n, n, 1., A.data(), n, nu + n, 1.,
                                        rdx.data());
  for (int_t i = 0; i < n; i++)
    rdx[i] += ri[i] * d[i] - ri[n + i] * d[n + i];
  SoC::augment_rd(rdx.data(), 0);
  // Create the semiproduct phiaQt = inv(phiq)*At
  std::copy(A.data(), A.data() + n * n, phiqAt.data());
  phiq[0].fastsolve(phiqAt.data(), n);
  std::transform(phiqAt.data(), phiqAt.data() + n * n, M,
                 std::negate<real_t>());
  // Updates the residual of the Shur Complement (stored in nu)
  std::copy(rdx.cbegin(), rdx.cbegin() + n, aux.begin());
  phiq[0].fastsolve(aux.data());
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, aux.data(), nu_aff);
  // Updates the residual of the Shur Complement (next block)
  detail::MatOps<real_t *>::affoperator('T', n, n, -1., phiqAt.data(), n,
                                        rdx.data(), 1., nu_aff + n);
}

// Affine solve, kth block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_nb(
    real_t *L, real_t *M, const real_t *nu, real_t *nu_aff, const int_t k) {
  aff_fs_nb(typename std::conditional < detail::is_pdmat<Q_t>::value ||
                detail::softcons<SoftCons>::linear,
            std::true_type, std::false_type > ::type{}, L, M, nu, nu_aff, k);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_nb(
    std::false_type, real_t *L, real_t *M, const real_t *nu, real_t *nu_aff,
    int_t k) {
  // Update first block in the Shur Complement
  phiq[k] = Q;
  phiq[k].add_bounds(d.data() + 2 * n * k, d.data() + 2 * n * k + n);
  SoC::augment_phi(phiq[k], k);
  for (int_t i = 0; i < n; i++)
    L[i + (2 * n - i - 1) * i / 2] += 1 / phiq[k][i];
  // Initialize next block in the Shur Complement
  for (int_t i = 0; i < n; i++)
    detail::MatOps<real_t *>::pprank(n, 1 / phiq[k][i], A.data() + i, n,
                                     L + n * (n + 1) / 2);
  // Creates the first block in ri (ri is initialized to s)
  detail::MatOps<real_t *>::vecsum(n, 1., x.data() + n * k,
                                   ri.data() + 2 * k * n);
  detail::MatOps<real_t *>::vecsum(n, -1., x.data() + n * k,
                                   ri.data() + 2 * k * n + n);
  detail::MatOps<real_t *>::vecsum(n, -1., ub.data(), ri.data() + 2 * k * n);
  detail::MatOps<real_t *>::vecsum(n, 1., lb.data(), ri.data() + 2 * k * n + n);
  // Create rdx
  detail::affoperator(Q, x.data() + k * n, rdx.data() + k * n);
  detail::MatOps<real_t *>::affoperator('N', n, n, 1., A.data(), n, nu + n, 1.,
                                        rdx.data() + k * n);
  for (int_t i = 0; i < n; i++)
    rdx[k * n + i] += ri[2 * k * n + i] * d[2 * k * n + i] -
                      ri[2 * k * n + n + i] * d[2 * k * n + n + i];
  SoC::augment_rd(rdx.data() + k * n, k);
  // Create the semiproduct phiaQt = inv(phiq[k])*At
  std::copy(A.data(), A.data() + n * n, phiqAt.data() + k * n * n);
  phiq[k].backsolve(phiqAt.data() + k * n * n, n);
  std::transform(phiqAt.data() + k * n * n, phiqAt.data() + (k + 1) * n * n, M,
                 std::negate<real_t>());
  // Updates the residual of the Shur Complement (stored in nu)
  std::copy(rdx.cbegin() + k * n, rdx.cbegin() + k * n + n, aux.begin());
  phiq[k].backsolve(aux.data());
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, aux.data(), nu_aff);
  // Updates the residual of the Shur Complement (next block)
  detail::MatOps<real_t *>::affoperator('T', n, n, -1.,
                                        phiqAt.data() + k * n * n, n,
                                        rdx.data() + k * n, 1., nu_aff + n);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_nb(
    std::true_type, real_t *L, real_t *M, const real_t *nu, real_t *nu_aff,
    int_t k) {
  // Initialize k-th block in the Shur Complement
  phiq[k] = Q;
  phiq[k].add_bounds(d.data() + 2 * k * n, d.data() + 2 * k * n + n);
  SoC::augment_phi(phiq[k], k);
  phiq[k].factorize();
  std::copy(A.cbegin(), A.cend(), Xq.begin());
  phiq[k].fastsemisolve('N', Xq.data(), n);
  phiq[k].fastsolve(identity.data(), n);
  detail::MatOps<real_t *>::pd2pp(n, identity.data(), identity.data());
  detail::MatOps<real_t *>::vecsum(n + (n + 1) / 2, real_t{1}, identity.data(),
                                   L);
  reset_identity();
  // Initialize NEXT block in the Shur Complement
  detail::MatOps<real_t *>::pprankkt(n, 1., Xq.data(), L + n * (n + 1) / 2, n);
  // Creates the first block in ri (ri is initialized to s)
  detail::MatOps<real_t *>::vecsum(n, 1., x.data() + n * k,
                                   ri.data() + 2 * k * n);
  detail::MatOps<real_t *>::vecsum(n, -1., x.data() + n * k,
                                   ri.data() + 2 * k * n + n);
  detail::MatOps<real_t *>::vecsum(n, -1., ub.data(), ri.data() + 2 * k * n);
  detail::MatOps<real_t *>::vecsum(n, 1., lb.data(), ri.data() + 2 * k * n + n);
  // Create rdx
  detail::affoperator(Q, x.data() + k * n, rdx.data() + k * n);
  detail::MatOps<real_t *>::affoperator('N', n, n, 1., A.data(), n, nu + n, 1.,
                                        rdx.data() + k * n);
  for (int_t i = 0; i < n; i++)
    rdx[k * n + i] += ri[2 * k * n + i] * d[2 * k * n + i] -
                      ri[2 * k * n + n + i] * d[2 * k * n + n + i];
  SoC::augment_rd(rdx.data() + k * n, k);
  // Create the semiproduct phiaQt = inv(phiq)*At
  std::copy(A.data(), A.data() + n * n, phiqAt.data() + k * n * n);
  phiq[k].fastsolve(phiqAt.data() + k * n * n, n);
  std::transform(phiqAt.data() + k * n * n, phiqAt.data() + (k + 1) * n * n, M,
                 std::negate<real_t>());
  // Updates the residual of the Shur Complement (stored in nu)
  std::copy(rdx.cbegin() + k * n, rdx.cbegin() + k * n + n, aux.begin());
  phiq[k].fastsolve(aux.data());
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, aux.data(), nu_aff);
  // Updates the residual of the Shur Complement (next block)
  detail::MatOps<real_t *>::affoperator('T', n, n, -1.,
                                        phiqAt.data() + k * n * n, n,
                                        rdx.data() + k * n, 1., nu_aff + n);
}

// Affine solve, last block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_lb(
    real_t *L, const real_t *nu, real_t *nu_aff) {
  aff_fs_lb(typename std::conditional<!detail::samecons<TerminalCons>::value,
                                      std::true_type, std::false_type>::type{},
            L, nu, nu_aff);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_lb(
    std::false_type, real_t *L, const real_t *nu, real_t *nu_aff) {
  aff_fs_lb_2(typename std::conditional < detail::is_pdmat<P_t>::value ||
                  detail::softcons<SoftCons>::linear,
              std::true_type, std::false_type > ::type{}, L, nu, nu_aff);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_lb_2(
    std::false_type, real_t *L, const real_t *nu, real_t *nu_aff) {
  // Update last block in the Shur Complement
  phip = P;
  phip.add_bounds(d.data() + 2 * n * (T - 1), d.data() + 2 * n * (T - 1) + n);
  SoC::augment_phi(phip, (T - 1));
  for (int_t i = 0; i < n; i++)
    L[i + (2 * n - i - 1) * i / 2] += 1 / phip[i];
  // Initialize next block in the Shur Complement
  // *NOPE
  // Creates the first block in ri (ri is initialized to s)
  detail::MatOps<real_t *>::vecsum(n, 1., x.data() + n * (T - 1),
                                   ri.data() + 2 * (T - 1) * n);
  detail::MatOps<real_t *>::vecsum(n, -1., x.data() + n * (T - 1),
                                   ri.data() + 2 * (T - 1) * n + n);
  detail::MatOps<real_t *>::vecsum(n, -1., ub.data(),
                                   ri.data() + 2 * (T - 1) * n);
  detail::MatOps<real_t *>::vecsum(n, 1., lb.data(),
                                   ri.data() + 2 * (T - 1) * n + n);
  // Create rdx
  detail::affoperator(P, x.data() + (T - 1) * n, rdx.data() + (T - 1) * n);
  for (int_t i = 0; i < n; i++)
    rdx[(T - 1) * n + i] +=
        ri[2 * (T - 1) * n + i] * d[2 * (T - 1) * n + i] -
        ri[2 * (T - 1) * n + n + i] * d[2 * (T - 1) * n + n + i];
  SoC::augment_rd(rdx.data() + (T - 1) * n, (T - 1));
  // Create the semiproduct phiaQt = inv(phiq[k])*At
  std::copy(A.data(), A.data() + n * n, phiqAt.data() + (T - 1) * n * n);
  phip.fastsolve(phiqAt.data() + (T - 1) * n * n, n);
  // Updates the residual of the Shur Complement (stored in nu)
  std::copy(rdx.cbegin() + (T - 1) * n, rdx.cbegin() + (T - 1) * n + n,
            aux.begin());
  phip.backsolve(aux.data());
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, aux.data(), nu_aff);
  // Updates the residual of the Shur Complement (next block)
  // *NOPE
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_lb_2(
    std::true_type, real_t *L, const real_t *nu, real_t *nu_aff) {
  // Initialize k-th block in the Shur Complement
  phip = P;
  phip.add_bounds(d.data() + 2 * (T - 1) * n, d.data() + 2 * (T - 1) * n + n);
  SoC::augment_phi(phip, T - 1);
  phip.factorize();
  std::copy(A.cbegin(), A.cend(), Xq.begin());
  phip.fastsemisolve('N', Xq.data(), n);
  phip.fastsolve(identity.data(), n);
  detail::MatOps<real_t *>::pd2pp(n, identity.data(), identity.data());
  detail::MatOps<real_t *>::vecsum(n + (n + 1) / 2, real_t{1}, identity.data(),
                                   L);
  reset_identity();
  // Initialize NEXT block in the Shur Complement
  // *NOPE
  // Creates the last block in ri (ri is initialized to s)
  detail::MatOps<real_t *>::vecsum(n, 1., x.data() + (T - 1) * n,
                                   ri.data() + 2 * (T - 1) * n);
  detail::MatOps<real_t *>::vecsum(n, -1., x.data() + (T - 1) * n,
                                   ri.data() + 2 * (T - 1) * n + n);
  detail::MatOps<real_t *>::vecsum(n, -1., ub.data(),
                                   ri.data() + 2 * (T - 1) * n);
  detail::MatOps<real_t *>::vecsum(n, 1., lb.data(),
                                   ri.data() + 2 * (T - 1) * n + n);
  // Create rdx
  detail::affoperator(P, x.data() + (T - 1) * n, rdx.data() + (T - 1) * n);
  for (int_t i = 0; i < n; i++)
    rdx[(T - 1) * n + i] +=
        ri[2 * (T - 1) * n + i] * d[2 * (T - 1) * n + i] -
        ri[2 * (T - 1) * n + n + i] * d[2 * (T - 1) * n + n + i];
  SoC::augment_rd(rdx.data() + (T - 1) * n, T - 1);
  // Create the semiproduct phiaQt = inv(phiq)*At
  std::copy(A.data(), A.data() + n * n, phiqAt.data() + (T - 1) * n * n);
  phip.fastsolve(phiqAt.data() + (T - 1) * n * n, n);
  // Updates the residual of the Shur Complement (stored in nu)
  std::copy(rdx.cbegin() + (T - 1) * n, rdx.cbegin() + (T - 1) * n + n,
            aux.begin());
  phip.fastsolve(aux.data());
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, aux.data(), nu_aff);
  // Updates the residual of the Shur Complement (next block)
  // *NOPE
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fs_lb(
    std::true_type, real_t *L, const real_t *nu, real_t *nu_aff) {
  phip = P;
  std::copy(A.cbegin(), A.cend(), Xq.begin());
  std::copy(A.data(), A.data() + n * n, phiqAt.data() + (T - 1) * n * n);
  TC::aff_fs(L, nu, nu_aff, P, phip, Xq.data(), x.data() + (T - 1) * n,
             rdx.data() + (T - 1) * n, phiqAt.data() + (T - 1) * n * n,
             d.data() + (T - 1) * 2 * n, ri.data() + (T - 1) * 2 * n,
             identity.data());
  reset_identity();
}

// Intermediate calculations
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_ic() {
  // Copy -rdx into x_aff (helpful for backsolving)
  std::transform(rdx.cbegin(), rdx.cend(), x_aff.begin(),
                 std::negate<real_t>());
}

// Backsolve, last block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_bs_lb(
    const real_t *nu_aff) {
  aff_bs_lb(typename std::conditional<TC::has_cons, std::true_type,
                                      std::false_type>::type{},
            nu_aff);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_bs_lb(
    std::false_type, const real_t *nu_aff) {
  // Calculate x_affine[i], which is already initialized to -rdx[i]
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, nu_aff,
                                   x_aff.data() + (T - 1) * n);
  phip.fastsolve(x_aff.data() + (T - 1) * n);
  // Calculate "lambda_aff[T-1]=Fu*u_aff+Fu*u-fu" & "s_aff[T-1]=-Fu*u_aff-ri"
  std::copy(x_aff.data() + (T - 1) * n, x_aff.data() + T * n,
            lambda_aff.begin() + (T - 1) * 2 * n);
  std::transform(lambda_aff.cbegin() + 2 * (T - 1) * n,
                 lambda_aff.cbegin() + 2 * (T - 1) * n + n,
                 lambda_aff.begin() + 2 * (T - 1) * n + n,
                 std::negate<real_t>());
  // Evaluate s_aff
  std::transform(lambda_aff.cbegin() + 2 * (T - 1) * n, lambda_aff.cend(),
                 s_aff.begin() + 2 * (T - 1) * n, std::negate<real_t>());
  std::transform(s_aff.cbegin() + 2 * (T - 1) * n, s_aff.cend(),
                 ri.cbegin() + 2 * (T - 1) * n, s_aff.begin() + 2 * (T - 1) * n,
                 std::minus<real_t>());
  // Evaluate lambda_aff
  detail::MatOps<real_t *>::vecsum(n, 1., x.data() + (T - 1) * n,
                                   lambda_aff.data() + 2 * (T - 1) * n);
  detail::MatOps<real_t *>::vecsum(n, -1., ub.data(),
                                   lambda_aff.data() + 2 * (T - 1) * n);
  detail::MatOps<real_t *>::vecsum(n, -1., x.data() + (T - 1) * n,
                                   lambda_aff.data() + 2 * (T - 1) * n + n);
  detail::MatOps<real_t *>::vecsum(n, 1., lb.data(),
                                   lambda_aff.data() + 2 * (T - 1) * n + n);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_bs_lb(
    std::true_type, const real_t *nu_aff) {
  TC::aff_bs(phip, nu_aff, x.data() + (T - 1) * n, x_aff.data() + (T - 1) * n,
             lambda.data() + 2 * n * (T - 1),
             lambda_aff.data() + 2 * n * (T - 1), s.data() + 2 * n * (T - 1),
             s_aff.data() + 2 * n * (T - 1), ri.data() + 2 * n * (T - 1),
             aux.data());
}

// Backsolve, nth block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_bs_nb(
    const real_t *nu_aff, const int_t k) {
  // Calculate x_affine[i], which is already initialized to -rdx[i]
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, nu_aff, x_aff.data() + k * n);
  phiq[k].fastsolve(x_aff.data() + k * n);
  detail::MatOps<real_t *>::affoperator('N', n, n, real_t{-1},
                                        phiqAt.data() + k * n * n, n,
                                        nu_aff + n, 1., x_aff.data() + k * n);
  // Calculate "lambda_aff[T-1]=Fu*u_aff+Fu*u-fu" & "s_aff[T-1]=-Fu*u_aff-ri"
  std::copy(x_aff.data() + k * n, x_aff.data() + (k + 1) * n,
            lambda_aff.begin() + k * 2 * n);
  std::transform(lambda_aff.cbegin() + 2 * k * n,
                 lambda_aff.cbegin() + 2 * k * n + n,
                 lambda_aff.begin() + 2 * k * n + n, std::negate<real_t>());
  // Evaluate s_aff
  std::transform(lambda_aff.cbegin() + 2 * k * n,
                 lambda_aff.cbegin() + 2 * (k + 1) * n,
                 s_aff.begin() + 2 * k * n, std::negate<real_t>());
  std::transform(s_aff.cbegin() + 2 * k * n, s_aff.cbegin() + 2 * (k + 1) * n,
                 ri.cbegin() + 2 * k * n, s_aff.begin() + 2 * k * n,
                 std::minus<real_t>());
  // Evaluate lambda_aff
  detail::MatOps<real_t *>::vecsum(n, 1., x.data() + k * n,
                                   lambda_aff.data() + 2 * k * n);
  detail::MatOps<real_t *>::vecsum(n, -1., ub.data(),
                                   lambda_aff.data() + 2 * k * n);
  detail::MatOps<real_t *>::vecsum(n, -1., x.data() + k * n,
                                   lambda_aff.data() + 2 * k * n + n);
  detail::MatOps<real_t *>::vecsum(n, 1., lb.data(),
                                   lambda_aff.data() + 2 * k * n + n);
}

// Final calculations
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::aff_fc() {
  // Final evaluation lambda_aff *= d
  std::transform(lambda_aff.cbegin(), lambda_aff.cend(), d.cbegin(),
                 lambda_aff.begin(), std::multiplies<real_t>());
}

// Pre-computations
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_pc(
    real_t sigma_mu_aff) {
  // Evaluates rs and stores it in ri
  for (int_t i = 0; i < k; i++)
    ri[i] = (s_aff[i] * lambda_aff[i] - sigma_mu_aff) / s[i];
  std::fill(rdx.begin(), rdx.end(), real_t{0});
}

// cc step, forward solve, first block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_fs_fb(
    real_t *nu_cc) {
  // Create first block in rd
  std::transform(ri.cbegin(), ri.cbegin() + n, rdx.begin(),
                 std::negate<real_t>());
  detail::MatOps<real_t *>::vecsum(n, 1, ri.data() + n, rdx.data());
  // Update first block in rsc += (phiq)^-1*rd (stored in nu)
  std::copy(rdx.cbegin(), rdx.cbegin() + n, aux.begin());
  phiq[0].fastsolve(aux.data());
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, aux.data(), nu_cc);
  // Initialize next block
  detail::MatOps<real_t *>::affoperator('T', n, n, -1., phiqAt.data(), n,
                                        rdx.data(), 0., nu_cc + n);
}

// cc step, forward solve, nth block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_fs_nb(
    real_t *nu_cc, const int_t k) {
  // Create i-th block in rd
  std::transform(ri.cbegin() + 2 * k * n, ri.cbegin() + 2 * k * n + n,
                 rdx.data() + k * n, std::negate<real_t>());
  detail::MatOps<real_t *>::vecsum(n, 1., ri.data() + 2 * k * n + n,
                                   rdx.data() + k * n);
  // Update i-th block in rsc += (phirBt)^T*rd (stored in nu)
  std::copy(rdx.cbegin() + k * n, rdx.cbegin() + (k + 1) * n, aux.begin());
  phiq[k].fastsolve(aux.data());
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, aux.data(), nu_cc);
  // Initialize next block
  detail::MatOps<real_t *>::affoperator('T', n, n, -1.,
                                        phiqAt.data() + k * n * n, n,
                                        rdx.data() + k * n, 0., nu_cc + n);
}

// cc step, forward solve, last block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_fs_lb(
    real_t *nu_cc) {
  cc_fs_lb(typename std::conditional<TC::has_cons, std::true_type,
                                     std::false_type>::type{},
           nu_cc);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_fs_lb(
    std::false_type, real_t *nu_cc) {
  // Create last block in rd
  std::transform(ri.cbegin() + 2 * (T - 1) * n,
                 ri.cbegin() + 2 * (T - 1) * n + n, rdx.data() + (T - 1) * n,
                 std::negate<real_t>());
  detail::MatOps<real_t *>::vecsum(n, 1, ri.data() + 2 * (T - 1) * n + n,
                                   rdx.data() + (T - 1) * n);
  // Update i-th block in rsc += (phirBt)^T*rd (stored in nu)
  std::copy(rdx.cbegin() + (T - 1) * n, rdx.cend(), aux.begin());
  phip.fastsolve(aux.data());
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, aux.data(), nu_cc);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_fs_lb(
    std::true_type, real_t *nu_cc) {
  TC::cc_fs(phip, nu_cc, ri.data() + 2 * n * (T - 1), rdx.data() + n * (T - 1),
            aux.data());
}

// Intermediate calculations
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_ic() {
  // Copy -rdx into x_aff (helpful for backsolving)
  std::transform(rdx.cbegin(), rdx.cend(), x_cc.begin(), std::negate<real_t>());
}

// cc backward solve, last block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_bs_lb(
    const real_t *nu_cc) {
  cc_bs_lb(typename std::conditional<!detail::samecons<TerminalCons>::value,
                                     std::true_type, std::false_type>::type{},
           nu_cc);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_bs_lb(
    std::false_type, const real_t *nu_cc) {
  // Calculate x_cc[T-1], which is initialized to -rdx
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, nu_cc,
                                   x_cc.data() + (T - 1) * n);
  phip.fastsolve(x_cc.data() + (T - 1) * n);
  // Calculate "lambda_cc[T-1]=Fu*u_cc+Fu*u-fu" & "s_aff[T-1]=-Fu*u_aff"
  std::copy(x_cc.data() + (T - 1) * n, x_cc.data() + T * n,
            lambda_cc.begin() + (T - 1) * 2 * n);
  std::transform(lambda_cc.cbegin() + 2 * (T - 1) * n,
                 lambda_cc.cbegin() + 2 * (T - 1) * n + n,
                 lambda_cc.begin() + 2 * (T - 1) * n + n,
                 std::negate<real_t>());
  // Evaluate s_cc
  std::transform(lambda_cc.cbegin() + 2 * (T - 1) * n,
                 lambda_cc.cbegin() + 2 * T * n, s_cc.begin() + 2 * (T - 1) * n,
                 std::negate<real_t>());
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_bs_lb(
    std::true_type, const real_t *nu_cc) {
  TC::cc_bs(nu_cc + (T - 1) * n, aux.data(), phip, x.data() + (T - 1) * n,
            x_cc.data() + (T - 1) * n, lambda_cc.data() + (T - 1) * 2 * n,
            s_cc.data() + (T - 1) * 2 * n);
}

// cc backward solve, nth block
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_bs_nb(
    const real_t *nu_cc, const int_t k) {
  // Calculate x_cc[T-1], which is initialized to -rdx
  detail::MatOps<real_t *>::vecsum(n, real_t{1}, nu_cc, x_cc.data() + k * n);
  phiq[k].fastsolve(x_cc.data() + k * n);
  detail::MatOps<real_t *>::affoperator('N', n, n, -1.,
                                        phiqAt.data() + k * n * n, n, nu_cc + n,
                                        1., x_cc.data() + n * k);
  // Calculate "lambda_cc[T-1]=Fu*u_cc+Fu*u-fu" & "s_aff[T-1]=-Fu*u_aff"
  std::copy(x_cc.data() + k * n, x_cc.data() + (k + 1) * n,
            lambda_cc.begin() + k * 2 * n);
  std::transform(lambda_cc.cbegin() + 2 * k * n,
                 lambda_cc.cbegin() + 2 * k * n + n,
                 lambda_cc.begin() + 2 * k * n + n, std::negate<real_t>());
  // Evaluate s_cc
  std::transform(lambda_cc.cbegin() + 2 * k * n,
                 lambda_cc.cbegin() + 2 * (k + 1) * n, s_cc.begin() + 2 * k * n,
                 std::negate<real_t>());
}

// Final computations
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::cc_fc() {
  // Final computation lambda_cc = d * (lambda_cc - inv(LAMBDA)*rs)
  // But recall that you have rs := rs./s
  // So you better do lambca_cc <- d .* lambda_cc - rs
  for (int_t i = 0; i < k; i++)
    lambda_cc[i] = d[i] * lambda_cc[i] - ri[i];
  // Aggregate affine and cc steps
  detail::MatOps<real_t *>::vecsum(T * n, real_t{1}, x_cc.data(), x_aff.data());
  detail::MatOps<real_t *>::vecsum(k, 1., lambda_cc.data(), lambda_aff.data());
  detail::MatOps<real_t *>::vecsum(k, 1., s_cc.data(), s_aff.data());
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
real_t
SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::duality_gap()
    const {
  return detail::MatOps<real_t *>::dotproduct(k, lambda.data(), s.data());
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
real_t
SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::affine_gap(
    const real_t &alpha_p, const real_t &alpha_d) {
  s_cc = s;
  lambda_cc = lambda;
  detail::MatOps<real_t *>::vecsum(k, alpha_p, s_aff.data(), s_cc.data());
  detail::MatOps<real_t *>::vecsum(k, alpha_d, lambda_aff.data(),
                                   lambda_cc.data());
  return mpc::detail::MatOps<real_t *>::dotproduct(k, lambda_cc.data(),
                                                   s_cc.data());
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
real_t
SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::affine_gap(
    const real_t &alpha) {
  s_cc = s;
  lambda_cc = lambda;
  detail::MatOps<real_t *>::vecsum(k, alpha, s_aff.data(), s_cc.data());
  detail::MatOps<real_t *>::vecsum(k, alpha, lambda_aff.data(),
                                   lambda_cc.data());
  return mpc::detail::MatOps<real_t *>::dotproduct(k, lambda_cc.data(),
                                                   s_cc.data());
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::linesearch(
    real_t &alpha_p, real_t &alpha_d) {
  // Performs linesearch on affine vector
  std::transform(s.cbegin(), s.cend(), s_aff.cbegin(), aux.begin(),
                 std::divides<real_t>());
  for (const auto &elem : aux) {
    if (elem >= 0)
      continue;
    if (elem > alpha_p)
      alpha_p = elem;
  }
  std::transform(lambda.cbegin(), lambda.cend(), lambda_aff.cbegin(),
                 aux.begin(), std::divides<real_t>());
  for (const auto &elem : aux) {
    if (elem >= 0)
      continue;
    if (elem > alpha_d)
      alpha_d = elem;
  }
  alpha_p = std::min(real_t{1}, -alpha_p);
  alpha_d = std::min(real_t{1}, -alpha_d);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::linesearch(
    real_t &alpha) {
  // Performs linesearch on affine vector
  std::transform(s.cbegin(), s.cend(), s_aff.cbegin(), aux.begin(),
                 std::divides<real_t>());
  for (const auto &elem : aux) {
    if (elem >= 0)
      continue;
    if (elem > alpha)
      alpha = elem;
  }
  std::transform(lambda.cbegin(), lambda.cend(), lambda_aff.cbegin(),
                 aux.begin(), std::divides<real_t>());
  for (const auto &elem : aux) {
    if (elem >= 0)
      continue;
    if (elem > alpha)
      alpha = elem;
  }
  alpha = std::min(real_t{1}, -alpha);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::take_step(
    const real_t a) {
  detail::MatOps<real_t *>::vecsum(T * n, a, x_aff.data(), x.data());
  detail::MatOps<real_t *>::vecsum(k, a, lambda_aff.data(), lambda.data());
  detail::MatOps<real_t *>::vecsum(k, a, s_aff.data(), s.data());
  std::transform(lambda.cbegin(), lambda.cend(), s.cbegin(), d.begin(),
                 std::divides<real_t>());
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::new_x0() {
  // Rotate
  std::rotate(x.begin(), x.begin() + n, x.end());
  std::rotate(lambda.begin(), lambda.begin() + 2 * n, lambda.end());
  std::rotate(s.begin(), s.begin() + 2 * n, s.end());
  // Complete the tail
  std::copy(x.cbegin() + (T - 2) * n, x.cbegin() + (T - 1) * n,
            x.begin() + (T - 1) * n);
  std::copy(lambda.cbegin() + 2 * (T - 1) * n - TC::k,
            lambda.cbegin() + 2 * (T - 1) * n,
            lambda.begin() + 2 * (T - 1) * n);
  std::copy(s.cbegin() + 2 * (T - 1) * n - TC::k, s.cbegin() + 2 * (T - 1) * n,
            s.begin() + 2 * (T - 1) * n);
  // Add some constant
  std::transform(lambda.cbegin(), lambda.cend(), lambda.begin(),
                 [](real_t input) { return input + 1; });
  std::transform(s.cbegin(), s.cend(), s.begin(),
                 [](real_t input) { return input + 1; });
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons,
                  SoftCons>::reset_identity() {
  std::fill(identity.begin(), identity.end(), real_t{0});
  for (int_t i = 0; i < n; i++)
    identity[i + n * i] = 1;
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::eq_residual(
    real_t *nu_aff, const real_t *x0) const {
  std::transform(x.cbegin(), x.cend(), nu_aff, std::negate<real_t>());
  detail::MatOps<real_t *>::affoperator('T', n, n, real_t{1}, A.data(), n, x0,
                                        real_t{1}, nu_aff);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::eq_residual(
    real_t *nu_aff, const int_t i) const {
  detail::MatOps<real_t *>::affoperator('T', n, n, real_t{1}, A.data(), n,
                                        x.data() + n * (i - 1), real_t{1},
                                        nu_aff);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
void SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::opt_val(
    real_t &opt_val) const {
  for (int_t i = 0; i < T - 1; i++)
    opt_val += detail::quadratic_form(Q, x.data() + i * n);
  opt_val += detail::quadratic_form(P, x.data() + (T - 1) * n);
}

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
int_t SimpleBounds<real_t, int_t, Q_t, P_t, TerminalCons, SoftCons>::ncons()
    const {
  return k;
}

// ********************************************************************* //
// ************************* END BOUNDS ******************************** //
// ********************************************************************* //

} // namespace StateCons
} // namespace mpc

#endif
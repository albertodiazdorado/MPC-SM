#ifndef TERMINAL_CONS_CPP_
#define TERMINAL_CONS_CPP_

#include <vector>

#include "constraints/SoftCons.cpp"
#include "matrix/diagmat.cpp"
#include "matrix/pdmat.cpp"

namespace mpc {
namespace TerminalCons {
// ********************************************************************* //
// ************************* FORWARD DECLARE *************************** //
// ********************************************************************* //
template <class real_t, class int_t> class SameCons;

template <class real_t, class int_t, template <class, class> class P_t,
          template <class, class> class SoftCons = mpc::SoftCons::Unconstrained>
class Unconstrained;

template <class real_t, class int_t, template <class, class> class P_t,
          template <class, class> class SoftCons = mpc::SoftCons::Unconstrained>
class SimpleBounds;

template <class real_t, class int_t, template <class, class> class P_t,
          template <class, class> class SoftCons = mpc::SoftCons::Unconstrained>
class LinearBounds;

template <class real_t, class int_t, template <class, class> class P_t,
          template <class, class> class SoftCons = mpc::SoftCons::Unconstrained>
class FullBounds;

// ********************************************************************* //
// **************************** SAME CONS ****************************** //
// ********************************************************************* //
template <class real_t, class int_t> class SameCons {
public:
  SameCons() = default;
  SameCons(int_t n, int_t k_, std::vector<real_t> &lbf,
           std::vector<real_t> &ubf, std::vector<real_t> &Ff,
           std::vector<real_t> &ff, std::vector<real_t> &slbf,
           std::vector<real_t> &subf, std::vector<real_t> &sFf,
           std::vector<real_t> &sff)
      : k{k_} {}
  virtual ~SameCons() = default;

  const int_t k;
  static constexpr bool has_cons{false};
  void initialize_x(real_t *x, real_t *s) const {};
};

// ********************************************************************* //
// ************************* SIMPLE BOUNDS ***************************** //
// ********************************************************************* //
template <class real_t, class int_t, template <class, class> class P_t,
          template <class, class> class SoftCons>
class SimpleBounds : public SoftCons<real_t, int_t> {
private:
  using SoC = SoftCons<real_t, int_t>;
  using mat_t = typename std::conditional<
      (detail::is_pdmat<P_t>::value) && (detail::softcons<SoftCons>::linear),
      matrix::PDMat<real_t, int_t>, matrix::DiagMat<real_t, int_t>>::type;

  const std::vector<real_t> lb, ub;

public:
  int_t k, n;
  const bool l;
  static constexpr bool has_cons{true};

  const bool linear() { return l; }

  SimpleBounds() = default;
  SimpleBounds(int_t n, int_t k, std::vector<real_t> &lbf,
               std::vector<real_t> &ubf, std::vector<real_t> &Ff,
               std::vector<real_t> &ff, std::vector<real_t> &slbf,
               std::vector<real_t> &subf, std::vector<real_t> &sFf,
               std::vector<real_t> &sff)
      : SoC{slbf, subf, sFf, sff}, lb{std::move(lbf)}, ub{std::move(ubf)},
        k{2 * n}, n{n}, l{detail::softcons<SoftCons>::linear} {};

  void initialize_x(real_t *x) {
    std::transform(lb.cbegin(), lb.cend(), ub.cbegin(), x,
                   [](real_t lb, real_t ub) { return (lb + ub) / 2; });
  }

  void aff_fs(real_t *L, const real_t *nu, real_t *nu_aff,
              P_t<real_t, int_t> &P, mat_t &phip, real_t *Xq, const real_t *x,
              real_t *rdx, real_t *phiqAt, real_t *d, real_t *ri,
              real_t *identity) {
    aff_fs(typename std::conditional < detail::is_pdmat<P_t>::value &&
               detail::softcons<SoftCons>::linear,
           std::true_type, std::false_type > ::type{}, L, nu, nu_aff, P, phip,
           Xq, x, rdx, phiqAt, d, ri, identity);
  }

  void aff_fs(std::false_type, real_t *L, const real_t *nu, real_t *nu_aff,
              P_t<real_t, int_t> &P, mat_t &phip, real_t *Xq, const real_t *x,
              real_t *rdx, real_t *phiqAt, real_t *d, real_t *ri,
              real_t *identity) {
    // Update first block in the Shur Complement
    // phip already initialized to P
    phip.add_bounds(d, d + n);
    SoC::augment_phi(phip, 0);
    for (int_t i = 0; i < n; i++)
      L[i + (2 * n - i - 1) * i / 2] += 1 / phip[i];
    // Initialize next block in the Shur Complement
    // *NOPE
    // Creates the first block in ri (ri is initialized to s)
    detail::MatOps<real_t *>::vecsum(n, 1., x, ri);
    detail::MatOps<real_t *>::vecsum(n, -1., x, ri + n);
    detail::MatOps<real_t *>::vecsum(n, -1., ub.data(), ri);
    detail::MatOps<real_t *>::vecsum(n, 1., lb.data(), ri + n);
    // Create rdx
    detail::affoperator(P, x, rdx);
    for (int_t i = 0; i < n; i++)
      rdx[i] += ri[i] * d[i] - ri[n + i] * d[n + i];
    SoC::augment_rd(rdx, 0);
    // Create the semiproduct phiaQt = inv(phiq[k])*At
    // phiqAt initialized to A^T
    phip.backsolve(phiqAt, n);
    // Updates the residual of the Shur Complement (stored in nu)
    std::copy(rdx, rdx + n, identity);
    phip.backsolve(identity);
    detail::MatOps<real_t *>::vecsum(n, real_t{1}, identity, nu_aff);
    // Updates the residual of the Shur Complement (next block)
    // *NOPE
  }

  void aff_fs(std::true_type, real_t *L, const real_t *nu, real_t *nu_aff,
              P_t<real_t, int_t> &P, mat_t &phip, real_t *Xq, const real_t *x,
              real_t *rdx, real_t *phiqAt, real_t *d, real_t *ri,
              real_t *identity) {
    // phip already initialized to P
    phip.add_bounds(d, d + n);
    SoC::augment_phi(phip, 0);
    phip.factorize();
    // Xq initialized to A^T
    phip.fastsemisolve('N', Xq, n);
    phip.fastsolve(identity, n);
    detail::MatOps<real_t *>::pd2pp(n, identity, identity);
    detail::MatOps<real_t *>::vecsum(n + (n + 1) / 2, identity, L);
    // Initialize NEXT block in the Shur Complement
    // *NOPE
    // Creates the last block in ri (ri is initialized to s)
    detail::MatOps<real_t *>::vecsum(n, 1., x, ri);
    detail::MatOps<real_t *>::vecsum(n, -1., x, ri + n);
    detail::MatOps<real_t *>::vecsum(n, -1., ub.data(), ri);
    detail::MatOps<real_t *>::vecsum(n, 1., lb.data(), ri + n);
    // Create rdx
    detail::affoperator(P, x, rdx);
    for (int_t i = 0; i < n; i++)
      rdx[i] += ri[i] * d[i] - ri[n + i] * d[n + i];
    SoC::augment_rd(rdx, 0);
    // Create the semiproduct phiaQt = inv(phiq)*At
    // phiqAt initialized to A^T
    phip.fastsolve(phiqAt, n);
    // Updates the residual of the Shur Complement (stored in nu)
    std::copy(rdx, rdx + n, identity);
    phip.fastsolve(identity);
    detail::MatOps<real_t *>::vecsum(n, real_t{1}, identity, nu_aff);
    // Updates the residual of the Shur Complement (next block)
    // *NOPE
  }

  void aff_bs(const mat_t &phip, const real_t *nu_aff, const real_t *x,
              real_t *x_aff, const real_t *lambda, real_t *lambda_aff,
              const real_t *s, real_t *s_aff, const real_t *ri, real_t *aux) {
    // Calculate x_affine[i], which is already initialized to -rdx[i]
    std::copy(nu_aff, nu_aff + n, aux);
    phip.fastsolve(aux);
    detail::MatOps<real_t *>::vecsum(n, real_t{-1}, aux, nu_aff);
    // Calculate "lambda_aff[T-1]=Fu*u_aff+Fu*u-fu" & "s_aff[T-1]=-Fu*u_aff-ri"
    std::copy(x_aff, x_aff + n, lambda_aff);
    std::transform(lambda_aff, lambda_aff + n, lambda_aff + n,
                   std::negate<real_t>());
    // Evaluate s_aff
    std::transform(lambda_aff, lambda_aff + 2 * n, s_aff,
                   std::negate<real_t>());
    std::transform(s_aff, s_aff + 2 * n, ri, s_aff, std::minus<real_t>());
    // Evaluate lambda_aff
    detail::MatOps<real_t *>::vecsum(n, 1., x, lambda_aff);
    detail::MatOps<real_t *>::vecsum(n, -1., ub.data(), lambda_aff);
    detail::MatOps<real_t *>::vecsum(n, -1., x, lambda_aff + n);
    detail::MatOps<real_t *>::vecsum(n, 1., lb.data(), lambda_aff + n);
  }

  void cc_fs(const mat_t &phip, real_t *nu_cc, const real_t *ri, real_t *rdx,
             real_t *aux) {
    // Create last block in rd
    std::transform(ri, ri + n, rdx, std::negate<real_t>());
    std::transform(ri + n, ri + 2 * n, rdx, std::plus<real_t>());
    // Update i-th block in rsc += (phirBt)^T*rd (stored in nu)
    std::copy(rdx, rdx + n, aux);
    phip.fastsolve(aux);
    detail::MatOps<real_t *>::vecsum(n, real_t{-1}, aux, nu_cc);
  }

  void cc_bs(const real_t *nu_cc, real_t *aux, const mat_t &phip,
             const real_t *x, real_t *x_cc, real_t *lambda_cc, real_t *s_cc) {
    // Calculate x_cc[T-1], which is initialized to -rdx
    std::copy(nu_cc, nu_cc + n, aux);
    phip.fastsolve(aux);
    detail::MatOps<real_t *> vecsum(n, -1, aux, x_cc);
    // Calculate "lambda_cc[T-1]=Fu*u_cc+Fu*u-fu" & "s_aff[T-1]=-Fu*u_aff"
    std::copy(x_cc, x_cc + n, lambda_cc);
    std::transform(lambda_cc, lambda_cc + n, lambda_cc + n,
                   std::negate<real_t>());
    // Evaluate s_cc
    std::transform(lambda_cc, lambda_cc + 2 * n, s_cc, std::negate<real_t>());
    // Evaluate lambda_cc
    detail::MatOps<real_t *>::vecsum(n, 1., x, lambda_cc);
    detail::MatOps<real_t *>::vecsum(n, -1., ub.data(), lambda_cc);
    detail::MatOps<real_t *>::vecsum(n, -1., x, lambda_cc + n);
    detail::MatOps<real_t *>::vecsum(n, 1., lb.data(), lambda_cc + n);
  }
};
} // namespace TerminalCons

namespace detail {
// Terminal cons traits
template <template <class, class> class> struct samecons {
  static constexpr bool value = false;
};

template <> struct samecons<TerminalCons::SameCons> {
  static constexpr bool value = true;
};

} // namespace detail
} // namespace mpc

#endif
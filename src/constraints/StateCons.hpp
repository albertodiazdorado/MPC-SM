#ifndef STATE_CONS_HPP_
#define STATE_CONS_HPP_

#include <vector>

#include "constraints/SoftCons.cpp"
#include "constraints/TerminalCons.cpp"
#include "matrix/diagmat.cpp"
#include "matrix/pdmat.cpp"

namespace mpc {
namespace StateCons {

// ********************************************************************* //
// ************************* Forward declare *************************** //
// ********************************************************************* //
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
class SimpleBounds;

template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons,
          template <class, class> class SoftCons>
class Unconstrained;

// ********************************************************************* //
// ************************** SIMPLE BOUNDS **************************** //
// ********************************************************************* //
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons =
              mpc::TerminalCons::SameCons,
          template <class, class> class SoftCons = mpc::SoftCons::Unconstrained>
class SimpleBounds : public SoftCons<real_t, int_t>,
                     public TerminalCons<real_t, int_t> {
private:
  // Using
  using SoC = SoftCons<real_t, int_t>;
  using TC = TerminalCons<real_t, int_t>;
  using mat_t = typename std::conditional<
      (detail::is_pdmat<Q_t>::value) || (detail::softcons<SoftCons>::linear),
      matrix::PDMat<real_t, int_t>, matrix::DiagMat<real_t, int_t>>::type;
  using mat_tp =
      typename std::conditional<detail::is_pdmat<P_t>::value ||
                                    (!detail::samecons<TerminalCons>::value) ||
                                    detail::softcons<SoftCons>::linear,
                                matrix::PDMat<real_t, int_t>,
                                matrix::DiagMat<real_t, int_t>>::type;

  // Problem data
  const int_t m, n, T;
  Q_t<real_t, int_t> Q;
  P_t<real_t, int_t> P;
  std::vector<real_t> A;
  const std::vector<real_t> lb, ub;
  const int_t k;

  // Vector variables
  std::vector<real_t> x, x_aff, x_cc;
  std::vector<real_t> lambda, s, d;
  std::vector<real_t> lambda_aff, s_aff;
  std::vector<real_t> lambda_cc, s_cc;
  std::vector<real_t> rdx, ri;

  // Matrix variables
  std::vector<mat_t> phiq;
  mat_tp phip;
  std::vector<real_t> phiqAt;
  std::vector<real_t> Xq;

  // Auxiliary variables
  real_t alpha;
  std::vector<real_t> aux;
  std::vector<real_t> identity;

  // Different initializations depending on the input data
  void initialize(std::false_type);
  void initialize(std::true_type);

  // First block forward solve
  void aff_fs_fb(std::false_type, real_t *L, real_t *M, const real_t *nu,
                 real_t *nu_aff);
  void aff_fs_fb(std::true_type, real_t *L, real_t *M, const real_t *nu,
                 real_t *nu_aff);
  // kth block forward solve
  void aff_fs_nb(std::false_type, real_t *L, real_t *M, const real_t *nu,
                 real_t *nu_aff, const int_t k);
  void aff_fs_nb(std::true_type, real_t *L, real_t *M, const real_t *nu,
                 real_t *nu_aff, const int_t k);
  // Last block forward solve
  void aff_fs_lb(std::false_type, real_t *L, const real_t *nu, real_t *nu_aff);
  void aff_fs_lb_2(std::false_type, real_t *L, const real_t *nu,
                   real_t *nu_aff);
  void aff_fs_lb_2(std::true_type, real_t *L, const real_t *nu, real_t *nu_aff);
  void aff_fs_lb(std::true_type, real_t *L, const real_t *nu, real_t *nu_aff);
  // Last block backwards solve
  void aff_bs_lb(std::false_type, const real_t *nu_aff);
  void aff_bs_lb(std::true_type, const real_t *nu_aff);

  // Last block forward solve
  void cc_fs_lb(std::false_type, real_t *nu_cc);
  void cc_fs_lb(std::true_type, real_t *nu_cc);

  // Last block backward solve
  void cc_bs_lb(std::false_type, const real_t *nu_cc);
  void cc_bs_lb(std::true_type, const real_t *nu_cc);

  void reset_identity();

public:
  SimpleBounds() = default;
  SimpleBounds(const int_t n, const int_t m, const int_t T,
               Q_t<real_t, int_t> &Q, P_t<real_t, int_t> &P,
               std::vector<real_t> &A, std::vector<real_t> &lb,
               std::vector<real_t> &ub,
               std::vector<real_t> &F = std::vector<real_t>(0),
               std::vector<real_t> &f = std::vector<real_t>(0),
               std::vector<real_t> &slb = std::vector<real_t>(0),
               std::vector<real_t> &sub = std::vector<real_t>(0),
               std::vector<real_t> &sF = std::vector<real_t>(0),
               std::vector<real_t> &sf = std::vector<real_t>(0),
               std::vector<real_t> &lbf = std::vector<real_t>(0),
               std::vector<real_t> &ubf = std::vector<real_t>(0),
               std::vector<real_t> &Ff = std::vector<real_t>(0),
               std::vector<real_t> &ff = std::vector<real_t>(0),
               std::vector<real_t> &slbf = std::vector<real_t>(0),
               std::vector<real_t> &subf = std::vector<real_t>(0),
               std::vector<real_t> &sFf = std::vector<real_t>(0),
               std::vector<real_t> &sff = std::vector<real_t>(0));

  // Forward solve: first, nth and last block
  void aff_fs_fb(real_t *L, real_t *M, const real_t *nu, real_t *nu_aff);
  void aff_fs_nb(real_t *L, real_t *M, const real_t *nu, real_t *nu_aff,
                 const int_t k);
  void aff_fs_lb(real_t *L, const real_t *nu, real_t *nu_aff);
  // Intermediate computations
  void aff_ic();
  // Backsolve: last block, nth block
  void aff_bs_lb(const real_t *nu_aff);
  void aff_bs_nb(const real_t *nu_aff, const int_t k);
  // Final computations
  void aff_fc();

  // Pre-computations
  void cc_pc(real_t sigma_mu_aff);
  // Forward solve: first block, nth block, last block
  void cc_fs_fb(real_t *nu_cc);
  void cc_fs_nb(real_t *nu_cc, const int_t k);
  void cc_fs_lb(real_t *nu_cc);
  // Intermediate computations
  void cc_ic();
  // Backward solve: last block, nth block
  void cc_bs_lb(const real_t *nu_cc);
  void cc_bs_nb(const real_t *nu_cc, int_t k);
  void cc_fc();

  real_t duality_gap() const;
  real_t affine_gap(const real_t &, const real_t &);
  real_t affine_gap(const real_t &);
  void linesearch(real_t &alpha_p, real_t &alpha_d);
  void linesearch(real_t &alpha);
  void take_step(const real_t);
  void new_x0();
  void eq_residual(real_t *, const real_t *) const;
  void eq_residual(real_t *, const int_t) const;

  std::vector<real_t> optimal_states() const { return x; };
  void opt_val(real_t &) const;
  int_t ncons() const;
};

// ********************************************************************* //
// ************************** UNCONSTRAINED **************************** //
// ********************************************************************* //
template <class real_t, class int_t, template <class, class> class Q_t,
          template <class, class> class P_t,
          template <class, class> class TerminalCons =
              mpc::TerminalCons::SameCons,
          template <class, class> class SoftCons = mpc::SoftCons::Unconstrained>
class Unconstrained : public SoftCons<real_t, int_t>,
                      public TerminalCons<real_t, int_t> {
private:
  // Using
  using SoC = SoftCons<real_t, int_t>;
  using TC = TerminalCons<real_t, int_t>;
  using mat_t = typename std::conditional<
      (detail::is_pdmat<Q_t>::value) || (detail::softcons<SoftCons>::linear),
      matrix::PDMat<real_t, int_t>, matrix::DiagMat<real_t, int_t>>::type;
  using mat_tp = typename std::conditional<
      detail::is_pdmat<P_t>::value || !detail::samecons<TerminalCons>::value ||
          detail::softcons<SoftCons>::linear,
      matrix::PDMat<real_t, int_t>, matrix::DiagMat<real_t, int_t>>::type;

  // Problem data
  const int_t m, n, T;
  Q_t<real_t, int_t> Q;
  P_t<real_t, int_t> P;
  std::vector<real_t> A;
  const int_t k;

  // Vector variables
  std::vector<real_t> x, x_aff, x_cc;
  std::vector<real_t> lambda, s, d;
  std::vector<real_t> lambda_aff, s_aff;
  std::vector<real_t> lambda_cc, s_cc;
  std::vector<real_t> rdx, ri;

  // Matrix variables
  std::vector<mat_t> phiq;
  mat_tp phip;
  std::vector<real_t> phiqAt;
  std::vector<real_t> Xq;

  // Auxiliary variables
  real_t alpha;
  std::vector<real_t> aux;
  std::vector<real_t> identity;

  // Different initializations depending on the input data
  void initialize(std::false_type);
  void initialize(std::true_type);

  // First block forward solve
  void aff_fs_fb(std::false_type, real_t *L, real_t *M, const real_t *nu,
                 real_t *nu_aff);
  void aff_fs_fb(std::true_type, real_t *L, real_t *M, const real_t *nu,
                 real_t *nu_aff);
  // kth block forward solve
  void aff_fs_nb(std::false_type, real_t *L, real_t *M, const real_t *nu,
                 real_t *nu_aff, const int_t k);
  void aff_fs_nb(std::true_type, real_t *L, real_t *M, const real_t *nu,
                 real_t *nu_aff, const int_t k);
  // Last block forward solve
  void aff_fs_lb(std::false_type, real_t *L, const real_t *nu, real_t *nu_aff);
  void aff_fs_lb_2(std::false_type, real_t *L, const real_t *nu,
                   real_t *nu_aff);
  void aff_fs_lb_2(std::true_type, real_t *L, const real_t *nu, real_t *nu_aff);
  void aff_fs_lb(std::true_type, real_t *L, const real_t *nu, real_t *nu_aff);
  // Last block backwards solve
  void aff_bs_lb(std::false_type, const real_t *nu_aff);
  void aff_bs_lb(std::true_type, const real_t *nu_aff);

  // Last block forward solve
  void cc_fs_lb(std::false_type, real_t *nu_cc);
  void cc_fs_lb(std::true_type, real_t *nu_cc);

  // Last block backward solve
  void cc_bs_lb(std::false_type, const real_t *nu_cc);
  void cc_bs_lb(std::true_type, const real_t *nu_cc);

  void reset_identity();

public:
  Unconstrained() = default;
  Unconstrained(const int_t n, const int_t m, const int_t T,
                Q_t<real_t, int_t> &Q, P_t<real_t, int_t> &P,
                std::vector<real_t> &A, std::vector<real_t> &lb,
                std::vector<real_t> &ub,
                std::vector<real_t> &F = std::vector<real_t>(0),
                std::vector<real_t> &f = std::vector<real_t>(0),
                std::vector<real_t> &slb = std::vector<real_t>(0),
                std::vector<real_t> &sub = std::vector<real_t>(0),
                std::vector<real_t> &sF = std::vector<real_t>(0),
                std::vector<real_t> &sf = std::vector<real_t>(0),
                std::vector<real_t> &lbf = std::vector<real_t>(0),
                std::vector<real_t> &ubf = std::vector<real_t>(0),
                std::vector<real_t> &Ff = std::vector<real_t>(0),
                std::vector<real_t> &ff = std::vector<real_t>(0),
                std::vector<real_t> &slbf = std::vector<real_t>(0),
                std::vector<real_t> &subf = std::vector<real_t>(0),
                std::vector<real_t> &sFf = std::vector<real_t>(0),
                std::vector<real_t> &sff = std::vector<real_t>(0));

  // Forward solve: first, nth and last block
  void aff_fs_fb(real_t *L, real_t *M, const real_t *nu, real_t *nu_aff);
  void aff_fs_nb(real_t *L, real_t *M, const real_t *nu, real_t *nu_aff,
                 const int_t k);
  void aff_fs_lb(real_t *L, const real_t *nu, real_t *nu_aff);
  // Intermediate computations
  void aff_ic();
  // Backsolve: last block, nth block
  void aff_bs_lb(const real_t *nu_aff);
  void aff_bs_nb(const real_t *nu_aff, const int_t k);
  // Final computations
  void aff_fc();

  // Pre-computations
  void cc_pc(real_t sigma_mu_aff);
  // Forward solve: first block, nth block, last block
  void cc_fs_fb(real_t *nu_cc);
  void cc_fs_nb(real_t *nu_cc, const int_t k);
  void cc_fs_lb(real_t *nu_cc);
  // Intermediate computations
  void cc_ic();
  // Backward solve: last block, nth block
  void cc_bs_lb(const real_t *nu_cc);
  void cc_bs_nb(const real_t *nu_cc, int_t k);
  void cc_fc();

  real_t duality_gap() const;
  real_t affine_gap(const real_t &, const real_t &);
  real_t affine_gap(const real_t &);
  void linesearch(real_t &alpha_p, real_t &alpha_d);
  void take_step(const real_t);
  void new_x0();
  void eq_residual(real_t *, const real_t *) const;
  void eq_residual(real_t *, const int_t) const;

  std::vector<real_t> optimal_states() const { return x; };
  void opt_val(real_t &);
  int_t ncons() const;
};

} // namespace StateCons
} // namespace mpc

#endif
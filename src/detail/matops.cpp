#ifndef MATOPS_CPP_
#define MATOPS_CPP_

#include <vector>

// ************************************************************************** //
// ************************** Load LAPACK functions ************************* //
// ************************************************************************** //
extern "C" {
// Vector operations
void saxpy_(const int *, const float *, const float *, const int *, float *,
            const int *);
void daxpy_(const int *, const double *, const double *, const int *, double *,
            const int *);
float sdot_(const int *, const float *, const int *, const float *,
            const int *);
double ddot_(const int *, const double *, const int *, const double *,
             const int *);
// Vector matrix operations
void sgemv_(const char *, const int *, const int *, const float *,
            const float *, const int *, const float *, const int *,
            const float *, float *, const int *);
void dgemv_(const char *, const int *, const int *, const double *,
            const double *, const int *, const double *, const int *,
            const double *, double *, const int *);
// Positive definite, packed  matrix routines
void spptrf_(const char *, const int *, float *, int *);
void spptrs_(const char *, const int *, const int *, const float *, float *,
             const int *, int *);
void dpptrf_(const char *, const int *, double *, int *);
void dpptrs_(const char *, const int *, const int *, const double *, double *,
             const int *, int *);
void sspmv_(const char *, const int *, const float *, const float *,
            const float *, const int *, const float *, float *, const int *);
void dspmv_(const char *, const int *, const double *, const double *,
            const double *, const int *, const double *, double *, const int *);
void sspr_(const char *, const int *, const float *, const float *, const int *,
           float *);
void dspr_(const char *, const int *, const double *, const double *,
           const int *, double *);
//  general matrix routines
void sgetrf_(const int *, const int *, float *, const int *, int *, int *);
void sgetrs_(const char *, const int *, const int *, const float *, const int *,
             const int *, float *, const int *, const int *);
void dgetrf_(const int *, const int *, double *, const int *, int *, int *);
void dgetrs_(const char *, const int *, const int *, const double *,
             const int *, const int *, double *, const int *, const int *);
// Triangular, packed matrix routines
void stptrs_(const char *, const char *, const char *, const int *, const int *,
             const float *, float *, const int *, const int *);
void dtptrs_(const char *, const char *, const char *, const int *, const int *,
             const double *, double *, const int *, const int *);
void strttp_(const char *, const int *, const float *, const int *, float *,
             const int *);
void dtrttp_(const char *, const int *, const double *, const int *, double *,
             const int *);
}

namespace mpc {

namespace matrix {
template <class val_t, class idx_t> struct IMatrix;
template <class val_t, class idx_t> struct DiagMat;
template <class val_t, class idx_t> struct PDMat;
} // namespace matrix

namespace detail {

template <class real_t> struct MatOps;

// ************************************************************************** //
// ************************* SPECIALIZATION IN FLOAT ************************ //
// ************************************************************************** //
template <> struct MatOps<float *> {
  /**
   * @brief Inplace vector sum y = alpha*x + y
   *
   * @param N Number of elements in input vector
   * @param a Scalar alpha multiplying x
   * @param x First vector
   * @param incx Increment in first vector
   * @param y Second vector
   * @param incy Increment in second vector
   */
  static void vecsum(const int N, const float a, const float *x, const int incx,
                     float *y, const int incy) {
    saxpy_(&N, &a, x, &incx, y, &incy);
  }

  /**
   * @brief Inplace vector sum y = alpha*x + y, with increment one
   *
   * @param N Number of elements in input vector
   * @param a Scalar alpha multiplying x
   * @param x First vector
   * @param y Second vector
   */
  static void vecsum(const int N, const float a, const float *x, float *y) {
    int inc = 1;
    saxpy_(&N, &a, x, &inc, y, &inc);
  }

  /**
   * @brief Returns the dot product of two vectors
   *
   * NOTE: Call dotproduct on (x,x) for the square of the euclidean norm
   *
   * @param N Vector size
   * @param x Pointer to first vector
   * @param incx Increment in vector x
   * @param y Pointer to second vector
   * @param incy Increment in vector y
   * @return float Dot product <x,y>
   */
  static float dotproduct(int N, const float *x, const int incx, const float *y,
                          const int incy) {
    return sdot_(&N, x, &incx, y, &incy);
  }

  /**
   * @brief Returns the dot product of two vectors, assuming increment equal to
   * one in both of them
   *
   * NOTE: Call dotproduct on (x,x) for the square of the euclidean norm
   *
   * @param N Vector size
   * @param x Pointer to first vector
   * @param y Pointer to second vector
   * @return float Dot product <x,y>
   */
  static float dotproduct(int N, const float *x, const float *y) {
    int inc{1};
    return sdot_(&N, x, &inc, y, &inc);
  }

  /**
   * @brief Affine operator y = alpha*A*x + beta*y
   *
   * @param trans Use 'N' for normal matrix, use 'T' for matrix transpose
   * @param m Number of rows in A
   * @param n Number of columns in A
   * @param alpha Scalar alpha
   * @param A Affine operator
   * @param lda Leading dimension of A (m in column major format)
   * @param x Transformed vector
   * @param incx Increment of the elements in x
   * @param beta Scalar beta
   * @param y Destination vector
   * @param incy Increment of the elements in y
   */
  static void affoperator(const char trans, const int m, const int n,
                          const float alpha, const float *A, const int lda,
                          const float *x, const int incx, const float beta,
                          float *y, const int incy) {
    sgemv_(&trans, &m, &n, &alpha, A, &lda, x, &incx, &beta, y, &incy);
  }

  /**
   * @brief Affine operator y = alpha*A*x + beta*y, where increment = 1 is
   * assumed
   *
   * @param trans Use 'N' for normal matrix, use 'T' for matrix transpose
   * @param m Number of rows in A
   * @param n Number of columns in A
   * @param alpha Scalar alpha
   * @param A Affine operator
   * @param lda Leading dimension of A (m in column major format)
   * @param x Transformed vector
   * @param beta Scalar beta
   * @param y Destination vector
   */
  static void affoperator(const char trans, const int m, const int n,
                          const float alpha, const float *A, const int lda,
                          const float *x, const float beta, float *y) {
    int inc{1};
    sgemv_(&trans, &m, &n, &alpha, A, &lda, x, &inc, &beta, y, &inc);
  }

  /**
   * @brief Affine operator y = alpha*Ax + beta*y, where A is a symmetric matrix
   * supplied in lower triangular packed form
   *
   * @param n Size of A
   * @param alpha Scalar alpha
   * @param A Affine operator in packed form
   * @param x Transformed vector
   * @param incx Increment of the elements in x
   * @param beta Scalar beta
   * @param y Detination vector
   * @param incy Increment of the elements in y
   */
  static void ppoperator(const int n, const float alpha, const float *A,
                         const float *x, int incx, const float beta, float *y,
                         int incy) {
    char uplo{'L'};
    sspmv_(&uplo, &n, &alpha, A, x, &incx, &beta, y, &incy);
  }

  /**
   * @brief Affine operator y = alpha*Ax + beta*y, where A is a symmetric matrix
   * supplied in lower triangular packed form. Increment = 1 is assumed both for
   * x and y
   *
   * @param n Size of A
   * @param alpha Scalar alpha
   * @param A Affine operator in packed form
   * @param x Transformed vector
   * @param beta Scalar beta
   * @param y Detination vector
   */
  static void ppoperator(const int n, const float alpha, const float *A,
                         const float *x, const float beta, float *y) {
    char uplo{'L'};
    int inc{1};
    sspmv_(&uplo, &n, &alpha, A, x, &inc, &beta, y, &inc);
  }

  /**
   * @brief Affine operator y = Ax + y, where A is a symmetric matrix
   * supplied in lower triangular packed form. Increment = 1 is assumed both for
   * x and y
   *
   * @param n Size of A
   * @param A Affine operator in packed form
   * @param x Transformed vector
   * @param y Detination vector
   */
  static void ppoperator(const int n, const float *A, const float *x,
                         float *y) {
    char uplo{'L'};
    int inc{1};
    float alpha{1};
    sspmv_(&uplo, &n, &alpha, A, x, &inc, &alpha, y, &inc);
  }

  /**
   * @brief Positive, packed matrix factorization
   *
   * @param uplo 'L' for lower triangular, 'U' for upper triangular
   * reference
   * @param n Matrix dimension
   * @param matrix Pointer to data
   */
  static void ppfactor(const char uplo, const int n, float *matrix) {
    int info{0};
    spptrf_(&uplo, &n, matrix, &info);
    // check info
  }

  /**
   * @brief Positive, packed matrix backsolve using the factorization from
   * ppfactor
   *
   * @param uplo 'L' for lower triangular, 'U' for upper triangular reference
   * @param n Matrix dimension
   * @param nrhs Number of right-hand-side columns
   * @param factor Pointer to factorization
   * @param b Right-hand-side
   * @param ldb Leading dimension of b (number of rows)
   */
  static void ppsolve(const char uplo, const int n, const int nrhs,
                      const float *factor, float *b, const int ldb) {
    int info{0};
    spptrs_(&uplo, &n, &nrhs, factor, b, &ldb, &info);
    // check info
  }

  /**
   * @brief Solves the system Lx=b or L'x=b, where L is packed, lower triangular
   *
   * @param T Use 'T' for transpose triangular matrix, use 'N' for normal one
   * @param n Number of rows of the triangular matrix
   * @param L Lower triangular matrix
   * @param b Right hand side
   */
  static void tpsolve(const char T, const int n, const float *L, float *b,
                      int nrhs = 1) {
    char uplo = 'L', diag = 'N';
    int info{0};
    stptrs_(&uplo, &T, &diag, &n, &nrhs, L, b, &n, &info);
  }

  /**
   * @brief Does the rank update A += alpha * x * x', where A is a symmetric
   * matrix supplied in packed format
   *
   * @param N Dimension of A
   * @param alpha Real number
   * @param x Vector
   * @param incx Increment in x
   * @param A Symmetric matirx in packed format (lower triangular)
   */
  static void pprank(const int N, const float alpha, const float *x,
                     const int incx, float *A) {
    char uplo{'L'};
    sspr_(&uplo, &N, &alpha, x, &incx, A);
  }

  /**
   * @brief Does the rank k update A += alpha * X * X', where A is a symmetric
   * matrix supplied in packed format and X is a full matrix of dimension N x M
   *
   * @param N Dimension of A
   * @param alpha Real number
   * @param X Matrix of dimensions (N,M)
   * @param incx Increment in x
   * @param A Symmetric matrix in packed format (lower triangular)
   * @param M Number of columns in X (if M=1 is used, pprank is preferred)
   */
  static void pprankk(const int N, const float alpha, const float *X,
                      const int incx, float *A, const int M = 1) {
    char uplo{'L'};
    for (int i = 0; i < M; i++)
      sspr_(&uplo, &N, &alpha, X + i * N, &incx, A);
  }

  /**
   * @brief Does the rank k update A += alpha * X' * X, where A is a symmetric
   * matrix NxN supplied in packed format and X is a full matrix of dimension M
   * x N
   *
   * @param N Dimension of A
   * @param alpha Real number
   * @param X Matrix of dimensions (M,N)
   * @param A Symmetric matrix in packed format (lower triangular)
   * @param M Number of rows in X (if M=1 is used, pprank is preferred)
   */
  static void pprankkt(const int N, const float alpha, const float *X, float *A,
                       const int M = 1) {
    char uplo{'L'};
    for (int i = 0; i < M; i++)
      sspr_(&uplo, &N, &alpha, X + i, &M, A);
  }

  /**
   * @brief General m by n matrix LU factorization
   *
   * @param m Number of rows
   * @param n Number of columns
   * @param matrix Array of appropriate dimension
   * @param lda Leading dimension of A (number of rows)
   * @param ipiv Integer vector for the permutation indexing
   */
  static void gefactor(const int m, const int n, float *matrix, const int lda,
                       int *ipiv) {
    int info{0};
    sgetrf_(&m, &n, matrix, &lda, ipiv, &info);
    // check info
  }

  /**
   * @brief Solve the system Gx = b, where G is a general mxn matrix that has
   * been factorized using getrf
   *
   * @param trans Specifies whether you solve for G or G'
   * @param n Number of rows of matrix G
   * @param nrhs Number of right hand sides
   * @param factor Factorization G = LU as returned by getrf
   * @param lda Leading dimension of G (number of rows)
   * @param ipiv Pivoting array as returned by getrf
   * @param b Right hand side
   * @param ldb Leading dimension of b (number of rows)
   */
  static void gesolve(const char trans, const int n, const int nrhs,
                      const float *factor, const int lda, const int *ipiv,
                      float *b, const int ldb) {
    int info{0};
    sgetrs_(&trans, &n, &nrhs, factor, &lda, ipiv, b, &ldb, &info);
    // check info
  }

  /**
   * @brief Takes a symmetric matrix F in full format and writes it in packed
   * format in P
   *
   * @param n Size of the input matrix (number of rows / columns)
   * @param F Input matrix (full matrix)
   * @param P Output matrix (packed matrix)
   */
  static void pd2pp(const int n, const float *F, float *P) {
    int info{0};
    char uplo{'L'};
    strttp_(&uplo, &n, F, &n, P, &info);
  }
};

// ************************************************************************** //
// ************************* SPECIALIZATION IN DOUBLE *********************** //
// ************************************************************************** //
template <> struct MatOps<double *> {
  /**
   * @brief Inplace vector sum y = alpha*x + y
   *
   * @param N Number of elements in input vector
   * @param a Scalar alpha multiplying x
   * @param x First vector
   * @param incx Increment in first vector
   * @param y Second vector
   * @param incy Increment in second vector
   */
  static void vecsum(const int N, const double a, const double *x,
                     const int incx, double *y, const int incy) {
    daxpy_(&N, &a, x, &incx, y, &incy);
  }

  /**
   * @brief Inplace vector sum y = alpha*x + y, with increment one
   *
   * @param N Number of elements in input vector
   * @param a Scalar alpha multiplying x
   * @param x First vector
   * @param y Second vector
   */
  static void vecsum(const int N, const double a, const double *x, double *y) {
    // for (int idx = 0; idx < N; idx++)
    //   y[idx] += a * x[idx];
    int inc = 1;
    daxpy_(&N, &a, x, &inc, y, &inc);
  }

  /**
   * @brief Returns the dot product of two vectors
   *
   * NOTE: Call dotproduct on (x,x) for the square of the euclidean norm
   *
   * @param N Vector size
   * @param x Pointer to first vector
   * @param incx Increment in vector x
   * @param y Pointer to second vector
   * @param incy Increment in vector y
   * @return double Dot product <x,y>
   */
  static double dotproduct(int N, const double *x, const int incx,
                           const double *y, const int incy) {
    return ddot_(&N, x, &incx, y, &incy);
  }

  /**
   * @brief Returns the dot product of two vectors, assuming increment equal to
   * one in both of them
   *
   * NOTE: Call dotproduct on (x,x) for the square of the euclidean norm
   *
   * @param N Vector size
   * @param x Pointer to first vector
   * @param y Pointer to second vector
   * @return double Dot product <x,y>
   */
  static double dotproduct(int N, const double *x, const double *y) {
    int inc{1};
    return ddot_(&N, x, &inc, y, &inc);
  }

  /**
   * @brief Affine operator y = alpha*A*x + beta*y
   *
   * @param trans Use 'N' for normal matrix, use 'T' for matrix transpose
   * @param m Number of rows in A
   * @param n Number of columns in A
   * @param alpha Scalar alpha
   * @param A Affine operator
   * @param lda Leading dimension of A (m in column major format)
   * @param x Transformed vector
   * @param incx Increment of the elements in x
   * @param beta Scalar beta
   * @param y Destination vector
   * @param incy Increment of the elements in y
   */
  static void affoperator(const char trans, const int m, const int n,
                          const double alpha, const double *A, const int lda,
                          const double *x, const int incx, const double beta,
                          double *y, const int incy) {
    dgemv_(&trans, &m, &n, &alpha, A, &lda, x, &incx, &beta, y, &incy);
  }

  /**
   * @brief Affine operator y = alpha*A*x + beta*y, where increment = 1 is
   * assumed
   *
   * @param trans Use 'N' for normal matrix, use 'T' for matrix transpose
   * @param m Number of rows in A
   * @param n Number of columns in A
   * @param alpha Scalar alpha
   * @param A Affine operator
   * @param lda Leading dimension of A (m in column major format)
   * @param x Transformed vector
   * @param beta Scalar beta
   * @param y Destination vector
   */
  static void affoperator(const char trans, const int m, const int n,
                          const double alpha, const double *A, const int lda,
                          const double *x, const double beta, double *y) {
    int inc{1};
    dgemv_(&trans, &m, &n, &alpha, A, &lda, x, &inc, &beta, y, &inc);
  }

  /**
   * @brief Affine operator y = alpha*Ax + beta*y, where A is a symmetric matrix
   * supplied in lower triangular packed form
   *
   * @param n Size of A
   * @param alpha Scalar alpha
   * @param A Affine operator in packed form
   * @param x Transformed vector
   * @param incx Increment of the elements in x
   * @param beta Scalar beta
   * @param y Detination vector
   * @param incy Increment of the elements in y
   */
  static void ppoperator(const int n, const double alpha, const double *A,
                         const double *x, int incx, const double beta,
                         double *y, int incy) {
    char uplo{'L'};
    dspmv_(&uplo, &n, &alpha, A, x, &incx, &beta, y, &incy);
  }

  /**
   * @brief Affine operator y = alpha*Ax + beta*y, where A is a symmetric matrix
   * supplied in lower triangular packed form. Increment = 1 is assumed both for
   * x and y
   *
   * @param n Size of A
   * @param alpha Scalar alpha
   * @param A Affine operator in packed form
   * @param x Transformed vector
   * @param beta Scalar beta
   * @param y Detination vector
   */
  static void ppoperator(const int n, const double alpha, const double *A,
                         const double *x, const double beta, double *y) {
    char uplo{'L'};
    int inc{1};
    dspmv_(&uplo, &n, &alpha, A, x, &inc, &beta, y, &inc);
  }

  /**
   * @brief Affine operator y = Ax + y, where A is a symmetric matrix
   * supplied in lower triangular packed form. Increment = 1 is assumed both for
   * x and y
   *
   * @param n Size of A
   * @param A Affine operator in packed form
   * @param x Transformed vector
   * @param y Detination vector
   */
  static void ppoperator(const int n, const double *A, const double *x,
                         double *y) {
    char uplo{'L'};
    int inc{1};
    double alpha{1};
    dspmv_(&uplo, &n, &alpha, A, x, &inc, &alpha, y, &inc);
  }

  /**
   * @brief Positive, packed matrix factorization
   *
   * @param uplo 'L' for lower triangular, 'U' for upper triangular reference
   * @param n Matrix dimension
   * @param matrix Pointer to data
   */
  static void ppfactor(const char uplo, const int n, double *matrix) {
    int info{0};
    dpptrf_(&uplo, &n, matrix, &info);
    // check info
  }

  /**
   * @brief Positive, packed matrix backsolve using the factorization from
   * ppfactor
   *
   * @param uplo 'L' for lower triangular, 'U' for upper triangular reference
   * @param n Matrix dimension
   * @param nrhs Number of right-hand-side columns
   * @param factor Pointer to factorization
   * @param b Right-hand-side
   * @param ldb Leading dimension of b (number of rows)
   */
  static void ppsolve(const char uplo, const int n, const int nrhs,
                      const double *factor, double *b, const int ldb) {
    int info{0};
    dpptrs_(&uplo, &n, &nrhs, factor, b, &ldb, &info);
    // check info
  }

  /**
   * @brief Solves the system Lx=b, where L is packed, lower triangular
   *
   * @param T Use 'T' for transpose triangular matrix, use 'N' for normal one
   * @param n Number of rows of the triangular matrix
   * @param L Lower triangular matrix
   * @param b Right hand side
   */
  static void tpsolve(const char T, const int n, const double *L, double *b,
                      int nrhs = 1) {
    char uplo = 'L', diag = 'N';
    int info{0};
    dtptrs_(&uplo, &T, &diag, &n, &nrhs, L, b, &n, &info);
  }

  /**
   * @brief Does the rank update A += alpha * x * x', where A is a symmetric
   * matrix supplied in packed format
   *
   * @param N Dimension of A
   * @param alpha Real number
   * @param x Vector
   * @param incx Increment in x
   * @param A Symmetric matirx in packed format (lower triangular)
   */
  static void pprank(const int N, const double alpha, const double *x,
                     const int incx, double *A) {
    char uplo{'L'};
    dspr_(&uplo, &N, &alpha, x, &incx, A);
  }

  /**
   * @brief Does the rank k update A += alpha * X * X', where A is a symmetric
   * matrix supplied in packed format and X is a full matrix of dimension N x M
   *
   * @param N Dimension of A
   * @param alpha Real number
   * @param X Matrix of dimensions (N,M)
   * @param incx Increment in x
   * @param A Symmetric matrix in packed format (lower triangular)
   * @param M Number of columns in X (if M=1 is used, pprank is preferred)
   */
  static void pprankk(const int N, const double alpha, const double *X,
                      const int incx, double *A, const int M = 1) {
    char uplo{'L'};
    for (int i = 0; i < M; i++)
      dspr_(&uplo, &N, &alpha, X + i * N, &incx, A);
  }

  /**
   * @brief Does the rank k update A += alpha * X' * X, where A is a symmetric
   * matrix supplied in packed format and X is a full matrix of dimension M x N
   *
   * @param N Dimension of A
   * @param alpha Real number
   * @param X Matrix of dimensions (M,N)
   * @param A Symmetric matrix in packed format (lower triangular)
   * @param M Number of rows in X (if M=1 is used, pprank is preferred)
   */
  static void pprankkt(const int N, const double alpha, const double *X,
                       double *A, const int M = 1) {
    char uplo{'L'};
    for (int i = 0; i < M; i++)
      dspr_(&uplo, &N, &alpha, X + i, &M, A);
  }

  /**
   * @brief General m by n matrix LU factorization
   *
   * @param m Number of rows
   * @param n Number of columns
   * @param matrix Array of appropriate dimension
   * @param lda Leading dimension of A (number of rows)
   * @param ipiv Integer vector for the permutation indexing
   */
  static void gefactor(const int m, const int n, double *matrix, const int lda,
                       int *ipiv) {
    int info{0};
    dgetrf_(&m, &n, matrix, &lda, ipiv, &info);
    // check info
  }

  /**
   * @brief Solve the system Gx = b, where G is a general mxn matrix that has
   * been factorized using getrf
   *
   * @param trans Specifies whether you solve for G or G'
   * @param n Number of rows of matrix G
   * @param nrhs Number of right hand sides
   * @param factor Factorization G = LU as returned by getrf
   * @param lda Leading dimension of G (number of rows)
   * @param ipiv Pivoting array as returned by getrf
   * @param b Right hand side
   * @param ldb Leading dimension of b (number of rows)
   */
  static void gesolve(const char trans, const int n, const int nrhs,
                      const double *factor, const int lda, const int *ipiv,
                      double *b, const int ldb) {
    int info{0};
    dgetrs_(&trans, &n, &nrhs, factor, &lda, ipiv, b, &ldb, &info);
    // check info
  }

  /**
   * @brief Takes a symmetric matrix F in full format and writes it in packed
   * format in P
   *
   * @param n Size of the input matrix (number of rows / columns)
   * @param F Input matrix (full matrix)
   * @param P Output matrix (packed matrix)
   */
  static void pd2pp(const int n, const double *F, double *P) {
    int info{0};
    char uplo{'L'};
    dtrttp_(&uplo, &n, F, &n, P, &info);
  }
};

/**
 * @brief In-place matrix transposition
 *
 * @tparam real_t Floating point type
 * @tparam int_t Integer type
 * @param m Rows in A (columns in A^T)
 * @param n Columns in A (rows in A^T)
 * @param A Matrix
 */
template <class real_t, class int_t>
void transpose(const int_t m, const int_t n, real_t *A) {
  std::vector<real_t> aux(n * m);
  std::copy(A, A + n * m, aux.begin());
  for (int_t i = 0; i < n; i++)   // Rows in transposed matrix
    for (int_t j = 0; j < m; j++) // Columns in transposed matrix
      A[i + j * n] = aux[j + i * m];
}

/**
 * @brief In-place matrix transposition, taking a vector as input
 *
 * @tparam real_t Floating point type
 * @tparam int_t Integer type
 * @param m Rows in A (columns in A^T)
 * @param n Columns in A (rows in A^T)
 * @param A Matrix
 */
template <class real_t, class int_t>
void transpose(const int_t m, const int_t n, std::vector<real_t> &A) {
  std::vector<real_t> aux{A};
  for (int_t i = 0; i < n; i++)   // Rows in transposed matrix
    for (int_t j = 0; j < m; j++) // Columns in transposed matrix
      A[i + j * n] = aux[j + i * m];
}

/**
 * @brief Matrix addition L := alpha * D + L, where L is a symmetric matrix
 * supplied in lower triangular form and D is a diagonal matrix
 *
 * @tparam real_t
 * @tparam int_t
 * @param alpha
 * @param D
 * @param L
 */
template <class real_t, class int_t>
void matsum(const real_t alpha, const mpc::matrix::DiagMat<real_t, int_t> &D,
            real_t *L) {
  for (int_t i = 0; i < D.nrows(); i++)
    L[i + (2 * D.nrows() - i - 1) * i / 2] += D[i];
}

/**
 * @brief Matrix addition L := alpha * P + L, where L is a symmetric matrix
 * supplied in lower triangular form and P is a symmetric matrix supplied in
 * lower triangular form
 *
 * @tparam real_t
 * @tparam int_t
 * @param alpha
 * @param P
 * @param L
 */
template <class real_t, class int_t>
void matsum(const real_t alpha, const mpc::matrix::PDMat<real_t, int_t> &P,
            real_t *L) {
  MatOps<real_t *>::vecsum(P.ncols() * (P.ncols() + 1) / 2, alpha, P.data(), 1,
                           L, 1);
}

/**
 * @brief Affine operator y := alpha*A*x + beta*y, where A is a diagonal matrix
 *
 * @tparam real_t
 * @tparam int_t
 * @param alpha Scalar alpha
 * @param A Diagonal matrix (affine operator)
 * @param x Transformed vector
 * @param beta Scalar beta
 * @param y Detination vector
 */
template <class real_t, class int_t>
void affoperator(const real_t alpha, const matrix::DiagMat<real_t, int_t> &A,
                 const real_t *x, const real_t beta, real_t y) {
  for (int_t i = 0; i < A.ncols(); i++)
    y[i] = alpha * A[i] * x[i] + beta * y[i];
}

/**
 * @brief Affine operator y := A*x + y, where A is a diagonal matrix
 *
 * @tparam real_t
 * @tparam int_t
 * @param A Diagonal matrix (affine operator)
 * @param x Transformed vector
 * @param y Detination vector
 */
template <class real_t, class int_t>
void affoperator(const matrix::DiagMat<real_t, int_t> &A, const real_t *x,
                 real_t *y) {
  for (int_t i = 0; i < A.ncols(); i++)
    y[i] += A[i] * x[i];
}

/**
 * @brief Affine operator y := A*x, where A is a diagonal matrix
 *
 * @tparam real_t
 * @tparam int_t
 * @param A Diagonal matrix (affine operator)
 * @param x Transformed vector
 * @param y Detination vector
 */
template <class real_t, class int_t>
void affasign(const matrix::DiagMat<real_t, int_t> &A, const real_t *x,
              real_t *y) {
  for (int_t i = 0; i < A.ncols(); i++)
    y[i] = A[i] * x[i];
}

/**
 * @brief Affine operator y := alpha*A*x + beta*y, where A is a positve definite
 * matrix
 *
 * @tparam real_t
 * @tparam int_t
 * @param alpha Scalar alpha
 * @param A Diagonal matrix (affine operator)
 * @param x Transformed vector
 * @param beta Scalar beta
 * @param y Detination vector
 */
template <class real_t, class int_t>
void affoperator(const real_t alpha, const matrix::PDMat<real_t, int_t> &A,
                 const real_t *x, const real_t beta, real_t *y) {
  MatOps<real_t *>::ppoperator(A.ncols(), alpha, A.data(), x, beta, y);
}

/**
 * @brief Affine operator y := A*x + y, where A is a positive definite matrix
 *
 * @tparam real_t
 * @tparam int_t
 * @param A Diagonal matrix (affine operator)
 * @param x Transformed vector
 * @param y Detination vector
 */
template <class real_t, class int_t>
void affoperator(const matrix::PDMat<real_t, int_t> &A, const real_t *x,
                 real_t *y) {
  MatOps<real_t *>::ppoperator(A.ncols(), real_t{1}, A.data(), x, real_t{1}, y);
}

/**
 * @brief Affine operator y := A*x, where A is a positive definite matrix
 *
 * @tparam real_t
 * @tparam int_t
 * @param A Diagonal matrix (affine operator)
 * @param x Transformed vector
 * @param y Detination vector
 */
template <class real_t, class int_t>
void affasign(const matrix::PDMat<real_t, int_t> &A, const real_t *x,
              real_t *y) {
  MatOps<real_t *>::ppoperator(A.ncols(), real_t{1}, A.data(), x, real_t{0}, y);
}

template <class real_t, class int_t>
real_t quadratic_form(const matrix::DiagMat<real_t, int_t> &D,
                      const real_t *x) {
  std::vector<real_t> aux(D.ncols());
  affasign(D, x, aux.data());
  return MatOps<real_t *>::dotproduct(D.ncols(), x, aux.data());
}

template <class real_t, class int_t>
real_t quadratic_form(const matrix::PDMat<real_t, int_t> &P, const real_t *x) {
  std::vector<real_t> aux(P.ncols());
  affasign(P, x, aux.data());
  return MatOps<real_t *>::dotproduct(P.ncols(), x, aux.data());
}

} // namespace detail
} // namespace mpc

#endif
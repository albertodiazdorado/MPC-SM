#ifndef EXTRA_CPP_
#define EXTRA_CPP_

#include <algorithm>
#include <complex>
#include <cstdarg>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <random>
#include <string>
#include <vector>

#include "detail/matops.cpp"

namespace mpc {
namespace extra { /* Get the required LAPACK interface */
extern "C" {
void dgeev_(const char *, const char *, const int *, double *, const int *,
            double *, double *, double *, const int *, double *, const int *,
            double *, const int *, int *);
void sgeev_(const char *, const char *, const int *, float *, const int *,
            float *, float *, float *, const int *, float *, const int *,
            float *, const int *, int *);
void dgemm_(const char *, const char *, const int *, const int *, const int *,
            const double *, const double *, const int *, const double *,
            const int *, const double *, double *, const int *);
void sgemm_(const char *, const char *, const int *, const int *, const int *,
            const float *, const float *, const int *, const float *,
            const int *, const float *, float *, const int *);
void cgetrf_(const int *, const int *, std::complex<float> *, const int *,
             int *, int *);
void cgetrs_(const char *, const int *, const int *,
             const std::complex<float> *, const int *, const int *,
             std::complex<float> *, const int *, const int *);
void zgetrf_(const int *, const int *, std::complex<double> *, const int *,
             int *, int *);
void zgetrs_(const char *, const int *, const int *,
             const std::complex<double> *, const int *, const int *,
             std::complex<double> *, const int *, const int *);
void cgemm_(const char *, const char *, const int *, const int *, const int *,
            const std::complex<float> *, const std::complex<float> *,
            const int *, const std::complex<float> *, const int *,
            const std::complex<float> *, std::complex<float> *, const int *);
void zgemm_(const char *, const char *, const int *, const int *, const int *,
            const std::complex<double> *, const std::complex<double> *,
            const int *, const std::complex<double> *, const int *,
            const std::complex<double> *, std::complex<double> *, const int *);
void sger_(const int *, const int *, const float *, const float *, const int *,
           const float *, const int *, float *, const int *);
void dger_(const int *, const int *, const double *, const double *,
           const int *, const double *, const int *, double *, const int *);
}

// ************************************************************************ //
// ************************ FUNCTION DECLARATIONS ************************* //
// ************************************************************************ //
template <class real_t>
void matmult(const int, const int, const int, const real_t *, const real_t *,
             real_t *, const real_t = 0);

template <class real_t>
void allocate(const int, const int, const real_t *, const int, const int,
              real_t *, const int);

template <class real_t>
int mat_exponential(const int, const real_t *, real_t *);

template <class real_t>
int eigenvalue(const int, const real_t *, real_t *, real_t *, real_t *);

template <class real_t>
void rank_update(const int, const real_t, const real_t *, const int *,
                 real_t *);

template <class InputIt1, class InputIt2>
void writecsv(const std::string &, InputIt1, InputIt1, InputIt2);
// ************************************************************************ //
// ************************ FUNCTION DECLARATIONS ************************* //
// ************************************************************************ //

/* Write a wrapper to be able to use those monsters.
      A_old is the matrix whose eigenvalues shall be computed
      D is an array containing the real parto of the eigenvalues
      I imaginary part of the eigenvalues
      V is a matrix cointaining the eigenvectors
      info is an integer,returning 0 if everything went according to plan */
template <class real_t>
int eigenvalue(const int n, const real_t *A_old, real_t *D, real_t *I,
               real_t *V) {
  char JOBVL{'N'}, JOBVR{'V'};     // Get the right eigenvectors
  std::vector<real_t> left_eig(0); // Left eigenvectors not referenced
  std::vector<real_t> A(n * n);
  std::copy(A_old, A_old + n * n, A.data());
  int LWORK{8 * n};
  std::vector<real_t> WORK(LWORK); // Provide some workspace (large one)
  int info{0};
  dgeev_(&JOBVL, &JOBVR, &n, A.data(), &n, D, I, left_eig.data(), &n, V, &n,
         WORK.data(), &LWORK, &info);
  return info;
}

/* Specialize for FLOAT (single precision) */
template <>
int eigenvalue<float>(const int n, const float *A_old, float *D, float *I,
                      float *V) {
  char JOBVL{'N'}, JOBVR{'V'};    // Get the right eigenvectors
  std::vector<float> left_eig(0); // Left eigenvectors not referenced
  std::vector<float> A(n * n);
  std::copy(A_old, A_old + n * n, A.data());
  int LWORK{8 * n};
  std::vector<float> WORK(LWORK); // Provide some workspace (large one)
  int info{0};
  sgeev_(&JOBVL, &JOBVR, &n, A.data(), &n, D, I, left_eig.data(), &n, V, &n,
         WORK.data(), &LWORK, &info);
  return info;
}

/* Write a function that calculates the matrix exponential
      n: Dimension of square matrix A
      A: Matrix (n x n)
      expA: The matrix exponential of A (n x n)
*/
template <class real_t>
int mat_exponential(const int n, const real_t *A, real_t *expA) {

  /* Create the necessary auxiliar memory */
  int LWORK{8 * n}, info{0};
  std::vector<real_t> d(n), i(n), v(n * n), WORK(LWORK);
  std::vector<int> ipiv(n);

  /* Get the eigenvalues in A */
  info = eigenvalue(n, A, d.data(), i.data(), v.data());

  /* Check and exit if something went badly */
  if (info != 0)
    return info;

  /* Merge real and imaginary parts into a single complex number */
  std::vector<std::complex<real_t>> D(n);
  std::transform(d.cbegin(), d.cend(), i.cbegin(), D.begin(),
                 [](real_t d, real_t i) { return std::complex<real_t>(d, i); });

  /* Do the same with eigenvectors */
  std::vector<std::complex<real_t>> V(n * n);
  for (int eig = 0; eig < n; eig++) {
    if (i[eig] == 0)
      std::transform(v.cbegin() + eig * n, v.cbegin() + (eig + 1) * n,
                     V.begin() + eig * n,
                     [](real_t v) { return std::complex<real_t>(v, 0); });
    else {
      for (int i = 0; i < n; i++) {
        V[eig * n + i] =
            std::complex<real_t>(v[eig * n + i], v[(eig + 1) * n + i]);
        V[(eig + 1) * n + i] =
            std::complex<real_t>(v[eig * n + i], -v[(eig + 1) * n + i]);
      }
      eig++;
    }
  }

  /* Take the exponential of the eigenvalues */
  std::transform(D.cbegin(), D.cend(), D.begin(),
                 [](std::complex<real_t> v) { return std::exp(v); });

  /* Build a matrix from the exponentials and take the conjugate transpose of
  it
   */
  std::transform(D.cbegin(), D.cend(), D.begin(),
                 [](std::complex<real_t> v) { return std::conj(v); });
  std::vector<std::complex<real_t>> X(n * n);
  for (int idx = 0; idx < n; idx++)
    X[idx * n + idx] = D[idx];

  /* Factorize V in an auxiliar vector and solve V' * X = D',
        where V', D' denotes the conjugate transpose*/
  std::vector<std::complex<real_t>> aux{V};
  zgetrf_(&n, &n, aux.data(), &n, ipiv.data(), &info);
  if (info != 0)
    return info;
  char cmpl_conj{'C'};
  zgetrs_(&cmpl_conj, &n, &n, aux.data(), &n, ipiv.data(), X.data(), &n, &info);
  if (info != 0)
    return info;

  /* X' now containts the solution to X * V = D, which can be rewritten as
            V' * X' = D'.
    Next, evaluate the matrix exponential as V * X', where X' denotes again
    the
            conjugate transpose
  */
  std::vector<std::complex<real_t>> expA_complex(n * n);
  char hermitian{'C'}, notrans{'N'};
  std::complex<real_t> one(1, 0), zero(0, 0);
  zgemm_(&notrans, &hermitian, &n, &n, &n, &one, V.data(), &n, X.data(), &n,
         &zero, expA_complex.data(), &n);
  if (info != 0)
    return info;

  /* By construction, expA_complex should be pure real. Nevertheless, we will
   * just take the real parts here, and so disregard any numerical
   inaccuracy.
   */
  std::transform(expA_complex.cbegin(), expA_complex.cend(), expA,
                 [](std::complex<real_t> v) { return std::real(v); });

  /* Return info */
  return info;
}

template <>
int mat_exponential<float>(const int n, const float *A, float *expA) {

  /* Create the necessary auxiliar memory */
  int LWORK{8 * n}, info{0};
  std::vector<float> d(n), i(n), v(n * n), WORK(LWORK);
  std::vector<int> ipiv(n);

  /* Get the eigenvalues in A */
  info = eigenvalue(n, A, d.data(), i.data(), v.data());

  /* Check and exit if something went badly */
  if (info != 0)
    return info;

  /* Merge real and imaginary parts into a single complex number */
  std::vector<std::complex<float>> D(n);
  std::transform(d.cbegin(), d.cend(), i.cbegin(), D.begin(),
                 [](float d, float i) { return std::complex<float>(d, i); });

  /* Do the same with eigenvectors */
  std::vector<std::complex<float>> V(n * n);
  for (int eig = 0; eig < n * n; eig++) {
    if (i[eig] == 0)
      std::transform(v.cbegin() + eig * n, v.cbegin() + (eig + 1) * n,
                     V.begin() + eig * n,
                     [](float v) { return std::complex<float>(v, 0); });
    else
      for (int i = 0; i < n; i++) {
        V[eig * n + i] =
            std::complex<float>(v[eig * n + i], v[(eig + 1) * n + i]);
        V[(eig + 1) * n + i] =
            std::complex<float>(v[eig * n + i], -v[(eig + 1) * n + i]);
        eig++;
      }
  }

  /* Take the exponential of the eigenvalues */
  std::transform(D.cbegin(), D.cend(), D.begin(),
                 [](std::complex<float> v) { return std::exp(v); });

  /* Build a matrix from the exponentials and take the conjugate transpose of it
   */
  std::transform(D.cbegin(), D.cend(), D.begin(),
                 [](std::complex<float> v) { return std::conj(v); });
  std::vector<std::complex<float>> X(n * n);
  for (int idx = 0; idx < n; idx++)
    X[idx * n + idx] = D[idx];

  /* Factorize V in an auxiliar vector and solve V' * X = D',
        where V', D' denotes the conjugate transpose*/
  std::vector<std::complex<float>> aux{V};
  cgetrf_(&n, &n, aux.data(), &n, ipiv.data(), &info);
  if (info != 0)
    return info;
  char cmpl_conj{'C'};
  cgetrs_(&cmpl_conj, &n, &n, aux.data(), &n, ipiv.data(), X.data(), &n, &info);
  if (info != 0)
    return info;

  /* X' now containts the solution to X * V = D, which can be rewritten as
            V' * X' = D'.
    Next, evaluate the matrix exponential as V * X', where X' denotes again the
            conjugate transpose
  */
  std::vector<std::complex<float>> expA_complex(n * n);
  char hermitian{'H'}, notrans{'N'};
  std::complex<float> one(1, 0), zero(0, 0);
  cgemm_(&notrans, &hermitian, &n, &n, &n, &one, V.data(), &n, X.data(), &n,
         &zero, expA_complex.data(), &n);
  if (info != 0)
    return info;

  /* By construction, expA_complex should be pure real. Nevertheless, we will
   * just take the real parts here, and so disregard any numerical inaccuracy.
   */
  std::transform(expA_complex.cbegin(), expA_complex.cend(), expA,
                 [](std::complex<float> v) { return std::real(v); });

  /* Return info */
  return info;
}

/* Implements C = A*B + C, where
 *  A is (n x k)
 *  B is (k x m)
 *  C is (n x m)
 */
template <>
void matmult<double>(const int n, const int m, const int k, const double *A,
                     const double *B, double *C, const double beta) {
  char trans{'N'};
  double alpha{1};
  dgemm_(&trans, &trans, &n, &m, &k, &alpha, A, &n, B, &k, &beta, C, &n);
}
template <>
void matmult<float>(const int n, const int m, const int k, const float *A,
                    const float *B, float *C, const float beta) {
  char trans{'N'};
  float alpha{1};
  sgemm_(&trans, &trans, &n, &m, &k, &alpha, A, &n, B, &k, &beta, C, &n);
}

/* Write one (n by m) block a into the matrix A at position (i,j)
 * lda is the number of rows in A
 */
template <class real_t>
void allocate(const int n, const int m, const real_t *a, const int i,
              const int j, real_t *A, const int lda) {
  int skip = lda * j;
  for (int idx = 0; idx < m; idx++)
    std::copy(a + idx * n, a + (idx + 1) * n, A + skip + i + idx * lda);
}

template <class real_t>
void rank_update(const int n, const real_t alpha, const real_t *x,
                 const int incx, real_t *A) {
  dger_(&n, &n, &alpha, x, &incx, x, &incx, A, &n);
}

template <>
void rank_update<float>(const int n, const float alpha, const float *x,
                        const int incx, float *A) {
  sger_(&n, &n, &alpha, x, &incx, x, &incx, A, &n);
}

template <class Sink, class T1, class T2>
void writeline(Sink &&s, T1 &&val1, T2 &&val2) {
  s << val1 << ',' << val2 << '\n';
}

template <class Sink, class T1, class... Ts>
void writeline(Sink &&s, T1 &&val1, Ts &&... vals) {
  s << val1 << ',';
  writeline(s, std::forward<Ts>(vals)...);
}

// template <class String, class ForwardIt, class InputIt, class... InputIts>
// void writecsv(String &&filename, ForwardIt lbegin, ForwardIt lend,
//               InputIt fbegin, InputIt fend, InputIts... rbegin) {
//   std::ofstream file{filename};
//   std::size_t n(0), N(std::distance(lbegin, lend));
//   file << std::scientific;

//   while (lbegin != lend) {
//     file << *lbegin++;
//     if (n++ != N - 1)
//       file << ',';
//   }
//   file << '\n';

//   while (fbegin != fend)
//     writeline(file, *fbegin++, *rbegin++...);
//   file << '\n';
// }

template <class String, class InputIt, class... InputIts>
void writecsv(String &&filename, InputIt fbegin, InputIt fend,
              InputIts... rbegin) {
  std::ofstream file{filename};
  file << std::scientific;

  while (fbegin != fend)
    writeline(file, *fbegin++, *rbegin++...);
  file << '\n';
}

template <class real_t, class idx_t> struct mpc_system {
  idx_t nx, nu, T;
  std::vector<real_t> A, B, R, Q, P, Bd;
  std::vector<real_t> x0;
  std::vector<real_t> ubu, lbu, ubx, lbx;

  mpc_system() = default;
  mpc_system(const std::string filename) { parse_system(filename, this); }
  real_t *get_x0() { return x0.data(); }
  void set_x0(const std::vector<real_t> &x_new) {
    std::copy(x_new.cbegin(), x_new.cend(), x0.begin());
  }
  void set_x0(const real_t *x_new) { std::copy(x_new, x_new + nx, x0.begin()); }
  real_t *simulate(const real_t *u0) {
    std::vector<real_t> x1{x0};
    detail::MatOps<real_t *>::affoperator('N', nx, nx, 1., A.data(), nx,
                                          x0.data(), 0., x1.data());
    detail::MatOps<real_t *>::affoperator('N', nx, nu, 1., B.data(), nx, u0, 1.,
                                          x1.data());
    x0 = x1;
    return x0.data();
  }
  real_t *simulate(const real_t *u0, const real_t *d0) {
    std::vector<real_t> x1{x0};
    detail::MatOps<real_t *>::affoperator('N', nx, nx, 1., A.data(), nx,
                                          x0.data(), 0., x1.data());
    detail::MatOps<real_t *>::affoperator('N', nx, nu, 1., B.data(), nx, u0, 1.,
                                          x1.data());
    detail::MatOps<real_t *>::affoperator('N', nx, nx / 2, 1., Bd.data(), nx,
                                          d0, 1., x1.data());
    x0 = x1;
    return x0.data();
  }
  real_t *simulate(const std::vector<real_t> &u0) {
    return simulate(u0.data());
  }
  real_t *simulate(const std::vector<real_t> &u0,
                   const std::vector<real_t> &d0) {
    return simulate(u0.data(), d0.data());
  }
};

template <class real_t, class idx_t>
mpc_system<real_t, idx_t> parse_system(const std::string filename,
                                       mpc_system<real_t, idx_t> &sys) {
  std::ifstream input{filename, std::ios_base::binary};
  if (input) {
    input.read(reinterpret_cast<char *>(&(sys.nx)), sizeof(idx_t));
    input.read(reinterpret_cast<char *>(&(sys.nu)), sizeof(idx_t));
    input.read(reinterpret_cast<char *>(&(sys.T)), sizeof(idx_t));
    idx_t nx = sys.nx;
    idx_t nu = sys.nu;
    sys.A = std::vector<real_t>(nx * nx);
    sys.B = std::vector<real_t>(nx * nu);
    sys.R = std::vector<real_t>(nu * nu);
    sys.Q = std::vector<real_t>(nx * nx);
    sys.P = std::vector<real_t>(nx * nx);
    sys.Bd = std::vector<real_t>(nx * nx / 2);
    sys.ubu = sys.lbu = std::vector<real_t>(nu);
    sys.ubx = sys.lbx = std::vector<real_t>(nx);
    input.read(reinterpret_cast<char *>(sys.A.data()),
               nx * nx * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.B.data()),
               nx * nu * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.R.data()),
               nu * nu * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.Q.data()),
               nx * nx * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.P.data()),
               nx * nx * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.Bd.data()),
               nx * nx / 2 * sizeof(real_t));
    sys.x0 = std::vector<real_t>(nx);
    input.read(reinterpret_cast<char *>(sys.ubu.data()), nu * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.lbu.data()), nu * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.ubx.data()), nx * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.lbx.data()), nx * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.x0.data()), nx * sizeof(real_t));
  }
  return sys;
}

template <class real_t, class idx_t> struct qp_system {
  idx_t n0, n, k;
  std::vector<real_t> H, H2, h, ub, lb, F, Ft, A, ubx, lbx, r;
  std::vector<real_t> linear_penalty;
  std::vector<real_t> gradient, rhs, lhs{std::vector<real_t>(0)};
  real_t residual{0};
  size_t get_vars() { return n; }
  size_t get_cons() { return k; }
  real_t *get_hessian() { return H.data(); }
  real_t *get_half_hessian() { return H2.data(); }
  real_t *get_gradient(const real_t *x0) {
    detail::MatOps<real_t *>::affoperator('N', n, n0, 1., h.data(), n, x0, 0.,
                                          gradient.data());
    return gradient.data();
  }
  real_t *get_soft_gradient(const real_t *x0) {
    std::copy(linear_penalty.cbegin(), linear_penalty.cend(), gradient.begin());
    detail::MatOps<real_t *>::affoperator('N', n, n0, 1., h.data(), n, x0, 1.,
                                          gradient.data());
    return gradient.data();
  }
  real_t *get_ub() { return ub.data(); }
  real_t *get_lb() { return lb.data(); }
  real_t *get_constraints() { return F.data(); }
  real_t *get_constraints_transpose() { return Ft.data(); }
  real_t *get_rhs(const real_t *x0) {
    std::copy(ubx.cbegin(), ubx.cend(), rhs.begin());
    std::copy(lbx.cbegin(), lbx.cend(), rhs.begin() + k / 2);
    detail::MatOps<real_t *>::affoperator('N', k / 2, n0, -1., A.data(), k / 2,
                                          x0, 1., rhs.data());
    detail::MatOps<real_t *>::affoperator('N', k / 2, n0, 1., A.data(), k / 2,
                                          x0, -1., rhs.data() + k / 2);
    return rhs.data();
  }
  real_t *get_lhs() { return lhs.data(); }
  real_t get_residual(const real_t *x0) {
    std::vector<real_t> aux(n0);
    detail::MatOps<real_t *>::affoperator('N', n0, n0, 1., r.data(), n0, x0, 0.,
                                          aux.data());
    residual = 0.5 * detail::MatOps<real_t *>::dotproduct(n0, x0, aux.data());
    return residual;
  }
};

template <class real_t, class idx_t>
qp_system<real_t, idx_t> parse_system(const std::string filename,
                                      qp_system<real_t, idx_t> &sys) {
  std::ifstream input{filename, std::ios_base::binary};
  if (input) {
    input.read(reinterpret_cast<char *>(&(sys.n0)), sizeof(idx_t));
    input.read(reinterpret_cast<char *>(&(sys.n)), sizeof(idx_t));
    input.read(reinterpret_cast<char *>(&(sys.k)), sizeof(idx_t));
    idx_t n = sys.n;
    idx_t k = sys.k / 2;
    idx_t n0 = sys.n0;
    sys.H = sys.H2 = std::vector<real_t>(n * n);
    sys.h = std::vector<real_t>(n * n0);
    sys.ub = std::vector<real_t>(n);
    sys.lb = std::vector<real_t>(n);
    sys.F = std::vector<real_t>(2 * k * n);
    sys.A = std::vector<real_t>(k * n0);
    sys.ubx = std::vector<real_t>(k);
    sys.lbx = std::vector<real_t>(k);
    sys.r = std::vector<real_t>(n0 * n0);
    input.read(reinterpret_cast<char *>(sys.H.data()), n * n * sizeof(real_t));
    std::transform(sys.H.data(), sys.H.data() + n * n, sys.H2.data(),
                   [](real_t h) { return 0.5 * h; });
    input.read(reinterpret_cast<char *>(sys.h.data()), n * n0 * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.ub.data()), n * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.lb.data()), n * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.F.data()),
               2 * k * n * sizeof(real_t));
    sys.Ft = std::vector<real_t>{sys.F};
    detail::transpose(2 * k, n, sys.Ft.data());
    input.read(reinterpret_cast<char *>(sys.A.data()), k * n0 * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.ubx.data()), k * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.lbx.data()), k * sizeof(real_t));
    input.read(reinterpret_cast<char *>(sys.r.data()),
               n0 * n0 * sizeof(real_t));
    sys.gradient = std::vector<real_t>(n);
    sys.rhs = std::vector<real_t>(2 * k);
    sys.linear_penalty = std::vector<real_t>(n);
    input.read(reinterpret_cast<char *>(sys.linear_penalty.data()),
               n * sizeof(real_t));
  }
  return sys;
}

template <class real_t> class disturbance {
private:
  size_t n;
  std::vector<real_t> d;
  std::vector<real_t> dmin, dmax;
  std::mt19937 gen;
  std::vector<std::uniform_real_distribution<real_t>> dis;

public:
  disturbance<real_t>() = default;
  disturbance(size_t _n, std::vector<real_t> dmin, std::vector<real_t> dmax)
      : n{_n}, d{std::vector<real_t>(n)}, dmin{std::move(dmin)}, dmax{std::move(
                                                                     dmax)},
        dis{std::vector<std::uniform_real_distribution<real_t>>(n)} {
    std::random_device rd;
    gen = std::mt19937(rd());
    for (size_t idx = 0; idx < n; idx++)
      dis[idx] = std::uniform_real_distribution<real_t>(dmin[idx], dmax[idx]);
  }
  disturbance(size_t _n, real_t _dmin, real_t _dmax)
      : n{_n}, d{std::vector<real_t>(n)}, dmin{std::vector<real_t>(n, _dmin)},
        dmax{std::vector<real_t>(n, _dmax)},
        dis{std::vector<std::uniform_real_distribution<real_t>>(n)} {
    std::random_device rd;
    gen = std::mt19937(rd());
    for (size_t idx = 0; idx < n; idx++)
      dis[idx] = std::uniform_real_distribution<real_t>(dmin[idx], dmax[idx]);
  }
  disturbance(size_t _n, std::vector<real_t> _dmin, std::vector<real_t> _dmax,
              uint16_t seed)
      : n{_n}, d{std::vector<real_t>(n)}, dmin{std::move(_dmin)},
        dmax{std::move(_dmax)}, gen{std::mt19937(seed)},
        dis{std::vector<std::uniform_real_distribution<real_t>>(n)} {
    for (size_t idx = 0; idx < n; idx++)
      dis[idx] = std::uniform_real_distribution<real_t>(dmin[idx], dmax[idx]);
  }
  disturbance(size_t _n, real_t _dmin, real_t _dmax, uint16_t seed)
      : n{_n}, d{std::vector<real_t>(_n)}, dmin{std::vector<real_t>(_n, _dmin)},
        dmax{std::vector<real_t>(_n, _dmax)}, gen{std::mt19937(seed)},
        dis{std::vector<std::uniform_real_distribution<real_t>>(_n)} {
    for (size_t idx = 0; idx < n; idx++)
      dis[idx] = std::uniform_real_distribution<real_t>(dmin[idx], dmax[idx]);
  }
  real_t *get_disturbance() {
    for (size_t idx = 0; idx < n; idx++)
      d[idx] = dis[idx](gen);
    return d.data();
  }
  real_t *get_disturbance(uint16_t seed) {
    gen.seed(seed);
    for (size_t idx = 0; idx < n; idx++)
      d[idx] = dis[idx](gen);
    return d.data();
  }
};
} // namespace extra
} // namespace mpc

#endif
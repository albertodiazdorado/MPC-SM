#include "constraints/SoftCons.cpp"
#include "matrix/diagmat.cpp"
#include "matrix/pdmat.cpp"
#include <type_traits>

namespace mpc {
namespace detail {

// SoftCons traits
template <template <class, class> class> struct softcons {
  static constexpr bool value = true;
  static constexpr bool linear = true;
};

template <> struct softcons<SoftCons::SimpleBounds> {
  static constexpr bool value = true;
  static constexpr bool linear = false;
};

template <> struct softcons<SoftCons::Unconstrained> {
  static constexpr bool value = false;
  static constexpr bool linear = false;
};

// Matrix traits
template <template <class, class> class> struct is_pdmat {
  static constexpr bool value = false;
};

template <> struct is_pdmat<matrix::PDMat> {
  static constexpr bool value = true;
};

} // namespace detail
} // namespace mpc
#ifndef MEHROTRA_CPP_
#define MEHROTRA_CPP_

#include <cmath>
#include <iostream>

#include "constraints/InputCons.cpp"
#include "constraints/SoftCons.cpp"
#include "constraints/StateCons.hpp"
#include "policies/CentPolicy.cpp"

namespace mpc {
namespace solver {

template <class real_t> struct mpcdata {
  // Problem data
  size_t n, m;
  size_t T;
  std::vector<real_t> A, B;
  std::vector<real_t> R, Q, P;

  // Hard constrainst
  std::vector<real_t> lbu{std::vector<real_t>(0)}, ubu{std::vector<real_t>(0)};
  std::vector<real_t> Fu{std::vector<real_t>(0)}, fu{std::vector<real_t>(0)};
  std::vector<real_t> lbx{std::vector<real_t>(0)}, ubx{std::vector<real_t>(0)};
  std::vector<real_t> Fx{std::vector<real_t>(0)}, fx{std::vector<real_t>(0)};
  std::vector<real_t> lbt{std::vector<real_t>(0)}, ubt{std::vector<real_t>(0)};
  std::vector<real_t> Ft{std::vector<real_t>(0)}, ft{std::vector<real_t>(0)};

  // Soft constraints
  std::vector<real_t> slbu{std::vector<real_t>(0)},
      subu{std::vector<real_t>(0)};
  std::vector<real_t> sFu{std::vector<real_t>(0)}, sfu{std::vector<real_t>(0)};
  std::vector<real_t> slbx{std::vector<real_t>(0)},
      subx{std::vector<real_t>(0)};
  std::vector<real_t> sFx{std::vector<real_t>(0)}, sfx{std::vector<real_t>(0)};
  std::vector<real_t> slbt{std::vector<real_t>(0)},
      subt{std::vector<real_t>(0)};
  std::vector<real_t> sFt{std::vector<real_t>(0)}, sft{std::vector<real_t>(0)};

  // Methods
  mpcdata() = default;
  mpcdata(size_t a, size_t b, size_t c) : n{a}, m{b}, T{c} {}
  mpcdata(size_t a, size_t b, size_t c, std::vector<real_t> A,
          std::vector<real_t> B)
      : n{a}, m{b}, T{c}, A{std::move(A)}, B{std::move(B)} {}
  mpcdata(size_t a, size_t b, size_t c, std::vector<real_t> A,
          std::vector<real_t> B, std::vector<real_t> R, std::vector<real_t> Q,
          std::vector<real_t> P)
      : n{a}, m{b}, T{c}, A{std::move(A)}, B{std::move(B)}, R{std::move(R)},
        Q{std::move(Q)}, P{std::move(P)} {}
};

template <class real_t> struct mpcoptions {
  real_t feasibility_tolerance{1e-2};
  real_t optimality_tolerance{1e-4};
  real_t eta{0.99};
  size_t max_iterations{400};
  mpcoptions() = default;
};

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy = mpc::policy::QubicCentering>
class Mehrotra final : public InputCons<real_t, int_t>,
                       public StateCons<real_t, int_t>,
                       public CentPolicy<real_t> {
private:
  int_t n, m, T;
  int_t ls, ms; // Size of the blocks in L and M
  std::vector<real_t> nu, nu_aff, nu_cc;
  std::vector<real_t> L, M;
  std::vector<real_t> x0;
  real_t mu{0}, mu_aff{0}, sigma{0};
  real_t f_tol{1e-2}, opt_tol{1e-4}, eta{0.99};
  real_t alpha_p{0}, alpha_d{0}, opt_val{0};
  int_t k{0}, K{400};

public:
  // aliases
  using IC = InputCons<real_t, int_t>;
  using SC = StateCons<real_t, int_t>;

  // Constructors
  Mehrotra() = default;
  Mehrotra(int_t n, int_t m, int_t T, std::vector<real_t> A,
           std::vector<real_t> B, R_t<real_t, int_t> R, Q_t<real_t, int_t> Q,
           P_t<real_t, int_t> P, std::vector<real_t> x0,
           std::vector<real_t> lbu = std::vector<real_t>(0),
           std::vector<real_t> ubu = std::vector<real_t>(0),
           std::vector<real_t> Fu = std::vector<real_t>(0),
           std::vector<real_t> fu = std::vector<real_t>(0),
           std::vector<real_t> lbx = std::vector<real_t>(0),
           std::vector<real_t> ubx = std::vector<real_t>(0),
           std::vector<real_t> Fx = std::vector<real_t>(0),
           std::vector<real_t> fx = std::vector<real_t>(0),
           std::vector<real_t> lbf = std::vector<real_t>(0),
           std::vector<real_t> ubf = std::vector<real_t>(0),
           std::vector<real_t> Ff = std::vector<real_t>(0),
           std::vector<real_t> ff = std::vector<real_t>(0),
           std::vector<real_t> slbu = std::vector<real_t>(0),
           std::vector<real_t> subu = std::vector<real_t>(0),
           std::vector<real_t> sFu = std::vector<real_t>(0),
           std::vector<real_t> sfu = std::vector<real_t>(0),
           std::vector<real_t> slbx = std::vector<real_t>(0),
           std::vector<real_t> subx = std::vector<real_t>(0),
           std::vector<real_t> sFx = std::vector<real_t>(0),
           std::vector<real_t> sfx = std::vector<real_t>(0),
           std::vector<real_t> slbf = std::vector<real_t>(0),
           std::vector<real_t> subf = std::vector<real_t>(0),
           std::vector<real_t> sFf = std::vector<real_t>(0),
           std::vector<real_t> sff = std::vector<real_t>(0));
  Mehrotra(mpcdata<real_t> &, std::vector<real_t> x0);

  // Solver
  void affine_step();
  void cc_step();
  void take_step();
  bool terminate();
  void iterate();
  void solve();
  void solve(std::vector<real_t>);
  void solve(real_t *);
  void eq_residual();

  void solve_verbose();
  void set_options(const mpcoptions<real_t> &opt) {
    f_tol = opt.feasibility_tolerance;
    opt_tol = opt.optimality_tolerance;
    eta = opt.eta;
    K = opt.max_iterations;
  }

  // Detail
  void forward_solve(real_t *);
  void forward_solve(real_t *, const int_t);
  void forward_solve_lb(real_t *);
  void backward_solve(real_t *);
  void backward_solve(real_t *, const int_t);
  void fast_forward_solve(real_t *);
  void fast_forward_solve(real_t *, const int_t);

  // MPC receding horizon
  void new_x0(std::vector<real_t> &);
  void new_x0(real_t *);

  // Other functionalities
  real_t get_opt_val();
  std::vector<real_t> get_opt_input() const;
  std::vector<real_t> get_opt_states() const;
  int_t get_k() const;
  real_t get_duality_gap() const;
  real_t get_eq_residual() const;
  real_t get_step_size() const;
};

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
         CentPolicy>::Mehrotra(int_t _n, int_t _m, int_t _T, std::vector<real_t> A,
                               std::vector<real_t> B, R_t<real_t, int_t> R,
                               Q_t<real_t, int_t> Q, P_t<real_t, int_t> P,
                               std::vector<real_t> x0, std::vector<real_t> lbu,
                               std::vector<real_t> ubu, std::vector<real_t> Fu,
                               std::vector<real_t> fu, std::vector<real_t> lbx,
                               std::vector<real_t> ubx, std::vector<real_t> Fx,
                               std::vector<real_t> fx, std::vector<real_t> lbf,
                               std::vector<real_t> ubf, std::vector<real_t> Ff,
                               std::vector<real_t> ff, std::vector<real_t> slbu,
                               std::vector<real_t> subu,
                               std::vector<real_t> sFu, std::vector<real_t> sfu,
                               std::vector<real_t> slbx,
                               std::vector<real_t> subx,
                               std::vector<real_t> sFx, std::vector<real_t> sfx,
                               std::vector<real_t> slbf,
                               std::vector<real_t> subf,
                               std::vector<real_t> sFf, std::vector<real_t> sff)
    : IC{_n, _m, _T, R, B, lbu, ubu, Fu, fu, slbu, subu, sFu, sfu},
      SC{_n,    _m,   _T,   Q,   P,   A,  lbx, ubx,  Fx,   fx,  slbx,
         subx, sFx, sfx, lbf, ubf, Ff, ff,  slbf, subf, sFf, sff},
      n{_n}, m{_m}, T{_T}, nu{std::vector<real_t>(this->n * this->T)}, nu_aff{nu},
      nu_cc{nu}, L{std::vector<real_t>(T * (n * (n + 1) / 2))},
      M{std::vector<real_t>((T - 1) * n * n)}, x0{std::move(x0)} {
  ls = n * (n + 1) / 2;
  ms = n * n;
  mu = (IC::duality_gap() + SC::duality_gap()) / (IC::ncons() + SC::ncons());
  // Initialize nu_aff with the equality residual
  // re[block] = Ax[i] + Bu[i] - x[i-1]
  SC::eq_residual(nu_aff.data(), this->x0.data());
  IC::eq_residual(nu_aff.data(), 0);
  for (int_t i = 1; i < T; i++) {
    IC::eq_residual(nu_aff.data() + n * i, i);
    SC::eq_residual(nu_aff.data() + n * i, i);
  }
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
         CentPolicy>::Mehrotra(mpcdata<real_t> &obj, std::vector<real_t> x0)
    : Mehrotra(
          obj.n, obj.m, obj.T, obj.A, obj.B, R_t<real_t, int_t>(obj.m, obj.R),
          Q_t<real_t, int_t>(obj.n, obj.Q), P_t<real_t, int_t>(obj.n, obj.P),
          std::move(x0), obj.lbu, obj.ubu, obj.Fu, obj.fu, obj.lbx, obj.ubx,
          obj.Fx, obj.fx, obj.lbt, obj.ubt, obj.Ft, obj.ft, obj.slbu, obj.subu,
          obj.sFu, obj.sfu, obj.slbx, obj.subx, obj.sFx, obj.sfx, obj.slbt,
          obj.subt, obj.sFt, obj.sft) {}

/**
 * @brief Performs the AFFINE STEP of the Mehrotra algorithm. The function
 * assumes that the equality residual is readily available and stored in
 * nu_aff (this shall we done either in initialization or in terminate())
 *
 * In the forward solve step, the function initializes rdx and rdu with
 * proper values and then delegates to InputCons and StateCons to: build
 * the Shur Complement block, factorize it and forward solve.
 *
 * In the backwards step, there is nothing to initialize and the whole
 * work is delegated to the corresponding policies
 *
 *
 * @tparam real_t Floating point type
 * @tparam int_t Integer type
 * @tparam InputCons Policy managing the input constraints
 * @tparam StateCons Policy managing the state constraints
 * @tparam CentPolicy Policy managing the centering+correction execution
 */
template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::affine_step() {
  // **** Forward solve **** //
  std::fill(L.begin(), L.end(), real_t{0});
  IC::aff_fs(L.data(), nu.data(), nu_aff.data());
  SC::aff_fs_fb(L.data(), M.data(), nu.data(), nu_aff.data());
  forward_solve(nu_aff.data());
  for (int_t i = 1; i < T - 1; i++) {
    IC::aff_fs(L.data() + i * ls, nu.data() + i * n, nu_aff.data() + i * n, i);
    SC::aff_fs_nb(L.data() + i * ls, M.data() + i * ms, nu.data() + i * n,
                  nu_aff.data() + i * n, i);
    forward_solve(nu_aff.data() + n * i, i);
  }
  IC::aff_fs(L.data() + (T - 1) * ls, nu.data() + (T - 1) * n,
             nu_aff.data() + (T - 1) * n, T - 1);
  SC::aff_fs_lb(L.data() + (T - 1) * ls, nu.data() + (T - 1) * n,
                nu_aff.data() + (T - 1) * n);
  forward_solve_lb(nu_aff.data() + (T - 1) * n);

  // **** Transition **** //
  IC::aff_ic();
  SC::aff_ic();

  // **** Backwards solve **** //
  backward_solve(nu_aff.data() + (T - 1) * n);
  IC::aff_bs(nu_aff.data() + (T - 1) * n, T - 1);
  SC::aff_bs_lb(nu_aff.data() + (T - 1) * n);
  for (int_t i = 0; i < T - 1; i++) {
    backward_solve(nu_aff.data() + (T - 2 - i) * n, T - 2 - i);
    IC::aff_bs(nu_aff.data() + (T - 2 - i) * n, T - 2 - i);
    SC::aff_bs_nb(nu_aff.data() + (T - 2 - i) * n, T - 2 - i);
  }

  // **** Final computations **** //
  IC::aff_fc();
  SC::aff_fc();
}

/**
 * @brief Performs the CENTERING + CORRECTION STEP of the Mehrotra algorithm.
 * The function does not do anything by itself and delegates everything to the
 * corresponding policies.
 *
 * @tparam real_t Floating point type
 * @tparam int_t Integer type
 * @tparam InputCons Policy managing the input constraints
 * @tparam StateCons Policy managing the state constraints
 * @tparam CentPolicy Policy managing the centering+correction execution
 */
template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::cc_step() {
  // **** Precomputations **** //
  IC::cc_pc(sigma * mu_aff);
  SC::cc_pc(sigma * mu_aff);
  // **** Forward solve **** //
  IC::cc_fs(nu_cc.data());
  SC::cc_fs_fb(nu_cc.data());
  fast_forward_solve(nu_cc.data());
  for (int_t i = 1; i < T - 1; i++) {
    IC::cc_fs(nu_cc.data() + n * i, i);
    SC::cc_fs_nb(nu_cc.data() + n * i, i);
    fast_forward_solve(nu_cc.data() + i * n, i);
  }
  IC::cc_fs(nu_cc.data() + n * (T - 1), T - 1);
  SC::cc_fs_lb(nu_cc.data() + n * (T - 1));
  fast_forward_solve(nu_cc.data() + (T - 1) * n, T - 1);

  // **** Transition **** //
  IC::cc_ic();
  SC::cc_ic();

  // **** Backwards solve **** //
  backward_solve(nu_cc.data() + (T - 1) * n);
  IC::cc_bs(nu_cc.data() + n * (T - 1));
  SC::cc_bs_lb(nu_cc.data() + (T - 1) * n);
  for (int_t i = 0; i < T - 1; i++) {
    backward_solve(nu_cc.data() + (T - 2 - i) * n, T - 2 - i);
    IC::cc_bs(nu_cc.data() + n * (T - 2 - i), T - 2 - i);
    SC::cc_bs_nb(nu_cc.data() + n * (T - 2 - i), T - 2 - i);
  }

  // **** Final computations **** //
  IC::cc_fc();
  SC::cc_fc();
}

/**
 * @brief Once that both the AFFINE and the CENTERING + CORRECTION steps have
 * been calculated, and the linsesearch has been done as well, this function
 * actually takes the step.
 *
 * The function takes the step in the primal variables,
 * and delegates to the corresponding policies for the dual variables.
 *
 * @tparam real_t Floating point type
 * @tparam int_t Integer type
 * @tparam InputCons Policy managing the input constraints
 * @tparam StateCons Policy managing the state constraints
 * @tparam CentPolicy Policy managing the centering+correction execution
 */
template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::take_step() {
  detail::MatOps<real_t *>::vecsum(T * n, alpha_p, nu_aff.data(), nu.data());
  detail::MatOps<real_t *>::vecsum(T * n, alpha_p, nu_cc.data(), nu.data());
  IC::take_step(alpha_p);
  SC::take_step(alpha_p);
}

/**
 * @brief Returns TRUE if the algorithm has terminated; returns FALSE
 * otherwise
 *
 * First, the function builds the equality residual for the next step and
 * evaluates the duality gap
 *
 * Next, it checks the optimality condition. If not satisfied, the function
 * returns FALSE. If satisfied, the function checks the feasibility condition.
 * If not satisfied, it returns FALSE. Otherwise it returns TRUE
 *
 *
 *
 * @tparam real_t Floating point type
 * @tparam int_t Integer type
 * @tparam InputCons Policy managing the input constraints (inherits from
 * mpc::IC::InCons)
 * @tparam StateCons Policy managing the state constraints (inherits from
 * mpc::SC::StCons)
 * @tparam CentPolicy Policy managing the Centering + Correction policy
 * @return true The algorithm has terminated
 * @return false The algorithm has not terminated
 */
template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
bool Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::terminate() {
  // Check iteration count
  if (k >= K)
    return true;

  // Build equality residual and store it in nu_aff
  SC::eq_residual(nu_aff.data(), x0.data());
  IC::eq_residual(nu_aff.data(), 0);
  for (int_t i = 1; i < T; i++) {
    IC::eq_residual(nu_aff.data() + n * i, i);
    SC::eq_residual(nu_aff.data() + n * i, i);
  }

  // Evaluate the duality gap
  mu = (IC::duality_gap() + SC::duality_gap()) / (IC::ncons() + SC::ncons());

  // Check the duality gap
  if (mu > opt_tol)
    return false;
  // Check feasibility
  if (mpc::detail::MatOps<real_t *>::dotproduct(
          n * T, nu_aff.data(), nu_aff.data()) > (f_tol * f_tol))
    return false;
  // Otherwise, return true
  return true;
}

/**
 * @brief Performs one full ITERATION of the Mehrotra algorithm. The steps are
 * as follows:
 *
 * 1. Affine step
 * 2. Linesearch
 * 3. Centering + Correction policy
 * 4. Centering + Correction step
 * 5. Linesearch
 * 6. Take step
 *
 * @tparam real_t Floating point type
 * @tparam int_t Integer type
 * @tparam InputCons Policy managing the input constraints
 * @tparam StateCons Policy managing the state constraints
 * @tparam CentPolicy Policy managing the centering+correction execution
 */
template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::iterate() {
  affine_step();
  IC::linesearch(alpha_p, alpha_d);
  SC::linesearch(alpha_p, alpha_d);
  mu_aff = (IC::affine_gap(std::min(alpha_p, alpha_d)) +
            SC::affine_gap(std::min(alpha_p, alpha_d))) /
           (IC::ncons() + SC::ncons());
  // IC::linesearch(alpha_p);
  // SC::linesearch(alpha_p);
  // mu_aff = (IC::affine_gap(alpha_p) + SC::affine_gap(alpha_p)) /
  //          (IC::ncons() + SC::ncons());
  sigma = CentPolicy<real_t>::centering_policy(mu, mu_aff);
  cc_step();
  IC::linesearch(alpha_p, alpha_d);
  SC::linesearch(alpha_p, alpha_d);
  alpha_p = eta * std::min(alpha_p, alpha_d);
  // IC::linesearch(alpha_p);
  // SC::linesearch(alpha_p);
  // alpha_p = eta * alpha_p;
  take_step();
  k++;
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::forward_solve(real_t *x) {
  // L[i] <- cholfact(L[i])
  detail::MatOps<real_t *>::ppfactor('L', n, L.data());
  // x <- L[i]\i
  detail::MatOps<real_t *>::tpsolve('N', n, L.data(), x);
  // M[i] <- L[i]\M[i]
  detail::MatOps<real_t *>::tpsolve('N', n, L.data(), M.data(), n);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::forward_solve(real_t *x, const int_t i) {
  // L[i] -= M[i-1]*M[i-1]^T
  detail::MatOps<real_t *>::pprankkt(n, -1, M.data() + (i - 1) * ms,
                                     L.data() + i * ls, n);
  // L[i] <- cholfact(L[i])
  detail::MatOps<real_t *>::ppfactor('L', n, L.data() + i * ls);
  // x[i] -= M[i-1]^T * x[i-1]
  detail::MatOps<real_t *>::affoperator('T', n, n, -1., M.data() + (i - 1) * ms,
                                        n, x - n, 1, 1., x, 1);
  // x[i] <- L[i]\x[i]
  detail::MatOps<real_t *>::tpsolve('N', n, L.data() + i * ls, x);
  // M[i] <- L[i]\M[i]
  detail::MatOps<real_t *>::tpsolve('N', n, L.data() + i * ls,
                                    M.data() + i * ms, n);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::forward_solve_lb(real_t *x) {
  // L[i] -= M[i-1]*M[i-1]^T
  detail::MatOps<real_t *>::pprankkt(n, -1, M.data() + (T - 2) * ms,
                                     L.data() + (T - 1) * ls, n);
  // L[i] <- cholfact(L[i])
  detail::MatOps<real_t *>::ppfactor('L', n, L.data() + (T - 1) * ls);
  // x[i] -= M[i-1]^T * x[i-1]
  detail::MatOps<real_t *>::affoperator('T', n, n, -1., M.data() + (T - 2) * ms,
                                        n, x - n, 1, 1., x, 1);
  // x[i] <- L[i]\x[i]
  detail::MatOps<real_t *>::tpsolve('N', n, L.data() + (T - 1) * ls, x);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::backward_solve(real_t *nu_aff) {
  detail::MatOps<real_t *>::tpsolve('T', n, L.data() + (T - 1) * ls, nu_aff);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::backward_solve(real_t *nu_aff, const int_t i) {
  mpc::detail::MatOps<real_t *>::affoperator('N', n, n, -1., M.data() + i * ms,
                                             n, nu_aff + n, 1., nu_aff);
  mpc::detail::MatOps<real_t *>::tpsolve('T', n, L.data() + i * ls, nu_aff);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::fast_forward_solve(real_t *x) {
  detail::MatOps<real_t *>::tpsolve('N', n, L.data(), x);
  // detail::MatOps<real_t *>::tpsolve('N', n, L.data(), M.data(), n);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::fast_forward_solve(real_t *x, const int_t i) {
  detail::MatOps<real_t *>::affoperator('T', n, n, -1., M.data() + (i - 1) * ms,
                                        n, x - n, 1, 1., x, 1);
  detail::MatOps<real_t *>::tpsolve('N', n, L.data() + i * ls, x);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::new_x0(std::vector<real_t> &x0_new) {
  k = 0;
  x0 = x0_new;
  std::rotate(nu.begin(), nu.begin() + n, nu.end());
  std::copy(nu.begin() + n * (T - 2), nu.begin() + n * (T - 1),
            nu.begin() + n * (T - 1));
  IC::new_x0(x0_new);
  SC::new_x0();
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::new_x0(real_t *x0_new) {
  k = 0;
  std::copy(x0_new, x0_new + n, x0.data());
  std::rotate(nu.begin(), nu.begin() + n, nu.end());
  std::copy(nu.begin() + n * (T - 2), nu.begin() + n * (T - 1),
            nu.begin() + n * (T - 1));
  IC::new_x0();
  SC::new_x0();
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::solve() {
  // Initialize nu_aff with the equality residual
  // re[block] = Ax[i] + Bu[i] - x[i-1]
  eq_residual();
  k = 0;
  do {
    iterate();
  } while (terminate() == false);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::solve_verbose() {
  // Initialize nu_aff with the equality residual
  // re[block] = Ax[i] + Bu[i] - x[i-1]
  eq_residual();
  k = 0;
  do {
    if (k != 0) {
      std::cout << "\tEquality residual (norm): " << get_eq_residual()
                << std::endl;
      std::cout << "\tDuality_gap: " << mu << "\n\n";
    }
    iterate();
    std::cout << "\tIteration: " << k << std::endl;
    std::cout << "\tOptimal value: " << get_opt_val() << std::endl;
    if (alpha_p > 0.98 * eta)
      std::cout << "\tStep size: " << get_step_size()
                << "\t(Quadratic convergence)" << std::endl;
    else
      std::cout << "\tStep size: " << get_step_size()
                << "\t(Subquadratic convergence)" << std::endl;
  } while (terminate() == false);
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::solve(std::vector<real_t> x0) {
  new_x0(x0);
  solve();
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::solve(real_t *x0) {
  new_x0(x0);
  solve();
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
std::vector<real_t> Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
                             CentPolicy>::get_opt_input() const {
  return IC::optimal_input();
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
std::vector<real_t> Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
                             CentPolicy>::get_opt_states() const {
  return SC::optimal_states();
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
int_t Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
               CentPolicy>::get_k() const {
  return k;
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
real_t Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
                CentPolicy>::get_duality_gap() const {
  return mu;
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
real_t Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
                CentPolicy>::get_opt_val() {
  opt_val = 0;
  IC::opt_val(opt_val);
  SC::opt_val(opt_val);
  opt_val /= 2;
  return opt_val;
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
real_t Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
                CentPolicy>::get_eq_residual() const {
  return std::sqrt(detail::MatOps<real_t *>::dotproduct(n * T, nu_aff.data(),
                                                        nu_aff.data()));
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
real_t Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
                CentPolicy>::get_step_size() const {
  return alpha_p;
}

template <class real_t, class int_t, template <class, class> class R_t,
          template <class, class> class Q_t, template <class, class> class P_t,
          template <class, class> class InputCons,
          template <class, class> class StateCons,
          template <class> class CentPolicy>
void Mehrotra<real_t, int_t, R_t, Q_t, P_t, InputCons, StateCons,
              CentPolicy>::eq_residual() {
  // Build equality residual and store it in nu_aff
  SC::eq_residual(nu_aff.data(), x0.data());
  IC::eq_residual(nu_aff.data(), 0);
  for (int_t i = 1; i < T; i++) {
    IC::eq_residual(nu_aff.data() + n * i, i);
    SC::eq_residual(nu_aff.data() + n * i, i);
  }
}

} // namespace solver
} // namespace mpc

#endif
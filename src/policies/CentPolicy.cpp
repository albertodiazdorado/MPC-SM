#ifndef CENT_POL_CPP
#define CENT_POL_CPP

namespace mpc {
namespace policy {

template <class real_t> class QubicCentering {
public:
  real_t centering_policy(real_t mu, real_t mu_aff) {
    if (mu == 0)
      return 0;
    return std::min((mu_aff * mu_aff * mu_aff) / (mu * mu * mu), real_t{1});
    // return (mu_aff * mu_aff * mu_aff) / (mu * mu * mu);
  }
};

} // namespace policy
} // namespace mpc

#endif
#include <algorithm>
#include <iostream>
#include <vector>

int main() {
  std::vector<double> lb(10), ub{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, middle(10);

  std::transform(lb.cbegin(), lb.cend(), ub.cbegin(), middle.begin(),
                 [](double lb, double ub) { return (lb + ub) / 2; });

  std::cout << "The vector of middle points is:\t[";
  for (size_t i = 0; i < 10; i++)
    std::cout << middle[i] << ", ";
  std::cout << "]\n\n";
  return 0;
}
/* This file has the purpose of testing how to call executable files from c.
 * The program invokes the program theory/a.out and reads its output value
 */

/* First of all, include the necessary library
 */
#include <cstdlib>
#include <iostream>
#include <vector>

int main() {
  std::vector<int> output(10);
  for (size_t idx = 0; idx < output.size(); idx++)
    output[idx] = std::system("theory/a.out");

  std::cout << "\nThe sequence of returned values is: ";
  for (size_t idx = 0; idx < output.size(); idx++)
    std::cout << "\n\t" << output[idx];
  return 0;
}
#include <iostream>

int main() {
  uint16_t a{898};
  int b{a};
  int c = static_cast<int>(a);

  std::cout << "a: " << a << ",\tb: " << b << ",\tc: " << c << "\n\n";

  uint16_t *ptr_a = &a;
  int *ptr_b;
  int *ptr_c;

  // ptr_b = &a;
  // ptr_c = static_cast<int *>(&a);

  std::cout << "*ptr_a: " << *ptr_a << "\n\n";

  std::cout << "\nCONCLUSION:"
               "\n\t>> You can DEFINITELY convert an 'uint16_k' (aka short "
               "unsigned int) to int,"
               "\n\tas you can observe in the first part of the code.\n"
               "\n\t>> However, thigs are different when you work with "
               "POINTERS. From Microsoft, "
               "\n\t'A pointer to an object can be converted to a pointer to "
               "an object whose type requires"
               "\n\tless or equally strict storage alignment.' So, since 'int' "
               "is larger than"
               "\n\t'short unsigned int', you cannot do this conversion when "
               "working with pointers"
               "\n\t(Uncommenting the lines 14-15 prompts a compile error).";

  return 0;
}
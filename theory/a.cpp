#include <random>

int main() {
  int r;
  std::random_device rd;
  std::uniform_int_distribution<int> dis(0, 15);
  r = dis(rd);
  return r;
}
#include <algorithm>
#include <iostream>
#include <type_traits>
#include <vector>

template <class real_t, class int_t> struct IMatrix;

// Maybe we can have some ArrayView, later on.
template <class real_t, class int_t, bool const_t> struct DiagIter {
  DiagIter(IMatrix<real_t, int_t> *mat, int_t k = 0) : mat{mat}, k{k} {}

  typename std::conditional<const_t, const real_t &, real_t &>::type
  operator*() {
    return mat->operator()(k, k);
  }

  DiagIter &operator++() {
    k++;
    return *this;
  }

  DiagIter operator++(int) {
    DiagIter temp(*this);
    operator++();
    return temp;
  }

  template <bool T> bool operator==(const DiagIter<real_t, int_t, T> &rhs) {
    return this->mat == rhs.mat && this->k == rhs.k;
  }
  template <bool T> bool operator!=(const DiagIter<real_t, int_t, T> &rhs) {
    return !operator==(rhs);
  }

private:
  IMatrix<real_t, int_t> *mat;
  int_t k{0};
};

template <class real_t, class int_t> struct IMatrix {
  IMatrix() = default;
  IMatrix(int_t m, int_t n) : m{m}, n{n}, values{std::vector<real_t>(m * n)} {}
  IMatrix(int_t m, int_t n, std::vector<real_t> values)
      : m{m}, n{n}, values{std::move(values)} {}
  virtual ~IMatrix() = default;

  const real_t &operator[](const std::size_t idx) const { return values[idx]; }
  real_t &operator[](const std::size_t idx) { return values[idx]; }
  int_t get_m() const { return m; }
  int_t get_n() const { return n; }
  DiagIter<real_t, int_t, false> dbegin() {
    return DiagIter<real_t, int_t, false>(this);
  }
  DiagIter<real_t, int_t, false> dend() {
    return DiagIter<real_t, int_t, false>(this, std::min(m, n));
  }

  virtual const real_t &operator()(const std::size_t row,
                                   const std::size_t col) const {
    return values[m * col + row];
  }
  virtual real_t &operator()(const std::size_t row, const std::size_t col) {
    return values[m * col + row];
  }

private:
  int_t m, n;
  std::vector<real_t> values;
};

template <class real_t, class int_t>
struct GEMatrix : public IMatrix<real_t, int_t> {
  GEMatrix() = default;
  GEMatrix(int_t m, int_t n) : IMatrix<real_t, int_t>(m, n) {}
  GEMatrix(int_t m, int_t n, std::vector<real_t> values)
      : IMatrix<real_t, int_t>(m, n, std::move(values)) {}

private:
};

int main(int argc, char *argv[]) {
  std::vector<float> v1(5), v2(5);
  GEMatrix<float, int> mat(5, 5);

  for (int idx = 0; idx < 5; idx++)
    v1[idx] = v2[idx] = idx;

  std::transform(mat.dbegin(), mat.dend(), v1.begin(), mat.dbegin(), std::plus<float>{});

      for (int row = 0; row < 5; row++) {
    for (int col = 0; col < 5; col++)
      std::cout << '(' << row << ',' << col << "): " << mat(row, col) << ", ";

    std::cout << '\n';
  }

  return 0;
}

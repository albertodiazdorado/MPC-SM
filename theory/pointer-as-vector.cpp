#include <iostream>
#include <vector>

void copy(int, double *, double *);

int main() {
  std::vector<double> x{0, 0, 0, 0, 0}, y{1, 2, 3, 4, 5};
  std::cout << "\nVector x before copying y into it: \n\t[";
  for (auto &elem : x)
    std::cout << elem << ", ";
  std::cout << "]\n;";
  copy(5, x.data(), y.data());
  std::cout << "\nVector x AFTER copying y into it: \n\t[";
  for (auto &elem : x)
    std::cout << elem << ", ";
  std::cout << "]\n;";

  std::cout << "\n\nIN CONCLUSION: You can definitely access pointer "
               "elements as if pointers were vectors.";
  return 0;
}

void copy(int n, double *x, double *y) {
  for (int i = 0; i < n; i++)
    x[i] = y[i];
}
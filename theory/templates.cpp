#include <iostream>
#include <vector>

template <class real_t, int k> struct OP {
  static void divides(real_t *x) { *x /= k; };
};

template <class real_t> struct OP<real_t, 0> {
  static void divides(real_t *x){};
};

template <class real_t> struct envelop {
  template <int k> static void divides(real_t *x) { OP<real_t, k>::divides(x); }
};

template <class real_t> void function(real_t *x, real_t y) { *x += y; };

template <class real_t> void function(real_t *x) { function(x, 0); };

int main() {
  int x{50};
  std::cout << "x = " << x << std::endl;
  envelop<int>::divides<5>(&x);
  std::cout << "x/5 = " << x << std::endl;
  envelop<int>::divides<0>(&x);
  std::cout << "x/0 = " << x << std::endl;

  function(&x, 27);
  std::cout << "x + 27 = " << x << std::endl;
  function(&x, 0);
  std::cout << "x + 0 = " << x << std::endl;
  function(&x);
  std::cout << "x + 0 = " << x << std::endl;

  return 0;
}
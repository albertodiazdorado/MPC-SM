#!/bin/fish

# Define local variables
set --local time 0
set --local elapsed 0
set --local N 10
set --local mean 0

# For qpOASES
for iter in (seq $N)
  set time (../main.out | sed "s/cputime = \(.*\)/\1/g")
  set elapsed (math --scale=6 -- $time + $elapsed)
  # echo $elapsed
end

set mean (math --scale=6 -- $elapsed / $N)
echo "[qpOASES] Mean time: $mean ms over $N runs."

# For MPC
set elapsed 0

for iter in (seq $N)
  set time (./random.out | sed "s/cputime = \(.*\)/\1/g")
  set elapsed (math --scale=6 -- $time + $elapsed)
  # echo $elapsed
end

set mean (math --scale=6 -- $elapsed / $N)
echo "[MPC] Mean time: $mean ms over $N runs."

# For FastMPC
set elapsed 0

for iter in (seq $N)
  set time (./random.out | sed "s/cputime = \(.*\)/\1/g")
  set elapsed (math --scale=6 -- $time + $elapsed)
  # echo $elapsed
end

set mean (math --scale=6 -- $elapsed / $N)
echo "[FastMPC] Mean time: $mean ms over $N runs."
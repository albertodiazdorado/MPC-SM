#include <iostream>

template <class real_t> class A {
private:
  real_t number;

public:
  A() = default;
  A(real_t &x) : number{std::move(x)} {};
  real_t get() const { return number; };
};

int main() {
  double x{10}, y{10};
  A<double> MyClass{x};

  std::cout << "x: " << x << "\ny: " << y
            << "\nMyClass.get(): " << MyClass.get() << std::endl;
  return 0;
}
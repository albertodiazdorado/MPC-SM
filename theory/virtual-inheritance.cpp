#include <iostream>

class Base {
private:
  double val;

public:
  Base() = default;
  Base(double val) : val{val} {};
  ~Base() = default;
  double getval() { return val; }
  virtual double v_getval() { return val; }
  void setval(double val) { this->val = val; }
};

class Child1 : public Base {
private:
  double val;

public:
  Child1() = default;
  Child1(double val) : Base{-val}, val{val} {};
  double getval() { return 2 * val; };
  double v_getval() override { return 2 * val; };
  void setval(double val) { this->val = val; }
};

class Child2 : public Base {
private:
  double val;

public:
  Child2() = default;
  Child2(double val) : Base{-val}, val{val} {};
  double getval() { return 3 * val; };
  double v_getval() override { return 3 * val; };
  void setval(double val) { this->val = val; }
};

int main() {
  Base B(1);
  Child1 C1(2);
  Child2 C2(3);

  std::cout << "\n*************************************************************"
               "****************\nNORMAL OBJECTS:\n\t";
  std::cout << "B.getval():   " << B.getval()
            << ",\tC1.getval():   " << C1.getval()
            << ",\tC2.getval():   " << C2.getval() << std::endl;
  std::cout << "\tB.v_getval(): " << B.v_getval()
            << ",\tC1.v_getval(): " << C1.v_getval()
            << ",\tC2.v_getval(): " << C2.v_getval() << std::endl;

  // FORBIDDEN: No conversion from Base to Child1&&
  // C1 = B
  B = C2;
  std::cout << "\n*************************************************************"
               "****************\nSLICING (B<-C2)";
  std::cout << "\n\tB.getval():   " << B.getval()
            << ",\tC1.getval():   " << C1.getval()
            << ",\tC2.getval():   " << C2.getval() << std::endl;
  std::cout << "\tB.v_getval(): " << B.v_getval()
            << ",\tC1.v_getval(): " << C1.v_getval()
            << ",\tC2.v_getval(): " << C2.v_getval() << std::endl;

  // C1 = Child1(2);
  // C2 = Child2(3);
  // Base &B1 = C1;
  // Base &B2 = C2;
  Base &B2 = C2;
  std::cout << "\n*************************************************************"
               "****************\nPOINTERS (&B2=C2)";
  std::cout << "\n\tB2.getval():  " << B2.getval()
            << ",\tC2.getval():   " << C2.getval() << std::endl;
  std::cout << "\tB2.v_getval(): " << B2.v_getval()
            << ",\tC2.v_getval(): " << C2.v_getval() << std::endl;
  // << "\n\tB1.get2val():" << B1.get2val()
  // << "\tB2.get2val(): " << B2.get2val();

  std::cout
      << "\n***************************************************************"
         "**************"
         "\nAnd "
         "the reason why you need VIRTUAL functions is:\n"
         "\n\t>> Whenever you SLICE, you lose any information that was "
         "exclusive for "
         "\n\tthe Child. In the example, the field 'C2.Base::val' is written "
         "into B; "
         "\n\thowever, the methods C1.getval() and C1.v_getval() are SLICED "
         "apart. It "
         "\n\tdoes not matter if the methods are virtual or they are not: both "
         "are "
         "\n\tSLICED away."
         "\n\n\t>> When you use POINTERS or REFERENCES, things change. "
         "Non-virtual "
         "\n\tmembers (fields or methods) are sliced away. BUT: Virtual "
         "methods (i.e. "
         "\n\tv_getval()) is NOT SLICED. Rather than this, virtual methods (or "
         "fields)  "
         "\n\tare OVERRIDEN according to the Child (if there are overriden at "
         "all in "
         "\n\tthe Child)."
         "\n\n\t>> In you case, you want to keep a pointer to IMatrix, but "
         "then "
         "call the "
         "\n\tparticular methods of PDMat or DiagMat once the problem has been "
         "\n\tinitialized.";

  return 0;
}
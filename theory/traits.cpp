#include <iostream>
#include <vector>

#include "matrix/diagmat.cpp"
#include "matrix/pdmat.cpp"

// *************** My different constraints classes ************************* //
template <class real_t, class int_t> struct Unconstrained { int_t k{0}; };

template <class real_t, class int_t> struct SimpleBounds {
  int_t k;
  std::vector<real_t> lb_, ub_;
  SimpleBounds() = default;
  SimpleBounds(const std::vector<real_t> &lb, const std::vector<real_t> &ub)
      : lb_{lb}, ub_{ub} {
    k = lb.size() + ub.size();
  };
};

template <class real_t, class int_t> struct LinearBounds {
  int_t k;
  std::vector<real_t> F_, f_;
  LinearBounds(const std::vector<real_t> &F, const std::vector<real_t> &f)
      : F_{F}, f_{f} {
    k = f.size();
  };
};

template <class real_t, class int_t> struct FullBounds {
  int_t k;
  std::vector<real_t> lb_, ub_, F_, f_;
  FullBounds() = default;
  FullBounds(const std::vector<real_t> &lb, const std::vector<real_t> &ub,
             const std::vector<real_t> &F, const std::vector<real_t> &f)
      : lb_{lb}, ub_{ub}, F_{F}, f_{f} {
    k = lb.size() + ub.size() + f.size();
  };
};

// ***************************** My Traits ********************************* //
template <template <class, class> class T> struct has_cons {
  static constexpr bool value = true;
  static constexpr bool linear = true;
};

template <> struct has_cons<SimpleBounds> {
  static constexpr bool value = true;
  static constexpr bool linear = false;
};

template <> struct has_cons<Unconstrained> {
  static constexpr bool value = false;
  static constexpr bool linear = false;
};

template <template <class, class> class T> struct is_diagonal {
  static constexpr bool value = true;
};

template <> struct is_diagonal<mpc::matrix::PDMat> {
  static constexpr bool value = false;
};

// ******************** My (Pure) Virtual Base Class ************************ //
template <class real_t, class int_t, template <class, class> class mat_t,
          template <class, class> class cons_t, bool, bool>
class CoreBase {
public:
  int_t k;
  std::vector<real_t> x, lambda;
  cons_t<real_t, int_t> Constraints;

  CoreBase() = default;
  CoreBase(int_t n, const cons_t<real_t, int_t> &z)
      : k{0}, x{std::vector<real_t>(n)}, lambda{std::vector<real_t>(z.k)},
        Constraints{z} {};
  virtual void do_stuff(const std::vector<real_t> &A){};
  virtual ~CoreBase() = default;
};

// ****************** My Inherited, Specialized Classes ******************** //
// Forward declaration
template <class real_t, class int_t, template <class, class> class mat_t,
          template <class, class> class cons_t, bool, bool>
class Core;

// Specialization for the unconstrained case
template <class real_t, class int_t, template <class, class> class mat_t,
          template <class, class> class cons_t>
class Core<real_t, int_t, mat_t, cons_t, false, false>
    : public CoreBase<real_t, int_t, mat_t, cons_t, false, false> {
private:
  mat_t<real_t, int_t> phi;
  std::vector<real_t> AphiAt;

public:
  Core() = default;
  Core(const mat_t<real_t, int_t> &R, int_t n, const cons_t<real_t, int_t> &z)
      : CoreBase<real_t, int_t, mat_t, cons_t, false, false>{n, z}, phi{R},
        AphiAt{std::vector<real_t>(n * (n + 1) / 2)} {};
  void do_stuff(const std::vector<real_t> &A) override {
    std::cout
        << "\n\tUnconstrained case. Phi is of the same type as R, which in "
           "this case corresponds to ";
    if (is_diagonal<mat_t>::value == true)
      std::cout << "DIAGONAL MATRIX.";
    else
      std::cout << "POSITIVE DEFINITE MATRIX.";
    std::cout << "\n\tMatrices such as AphiAt might be precalculated."
              << std::endl;
  };
  virtual ~Core() = default;
};

// Specialization for the bounded case
template <class real_t, class int_t, template <class, class> class mat_t,
          template <class, class> class cons_t>
class Core<real_t, int_t, mat_t, cons_t, true, false>
    : public CoreBase<real_t, int_t, mat_t, cons_t, true, false> {
private:
  mat_t<real_t, int_t> phi;
  std::vector<real_t> phiAt;

public:
  Core() = default;
  Core(const mat_t<real_t, int_t> &R, int_t n, const cons_t<real_t, int_t> &z)
      : CoreBase<real_t, int_t, mat_t, cons_t, true, false>{n, z}, phi{R},
        phiAt{std::vector<real_t>(n * n)} {};
  void do_stuff(const std::vector<real_t> &A) override {
    std::cout
        << "\n\tSimple bounds case. Phi is of the same type as R, which in "
           "this case corresponds to ";
    if (is_diagonal<mat_t>::value == true)
      std::cout << "DIAGONAL MATRIX.";
    else
      std::cout << "POSITIVE DEFINITE MATRIX.";
    std::cout
        << "\n\tMatrices such as AphiAt may not be precomputed, since they "
           "change depending on the dual variables associated with the "
           "bounds."
        << std::endl;
  };
  virtual ~Core() = default;
};

// Specialization for the full case
template <class real_t, class int_t, template <class, class> class mat_t,
          template <class, class> class cons_t>
class Core<real_t, int_t, mat_t, cons_t, true, true>
    : public CoreBase<real_t, int_t, mat_t, cons_t, true, true> {
private:
  mpc::matrix::PDMat<real_t, int_t> phi;
  std::vector<real_t> phiAt;

public:
  Core() = default;
  Core(const mat_t<real_t, int_t> &R, int_t n, const cons_t<real_t, int_t> &z)
      : CoreBase<real_t, int_t, mat_t, cons_t, true, true>{n, z},
        phiAt{std::vector<real_t>(n * n)} {
    if (is_diagonal<mat_t>::value == false) {
      phi = mpc::matrix::PDMat<real_t, int_t>(n, R.data(), 'p');
    } else {
      std::vector<real_t> d(n * (n + 1) / 2);
      int_t aux{0};
      for (int_t i = 0; i < n; i++) {
        d[aux] = R[i];
        aux += n - i;
      }
    }
  };
  void do_stuff(const std::vector<real_t> &A) override {
    std::cout << "\n\tPhi is of type ";
    if (is_diagonal<mat_t>::value == true)
      std::cout << "DIAGONAL MATRIX";
    else
      std::cout << "POSITIVE DEFINITE MATRIX";
    std::cout
        << ", but we do not give a fuck.\n\tThere are linear constraints. "
           "Knowing that there is at least ONE linear "
           "constraint means that Phi must be of type POSITIVE DEFINITE "
           "MATRIX, since the "
           "augmentation Phi + F^T*D*F results in a positive definite "
           "matrix. There might exist simple BOUNDS also, but we do not care."
        << std::endl;
  }
  virtual ~Core() = default;
};

// ******************************* Envelope ****************************** //
template <class real_t, class int_t, template <class, class> class mat_t,
          template <class, class> class cons_t>
using Envelope = Core<real_t, int_t, mat_t, cons_t, has_cons<cons_t>::value,
                      has_cons<cons_t>::linear>;

// ******************************* Dummy ****************************** //
template <class T> struct is_integer { static const bool value = false; };

template <> struct is_integer<int> { static const bool value = true; };

template <bool, class real_t> struct divisor;

template <class real_t> struct divisor<false, real_t> {
  static void do_work(real_t *x) {
    *x /= 0;
    std::cout << "Dividing real number by zero.";
  };
};

template <class real_t> struct divisor<true, real_t> {
  static void do_work(real_t *x) {
    std::cout << "U shall not divide an integer number by zero, u dumbass.";
  }
};

template <class real_t> void divide(real_t *x) {
  divisor<is_integer<real_t>::value, real_t>::do_work(x);
}
// ******************************* Dummy ****************************** //

template <class real_t, class int_t>
using DiagMat = mpc::matrix::DiagMat<real_t, int_t>;
template <class real_t, class int_t>
using PDMat = mpc::matrix::PDMat<real_t, int_t>;

// ****************************** main.cpp ********************************* //
int main() {
  std::vector<double> lb{1, 2, 3}, ub{4, 5, 6}, F{5, 4, 6, 9}, f{0, 0};

  // My constraint classes
  Unconstrained<double, int> C1;
  SimpleBounds<double, int> C2(lb, ub);
  LinearBounds<double, int> C3(F, f);
  FullBounds<double, int> C4(lb, ub, F, f);

  // My matrices
  std::vector<double> d{1, 1, 2}, p{1, 2, 3, 4, 5, 6};
  std::vector<double> A(9, 2);
  DiagMat<double, int> D(d);
  PDMat<double, int> P(3, p);

  // Unconstrained cases
  Envelope<double, int, DiagMat, Unconstrained> E1(D, 3, C1);
  Envelope<double, int, PDMat, Unconstrained> E2(P, 3, C1);

  // Simple bounds
  Envelope<double, int, DiagMat, SimpleBounds> E3(D, 3, C2);
  Envelope<double, int, PDMat, SimpleBounds> E4(P, 3, C2);

  // Linear Cons
  Envelope<double, int, DiagMat, LinearBounds> E5(D, 3, C3);
  Envelope<double, int, PDMat, LinearBounds> E6(P, 3, C3);

  // Full cons
  Envelope<double, int, DiagMat, FullBounds> E7(D, 3, C4);
  Envelope<double, int, PDMat, FullBounds> E8(P, 3, C4);

  // Printing
  std::cout << "\n\n*************************\nUNCONSTRAINED CASE:";
  E1.do_stuff(A);
  E2.do_stuff(A);

  std::cout << "\n\n*************************\nSIMPLE BOUNDS:";
  E3.do_stuff(A);
  E4.do_stuff(A);

  std::cout << "\n\n*************************\nLINEAR CONSTRAINTS:";
  E5.do_stuff(A);
  E6.do_stuff(A);

  std::cout << "\n\n*************************\nFULL CONSTRAINTS:";
  E7.do_stuff(A);
  E8.do_stuff(A);

  std::cout << "\n\n*************************\nF";

  float x{4}, y{0};
  int z{5};

  divide(&x);
  std::cout << "\nx = " << x << "\n\n";
  divide(&y);
  std::cout << "\ny = " << y << "\n\n";
  divide(&z);
  std::cout << "\nz = " << z << "\n\n";

  return 0;
}

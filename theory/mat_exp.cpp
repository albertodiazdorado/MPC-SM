/* Learn how to evaluate the matrix exponential using LAPACK functions.
To do so, get the eignevalue decomposition and take the exponential of the
eigenvalues elementwise.
Then multiply by the orthogonal matrices to get the full matrix exponential. */

/* Include necessary libraries */
#include <algorithm>
#include <cmath>
#include <complex>
#include <iostream>
#include <vector>

#include "/home/add/Desktop/MPC-SM/src/detail/matops.cpp"

/* Get the required LAPACK interface */
extern "C" {
void dgeev_(const char *, const char *, const int *, double *, const int *,
            double *, double *, double *, const int *, double *, const int *,
            double *, const int *, int *);
void sgeev_(const char *, const char *, const int *, float *, const int *,
            float *, float *, float *, const int *, float *, const int *,
            float *, const int *, int *);
void dgemm_(const char *, const char *, const int *, const int *, const int *,
            const double *, const double *, const int *, const double *,
            const int *, const double *, double *, const int *);
void sgemm_(const char *, const char *, const int *, const int *, const int *,
            const float *, const float *, const int *, const float *,
            const int *, const float *, float *, const int *);
void cgetrf_(const int *, const int *, std::complex<float> *, const int *,
             int *, int *);
void cgetrs_(const char *, const int *, const int *,
             const std::complex<float> *, const int *, const int *,
             std::complex<float> *, const int *, const int *);
void zgetrf_(const int *, const int *, std::complex<double> *, const int *,
             int *, int *);
void zgetrs_(const char *, const int *, const int *,
             const std::complex<double> *, const int *, const int *,
             std::complex<double> *, const int *, const int *);
void cgemm_(const char *, const char *, const int *, const int *, const int *,
            const std::complex<float> *, const std::complex<float> *,
            const int *, const std::complex<float> *, const int *,
            const std::complex<float> *, std::complex<float> *, const int *);
void zgemm_(const char *, const char *, const int *, const int *, const int *,
            const std::complex<double> *, const std::complex<double> *,
            const int *, const std::complex<double> *, const int *,
            const std::complex<double> *, std::complex<double> *, const int *);
}

/* Write a wrapper to be able to use those monsters.
      A_old is the matrix whose eigenvalues shall be computed
      D is an array containing the real parto of the eigenvalues
      I imaginary part of the eigenvalues
      V is a matrix cointaining the eigenvectors
      info is an integer,returning 0 if everything went according to plan */
template <class real_t>
int eigenvalue(const int n, const real_t *A_old, real_t *D, real_t *I,
               real_t *V) {
  char JOBVL{'N'}, JOBVR{'V'};     // Get the right eigenvectors
  std::vector<real_t> left_eig(0); // Left eigenvectors not referenced
  std::vector<real_t> A(n * n);
  std::copy(A_old, A_old + n * n, A.data());
  int LWORK{8 * n};
  std::vector<real_t> WORK(LWORK); // Provide some workspace (large one)
  int info{0};
  dgeev_(&JOBVL, &JOBVR, &n, A.data(), &n, D, I, left_eig.data(), &n, V, &n,
         WORK.data(), &LWORK, &info);
  return info;
}

/* Specialize for FLOAT (single precision) */
template <>
int eigenvalue<float>(const int n, const float *A_old, float *D, float *I,
                      float *V) {
  char JOBVL{'N'}, JOBVR{'V'};    // Get the right eigenvectors
  std::vector<float> left_eig(0); // Left eigenvectors not referenced
  std::vector<float> A(n * n);
  std::copy(A_old, A_old + n * n, A.data());
  int LWORK{8 * n};
  std::vector<float> WORK(LWORK); // Provide some workspace (large one)
  int info{0};
  sgeev_(&JOBVL, &JOBVR, &n, A.data(), &n, D, I, left_eig.data(), &n, V, &n,
         WORK.data(), &LWORK, &info);
  return info;
}

/* Write a function that calculates the matrix exponential
      n: Dimension of square matrix A
      A: Matrix (n x n)
      expA: The matrix exponential of A (n x n)
*/
template <class real_t>
int mat_exponential(const int n, const real_t *A, real_t *expA) {

  /* Create the necessary auxiliar memory */
  int LWORK{8 * n}, info{0};
  std::vector<real_t> d(n), i(n), v(n * n), WORK(LWORK);
  std::vector<int> ipiv(n);

  /* Get the eigenvalues in A */
  info = eigenvalue(n, A, d.data(), i.data(), v.data());

  /* Check and exit if something went badly */
  if (info != 0)
    return info;

  /* Merge real and imaginary parts into a single complex number */
  std::vector<std::complex<real_t>> D(n);
  std::transform(d.cbegin(), d.cend(), i.cbegin(), D.begin(),
                 [](real_t d, real_t i) { return std::complex<real_t>(d, i); });

  /* Do the same with eigenvectors */
  std::vector<std::complex<real_t>> V(n * n);
  for (int eig = 0; eig < n; eig++) {
    if (i[eig] == 0)
      std::transform(v.cbegin() + eig * n, v.cbegin() + (eig + 1) * n,
                     V.begin() + eig * n,
                     [](real_t v) { return std::complex<real_t>(v, 0); });
    else
      for (int i = 0; i < n; i++) {
        V[eig * n + i] =
            std::complex<real_t>(v[eig * n + i], v[(eig + 1) * n + i]);
        V[(eig + 1) * n + i] =
            std::complex<real_t>(v[eig * n + i], -v[(eig + 1) * n + i]);
        eig++;
      }
  }

  /* Take the exponential of the eigenvalues */
  std::transform(D.cbegin(), D.cend(), D.begin(),
                 [](std::complex<real_t> v) { return std::exp(v); });

  /* Build a matrix from the exponentials and take the conjugate transpose of
  it
   */
  std::transform(D.cbegin(), D.cend(), D.begin(),
                 [](std::complex<real_t> v) { return std::conj(v); });
  std::vector<std::complex<real_t>> X(n * n);
  for (int idx = 0; idx < n; idx++)
    X[idx * n + idx] = D[idx];

  /* Factorize V in an auxiliar vector and solve V' * X = D',
        where V', D' denotes the conjugate transpose*/
  std::vector<std::complex<real_t>> aux{V};
  zgetrf_(&n, &n, aux.data(), &n, ipiv.data(), &info);
  if (info != 0)
    return info;
  char cmpl_conj{'C'};
  zgetrs_(&cmpl_conj, &n, &n, aux.data(), &n, ipiv.data(), X.data(), &n, &info);
  if (info != 0)
    return info;

  /* X' now containts the solution to X * V = D, which can be rewritten as
            V' * X' = D'.
    Next, evaluate the matrix exponential as V * X', where X' denotes again
    the
            conjugate transpose
  */
  std::vector<std::complex<real_t>> expA_complex(n * n);
  char hermitian{'C'}, notrans{'N'};
  std::complex<real_t> one(1, 0), zero(0, 0);
  zgemm_(&notrans, &hermitian, &n, &n, &n, &one, V.data(), &n, X.data(), &n,
         &zero, expA_complex.data(), &n);
  if (info != 0)
    return info;

  /* By construction, expA_complex should be pure real. Nevertheless, we will
   * just take the real parts here, and so disregard any numerical
   inaccuracy.
   */
  std::transform(expA_complex.cbegin(), expA_complex.cend(), expA,
                 [](std::complex<real_t> v) { return std::real(v); });

  /* Return info */
  return info;
}

template <>
int mat_exponential<float>(const int n, const float *A, float *expA) {

  /* Create the necessary auxiliar memory */
  int LWORK{8 * n}, info{0};
  std::vector<float> d(n), i(n), v(n * n), WORK(LWORK);
  std::vector<int> ipiv(n);

  /* Get the eigenvalues in A */
  info = eigenvalue(n, A, d.data(), i.data(), v.data());

  /* Check and exit if something went badly */
  if (info != 0)
    return info;

  /* Merge real and imaginary parts into a single complex number */
  std::vector<std::complex<float>> D(n);
  std::transform(d.cbegin(), d.cend(), i.cbegin(), D.begin(),
                 [](float d, float i) { return std::complex<float>(d, i); });

  /* Do the same with eigenvectors */
  std::vector<std::complex<float>> V(n * n);
  for (int eig = 0; eig < n * n; eig++) {
    if (i[eig] == 0)
      std::transform(v.cbegin() + eig * n, v.cbegin() + (eig + 1) * n,
                     V.begin() + eig * n,
                     [](float v) { return std::complex<float>(v, 0); });
    else
      for (int i = 0; i < n; i++) {
        V[eig * n + i] =
            std::complex<float>(v[eig * n + i], v[(eig + 1) * n + i]);
        V[(eig + 1) * n + i] =
            std::complex<float>(v[eig * n + i], -v[(eig + 1) * n + i]);
        eig++;
      }
  }

  /* Take the exponential of the eigenvalues */
  std::transform(D.cbegin(), D.cend(), D.begin(),
                 [](std::complex<float> v) { return std::exp(v); });

  /* Build a matrix from the exponentials and take the conjugate transpose of it
   */
  std::transform(D.cbegin(), D.cend(), D.begin(),
                 [](std::complex<float> v) { return std::conj(v); });
  std::vector<std::complex<float>> X(n * n);
  for (int idx = 0; idx < n; idx++)
    X[idx * n + idx] = D[idx];

  /* Factorize V in an auxiliar vector and solve V' * X = D',
        where V', D' denotes the conjugate transpose*/
  std::vector<std::complex<float>> aux{V};
  cgetrf_(&n, &n, aux.data(), &n, ipiv.data(), &info);
  if (info != 0)
    return info;
  char cmpl_conj{'C'};
  cgetrs_(&cmpl_conj, &n, &n, aux.data(), &n, ipiv.data(), X.data(), &n, &info);
  if (info != 0)
    return info;

  /* X' now containts the solution to X * V = D, which can be rewritten as
            V' * X' = D'.
    Next, evaluate the matrix exponential as V * X', where X' denotes again the
            conjugate transpose
  */
  std::vector<std::complex<float>> expA_complex(n * n);
  char hermitian{'H'}, notrans{'N'};
  std::complex<float> one(1, 0), zero(0, 0);
  cgemm_(&notrans, &hermitian, &n, &n, &n, &one, V.data(), &n, X.data(), &n,
         &zero, expA_complex.data(), &n);
  if (info != 0)
    return info;

  /* By construction, expA_complex should be pure real. Nevertheless, we will
   * just take the real parts here, and so disregard any numerical inaccuracy.
   */
  std::transform(expA_complex.cbegin(), expA_complex.cend(), expA,
                 [](std::complex<float> v) { return std::real(v); });

  /* Return info */
  return info;
}

int main() {
  int n{4};
  std::vector<double> A{2,  3,  5,  7,  11, 13, 17, 19,
                        23, 29, 31, 37, 41, 43, 47, 49};
  std::vector<double> D(n), I(n), V(n * n);
  eigenvalue(n, A.data(), D.data(), I.data(), V.data());

  // std::cout << "The eigenvalues in A are:";
  // for (int idx = 0; idx < n; idx++)
  //   std::cout << "\n\t" << D[idx] << " + " << I[idx] << " j,";

  // std::cout << "\n\nThe eigenvectors in A are:";
  // for (int idx = 0; idx < n; idx++) {
  //   std::cout << "\n\tEigenvalue " << idx << ": [ ";
  //   for (int i = 0; i < n; i++) {
  //     std::cout << V[n * idx + i] << ", ";
  //   }
  //   std::cout << "]";
  // }

  std::vector<double> expA(n * n);
  auto info = mat_exponential(n, A.data(), expA.data());
  if (info == 0) {
    std::cout << "\n\nThe matrix exponential of A is:";
    for (int i = 0; i < n; i++) {
      std::cout << "\n\t";
      for (int j = 0; j < n; j++) {
        std::cout << expA[n * j + i] << ", ";
      }
    }
  } else
    std::cout << "\n\nERROR CODE: " << info;
  std::cout << "\n\n";
  return 0;
}
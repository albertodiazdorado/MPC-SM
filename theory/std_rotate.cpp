#include <algorithm>
#include <iostream>
#include <vector>

int main() {
  std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9};

  std::cout << "\nVector BEFORE the rotation:\n\t[";
  for (auto &elem : v)
    std::cout << elem << "\t";
  std::cout << "]." << std::endl;

  std::rotate(v.begin(), v.begin() + 3, v.end());
  std::cout << "\nVector AFTER rotating three elements to the left:\n\t[";
  for (auto &elem : v)
    std::cout << elem << "\t";
  std::cout << "]." << std::endl;

  return 0;
}
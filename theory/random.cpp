#include <random>
#include <iostream>

int main(int argc, char* argv[]) {
  std::random_device rd;
  std::mt19937 gen(rd());

  std::normal_distribution<> gauss(10, 0.001);

  std::cout << "cputime = " << gauss(gen) << '\n';
  
  return 0;
}
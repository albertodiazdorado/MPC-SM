template <class int_t, int_t N>
struct Factorial {
  static constexpr int_t value = N*Factorial<int_t, N-1>::value;
};

template <class int_t>
struct Factorial<int_t, 1> {
  static constexpr int_t value = 1;
};

template <class int_t, int_t N>
int_t fact1() {
  return Factorial<int_t, N>::value;
}

template <class int_t, int_t N>
int_t fact2() {
  int_t result = 1;
  for (int_t idx = 2; idx < N; idx++)
    result *= idx;
  return result;
}

template <class int_t, int_t N>
int_t fact3() {
  return N*fact3<int_t, N-1>();
}

template <>
long int fact3<long int, 1>() {
  return 1;
}

template long int fact1<long int, 10>();
template long int fact2<long int, 10>();
template long int fact3<long int, 10>();
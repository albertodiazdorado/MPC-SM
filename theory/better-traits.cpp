#include <iostream>
#include <string>
#include <type_traits>

template <class real_t> struct DiagMat {
  std::string name() { return "Diagonal matrix"; }
};

template <class real_t> struct PDMat {
  std::string name() { return "PD matrix"; }
};

template <template <class> class> struct is_diag {
  static constexpr bool value = false;
};

template <> struct is_diag<DiagMat> { static constexpr bool value = true; };

template <class real_t, template <class> class M1, template <class> class M2>
struct Shell {
  std::string name() { return matrix_.name(); }

private:
  using mat_t =
      typename std::conditional<is_diag<M1>::value && is_diag<M2>::value,
                                DiagMat<real_t>, PDMat<real_t>>::type;
  mat_t matrix_;
};

int main(int argc, char *argv[]) {
  Shell<float, DiagMat, PDMat> s1;
  Shell<float, PDMat, DiagMat> s2;
  Shell<float, PDMat, PDMat> s3;
  Shell<float, DiagMat, DiagMat> s4;

  std::cout << "s1.name(): " << s1.name() << std::endl;
  std::cout << "s2.name(): " << s2.name() << std::endl;
  std::cout << "s3.name(): " << s3.name() << std::endl;
  std::cout << "s4.name(): " << s4.name() << std::endl;

  return 0;
}
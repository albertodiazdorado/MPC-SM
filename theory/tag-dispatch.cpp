#include <iostream>
#include <type_traits>

template <class real_t> struct PDMat {};

template <class real_t> struct DiagMat {};

template <template <class> class> struct is_diag {
  static constexpr bool value = false;
};

template <> struct is_diag<DiagMat> { static constexpr bool value = true; };

template <class real_t, template <class> class M1, template <class> class M2>
struct Policy {
public:
  real_t eval() const {
    return eval(typename std::conditional<is_diag<M1>::value, std::true_type,
                                          std::false_type>::type{},
                typename std::conditional<is_diag<M2>::value, std::true_type,
                                          std::false_type>::type{});
  }

private:
  real_t eval(std::false_type, std::false_type) const { return real_t{0}; }
  real_t eval(std::false_type, std::true_type) const { return real_t{1}; }
  real_t eval(std::true_type, std::false_type) const { return real_t{2}; }
  real_t eval(std::true_type, std::true_type) const { return real_t{3}; }
};

int main(int argc, char *argv[]) {
  Policy<float, PDMat, PDMat> p1;
  std::cout << "p1.eval(): " << p1.eval() << std::endl;

  // Policy<float, PDMat, DiagMat> p2;
  // std::cout << "p2.eval(): " << p2.eval() << std::endl;

  // Policy<float, DiagMat, PDMat> p3;
  // std::cout << "p3.eval(): " << p3.eval() << std::endl;

  // Policy<float, DiagMat, DiagMat> p4;
  // std::cout << "p4.eval(): " << p4.eval() << std::endl;

  return 0;
}
#include <iostream>

struct A {
  A() = default;
  // A(const int a) : a{a} {}

  template <class T> void operator()(T &&a) & { std::cout << a << '\n'; }

  template <class T> void operator()(T &&a) && { std::cout << a + 5 << '\n'; }

private:
  // int a;
};

template <class Func, class Var> void call(Func &&f, Var &&a) {
  std::forward<Func>(f)(std::forward<Var>(a));
}

template <class T> T callme(T &&t) {
  std::cout << __PRETTY_FUNCTION__ << '\n';
  return std::forward<T>(t);
}

int main(int argc, char *argv[]) {
  A a;
  a(5);
  A{}(6);

  call(a, 5);
  call(A{}, 6);

  callme(a);
  callme(A{});

  return 0;
}

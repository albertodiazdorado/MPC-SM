#ifndef WRITECSV_HPP_
#define WRITECSV_HPP_

#include <fstream>

template <class Sink, class T1, class T2>
void writeline(Sink &&s, T1 &&val1, T2 &&val2) {
  s << val1 << ',' << val2 << '\n';
}

template <class Sink, class T1, class... Ts>
void writeline(Sink &&s, T1 &&val1, Ts &&... vals) {
  s << val1 << ',';
  writeline(s, std::forward<Ts>(vals)...);
}

template <class String, class ForwardIt, class InputIt, class... InputIts>
void writecsv(String &&filename, ForwardIt lbegin, ForwardIt lend,
              InputIt fbegin, InputIt fend, InputIts... rbegin) {
  std::ofstream file{filename};
  std::size_t n(0), N(std::distance(lbegin, lend));

  while (lbegin != lend) {
    file << *lbegin++;
    if (n++ != N - 1)
      file << ',';
  }
  file << '\n';

  while (fbegin != fend)
    writeline(file, *fbegin++, *rbegin++...);
  file << '\n';
}

#endif
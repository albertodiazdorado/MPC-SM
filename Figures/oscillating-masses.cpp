// Include the qpOASES.hpp header
// It is located in ~/qpoases/include
// Please, do not forget to tell the compiler where to look for include files

// Printing options
constexpr bool ENABLE_PRINT = false;

// Add the option "-I /home/add/qpoases/include/"
// Add the option "-lqpOASES -L /home/add/qpoases/lib"
#include "qpOASES/QProblem.hpp"

#include "factory.cpp"

// Include additional files that you are going to use
#include <chrono>
#include <cstdlib>
#include <iostream>

// Add your solver
#include "constraints/InputCons.cpp"
#include "constraints/SC_SimpleBounds.cpp"
#include "solver/Mehrotra.cpp"

// Include namespace
using namespace mpc;

// Write the helpers that let you call Mehrotra sovler
template <class real_t, class int_t>
using DiagMat = mpc::matrix::DiagMat<real_t, int_t>;

template <class real_t, class int_t>
using MyInputCons =
    mpc::InputCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat>;

template <class real_t, class int_t>
using MyStateCons =
    mpc::StateCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat,
                                 mpc::matrix::DiagMat>;

using Mehrotra = mpc::solver::Mehrotra<double, int, DiagMat, DiagMat, DiagMat,
                                       MyInputCons, MyStateCons>;

// Write main file
int main(int argc, char *argv[]) {
  // *********************************************************************** //
  // ************************** CREATE PROBLEM DATA ************************ //
  // *********************************************************************** //

  /* Create dynamics */
  long n{6}, m{3}, T{10}, profile{1};
  switch (argc) {
  case 5:
    m = std::atol(argv[4]);
  case 4:
    profile = std::atol(argv[3]);
  case 3:
    T = std::atol(argv[2]);
  case 2:
    n = std::atol(argv[1]);
  default:
    break;
  }
  if (ENABLE_PRINT)
    std::cout << "Running with n = " << n << ", m = " << m << ", T = " << T
              << ", profile = " << profile << '\n';

  double eta{0.99};
  extra::Osc_Mass<double> test(n, m, profile);
  double Ts{0.5};
  test.discretize(Ts);

  /* Create penalties and constraints */
  std::vector<double> R(test.ninputs() * test.ninputs()),
      Q(test.nstates() * test.nstates()), P(test.nstates() * test.nstates());
  for (size_t idx = 0; idx < n; idx++) {
    Q[idx + test.nstates() * idx] = 5;
    Q[idx + n + test.nstates() * (idx + n)] = 1;
  }
  for (size_t idx = 0; idx < test.ninputs(); idx++)
    R[idx + idx * test.ninputs()] = 2;
  P = Q;
  std::vector<double> lbu(test.ninputs(), -0.5), ubu(test.ninputs(), 0.5),
      lbx(test.nstates(), -4), ubx(test.nstates(), 4);

  /* Create data structure */
  solver::mpcdata<double> MyData(test.nstates(), test.ninputs(), T, test.A,
                                 test.B);
  MyData.R = R;
  MyData.Q = Q;
  MyData.P = P;
  MyData.lbu = lbu;
  MyData.ubu = ubu;
  MyData.lbx = MyData.lbt = lbx;
  MyData.ubx = MyData.ubt = ubx;

  /* Create some initial state */
  std::vector<double> x0(test.nstates());
  for (size_t idx = 0; idx < n; idx = idx + 2) {
    x0[idx] = 1;
    x0[idx + 1] = -1;
  }

  // *********************************************************************** //
  // ******************************* MEHROTRA ****************************** //
  // *********************************************************************** //
  mpc::solver::mpcoptions<double> MyOptions;
  MyOptions.eta = eta;
  MyOptions.optimality_tolerance = 1e-5;
  Mehrotra FastMPC(MyData, x0);
  FastMPC.set_options(MyOptions);

  // *********************************************************************** //
  // ******************************* qpOASES ******************************* //
  // *********************************************************************** //
  extra::qpoases_dense_mpc<double> MydenseQP(MyData);

  // *********************************************************************** //
  // ***************************** RUN qpOASES ***************************** //
  // *********************************************************************** //
  /* Use MPC settings */
  qpOASES::QProblem OASES(MydenseQP.get_vars(), MydenseQP.get_cons());
  qpOASES::Options myOasesOptions;
  myOasesOptions.setToMPC();
  myOasesOptions.printLevel = qpOASES::PL_NONE;
  OASES.setOptions(myOasesOptions);

  /* Solve first QP. */
  qpOASES::int_t nWSR{1000};
  double cputime{1000};

  auto tstart = std::chrono::high_resolution_clock::now();
  OASES.init(MydenseQP.get_hessian(), MydenseQP.get_gradient(x0.data()),
             MydenseQP.get_F(), MydenseQP.get_lb(), MydenseQP.get_ub(),
             MydenseQP.get_lbF(), MydenseQP.get_f(x0.data()), nWSR, &cputime);
  auto tend = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> OASES_time = tend - tstart;

  /* Get and print solution of first QP. */
  std::vector<double> xOpt(T * m), uopt(T * m), xopt(T * n);
  if (ENABLE_PRINT) {
    OASES.getPrimalSolution(xOpt.data());
    std::cout << "\nThe minimizer is: ";
    for (size_t idx = 0; idx < xOpt.size(); idx++)
      std::cout << "\n\t" << xOpt[idx];
  }
  std::cout << "\nOptimal value: "
            << OASES.getObjVal() + MydenseQP.get_residual(x0.data())
            << std::endl;
  std::cout << "nWSR = " << nWSR;

  // *********************************************************************** //
  // **************************** RUN Mehrotra ***************************** //
  // *********************************************************************** //

  tstart = std::chrono::high_resolution_clock::now();
  FastMPC.solve();
  tend = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> Mehrotra_time = tend - tstart;
  std::cout << "\n\n\n***********************************\n"
            << "FastMPC REPORT:\n"
            << "Iterations needed: " << FastMPC.get_k() << std::endl;
  std::cout << "Duality gap: " << FastMPC.get_duality_gap() << std::endl;
  std::cout << "Norm of equality residual: " << FastMPC.get_eq_residual()
            << std::endl;
  std::cout << "Optimal value: " << FastMPC.get_opt_val() << std::endl;

  if (ENABLE_PRINT) {
    uopt = FastMPC.get_opt_input();
    std::cout << "\nThe vector of optimal inputs is :\n";
    for (size_t i = 0; i < uopt.size(); i++)
      std::cout << "\n\t" << uopt[i];
  }

  // *********************************************************************** //
  // ****************************** Comparison ***************************** //
  // *********************************************************************** //
  std::cout << "\n\n\n***********************************\nCOMPARISON:\n";
  std::cout << "\tcputime (Mehrotra): " << Mehrotra_time.count()
            << " miliseconds";
  std::cout << "\n\tcputime (qpOASES):  " << cputime * 1000 << " miliseconds"
            << std::endl;

  return 0;
}
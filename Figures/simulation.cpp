// Include the qpOASES.hpp header
// It is located in ~/qpoases/include
// Please, do not forget to tell the compiler where to look for include files

// Printing options
constexpr bool CUSTOM_TIMING = false;
constexpr bool ENABLE_DISTURBANCE = false;
constexpr bool PRINT_STUFF = false;

// Add the option "-I /home/add/qpoases/include/"
// Add the option "-lqpOASES -L /home/add/qpoases/lib"
#include "qpOASES/QProblem.hpp"

#include "factory.cpp"

// Include additional files that you are going to use
#include <chrono>
#include <iostream>

// Add your solver
#include "constraints/InputCons.cpp"
#include "constraints/SC_SimpleBounds.cpp"
#include "solver/Mehrotra.cpp"

// Include namespace
using namespace mpc;

// Write the helpers that let you call Mehrotra sovler
template <class real_t, class int_t>
using DiagMat = mpc::matrix::DiagMat<real_t, int_t>;
template <class real_t, class int_t>
using MyInputCons =
    mpc::InputCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat>;
template <class real_t, class int_t>
using MyStateCons =
    mpc::StateCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat,
                                 mpc::matrix::DiagMat>;
using Mehrotra = mpc::solver::Mehrotra<double, int, DiagMat, DiagMat, DiagMat,
                                       MyInputCons, MyStateCons>;

// Write main file
int main(int argc, char *argv[]) {
  std::size_t n{6}, m{3}, T{15}, profile{1};
  std::string sim_number;
  switch (argc) {
  case 5:
    profile = std::atol(argv[4]);
  case 4:
    T = std::atol(argv[3]);
  case 3:
    n = std::atol(argv[2]);
  case 2:
    sim_number = argv[1];
    break;
  default:
    sim_number = "1";
    break;
  }
  /* Generate the filename */
  std::string filename{"/home/add/Desktop/MPC-SM/"};
  filename.append(sim_number);
  filename.append(".csv");
  if (PRINT_STUFF) {
    std::cout << "Running with n = " << n << ", m = " << m << ", T = " << T
              << ", profile = " << profile << '\n';
    std::cout << "Writing to: " << filename << std::endl;
  }
  // *********************************************************************** //
  // ************************** CREATE PROBLEM DATA ************************ //
  // *********************************************************************** //

  /* Create dynamics */
  double eta{0.99};
  extra::Osc_Mass<double> test(n, m, profile);
  double Ts{0.5};
  test.discretize(Ts);

  /* Create penalties and constraints */
  std::vector<double> R(test.ninputs() * test.ninputs()),
      Q(test.nstates() * test.nstates()), P(test.nstates() * test.nstates());
  for (size_t idx = 0; idx < n; idx++) {
    Q[idx + test.nstates() * idx] = 5;
    Q[idx + n + test.nstates() * (idx + n)] = 1;
  }
  for (size_t idx = 0; idx < test.ninputs(); idx++)
    R[idx + idx * test.ninputs()] = 2;
  P = Q;
  std::vector<double> lbu(test.ninputs(), -0.5), ubu(test.ninputs(), 0.5),
      lbx(test.nstates(), -4), ubx(test.nstates(), 4);

  /* Create data structure */
  solver::mpcdata<double> MyData(test.nstates(), test.ninputs(), T, test.A,
                                 test.B);
  MyData.R = R;
  MyData.Q = Q;
  MyData.P = P;
  MyData.lbu = lbu;
  MyData.ubu = ubu;
  MyData.lbx = MyData.lbt = lbx;
  MyData.ubx = MyData.ubt = ubx;

  /* Create some initial state */
  std::vector<double> x0(test.nstates()), x1{x0};
  for (size_t idx = 0; idx < n; idx = idx + 2) {
    x0[idx] = 1;
    x0[idx + 1] = -1;
  }

  // *********************************************************************** //
  // ******************************* MEHROTRA ****************************** //
  // *********************************************************************** //
  mpc::solver::mpcoptions<double> MyOptions;
  MyOptions.eta = eta;
  MyOptions.optimality_tolerance = 1e-5;
  Mehrotra FastMPC(MyData, x0);
  FastMPC.set_options(MyOptions);

  // *********************************************************************** //
  // **************************** qpOASES data ***************************** //
  // *********************************************************************** //
  extra::qpoases_dense_mpc<double> MydenseQP(MyData);

  // *********************************************************************** //
  // ***************************** MPC-qpOASES ***************************** //
  // *********************************************************************** //
  /* Use MPC settings */
  qpOASES::QProblem OASES(MydenseQP.get_vars(), MydenseQP.get_cons());
  qpOASES::Options myOasesOptions;
  myOasesOptions.setToMPC();
  myOasesOptions.printLevel = qpOASES::PL_NONE;
  OASES.setOptions(myOasesOptions);

  // *********************************************************************** //
  // ************************** Simulation loop **************************** //
  // *********************************************************************** //
  /* Simulate some steps */
  size_t sim_steps{15};
  std::vector<double> mehrotraTime(sim_steps), qpOasesCustom(sim_steps);
  std::vector<qpOASES::int_t> nWSR(sim_steps, 1000);
  std::vector<double> cputime(sim_steps, 1000);
  std::vector<size_t> iter(sim_steps);

  /* Initial step */
  // Run qpOASES
  auto tstart = std::chrono::high_resolution_clock::now();
  OASES.init(MydenseQP.get_hessian(), MydenseQP.get_gradient(x0.data()),
             MydenseQP.get_F(), MydenseQP.get_lb(), MydenseQP.get_ub(),
             MydenseQP.get_lbF(), MydenseQP.get_f(x0.data()), nWSR[0],
             &cputime[0]);
  auto tend = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> tdiff = tend - tstart;
  qpOasesCustom[0] = tdiff.count();
  // Run Mehrotra
  tstart = std::chrono::high_resolution_clock::now();
  FastMPC.solve();
  tend = std::chrono::high_resolution_clock::now();
  tdiff = tend - tstart;
  mehrotraTime[0] = tdiff.count();
  iter[0] = FastMPC.get_k();

  /* Enable disturbances */
  std::vector<double> dmin(n, -0.5), dmax(n, 0.5);
  if (ENABLE_DISTURBANCE)
    test.enable_disturbance(dmin, dmax, 1);

  /* Simulation loop */
  for (size_t step = 1; step < sim_steps; step++) {
    /* Check solution quality */
    if (std::abs(FastMPC.get_opt_val() -
                 (OASES.getObjVal() + MydenseQP.get_residual(x0.data()))) >
            1e-3 &&
        PRINT_STUFF) {
      std::cerr << "\n\nERROR:\n\tIteration: " << step;
      std::cerr << "\n\tMehortra optimal value: " << FastMPC.get_opt_val();
      std::cerr << "\n\tqpOases optimal value: "
                << OASES.getObjVal() + MydenseQP.get_residual(x0.data());
      std::cerr << "\n\tMehrotra iterations: " << FastMPC.get_k();
      std::cerr << "\n\tqpOASES iterations: " << nWSR[step - 1] << "\n\n";
    }
    /* Simulate next step */
    test.simulate(x0.data(), FastMPC.get_opt_input().data(), x1.data());
    x0 = x1;
    /* Run next optimization */
    tstart = std::chrono::high_resolution_clock::now();
    OASES.hotstart(MydenseQP.get_gradient(x0.data()), MydenseQP.get_lb(),
                   MydenseQP.get_ub(), MydenseQP.get_lbF(),
                   MydenseQP.get_f(x0.data()), nWSR[step], &cputime[step]);
    tend = std::chrono::high_resolution_clock::now();
    tdiff = tend - tstart;
    qpOasesCustom[step] = tdiff.count();
    tstart = std::chrono::high_resolution_clock::now();
    FastMPC.solve(x0.data());
    tend = std::chrono::high_resolution_clock::now();
    tdiff = tend - tstart;
    mehrotraTime[step] = tdiff.count();
    iter[step] = FastMPC.get_k();
  }

  // *********************************************************************** //
  // ****************************** Comparison ***************************** //
  // *********************************************************************** //
  if (PRINT_STUFF) {
    std::cout << "\n\n\n***********************************\nCOMPARISON:";

    std::cout.precision(7);
    for (size_t step = 0; step < sim_steps; step++) {
      std::cout << "\n\nStep " << step << ":";
      std::cout << "\n\tMehrotra:          "
                << static_cast<double>(mehrotraTime[step])
                << " miliseconds;\t\tIterations: " << iter[step];
      std::cout << "\n\tqpOASES (mpc):     " << cputime[step] * 1000
                << " miliseconds;\t\tIterations: " << nWSR[step];
      if (CUSTOM_TIMING)
        std::cout << "\n\tqpOASES (all):     " << qpOasesCustom[step]
                  << " miliseconds";
    }
  }

  // *********************************************************************** //
  // **************************** Print to CSV ***************************** //
  // *********************************************************************** //
  std::transform(cputime.cbegin(), cputime.cend(), cputime.begin(),
                 [](double time) { return time * 1000; });
  extra::writecsv(filename, mehrotraTime.begin(), mehrotraTime.end(),
                  cputime.begin(), iter.begin(), nWSR.begin());

  return 0;
}
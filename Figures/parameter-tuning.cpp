#include "factory.cpp"

// Include additional files that you are going to use
#include <chrono>
#include <iostream>

// Add your solver
#include "constraints/InputCons.cpp"
#include "constraints/SC_SimpleBounds.cpp"
#include "detail/extra.cpp"
#include "solver/Mehrotra.cpp"

// Include namespace
using namespace mpc;

// Write the helpers that let you call Mehrotra sovler
template <class real_t, class int_t>
using DiagMat = mpc::matrix::DiagMat<real_t, int_t>;

template <class real_t, class int_t>
using MyInputCons =
    mpc::InputCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat>;

template <class real_t, class int_t>
using MyStateCons =
    mpc::StateCons::SimpleBounds<real_t, int_t, mpc::matrix::DiagMat,
                                 mpc::matrix::DiagMat>;

using Mehrotra = mpc::solver::Mehrotra<double, int, DiagMat, DiagMat, DiagMat,
                                       MyInputCons, MyStateCons>;

// Write main file
int main() {
  // *********************************************************************** //
  // ************************** CREATE PROBLEM DATA ************************ //
  // *********************************************************************** //

  /* Create dynamics */
  size_t n{12}, m{3}, T{100};
  size_t profile{1};
  double eta{0.95};
  extra::Osc_Mass<double> test(n, m, profile);
  double Ts{0.5};
  test.discretize(Ts);

  /* Create penalties */
  std::vector<double> R(test.ninputs() * test.ninputs()),
      Q(test.nstates() * test.nstates()), P(test.nstates() * test.nstates());
  for (size_t idx = 0; idx < n; idx++) {
    Q[idx + test.nstates() * idx] = 5;
    Q[idx + n + test.nstates() * (idx + n)] = 1;
  }
  for (size_t idx = 0; idx < test.ninputs(); idx++)
    R[idx + idx * test.ninputs()] = 2;
  P = Q;

  /* Create constraints */
  std::vector<double> lbu(test.ninputs(), -0.5), ubu(test.ninputs(), 0.5),
      lbx(test.nstates(), -4), ubx(test.nstates(), 4);

  /* Create data structure */
  solver::mpcdata<double> MyData(test.nstates(), test.ninputs(), T, test.A,
                                 test.B);
  MyData.R = R;
  MyData.Q = Q;
  MyData.P = P;
  MyData.lbu = lbu;
  MyData.ubu = ubu;
  MyData.lbx = MyData.lbt = lbx;
  MyData.ubx = MyData.ubt = ubx;

  /* Create some initial state */
  std::vector<double> x0(test.nstates());
  for (size_t idx = 0; idx < n; idx = idx + 2) {
    x0[idx] = 1;
    x0[idx + 1] = -1;
  }

  // *********************************************************************** //
  // ******************************* MEHROTRA ****************************** //
  // *********************************************************************** //
  mpc::solver::mpcoptions<double> MyOptions;
  Mehrotra FastMPC(MyData, x0);

  // *********************************************************************** //
  // **************************** RUN Mehrotra ***************************** //
  // *********************************************************************** //
  // Solve for eta = 0.99
  MyOptions.eta = eta;
  MyOptions.max_iterations = 200;
  MyOptions.optimality_tolerance = 1e-6;
  FastMPC.set_options(MyOptions);
  std::vector<double> re(MyOptions.max_iterations), mu(re.size()),
      stepsize(re.size());
  std::vector<size_t> k(re.size());
  size_t idx{0};

  // Initialize nu_aff with the equality residual
  // re[block] = Ax[i] + Bu[i] - x[i-1]
  FastMPC.eq_residual();
  do {
    k[idx] = idx;
    re[idx] = FastMPC.get_eq_residual();
    mu[idx] = FastMPC.get_duality_gap();
    stepsize[idx] = FastMPC.get_step_size();
    idx++;
    FastMPC.iterate();
  } while (FastMPC.terminate() == false);

  std::cout << "\n\n\n***********************************\n"
            << "FastMPC REPORT:.\n"
            << "Iterations needed: " << FastMPC.get_k() << std::endl;
  std::cout << "Duality gap: " << FastMPC.get_duality_gap() << std::endl;
  std::cout << "Norm of equality residual: " << FastMPC.get_eq_residual()
            << std::endl;
  std::cout << "Optimal value: " << FastMPC.get_opt_val() << std::endl;

  // *********************************************************************** //
  // ************************** Write to a csv file ************************ //
  // *********************************************************************** //
  std::vector<std::string> header{"iteration", "eq residual", "duality gap",
                                  "step size"};
  mpc::extra::writecsv("eta99.csv", std::begin(header), std::end(header),
                       std::begin(k), std::end(k), std::begin(re),
                       std::begin(mu), std::begin(stepsize));

  return 0;
}
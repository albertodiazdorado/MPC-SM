#include "detail/extra.cpp"
#include "solver/Mehrotra.cpp"
#include <iostream>
#include <random>
#include <tuple>

namespace mpc {
namespace extra {

/* The triple is built by three integers:
  FIRST:  Number of the input
  SECOND: Mass upon which the force is exerted
  THIRD:  +1 for a positive force, -1 for a negative force
*/
using triple = std::tuple<int, int, int>;

// ************************************************************************ //
// ******************** PROBLEM INSTANCE CONVERSION *********************** //
// ************************************************************************ //
template <class real_t> struct qpoases_dense_mpc {
private:
  size_t n, m, T;
  size_t N;
  bool lbu{false}, ubu{false}, Fu{false}, lbx{false}, ubx{false}, Fx{false},
      lbt{false}, ubt{false}, Ft{false};
  size_t kFu{0}, klbu{0}, kubu{0}, kFx{0}, klbx{0}, kubx{0}, klbt{0}, kubt{0},
      kFt{0};
  size_t k_total{0};
  std::vector<real_t> A, B; // Natural matrix and controllability matrix
  std::vector<real_t> FxA;
  std::vector<real_t> H;      // Hessian (constant)
  std::vector<real_t> g, h;   // Gradient (g = h*x0; hence, h is a matrix)
  std::vector<real_t> lb, ub; // Lower and upper bounds (should be constant)
  std::vector<real_t> F;      // Constraint matrix (constant)
  std::vector<real_t> f; // Also known as ubF. Here it has been renamed för att
                         // det finns ingen lbF.
  std::vector<real_t> superubx, superlbx, superfx;
  std::vector<real_t> lbF;
  std::vector<real_t> superQ, superR;

public:
  qpoases_dense_mpc() = default;
  qpoases_dense_mpc(const solver::mpcdata<real_t> &obj)
      : n{obj.n}, m{obj.m}, T{obj.T}, lbF{std::vector<real_t>(0)} {
    /* Number of optimization variables in the resulting QP */
    N = T * m;

    /* Natural response matrix */
    A = std::vector<real_t>(n * n * T);
    std::vector<real_t> tempA{obj.A};
    detail::transpose(n, n, tempA);
    std::copy(tempA.cbegin(), tempA.cend(), A.begin());
    for (size_t idx = 1; idx < T; idx++)
      matmult(n, n, n, A.data() + (idx - 1) * (n * n), tempA.data(),
              A.data() + idx * n * n);
    mpc::detail::transpose(n, T * n, A.data());

    /* Controllability matrix */
    B = std::vector<real_t>(T * n * T * m);
    std::vector<real_t> temp(n * m), temp2(n * m);
    for (size_t col = 0; col < T; col++) {
      temp = obj.B;
      allocate(n, m, temp.data(), col * n, col * m, B.data(), T * n);
      for (size_t row = col + 1; row < T; row++) {
        matmult(n, m, n, obj.A.data(), temp.data(), temp2.data(), double{0});
        temp = temp2;
        allocate(n, m, temp.data(), row * n, col * m, B.data(), T * n);
      }
    }

    /* Lower bound */
    if (obj.lbu.size() == m) {
      lb = std::vector<real_t>(T * m);
      for (size_t idx = 0; idx < T; idx++)
        std::copy(obj.lbu.cbegin(), obj.lbu.cend(), lb.begin() + idx * m);
      lbu = true;
    } else if (obj.lbu.size() == 0) {
      lb = std::vector<real_t>(0);
    } else {
      std::cout << "\nlbu has wrong dimension.\n\tProvided: " << obj.lbu.size()
                << ".\n\tExpected: " << this->m << std::endl;
    }

    /* Upper bound */
    if (obj.ubu.size() == m) {
      ub = std::vector<real_t>(T * m);
      for (size_t idx = 0; idx < T; idx++)
        std::copy(obj.ubu.cbegin(), obj.ubu.cend(), ub.begin() + idx * m);
      ubu = true;
    } else if (obj.ubu.size() == 0) {
      ub = std::vector<real_t>(0);
    } else {
      std::cout << "\nlbu has wrong dimension.\n\tProvided: " << obj.ubu.size()
                << ".\n\tExpected: " << this->m << std::endl;
    }

    /* Sparse penalty matrices superQ and super R */
    superQ = std::vector<real_t>(T * T * n * n);
    superR = std::vector<real_t>(T * T * m * m);
    for (size_t idx = 0; idx < T; idx++) {
      allocate(n, n, obj.Q.data(), n * idx, n * idx, superQ.data(), T * n);
      allocate(m, m, obj.R.data(), m * idx, m * idx, superR.data(), T * m);
    }
    allocate(n, n, obj.P.data(), n * (T - 1), n * (T - 1), superQ.data(),
             T * n);

    /* Hessian = B'*superQ*B + superR */
    /* Gradient = [B'*superQ*A]*x0 */
    H = std::vector<real_t>{superR};
    h = std::vector<real_t>(T * m * n);
    g = std::vector<real_t>(T * m);
    std::vector<real_t> tempB{B}, tempH(T * m * T * n);
    detail::transpose(T * n, T * m, tempB);
    matmult(T * m, T * n, T * n, tempB.data(), superQ.data(), tempH.data(), 0.);
    matmult(T * m, n, T * n, tempH.data(), A.data(), h.data(),
            0.); // Note that g := h*x0
    matmult(T * m, T * m, T * n, tempH.data(), B.data(), H.data(), 1.);

    /* Check dimensions of the rest of constraints and save memory for them*/
    kFu = obj.fu.size();
    Fu = (kFu > 0);
    klbx = obj.lbx.size();
    lbx = (klbx > 0);
    kubx = obj.ubx.size();
    ubx = (kubx > 0);
    kFx = obj.fx.size();
    Fx = (kFx > 0);
    klbt = obj.lbt.size();
    lbt = (klbt > 0);
    kubt = obj.ubt.size();
    ubt = (kubt > 0);
    kFt = obj.Ft.size();
    Ft = (kFt > 0);
    k_total = T * kFu + (T - 1) * (klbx + kubx + kFx) + klbt + kubt + kFt;

    F = std::vector<real_t>(k_total * T * m);
    f = std::vector<real_t>(k_total);

    size_t skip{0};
    /* Build the superFu matrix and place it in F */
    if (Fu) {
      std::vector<real_t> superFu(T * T * kFu * m);
      for (size_t idx = 0; idx < T; idx++) {
        extra::allocate(kFu, m, obj.Fu.data(), kFu * idx, m * idx,
                        superFu.data(), T * kFu);
      }
      extra::allocate(T * kFu, T * m, superFu.data(), 0, 0, F.data(), k_total);
      for (size_t idx = 0; idx < T; idx++)
        std::copy(obj.fu.cbegin(), obj.fu.cend(), f.begin() + idx * kFu);
      skip += T * kFu;
    }

    /* Place the controlability matrix in place */
    if (ubx) {
      extra::allocate(T * n, T * m, B.data(), kFu * T, 0, F.data(), k_total);
      superubx = std::vector<real_t>(T * kubx);
      for (size_t idx = 0; idx < T - 1; idx++)
        std::copy(obj.ubx.cbegin(), obj.ubx.cend(),
                  superubx.begin() + kubx * idx);
      std::copy(obj.ubt.cbegin(), obj.ubt.cend(),
                superubx.begin() + (T - 1) * kubx);
    }
    if (lbx) {
      std::transform(B.cbegin(), B.cend(), tempB.begin(),
                     std::negate<real_t>());
      extra::allocate(T * n, T * m, tempB.data(), kFu * T + kubx * T, 0,
                      F.data(), k_total);
      superlbx = std::vector<real_t>(T * klbx);
      for (size_t idx = 0; idx < T - 1; idx++)
        std::copy(obj.lbx.cbegin(), obj.lbx.cend(),
                  superlbx.begin() + klbx * idx);
      std::copy(obj.lbt.cbegin(), obj.lbt.cend(),
                superlbx.begin() + (T - 1) * klbx);
    }

    /* Build the superFx matrix and place it in F */
    if (Fx || Ft) {
      std::vector<real_t> superFx((T * n) * ((T - 1) * kFx + kFt));
      for (size_t idx = 0; idx < (T - 1) * Fx; idx++) {
        extra::allocate(kFx, n, obj.Fx.data(), kFx * idx, n * idx,
                        superFx.data(), (T - 1) * kFx + kFt);
      }
      extra::allocate(kFt, n, obj.Ft.data(), kFx * (T - 1), n * (T - 1),
                      superFx.data(), (T - 1) * kFx + kFt);
      std::vector<real_t> temporary_product(((T - 1) * kFx + kFt) * T * m);
      extra::matmult((T - 1) * kFx + kFt, T * m, T * n, superFx.data(),
                     B.data(), temporary_product.data(), double{0});
      extra::allocate((T - 1) * kFx + kFt, T * m, temporary_product.data(),
                      (kFu + kubx + klbx) * T, 0, F.data(), k_total);
      /* Get FxA */
      FxA = std::vector<real_t>(n * ((T - 1) * kFx + kFt));
      extra::matmult((T - 1) * kFx + kFt, n, T * n, superFx.data(), A.data(),
                     FxA.data(), real_t{0});
      superfx = std::vector<real_t>((T - 1) * kFx + kFt);
      for (size_t idx = 0; idx < T - 1; idx++)
        std::copy(obj.fx.cbegin(), obj.fx.cend(), superfx.begin() + idx * kFx);
      std::copy(obj.ft.cbegin(), obj.ft.cend(),
                superfx.begin() + (T - 1) * kFx);
    }

    /* Don't forget to transpose F, cos qpOASES wants row major format */
    detail::transpose(k_total, T * m, F.data());
    /* Transpose H as well, just in case --even if it is symmetric */
    detail::transpose(T * m, T * m, H.data());
  }

  real_t *get_hessian() { return H.data(); }
  real_t *get_gradient(const real_t *x0) {
    detail::MatOps<real_t *>::affoperator('N', T * m, n, 1., h.data(), T * m,
                                          x0, 0., g.data());
    return g.data();
  }
  real_t *get_lb() { return lb.data(); }
  real_t *get_ub() { return ub.data(); }
  real_t *get_F() { return F.data(); }
  real_t *get_f(const real_t *x0) {
    if (ubx) {
      std::copy(superubx.cbegin(), superubx.cend(), f.begin() + kFu * T);
      detail::MatOps<real_t *>::affoperator('N', T * n, n, -1., A.data(), T * n,
                                            x0, 1., f.data() + kFu * T);
    }
    if (lbx) {
      std::copy(superlbx.cbegin(), superlbx.cend(),
                f.begin() + (kFu + kubx) * T);
      detail::MatOps<real_t *>::affoperator('N', T * n, n, 1., A.data(), T * n,
                                            x0, -1.,
                                            f.data() + (kFu + kubx) * T);
    }
    if (Fx || Ft) {
      std::copy(superfx.cbegin(), superfx.cend(),
                f.begin() + (kFu + kubx + klbx) * T);
      detail::MatOps<real_t *>::affoperator(
          'N', (T - 1) * kFx + kFt, n, -1., FxA.data(), (T - 1) * kFx + kFt, x0,
          1., f.data() + (kFu + kubx + klbx) * T);
    }
    return f.data();
  }
  real_t *get_lbF() { return lbF.data(); }
  size_t get_vars() { return N; }
  size_t get_cons() { return k_total; } // Just linear constraints
  std::vector<real_t> retrieve_states(const real_t *U, const real_t *x0) {
    std::vector<real_t> X(T * n);
    detail::MatOps<real_t *>::affoperator('N', T * n, n, 1., A.data(), T * n,
                                          x0, 0., X.data());
    detail::MatOps<real_t *>::affoperator('N', T * n, T * m, 1., B.data(),
                                          T * n, U, 1., X.data());
    return X;
  }
  double get_residual(const real_t *x0) const {
    std::vector<real_t> QA(T * n * n), AQA(n * n), Atrans{A}, rhs(n);
    detail::transpose(T * n, n, Atrans);
    extra::matmult(T * n, n, T * n, superQ.data(), A.data(), QA.data(), 0.);
    extra::matmult(n, n, T * n, Atrans.data(), QA.data(), AQA.data(), 0.);
    detail::MatOps<real_t *>::affoperator('N', n, n, 1., AQA.data(), n, x0, 0.,
                                          rhs.data());
    return 0.5 * detail::MatOps<real_t *>::dotproduct(n, x0, rhs.data());
  }
};

// ************************************************************************ //
// ************************ DISTURBANCE HANDLING ************************** //
// ************************************************************************ //
template <class real_t> struct DISTURB {
  std::mt19937 gen;
  std::vector<std::uniform_real_distribution<real_t>> dis;
  std::vector<real_t> d;
  std::vector<real_t> Bd;

  DISTURB() = default;
  DISTURB(const size_t n, const size_t m, const std::vector<real_t> &dmin,
          const std::vector<real_t> &dmax)
      : dis{std::vector<std::uniform_real_distribution<real_t>>(dmin.size())},
        d{std::vector<real_t>(2 * dmin.size())}, Bd{std::vector<real_t>(2 * n *
                                                                        m)} {
    std::random_device rd;
    gen = std::mt19937(rd());
    for (size_t idx = 0; idx < dmin.size(); idx++)
      dis[idx] = std::uniform_real_distribution<real_t>(dmin[idx], dmax[idx]);
    disturbance_matrix();
  }
  DISTURB(const size_t n, const size_t m, const std::vector<real_t> &dmin,
          const std::vector<real_t> &dmax, uint16_t seed)
      : gen{std::mt19937(seed)},
        dis{std::vector<std::uniform_real_distribution<real_t>>(dmin.size())},
        d{std::vector<real_t>(2 * dmin.size())}, Bd{std::vector<real_t>(2 * n *
                                                                        m)} {
    for (size_t idx = 0; idx < dmin.size(); idx++)
      dis[idx] = std::uniform_real_distribution<real_t>(dmin[idx], dmax[idx]);
    disturbance_matrix();
  }
  DISTURB(const size_t n, const size_t m, const real_t dmin, const real_t dmax)
      : dis{std::vector<std::uniform_real_distribution<real_t>>(n)},
        d{std::vector<real_t>(2 * n)}, Bd{std::vector<real_t>(2 * n * m)} {
    std::random_device rd;
    gen = std::mt19937(rd());
    for (size_t idx = 0; idx < n; idx++)
      dis[idx] = std::uniform_real_distribution<real_t>(dmin, dmax);
    disturbance_matrix();
  }
  DISTURB(const size_t n, const size_t m, const real_t dmin, const real_t dmax,
          uint16_t seed)
      : gen{std::mt19937(seed)},
        dis{std::vector<std::uniform_real_distribution<real_t>>(n)},
        d{std::vector<real_t>(2 * n)}, Bd{std::vector<real_t>(2 * n * m)} {
    for (size_t idx = 0; idx < n; idx++)
      dis[idx] = std::uniform_real_distribution<real_t>(dmin, dmax);
    disturbance_matrix();
  }

  void set_disturbance(size_t n, size_t m, const real_t *X) {
    std::copy(X, X + 2 * n * m, Bd);
  }

  real_t *get_disturbance() {
    for (size_t idx = 0; idx < dis.size(); idx++)
      d[dis.size() + idx] = dis[idx](gen);
    return d.data();
  }

  real_t *get_disturbance(uint16_t seed) {
    gen.seed(seed);
    for (size_t idx = 0; idx < dis.size(); idx++)
      d[dis.size() + idx] = dis[idx](gen);
    return d.data();
  }

private:
  void disturbance_matrix(size_t n) {
    for (size_t idx = 0; idx < n; idx++)
      Bd[n + idx] = 1;
  }
};

// ********************************************************************* //
// ***************************** INPUT MAPPING ************************* //
// ********************************************************************* //
std::vector<triple> profile1(int n, int m);
std::vector<triple> profile2(int n, int m);
std::vector<triple> profile3(int n, int m);

std::vector<triple> map_profile(size_t &n, size_t &m, int p = 1) {
  std::vector<triple> map;
  switch (p) {
  case 1: {
    m = n / 2;
    map = profile1(n, m);
    break;
  }
  case 2: {
    m = n - 1;
    map = profile2(n, m);
    break;
  }
  case 3: {
    m = 2;
    map = profile3(n, m);
    break;
  }
  default: {
    std::cout << "You are in the wrong neighbourhood, my friend."
              << "\nBetter go to a profiler that exists.\n\n";
  }
  }
  return map;
}

/* Automatic profile.
 * Sets the number of inputs to half of the states.
 */
std::vector<triple> profile1(int n, int m) {
  std::vector<triple> map(2 * m);
  for (int idx = 0; idx < m; idx++) {
    switch (idx % 3) {
    case 0: {
      map[2 * idx] = std::make_tuple(idx, 2 * idx, 1);
      map[2 * idx + 1] = std::make_tuple(idx, 2 * idx + 1, -1);
      break;
    }
    case 1: {
      map[2 * idx] = std::make_tuple(idx, 2 * idx, 1);
      map[2 * idx + 1] = std::make_tuple(idx, std::min(2 * idx + 2, n), -1);
      break;
    }
    case 2: {
      map[2 * idx] = std::make_tuple(idx, 2 * idx - 1, 1);
      map[2 * idx + 1] = std::make_tuple(idx, 2 * idx + 1, -1);
      break;
    }
    default:
      std::cout << "FATAL ERROR. Hit default.";
    }
  }
  return map;
}

/* Automatic profile.
 * Sets the number of inputs to the number of the states minus 1.
 * Connects the masses in pairs.
 */
std::vector<triple> profile2(int n, int m) {
  std::vector<triple> map(2 * m);
  for (int idx = 0; idx < m; idx++) {
    map[2 * idx] = std::make_tuple(idx, idx, 1);
    map[2 * idx + 1] = std::make_tuple(idx, idx + 1, -1);
  }
  return map;
}

/* Automatic profile.
 * One free input in the first mass, another free input in the last mass
 */
std::vector<triple> profile3(int n, int m) {
  std::vector<triple> map(2);
  map[0] = std::make_tuple(0, 0, 1);
  map[1] = std::make_tuple(1, n - 1, -1);
  return map;
}

// ************************************************************************ //
// ************************** CLASS DECLARATIONS ************************** //
// ************************************************************************ //
template <class real_t> class Osc_Mass {
private:
  size_t n, m, T;
  real_t Ts;
  std::vector<real_t> k, c, M; // Springs, damps, masses
  std::vector<triple> map;
  std::vector<real_t> I;    // Incidence matrix
  std::vector<real_t> K, C; // Sub-blocks (1,0) and (1,1)

  bool ENABLE_DISTURBANCE{false};
  DISTURB<real_t> disturbance;

  void incidence_matrix();

public:
  /* Default constructor */
  Osc_Mass() = default;

  /* Random constructor for given dimensions */
  Osc_Mass(size_t n_, size_t m_, int = 1);

  /* Constructor for given constant {k, M}
   *    c is assumed 0.
   */
  Osc_Mass(size_t, size_t, real_t, real_t, int = 1);
  Osc_Mass(size_t, size_t, std::vector<real_t> &, std::vector<real_t> &,
           int = 1);

  /* Constructor for given constant {k, M, c}   */
  Osc_Mass(size_t n_, size_t m_, real_t k, real_t M, real_t c, int = 1);
  Osc_Mass(size_t n_, size_t m_, std::vector<real_t> &k, std::vector<real_t> &M,
           std::vector<real_t> &c, int = 1);

  /* Full constructor, providing all the data*/
  Osc_Mass(size_t, size_t, real_t, real_t, real_t, std::vector<triple> &);
  Osc_Mass(size_t, size_t, std::vector<real_t> &, std::vector<real_t> &,
           std::vector<real_t> &, std::vector<triple> &);

  /* Give access to system matrices */
  std::vector<real_t> A, B;
  std::vector<real_t> Ac, Bc;

  /* User functions */
  int discretize(real_t = 0.5);
  size_t nstates() const { return 2 * n; }
  size_t ninputs() const { return m; }
  void simulate(const real_t *, const real_t *, real_t *);
  void simulate(const real_t *, const real_t *, real_t *, uint16_t);

  template <class T>
  void enable_disturbance(size_t n, size_t m, const T lb, const T ub) {
    ENABLE_DISTURBANCE = true;
    disturbance = DISTURB<real_t>(n, m, lb, ub);
  }
  template <class T>
  void enable_disturbance(size_t n, size_t m, const T lb, const T ub,
                          uint16_t seed) {
    ENABLE_DISTURBANCE = true;
    disturbance = DISTURB<real_t>(n, m, lb, ub, seed);
  }
};

// ************************************************************************ //
// ************************** HELPER FUNCTIONS **************************** //
// ************************************************************************ //

/* Create the incidence matrix of appropriate dimensions */
template <class real_t> void Osc_Mass<real_t>::incidence_matrix() {
  I = std::vector<real_t>(n * (n + 1));
  for (size_t idx = 0; idx < n; idx++) {
    I[idx + idx * n] = 1;
    I[idx + (idx + 1) * n] = -1;
  }
}

/* Discretizes the system, assuming that Ac and Bc are well defined */
template <class real_t> int Osc_Mass<real_t>::discretize(real_t _Ts) {
  Ts = _Ts;
  /* Little sanity check */
  if (Ts == 0) {
    std::cout << "Ts = " << Ts << " provided."
              << "Sadly, we cannot stop time and gotta break.\n";
  }
  if (Ts < 0) {
    std::cout << "Ts = " << Ts << " provided."
              << "\nNegative time not supported. The user is referred to the "
                 "General Relativity of Einstein for further details.\n";
  }
  /* Build the discretized matrices */
  A = std::vector<real_t>(4 * n * n);
  B = std::vector<real_t>(2 * n * m);

  /* Get A = expm(Ac*Ts) */
  std::vector<real_t> aux(4 * n * n);
  for (size_t idx = 0; idx < 4 * n * n; idx++)
    aux[idx] = Ac[idx] * Ts;
  auto info = mat_exponential(2 * n, aux.data(), A.data());
  if (info != 0) {
    std::cout << "\nERROR. Info = " << info << std::endl;
    return info;
  }

  /* Get B = [Ac\(A - I)]*Bc */
  std::copy(A.cbegin(), A.cend(), aux.begin());
  std::vector<real_t> factor{Ac};
  std::vector<int> ipiv(2 * n);
  for (size_t idx = 0; idx < 2 * n; idx++)
    aux[idx * 2 * n + idx] -= 1;
  mpc::detail::MatOps<real_t *>::gefactor(2 * n, 2 * n, factor.data(), 2 * n,
                                          ipiv.data());
  mpc::detail::MatOps<real_t *>::gesolve('N', 2 * n, 2 * n, factor.data(),
                                         2 * n, ipiv.data(), aux.data(), 2 * n);
  matmult(2 * n, m, 2 * n, aux.data(), Bc.data(), B.data(), 0.);
  return info;
}

template <class real_t>
void Osc_Mass<real_t>::simulate(const real_t *x0, const real_t *u0,
                                real_t *x1) {
  detail::MatOps<real_t *>::affoperator('N', 2 * n, 2 * n, 1., A.data(), 2 * n,
                                        x0, 0., x1);
  detail::MatOps<real_t *>::affoperator('N', 2 * n, m, 1., B.data(), 2 * n, u0,
                                        1., x1);
  if (ENABLE_DISTURBANCE) {
    detail::MatOps<real_t *>::vecsum(2 * n, 1., disturbance.get_disturbance(),
                                     x1);
  }
}

template <class real_t>
void Osc_Mass<real_t>::simulate(const real_t *x0, const real_t *u0, real_t *x1,
                                uint16_t seed) {
  detail::MatOps<real_t *>::affoperator('N', 2 * n, 2 * n, 1., A.data(), 2 * n,
                                        x0, 0., x1);
  detail::MatOps<real_t *>::affoperator('N', 2 * n, m, 1., B.data(), 2 * n, u0,
                                        1., x1);
  if (ENABLE_DISTURBANCE) {
    detail::MatOps<real_t *>::vecsum(2 * n, 1.,
                                     disturbance.get_disturbance(seed), x1);
  }
}

// ************************************************************************ //
// ***************************** CONSTRUCTOR ****************************** //
// ************************************************************************ //
/* Basic constructor */
template <class real_t>
Osc_Mass<real_t>::Osc_Mass(size_t _n, size_t _m, int p)
    : n{_n}, m{_m}, k{std::vector<real_t>(n + 1, 1)}, M{std::vector<real_t>(n,
                                                                            1)},
      K{std::vector<real_t>(n * n)}, Ac{std::vector<real_t>(4 * n * n)} {
  /* Create the K sub-block */
  incidence_matrix();

  std::vector<real_t> aux(n * n);
  for (size_t i = 0; i < n + 1; i++)
    mpc::extra::rank_update(n, k[i], I.data() + i * n, 1, K.data());

  /* Divide by the masses and negate K */
  std::transform(K.cbegin(), K.cend(), K.begin(), std::negate<real_t>());

  /* Create the identity in the (0,1)-block */
  for (size_t idx = 0; idx < n; idx++)
    Ac[2 * n * n + idx * 2 * n + idx] = 1;

  /* Put the K matrix in place */
  extra::allocate(n, n, K.data(), n, 0, Ac.data(), 2 * n);

  /* Build the tuple according to the pre-designed profiles */
  map = map_profile(n, m, p);
  Bc = std::vector<real_t>(2 * n * m);
  int col, row;
  for (size_t idx = 0; idx < map.size(); idx++) {
    col = std::get<0>(map[idx]); // The actuator number (<= m)
    row = std::get<1>(map[idx]); // The mass upon which the force actes
    Bc[2 * n * col + n + row] = std::get<2>(map[idx]);
  }
}

/* Constructor with uniform spring constants and masses */
template <class real_t>
Osc_Mass<real_t>::Osc_Mass(size_t _n, size_t _m, real_t _k, real_t _M, int p)
    : n{_n}, m{_m}, k{std::vector<real_t>(n + 1, k)}, M{std::vector<real_t>(
                                                          n, _M)},
      K{std::vector<real_t>(n * n)}, Ac{std::vector<real_t>(4 * n * n)} {
  /* Create the K sub-block */
  incidence_matrix();
  std::vector<real_t> aux(n * n);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, k[i], I.data() + i, n, K.data());

  /* Divide the rows in K by the corresponding mass */
  for (int row = 0; row < n; row++)
    for (int col = 0; col < n; col++)
      K[col * n + row] = -K[col * n + row] / M[row];

  /* Create the identity in the (0,1)-block */
  for (int idx = 0; idx < n; idx++)
    Ac[2 * n * n + idx * 2 * n + idx] = 1;

  /* Put the K matrix in place */
  extra::allocate(n, n, K.data(), n, 0, Ac.data(), 2 * n);

  /* Build the tuple according to the pre-designed profiles */
  map = map_profile(n, m, p);
  Bc = std::vector<real_t>(2 * n * m);
  int col, row;
  for (int idx = 0; idx < map.size(); idx++) {
    col = std::get<0>(map[idx]); // The actuator number (<= m)
    row = std::get<1>(map[idx]); // The mass upon which the force actes
    Bc[2 * n * col + n + row] = std::get<2>(map[idx]);
  }
}

/* Constructor with different spring constants and masses */
template <class real_t>
Osc_Mass<real_t>::Osc_Mass(size_t _n, size_t _m, std::vector<real_t> &_k,
                           std::vector<real_t> &_M, int p)
    : n{_n}, m{_m}, k{_k}, M{_M}, K{std::vector<real_t>(n * n)},
      Ac{std::vector<real_t>(4 * n * n)} {
  /* Few sanity checks */
  if (static_cast<int>(k.size()) != n + 1) {
    std::cout << "\nvector k (spring constants) of inappropriate dimensions."
              << "\n\tProvided " << k.size() << ", expected " << n + 1 << ".\n";
  }
  if (static_cast<int>(M.size()) != n) {
    std::cout << "\nvector M (masses) of inappropriate dimensions."
              << "\n\tProvided " << M.size() << ", expected " << n + 1 << ".\n";
  }

  /* Create the K sub-block */
  incidence_matrix();
  std::vector<real_t> aux(n * n);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, k[i], I.data() + i, n, K.data());

  /* Divide the rows in K by the corresponding mass */
  for (int row = 0; row < n; row++)
    for (int col = 0; col < n; col++)
      K[col * n + row] = -K[col * n + row] / M[row];

  /* Create the identity in the (0,1)-block */
  for (int idx = 0; idx < n; idx++)
    Ac[2 * n * n + idx * 2 * n + idx] = 1;

  /* Put the K matrix in place */
  extra::allocate(n, n, K.data(), n, 0, Ac.data(), 2 * n);

  /* Build the tuple according to the pre-designed profiles */
  map = map_profile(n, m, p);
  Bc = std::vector<real_t>(2 * n * m);
  int col, row;
  for (int idx = 0; idx < map.size(); idx++) {
    col = std::get<0>(map[idx]); // The actuator number (<= m)
    row = std::get<1>(map[idx]); // The mass upon which the force actes
    Bc[2 * n * col + n + row] = std::get<2>(map[idx]);
  }
}

/* Constructor with uniform masses, and spring and damping constants */
template <class real_t>
Osc_Mass<real_t>::Osc_Mass(size_t _n, size_t _m, real_t _k, real_t _M,
                           real_t _c, int p)
    : n{_n}, m{_m}, k{std::vector<real_t>(n + 1, k)}, M{std::vector<real_t>(
                                                          n, _M)},
      c{std::vector<real_t>(n + 1, _c)}, K{std::vector<real_t>(n * n)},
      C{std::vector<real_t>(n * n)}, Ac{std::vector<real_t>(4 * n * n)} {
  /* Create the K sub-block */
  incidence_matrix();
  std::vector<real_t> aux(n * n);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, k[i], I.data() + i, n, K.data());

  /* Create the C sub-block */
  std::fill(aux.begin(), aux.end(), 0);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, c[i], I.data() + i, n, C.data());

  /* Divide the rows in K and C by the corresponding mass */
  for (int row = 0; row < n; row++)
    for (int col = 0; col < n; col++) {
      K[col * n + row] = -K[col * n + row] / M[row];
      C[col * n + row] = -C[col * n + row] / M[row];
    }

  /* Create the identity in the (0,1)-block */
  for (int idx = 0; idx < n; idx++)
    Ac[2 * n * n + idx * 2 * n + idx] = 1;

  /* Put the matrices K and C in place */
  extra::allocate(n, n, K.data(), n, 0, Ac.data(), 2 * n);
  extra::allocate(n, n, C.data(), n, n, Ac.data(), 2 * n);

  /* Build the tuple according to the pre-designed profiles */
  map = map_profile(n, m, p);
  Bc = std::vector<real_t>(2 * n * m);
  int col, row;
  for (int idx = 0; idx < map.size(); idx++) {
    col = std::get<0>(map[idx]); // The actuator number (<= m)
    row = std::get<1>(map[idx]); // The mass upon which the force actes
    Bc[2 * n * col + n + row] = std::get<2>(map[idx]);
  }
}

/* Constructor with different masses, and spring and damping constants */
template <class real_t>
Osc_Mass<real_t>::Osc_Mass(size_t _n, size_t _m, std::vector<real_t> &_k,
                           std::vector<real_t> &_M, std::vector<real_t> &_c,
                           int p)
    : n{_n}, m{_m}, k{_k}, M{_M}, c{_c}, K{std::vector<real_t>(n * n)},
      C{std::vector<real_t>(n * n)}, Ac{std::vector<real_t>(4 * n * n)} {
  /* Few sanity checks */
  if (static_cast<int>(k.size()) != n + 1) {
    std::cout << "\nvector k (spring constants) of inappropriate dimensions."
              << "\n\tProvided " << k.size() << ", expected " << n + 1 << ".\n";
  }
  if (static_cast<int>(c.size()) != n + 1) {
    std::cout << "\nvector c (damping constants) of inappropriate dimensions."
              << "\n\tProvided " << c.size() << ", expected " << n + 1 << ".\n";
  }
  if (static_cast<int>(M.size()) != n) {
    std::cout << "\nvector M (masses) of inappropriate dimensions."
              << "\n\tProvided " << M.size() << ", expected " << n + 1 << ".\n";
  }

  /* Create the K sub-block */
  incidence_matrix();
  std::vector<real_t> aux(n * n);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, k[i], I.data() + i, n, K.data());

  /* Create the C sub-block */
  std::fill(aux.begin(), aux.end(), 0);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, c[i], I.data() + i, n, C.data());

  /* Divide the rows in K and C by the corresponding mass */
  for (int row = 0; row < n; row++)
    for (int col = 0; col < n; col++) {
      K[col * n + row] = -K[col * n + row] / M[row];
      C[col * n + row] = -C[col * n + row] / M[row];
    }

  /* Create the identity in the (0,1)-block */
  for (int idx = 0; idx < n; idx++)
    Ac[2 * n * n + idx * 2 * n + idx] = 1;

  /* Put the matrices K and C in place */
  extra::allocate(n, n, K.data(), n, 0, Ac.data(), 2 * n);
  extra::allocate(n, n, C.data(), n, n, Ac.data(), 2 * n);

  /* Build the tuple according to the pre-designed profiles */
  map = map_profile(n, m, p);
  Bc = std::vector<real_t>(2 * n * m);
  int col, row;
  for (int idx = 0; idx < map.size(); idx++) {
    col = std::get<0>(map[idx]); // The actuator number (<= m)
    row = std::get<1>(map[idx]); // The mass upon which the force actes
    Bc[2 * n * col + n + row] = std::get<2>(map[idx]);
  }
}

/* Constructor with uniform masses, and spring and damping constants.
 * Input map is provided.
 */
template <class real_t>
Osc_Mass<real_t>::Osc_Mass(size_t _n, size_t _m, real_t _k, real_t _M,
                           real_t _c, std::vector<triple> &_map)
    : n{_n}, m{_m}, k{std::vector<real_t>(n + 1, k)}, M{std::vector<real_t>(
                                                          n, _M)},
      c{std::vector<real_t>(n + 1, _c)}, map{_map}, K{std::vector<real_t>(n *
                                                                          n)},
      C{std::vector<real_t>(n * n)}, Ac{std::vector<real_t>(4 * n * n)} {
  /* Create the K sub-block */
  incidence_matrix();
  std::vector<real_t> aux(n * n);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, k[i], I.data() + i, n - 1, K.data());

  /* Create the C sub-block */
  std::fill(aux.begin(), aux.end(), 0);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, k[i], I.data() + i, n, K.data());

  /* Divide the rows in K and C by the corresponding mass */
  for (int row = 0; row < n; row++)
    for (int col = 0; col < n; col++) {
      K[col * n + row] = -K[col * n + row] / M[row];
      C[col * n + row] = -C[col * n + row] / M[row];
    }

  /* Create the identity in the (0,1)-block */
  for (int idx = 0; idx < n; idx++)
    Ac[2 * n * n + idx * 2 * n + idx] = 1;

  /* Put the matrices K and C in place */
  extra::allocate(n, n, K.data(), n, 0, Ac.data(), 2 * n);
  extra::allocate(n, n, C.data(), n, n, Ac.data(), 2 * n);

  /* Build the tuple according to the provided profile */
  int col, row;
  for (int idx = 0; idx < map.size(); idx++) {
    col = std::get<0>(map[idx]); // The actuator number (<= m)
    row = std::get<1>(map[idx]); // The mass upon which the force actes
    Bc[2 * n * col + n + row] = std::get<2>(map[idx]);
  }
}

/* Constructor with different masses, and spring and damping constants.
 * Input map is provided.
 */
template <class real_t>
Osc_Mass<real_t>::Osc_Mass(size_t _n, size_t _m, std::vector<real_t> &_k,
                           std::vector<real_t> &_M, std::vector<real_t> &_c,
                           std::vector<triple> &_map)
    : n{_n}, m{_m}, k{_k}, M{_M}, c{_c}, map{_map}, K{std::vector<real_t>(n *
                                                                          n)},
      C{std::vector<real_t>(n * n)}, Ac{std::vector<real_t>(4 * n * n)} {
  /* Few sanity checks */
  if (static_cast<int>(k.size()) != n + 1) {
    std::cout << "\nvector k (spring constants) of inappropriate dimensions."
              << "\n\tProvided " << k.size() << ", expected " << n + 1 << ".\n";
  }
  if (static_cast<int>(c.size()) != n + 1) {
    std::cout << "\nvector c (damping constants) of inappropriate dimensions."
              << "\n\tProvided " << c.size() << ", expected " << n + 1 << ".\n";
  }
  if (static_cast<int>(M.size()) != n) {
    std::cout << "\nvector M (masses) of inappropriate dimensions."
              << "\n\tProvided " << M.size() << ", expected " << n + 1 << ".\n";
  }

  /* Create the K sub-block */
  incidence_matrix();
  std::vector<real_t> aux(n * n);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, k[i], I.data() + i, n, K.data());

  /* Create the C sub-block */
  std::fill(aux.begin(), aux.end(), 0);
  for (int i = 0; i < n; i++)
    mpc::extra::rank_update(n, c[i], I.data() + i, n, C.data());

  /* Divide the rows in K and C by the corresponding mass */
  for (int row = 0; row < n; row++)
    for (int col = 0; col < n; col++) {
      K[col * n + row] = -K[col * n + row] / M[row];
      C[col * n + row] = -C[col * n + row] / M[row];
    }

  /* Create the identity in the (0,1)-block */
  for (int idx = 0; idx < n; idx++)
    Ac[2 * n * n + idx * 2 * n + idx] = 1;

  /* Put the matrices K and C in place */
  extra::allocate(n, n, K.data(), n, 0, Ac.data(), 2 * n);
  extra::allocate(n, n, C.data(), n, n, Ac.data(), 2 * n);

  /* Build the tuple according to the provided profile */
  int col, row;
  for (int idx = 0; idx < map.size(); idx++) {
    col = std::get<0>(map[idx]); // The actuator number (<= m)
    row = std::get<1>(map[idx]); // The mass upon which the force actes
    Bc[2 * n * col + n + row] = std::get<2>(map[idx]);
  }
}

} // namespace extra

} // namespace mpc
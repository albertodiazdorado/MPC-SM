int square_1(const int num) { return num * num; }

template <class T> T square_2(const T num) { return num * num; }

struct CustomType {
  CustomType() = default;
  CustomType(const int num) : num{num} {}
  CustomType operator*(const CustomType &rhs) const {
    return CustomType{num * rhs.num};
  }

private:
  int num{0};
};

int main() {
  int res1 = square_1(5);
  CustomType res2 = square_1(CustomType(5)); // does not compile

  int res3 = square_2(5);
  CustomType res4 = square_2(CustomType(5)); // does compile

  return 0;
}

struct Vector { /* ... */ };

struct Matrix {
  virtual Vector multiply(const Vector& rhs) {
    // general matrix-vector multiply
  }
};

struct DenseMatrix : Matrix {
  // inherits general matrix-vector multiply
};

struct DiagonalMatrix : Matrix {
  Vector multiply(const Vector& rhs) override {
    // efficient matrix-vector multiply by taking
    // only diagonal elements into account
  }
};